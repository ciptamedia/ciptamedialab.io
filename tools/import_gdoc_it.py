import csv
import json
import sys

from ruamel.yaml import YAML

yaml = YAML()


def get_filename(nomor):
    try:
        return '_hibahcme/%0.4d.md' % int(nomor)
    except ValueError:
        return '_hibahcme/%s.md' % nomor


def check_yaml_file(nomor):
    filename = get_filename(nomor)
    try:
        with open(filename, 'r') as f:
            docs = list(yaml.load_all(f))
            frontmatter = docs[0]
            if 'indonesia_timur' in frontmatter:
                print('Already set %s : %r' % (nomor, frontmatter['indonesia_timur']))
                return frontmatter['indonesia_timur']
    except Exception as e:
        print(e, file=sys.stderr)
        return True


def update_yaml_file(nomor):
    filename = get_filename(nomor)

    filename = get_filename(nomor)

    with open(filename, 'r') as f:
        contents = f.read()

    lokasi_pos = contents.find('lokasi:')

    with open(filename, 'w') as f:
        f.write(contents[:lokasi_pos])
        f.write('indonesia_timur: true\n')
        f.write(contents[lokasi_pos:])
        if contents[-1] != '\n':
            f.write('\n')


def output_yaml_files(data):
    for row in data:
        nomor = row['nomor']
        flag = row['it']
        if not nomor or nomor == 'Nomor' or nomor.startswith('Indonesia'):
            continue
        print('Processing %s' % nomor)
        if row['it'].lower() == 'no':
            continue
        elif check_yaml_file(nomor):
            continue
        elif row['it'].lower() == 'yes':
            print('Setting %s to true' % nomor)
            update_yaml_file(nomor)
        else:
            print('Dont understand %s : %s' % (nomor, row['it']))


def load_csv(filename):
    with open(filename) as f:
        reader = csv.DictReader(f)
        return list(reader)


def main(argv=None):
    filename = argv[1] if len(argv) == 2 else '../gdoc-it.csv'
    data = list(load_csv(filename))
    output_yaml_files(data)


if __name__ == '__main__':
    main(sys.argv)
