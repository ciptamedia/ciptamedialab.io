---
nama: Kahi Ata Ratu
proyek: Perempuan Sumba dan Musik Tradisional
title: |-
  Pertunjukan musik "Poetic Voices from the Island of Sumba"
event-startDate: '2018-08-06'
event-endDate: '2018-08-06'
lokasi: Rumah Sanur Creative Hub
alamat: 'Rumah Sanur Creative Hub, Bali'
img: /uploads/upcoming/kahi-ata-ratu-2.jpg
img_caption: 'Poster pertunjukan musik "Poetic Voices from the Island of Sumba"'
---
