---
nama: Kahi Ata Ratu
proyek: Perempuan Sumba dan Musik Tradisional
title: |-
  Pertunjukan musik "Perempuan Sumba dan Musik Tradisional"
event-startDate: '2018-08-05'
event-endDate: '2018-08-05'
lokasi: Bentara Budaya Bali
alamat: 'Jl. Ida Bagus Mantra 88, Ketewel, Gianyar, Bali'
img: /uploads/upcoming/kahi-ata-ratu-1.jpg
img_caption: 'Poster pertunjukan musik "Perempuan Sumba dan Musik Tradisional"'
---
