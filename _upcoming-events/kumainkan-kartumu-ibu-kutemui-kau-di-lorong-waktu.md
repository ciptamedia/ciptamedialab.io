---
nama: Kadek Sonia Piscayanti
proyek: 11 Ibu 11 Panggung 11 Kisah
title: |-
  Pertunjukan teater "Kumainkan Kartumu Ibu Kutemui Kau di Lorong Waktu" bersama Erna Dewi
event-startDate: '2018-07-24'
event-endDate: '2018-07-24'
lokasi: Kawasan Pantai Penimbangan
alamat: 'Gang Balbo no F8, Bali'
img: /uploads/upcoming/Poster-Kumainkan-Kartumu-Ibu-Kutemui-Kau-di-Lorong-Waktu---Erna-Dewi.jpg
img_caption: 'Poster "Kumainkan Kartumu Ibu Kutemui Kau di Lorong Waktu"'
---
