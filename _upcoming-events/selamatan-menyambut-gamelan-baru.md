---
nama: Endah Fitriana
proyek: Pembelian Gamelan
title: |-
  Selamatan menyambut gamelan baru ("Brokohan Gamelan") oleh anak-anak Pasinaon Omah Kendeng
event-startDate: '2018-08-11'
event-endDate: '2018-08-11'
lokasi: Omah Kendeng
alamat: 'Desa Sukolilo, Pati, Jawa Tengah'
img: /uploads/upcoming/Ft-10.jpg
img_caption: Endah Fitriana memainkan Gamelan baru
---
