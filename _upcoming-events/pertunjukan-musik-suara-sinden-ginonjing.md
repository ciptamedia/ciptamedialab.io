---
nama: Nur Handayani
proyek: 'Suara Sinden “Ginonjing”'
title: |-
  Pertunjukan musik "Suara Sindhen Ginonjing"
event-startDate: '2018-06-10'
event-endDate: '2018-06-10'
lokasi: Teater Besar ISI
alamat: 'Surakarta, Jawa Tengah'
img: /uploads/upcoming/Poster-Suara-Sinden-Ginonjing.jpg
img_caption: Poster Suara Sinden Ginonjing
---
