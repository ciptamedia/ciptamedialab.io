---
nama: Kadek Sonia Piscayanti
proyek: 11 Ibu 11 Panggung 11 Kisah
title: |-
  Pertunjukan teater "Kujahit Nasib di Mozaik Mimpi" bersama Yanti Pusparini
event-startDate: '2018-09-11'
event-endDate: '2018-09-11'
lokasi: Jalan Pulau Komodo 64
alamat: 'Jalan Pulau Komodo 64 X Banyuning, Bali'
img: /uploads/upcoming/Poster-Kujahit-Nasib-di-Mozaik-Mimpi---Yanti-Pusparini.jpg
img_caption: 'Poster "Kujahit Nasib di Mozaik Mimpi"'
---
