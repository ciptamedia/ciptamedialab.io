---
nama: Sartika Sari
proyek: Puisi dan Gerakan Perempuan
title: |-
  Peluncuran Buku "Seroean Kemadjoean" dan Pameran Puisi Perempuan
event-startDate: '2018-11-10'
event-endDate: '2018-11-10'
lokasi: Taman Budaya Sumatera Utara
alamat: 'Jl. Perintis Kemerdekaan No.33, Gaharu, Medan Tim., Kota Medan, Sumatera Utara'
img: /uploads/upcoming/seruan-kemajuan.jpeg
img_caption: 'Sampul buku "Seroean Kemadjoean"'
---
