---
nama: Luviana Ariyanti
proyek: Film Dokumenter Kekerasan Perempuan di Dunia Kerja
title: |-
  Malam Apresiasi Karya Pemutaran Film dan Diskusi "More Than Work"
event-startDate: '2019-06-26'
event-endDate: '2019-06-26'
lokasi: Studio Kopi Ndaleme Eyang
alamat: 'Jl. Pajajaran Timur 1 No. 10, Sumber, Solo'
img: /uploads/upcoming/more-than-work-solo.jpeg
img_caption: 'Poster pemutaran film dan diskusi'
---
