---
nama: Kadek Sonia Piscayanti
proyek: 11 Ibu 11 Panggung 11 Kisah
title: |-
  Pertunjukan teater "Kawitanku adalah Langit Luas Tak Terbatas" bersama Hermawati
event-startDate: '2018-08-31'
event-endDate: '2018-08-31'
lokasi: Jalan WR Supratman 211
alamat: 'Jalan WR Supratman 211, Singaraja, Bali'
img: /uploads/upcoming/Poster-Kawitanku-adalah-Langit-Luas-Tak-Terbatas.jpg
img_caption: 'Poster "Kawitanku adalah Langit Luas Tak Terbatas"'
---
