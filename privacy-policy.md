---
title: Kebijakan Privasi
layout: legal
---

1. Pembukaan

   Kebijakan Privasi ini menjelaskan pengumpulan, penggunaan, pemrosesan, pengalihan, dan pengungkapan informasi pribadi
   oleh situs Cipta Media.

   Kebijakan Privasi ini dimasukkan ke dalam dan menjadi bagian dari
   [Persyaratan Penggunaan](/terms-of-use){:target="_blank"}

   Kecuali dinyatakan lain pada situs web atau layanan yang diselenggarakan oleh Cipta Media, Kebijakan Privasi ini berlaku
   untuk penggunaan Anda atas semua konten dalam situs web yang dijalankan Cipta Media. Ini termasuk subdomain
   wiki.ciptamedia.org. Kebijakan Privasi ini juga berlaku untuk semua produk, informasi, dan layanan yang disediakan
   melalui Situs Web, termasuk tanpa batasan Formulir Laporan Aktivitas, Formulir Laporan Penggunaan Dana, Formulir dan
   konten lainnya.

   Dengan mengakses atau menggunakan salah satu Layanan, Anda menerima dan menyetujui praktik yang dijelaskan dalam
   Kebijakan Privasi ini.

2. Prinsip Kami

   Cipta Media telah merancang kebijakan ini untuk konsisten dengan prinsip-prinsip berikut:

   Kebijakan privasi harus dapat dibaca dan mudah ditemukan. Pengumpulan, penyimpanan, dan pemrosesan data harus
   disederhanakan semaksimal mungkin untuk meningkatkan keamanan, memastikan konsistensi, dan membuat praktik mudah bagi
   pengguna untuk dipahami. Praktik data harus selalu memenuhi harapan pengguna yang wajar.

3. Informasi Pribadi Cipta Media Mengumpulkan dan Bagaimana Digunakan

   Seperti yang digunakan dalam kebijakan ini, "informasi pribadi" berarti informasi yang memungkinkan seseorang untuk
   mengidentifikasi Anda, termasuk nama Anda, alamat email, alamat IP, atau informasi lain dari mana seseorang dapat
   menyimpulkan identitas Anda.

   Cipta Media mengumpulkan dan menggunakan informasi pribadi dengan cara-cara berikut:

   Analisis Situs Web untuk pelaporan: Saat Anda mengunjungi Situs Web kami dan menggunakan Layanan kami, Cipta Media
   mengumpulkan beberapa informasi tentang aktivitas Anda melalui alat seperti Google Analytics. Jenis informasi yang kami
   kumpulkan berfokus pada informasi umum seperti negara atau kota tempat Anda berada, halaman yang dikunjungi, waktu yang
   dihabiskan di halaman, peta panas aktivitas pengunjung di situs, informasi tentang browser yang Anda gunakan, dll. Cipta
   Media mengumpulkan dan menggunakan informasi ini sesuai dengan kepentingan sah kami dalam meningkatkan keamanan dan
   kegunaan Layanan kami. Informasi yang kami kumpulkan dan proses digunakan secara agregat untuk melihat tren tanpa secara
   sengaja mengidentifikasi individu, kecuali dalam kasus di mana Anda terlibat dalam transaksi dengan Cipta Media dengan
   meneriba hibah. Dalam kasus-kasus tersebut, Cipta Media menyimpan informasi tertentu tentang kunjungan Anda ke Layanan
   sesuai dengan peruntukkannya yang sah dalam memahami aktivitas Anda di dalam web dan informasi ini disimpan sehubungan
   dengan informasi pribadi lainnya yang Anda berikan kepada Cipta Media.

   Perhatikan bahwa Anda dapat mempelajari tentang praktik Google sehubungan dengan layanan analitiknya dan cara menyisih
   darinya dengan mengunduh add-on browser penyisih Google Analytics, tersedia di https://tools.google.com/dlpage/gaoptout.

   Informasi dari Cookie: Kami dan penyedia layanan kami (misalnya, Google Analytics seperti yang dijelaskan di atas) dapat
   mengumpulkan informasi menggunakan cookie atau teknologi serupa untuk tujuan yang dijelaskan di atas dan di bawah.
   Cookie adalah potongan informasi yang disimpan oleh browser Anda di hard drive atau memori komputer Anda atau perangkat
   akses Internet lainnya. Cookie dapat memungkinkan kami mempersonalisasi pengalaman Anda di Layanan, mempertahankan sesi
   terus-menerus, secara pasif mengumpulkan informasi demografis tentang komputer Anda, dan memantau iklan dan kegiatan
   lainnya. Situs web dapat menggunakan berbagai jenis cookie dan jenis penyimpanan lokal lainnya (seperti penyimpanan
   lokal berbasis browser atau plugin).

   Layanan Log-In: Ketika Anda melengkapi formulir data diri, yang merupakan salah satu dari layanan Cipta Media, di
   [Formulir Data Diri](/data-diri){:target="_blank"}, Anda akan diminta untuk login terlebih dahulu menggunakan akun
   Google, yang merupakan penyedia jasa pihak ketiga.  Login ini membantu memberikan informasi pribadi untuk menjadikan
   akun Anda akun terdaftar, dimana akun google Anda yang akan menyimpan kata sandi dan profil Anda. Cipta Media
   mengumpulkan dan menggunakan informasi terbatas yang Anda berikan pada formulir, Cipta Media tidak mengumpulkan dan
   memberikan informasi yang terkandung didalam akun google Anda.  Kami akan menggunakan informasi ini sesuai dengan
   peruntukkannya yang sah dalam menetapkan dan memelihara data diri Anda yang memberi Anda fitur-fitur yang kami
   berikan kepada Pengguna Terdaftar. Kami dapat menggunakan alamat email Anda untuk menghubungi Anda mengenai perubahan
   kebijakan ini atau kebijakan lain yang berlaku. Nama atau nama panggilan yang Anda berikan sehubungan dengan akun
   Anda dapat digunakan untuk menghubungkan Anda dengan konten apa pun yang Anda kirimkan ke Layanan apa pun. Selain
   itu, setiap kali Anda menggunakan Layanan Login Cipta Media untuk masuk ke Situs Web atau menggunakan Layanan, server
   kami menyimpan log teks biasa dari Situs Web yang Anda kunjungi dan ketika Anda mengunjunginya.

   Pemberitahuan: Ketika Anda mendaftar untuk upaya yang dilakukan oleh Cipta Media Anda otomatis akan terdaftar pada
   mailing-list mengenai hal-hal terbaru Cipta Media. Anda akan diminta untuk memberikan beberapa informasi pribadi dan
   Cipta Media mengumpulkan dan menggunakan informasi pribadi ini sesuai peruntukkannya yang sah dalam menyediakan
   berita dan pembaruan untuk, dan berkolaborasi dengan, pendukung dan sukarelawannya.

   Analisis Email: Ketika Anda menerima komunikasi dari Cipta Media setelah mendaftar untuk buletin Cipta Media,
   pembaruan kampanye, atau komunikasi email lain yang sedang berlangsung dari Cipta Media, kami menggunakan analitik
   untuk melacak apakah Anda membuka surat, mengeklik tautan, dan jika tidak berinteraksi dengan apa yang kami Kirim.
   Anda dapat memilih keluar dari pelacakan ini dengan mengirimkan surel ke info@ciptamedia.org.

   Jika Anda pengguna terdaftar, Anda akan diberikan halaman profil publik, halaman ini tidak dapat disunting langsung
   oleh Anda, namun Anda dapat mengajukan perubahannya pada Cipta Media dengan mengirimkan surel ke info@ciptamedia.org.
   Halaman ini telah diisi dengan informasi tertentu yang Anda berikan dalam formulir Anda. Semua informasi yang Anda
   tuliskan di dalam formulir akan dipublikasikan kecuali dinyatakan sebaliknya.

   Informasi Lain yang Disediakan Secara Sukarela: Ketika Anda memberikan umpan balik atau menyerahkan informasi pribadi
   ke Cipta Media, kami akan mengumpulkan informasi yang diberikan ini sesuai dengan kepentingannya yang sah dan
   mempertimbangkan kelanjutan program atau aktivitas tertentu yang Anda berikan umpan balik atau masukan lainnya.

4. Penyimpanan Informasi Pribadi

   Sebagian besar informasi pribadi yang dikumpulkan dan digunakan sebagaimana dijelaskan dalam Bagian 3 di atas
   dikumpulkan dan disimpan dalam basis data pusat yang disediakan oleh penyedia layanan pihak ketiga. Cipta Media
   mengumpulkan data ini sesuai dengan kepentingannya yang sah dalam memiliki informasi yang disimpan di satu lokasi untuk
   meminimalkan kompleksitas, meningkatkan konsistensi dalam praktik internal, lebih memahami komunitas pendukung, pegawai,
   dan sukarelawan, dan meningkatkan keamanan data.

5. Pengungkapan Informasi Pribadi Anda

   Cipta Media tidak mengungkapkan informasi pribadi kepada pihak ketiga kecuali sebagaimana ditentukan di tempat lain
   dalam kebijakan ini dan dalam contoh berikut:

   Cipta Media dapat berbagi informasi pribadi dengan kontraktor dan penyedia layanan kami untuk melakukan kegiatan yang
   dijelaskan dalam Bagian 3.

   Kami dapat mengungkapkan informasi pribadi Anda kepada pihak ketiga dengan iktikad baik bahwa pengungkapan tersebut
   cukup diperlukan untuk (a) mengambil tindakan terkait dugaan kegiatan ilegal; (b) menegakkan atau menerapkan Ketentuan
   Utama kami dan Kebijakan Privasi ini; © menegakkan Kode Etik dan kebijakan yang terkandung dan dimasukkan di dalamnya,
   atau (D) mematuhi proses hukum, seperti surat perintah penggeledahan, panggilan pengadilan, undang-undang, atau perintah
   pengadilan.

6. Keamanan Informasi Pribadi Anda

   Cipta Media telah menerapkan tindakan-tindakan untuk menjamin keamanan fisik, teknis, dan dan pengaturan yang wajar
   terkait dengan informasi pribadi terhadap penghancuran yang tidak disengaja atau melanggar hukum, atau kehilangan yang
   tidak disengaja, perubahan, pengungkapan atau akses yang tidak sah, sesuai dengan hukum yang berlaku. Namun, tidak ada
   situs web yang dapat sepenuhnya menghilangkan risiko keamanan. Pihak ketiga dapat menghindari tindakan keamanan kami
   untuk memotong atau mengakses transmisi atau komunikasi pribadi secara melanggar hukum. Jika ada pelanggaran data yang
   terjadi, kami akan memposting pemberitahuan yang cukup menonjol ke Situs Web dan mematuhi semua persyaratan privasi data
   yang berlaku lainnya termasuk, bila diperlukan, pemberitahuan pribadi kepada Anda jika Anda telah memberikan dan kami
   telah mempertahankan alamat email untuk Anda.

   Sistem akun Layanan Login Cipta Media memiliki risiko keamanan selain yang dijelaskan di atas. Antara lain, mereka
   rentan terhadap serangan DNS, dan menggunakan Layanan Login Cipta Media dapat meningkatkan risiko phishing.

7. Anak-anak

   Layanan tidak ditujukan untuk anak-anak di bawah usia 13 tahun. Kami tidak akan pernah secara sadar meminta informasi
   pribadi dari siapa pun yang berusia di bawah 13 tahun tanpa izin orang tua. Persyaratan Utama kami secara khusus
   melarang siapa pun yang menggunakan Layanan kami untuk mengirimkan informasi yang dapat diidentifikasi secara pribadi
   tentang orang-orang yang berusia di bawah 13 tahun. Setiap orang yang memberikan informasi pribadi mereka kepada Cipta
   Media melalui Layanan menyatakan bahwa mereka berusia {{ site.minimum_legal_age }} tahun atau lebih.

8. Penyedia Layanan Pihak Ketiga

   Cipta Media menggunakan penyedia layanan pihak ketiga sehubungan dengan Layanan, termasuk layanan hosting situs web,
   manajemen basis data, dan berbagai hal lain. Beberapa dari penyedia layanan ini dapat menempatkan cookie sesi di
   komputer Anda, dan mereka dapat mengumpulkan dan menyimpan informasi pribadi Anda atas nama kami sesuai dengan praktik
   dan tujuan data yang dijelaskan di atas dalam Bagian 3.

9. Situs Pihak Ketiga

   Layanan dapat menyediakan tautan ke berbagai situs web pihak ketiga. Anda harus berkonsultasi dengan kebijakan privasi
   masing-masing dari situs web pihak ketiga ini. Kebijakan Privasi ini tidak berlaku untuk, dan kami tidak dapat
   mengontrol aktivitas, seperti situs web lainnya.

10. Mentransfer Data ke Negara Lain

    Jika Anda mengakses atau menggunakan Layanan di wilayah dengan undang-undang yang mengatur pengumpulan, pemrosesan,
    transfer, dan penggunaan data, harap perhatikan bahwa ketika kami menggunakan dan membagikan data Anda sebagaimana
    ditentukan dalam kebijakan ini, kami dapat mentransfer informasi Anda ke penerima di negara selain dari negara tempat
    informasi dikumpulkan. Negara-negara tersebut mungkin tidak memiliki undang-undang perlindungan data yang sama dengan
    negara tempat Anda pertama kali memberikan informasi.

    Layanan saat ini disimpan di Netlify dan GitLab , yang berarti informasi pribadi Anda ada dibawah Syarat Penggunaan
    mereka. Silahkan merujuk ke Syarat Penggunaan [Netlify](https://www.netlify.com/tos/){:target="_blank"} dan
    [Gitlab](https://about.gitlab.com/terms/){:target="_blank"}

11. Perubahan Kebijakan Privasi ini

    Kami terkadang memperbarui Kebijakan Privasi ini. Ketika kami melakukannya, kami akan memberi Anda pemberitahuan tentang
    pembaruan tersebut melalui (minimal) pemberitahuan yang cukup menonjol di Situs Web dan Layanan, dan akan merevisi
    Tanggal Efektif di bawah ini. Kami mendorong Anda untuk secara berkala meninjau Kebijakan Privasi ini untuk tetap
    mendapat informasi tentang bagaimana kami melindungi, menggunakan, memproses, dan mentransfer informasi pribadi yang
    kami kumpulkan.
