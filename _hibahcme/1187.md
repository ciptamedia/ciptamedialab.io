---
nomor: 1187
nama: YUSTRINA A SONDONPATO
foto:
  filename: rina.jpg
  type: image/jpeg
  size: 141819
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/11e0ebc0-5d50-40ad-8202-b297189a87b5/rina.jpg
seni: kuliner
pengalaman: 5 THN
website: ''
sosmed: ''
file: null
title: Pengembangan kuliner dalam bentuk kue
lokasi: 'Desa Domato kec Jailolo selatan Kab, Halmahera Barat'
lokasi_id: ''
deskripsi: >-
  untuk dapat dipasarkan dan dapat di kembangkan karena dengan kekurangan bahan
  dan peralatan sederhana kami mengakibatkan kue yang kami buat sangat terbatas
  dengan bahan yang ada jadi tujuan kami adalah kue yang kami buat dapat
  dipasarkan dan dapat memenuhi kebutuhan kami 
kategori: kerjasama_kolaborasi
latar_belakang: karena ada kekurangan bahan dan juga peralatan pembuatan kue
masalah: >-
  kami ingin mengembangkan usaha kami dengan bahan yang berkecukupan dan  juga
  peralatan yang memadai
durasi: 5 bulan
sukses: >-
  peralatan lengkap atau memadai sudah bisa dipasarkan dan dapat memenuhi
  kebutuhan konsumen
dana: '30'
submission_id: 5ab7a78b21859937ecdc6f65
---
