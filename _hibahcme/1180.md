---
nomor: 1180
nama: 'Ika dama yanti nasution '
foto:
  filename: IMG_20150517_002743.jpg
  type: image/jpeg
  size: 499526
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/b8f27776-26c6-4af2-83a5-185579b794d6/IMG_20150517_002743.jpg
seni: lainnya
pengalaman: 'Baru memulai '
website: ''
sosmed: ''
file:
  filename: _20180325_195920.JPG
  type: image/jpeg
  size: 172294
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/5a759f53-5d99-425b-b2b7-59bdace39f27/_20180325_195920.JPG
title: 'Menggali jiwa seni menenun perempuan muda untuk berkarya '
lokasi: 'Kabupaten tapanuli selatan '
lokasi_id: ''
deskripsi: "Melakukan pelatihan kepada perempuan-perempuan muda di daerah tapanuli selatan untuk mencintai seni dan budayanya seperti menenun. \r\nMendokumentasikan proses menenun dalam bentuk film mulai dari pemilihan benang, pewarnaan alami, motif hingga menjadi sebuah ulos atau kreatifitas lainnya berbahan dasar ulos. \r\nMelakukan pertunjukan seni atau teater, pameran dan festival tentang kreatifitas hasil seni (ulos). \r\n"
kategori: kerjasama_kolaborasi
latar_belakang: "Perkembangan teknologi membuat para seniman perempuan dalam hal menenun semakin berkurang  dengan adanya pabrik ulos yang membuat harga ulos di pasaran semakin turun. Hal itu membuat adanya ketidakseimbangan antara modal yang dikeluarkan dengan harga dari sebuah karya  seni. Sehingga secara perlahan-lahan seniman perempuan akan meninggalkan budaya menenun dan mencoba mencari aktifitas lain untuk membantu memenuhi kebutuhan keluarga. \r\nHal itu juga akan berpengaruh kepada generasi muda yang tidak tertarik untuk mempelajari seni budayanya sendiri yang membuat kekhawatiran akan punahnya budaya yang harus dijaga dan dilestarikan secara turun temurun. \r\nSelain itu, perkembangan di dunia maya seperti media sosial membuat masyarakat lebih mencintai budaya luar yang lebih trendy dan fashionable sehingga permintaan di pasar akan kebutuhan adat dan budaya semakin menurun sehingga sangat berpengaruh bagi eksistensi perempuan terhadap seni menenun khususnya di tapanuli selatan. \r\n"
masalah: "Seni menenun yang mayoritas perempuan kini mulai tidak diminati oleh masyarakat karena adanya alat tenun mesin (pabrik)  yang membuat harga tersebut semakin tidak sesuai dengan hasil seni yang diciptakan. Hal tersebut membuat seniman perempuan daerah terhambat dalam berkarya dan berkreatifitas. Begitu juga dengan generasi muda yang tidak tertarik dalam seni menenun sehingga membuat kekhawatiran akan punahnya budaya secara perlahan-lahan. \r\nUntuk itu perlu dilakukan hal-hal yang bisa menjaga eksistensi perempuan dalam seni dan berkarya. "
durasi: 9 bulan
sukses: "Adanya sebuah karya seni yang diciptakan seperti ulos yang merupakan hasil karya perempuan-perempuan muda (16-32 tahun). \r\nAdanya kreatifitas perempuan dalam mengembangkan karya seni yang berbahan dasar ulos.\r\nAdanya sebuah dokumentasi berupa film untuk mengabadikan proses seni menenun yang bisa di wariskan kepada generasi muda. \r\nAdanya kegiatan pameran tentang karya seni ulos atau kreatifitas lainnya yang dipublikasikan kepada masyarakat dengan tujuan mengajak masyarakat untuk mencintai dan bangga dengan budayanya sendiri. "
dana: '193'
submission_id: 5ab7a16abcd8e876b23072b8
---
