---
nomor: f-056
nama: Wiwiek Sipala
foto:
  filename: f-056-wiwiek.jpg
  type: image/jpeg
  size: 114498
  url: /hibahcme/profile-pict/f-056-wiwiek.jpg
seni: seni_pertunjukkan
pengalaman: Sejak 1974
website:
sosmed:
file:
title: REKAMAN DVD 10 TARI KARYA BARU DAERAH SULAWESI SELATAN
lokasi: Sulawesi Selatan
lokasi_id: 
deskripsi: >-
  Melalui program sosialisasi kesenian seperti workshop, pementasan, dan perekaman ataupun pendokumentasian, diharapkan dapat meningkatkan wawasan ataupun apresiasi seni kepada masyarakat yang sekaligus dapat meningkatkan kualitas kehidup dalam arti yang seluas-luasnya dan khususnya di bidang Ekonomi Kreatif.
  Untuk realisasi pemikiran tersebut, SDC dalam salah satu program kreatifnya akan mengadakan perekaman/dokumentasi 10 (sepuluh) karya tari baru yang bersumber dari kekayaan khasanah kebudayaan tradisional daerah Sulawesi Selatan, karya saya sendiri. Perlu diketahui bahwa untuk merekam karya-karya tari tersebut sudah sejak lama saya mimpikan namun terhambat karena memang kondisinya belum memungkinkan. Selanjutnya, sangat besar harapan saya via Cipta Media Kreatif dapat mewujudkan mimpi itu jadi kenyataan agar karya – karya tari tersebut dapat tersosialisasikan sesuai maksud dan tujuan penciptaannya.
  10 (sepuluh ) karya yang akan didokumntasikan berupa karya pendek yang lazim di dalam dunia tari Indonesia disebut dengan ‘repertoar’. Tarian berdurasi antara 7 sd 12 menit. Perekaman melingkupi karya lama dan karya baru yang sebagian sudah berpentas pada fest nasional dan internasional , sejak tahun 1979 hingga kini.
  Karya tari merupakan karya baru atas tafsir budaya masyarakat Sulawesi Selatan, meliputi 4 (empat) etnik : etnik Makassar, Bugis, Luwuk, dan etnis Toraja
kategori: akses
latar_belakang: >-
  Wiwiek Sipala, membentuk kelompok kreatif bernama Sipala Dance Company disingkat SDC pada tanggal 10 Juni 1993. SDC diharapkan dapat mewadahi berbagai aktifitas bidang seni sebagai salah satu upaya pelestarian kesenian Nusantara.
  Pesatnya perkembangan teknologi dan komunikasi di era ini, menghasilkan keterbukaan jaringan lintas batas wilayah geografis yang menjadikan manusia di seantero dunia dengan cepat dan mudah saling berinteraksi tanpa mengenal batas usia, gender, ras, bangsa. Manusia mengalami perubahan yang merupakan perkembangan budaya kehidupan meliputi mentalitas, pemikiran, dan gaya hidup.
  Pemikiran di atas menurut kami adalah sangat relevan dan kami mendukung keputusan Internasional Conference on Cultural Tourism (ICCT) 1995 di Yogyakarta yang mendorong terwujudnya ‘Api Spiritualitas’ yang membangkitkan kesadaran masyarakat, bahwa perlunya melestarikan budaya lokal sebagai media ampuh untuk menanggulangi dampak buruk budaya global.
  Perkembangan tersebut, tidak dapat dihindari karena merupakan kodrat peradaban baru manusia di era kini, sebuah fenomena global yang berimbas pada perubahan perilaku manusia yang sangat signifikan dan kompleks, positif dan negatif. Tentu saja yang perioritas untuk dicermati adalah yang berdampak negatif, salah satunya adalah fenomena krisis kepercayaan diri dan identitas. Menurut pendapat kami, salah satu upaya untuk menetralisir masalah tersebut adalah melalui apresiasi budaya/kesenian tradisional Nusantara yang mulai termarjinalkan. Bila ditinjau lebih jauh, pada hakekatnya kesenian tradisional Nusantara bermuatan dan menyuarakan pandangan nilai-nilai luhur kehidupan yang merupakan kristalisasi jati diri bangsa Indonesia.
masalah: >-
  Perlu disampaikan bahwa peralatan musik dan kostum untuk 7 tarian, sudah dimiliki sudah kami mikili, sisanya untuk 2 tarian perlu diadakan dari 10 tarian, ada 6 tarian hanya memerlukan 2 x kali latihan untuk menyegarkan dan menyesuaikan dengan musik, sedang 4 tarian memerlukan latihan masing – masing 4 kali.
durasi: 9 bulan
sukses: >-
  1. Menyemarakkan materi kesenian daerah Wilayah Timur Indonesia khususnya di Sulawesi Selatan berupa CD yang sebelumnya hanya dibuat oleh Koreografer terkenal Ibu Nurhani Sapada pada tahun 1970,
  2. Membangkitkan kesadaran masyarakat bangsa Indonesia pada umumnya untuk cinta dan bangga terhadap budaya lokal,khususnya tari daerah Sulawesi Selatan.
dana: '363.350'
---
