---
nomor: f-059
nama: Rahmiana Rahman
foto:
  filename: f-059-Rahmiana.jpg
  type: image/jpeg
  size: 506471
  url: /hibahcme/profile-pict/f-059-Rahmiana.jpg
seni: sastra
pengalaman: Baru memulai
website:
sosmed:
file:
title: Kumpulan Esai Tentang Perempuan Kepulauan 
lokasi: Aceh
lokasi_id: 
deskripsi: >-
  Mengadakan residensi kepada 10 penulis perempuan muda terpilih di pulau-pulau sebagai bahan penelitian untuk membuat essay. 
  10 penulis ini merupakan hasil seleksi dari contoh karya yang akan mereka kirimkan saat mendaftar. Setelah terpilih, mereka akan mendapatkan workshop sebelum melakukan residensi.
  Selama 10 hari residensi, para penulis tersebut akan tinggal di rumah penduduk sehingga bisa semakin dekat dengan penduduk sebagai salah satu subjek penelitian.
kategori: riset_kajian_kuratorial
latar_belakang: >-
  Proyek ini dilatarbelakangi oleh kurangnya akses menulis bagi perempuan-perempuan Aceh. Padahal, semangat perempuan untuk menulis sangat tinggi. Saya bisa lihat sewaktu diadakan kegiatan Forum Aceh Menulis, dimana kehadiran perempuan menulis sangat tinggi. Sayangnya, akses dan wadah untuk memunculkan hasil-hasil tulisan perempuan masih sangat kurang di Aceh.
  Selain itu, saya tertarik menginisiasi ini karena banyak pulau di Aceh yang belum terekspos keindahan dan segala potensi didalamnya. Oleh karena itu, saya yakin kekuatan perempuan dalam menulis tentang kepulauan akan menjadi hal yang menarik. 
masalah: >-
  1. Kurangnya akses menulis bagi perempuan-perempuan di Aceh
  2. Kurangnya informasi pulau-pulau dan potensi SDM didalamnya
durasi: 5 bulan
sukses: >-
  1. 50 pendaftar perempuan untuk mengikuti selesi sebagai animo adanya program ini,
  2. 10 penulis terpilih akan mendapatkan pelatihan untuk menulis yang sesuai struktur dan mendapatkan residensi sebagai bahan acuan karya mereka,
  3. 10 penulis terpilih akan menciptakan karya antologi untuk dipublikasikan secara online dan offline.
dana: '83'
---
