---
nomor: 1243
nama: Nurmawati
foto:
  filename: DSC03815.JPG
  type: image/jpeg
  size: 134328
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/37e133da-8c7e-4128-bfe4-8a052a081629/DSC03815.JPG
seni: penelitian
pengalaman: 6 bulan
website: ''
sosmed: ''
file:
  filename: Makalah ATL_Nurmawati.docx
  type: application/vnd.openxmlformats-officedocument.wordprocessingml.document
  size: 33334
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/c307adeb-ab71-4028-abb2-2416f1f1c22b/Makalah%20ATL_Nurmawati.docx
title: Kue Tradisional Karya Para Perempuan Kelayu
lokasi: 'Selong, Lombok Timur, Nusa Tenggara Barat'
lokasi_id: ''
deskripsi: >-
  Kategori proyek ini adalah riset tentang kue tradisional karya para perempuan
  Kelayu. Adapun pokok-pokok masalah yang akan diteliti terkait kue tradisional
  karya para perempuan Kelayu adalah sebagai berikut. 1. Sejarah kue tradisional
  karya para perempuan Kelayu.  2. Jenis-jenis kue tradisional karya para
  perempuan Kelayu. 3. Cara pengolahan kue tradisional karya para perempuan
  Kelayu. 4. Dokumentasi kue tradisional karya para perempuan Kelayu. 5.
  Bentuk-bentuk pemasaran kue tradisional karya para perempuan Kelayu. Hasil
  riset ini diharapkan dapat bermanfaat sesuai pokok-pokok masalah yang
  diteliti. Adapun manfaat riset ini adalah sebagai berikut.1. Hasil riset
  tentang kue tradisional karya para perempuan Kelayu dapat dimanfaatkan sebagai
  sarana untuk menggali potensi kue tradisional Kelayu dalam rangka meningkatkan
  dan menggalakkan pengembangan pangan lokal serta melestarikan kue tradisional
  di era moderen ini. 2. Hasil riset ini dapatkan dimanfaatkan sebagai contoh
  aktivitas positif bagi para perempuan di tempat yang lain. 3. Hasil riset ini
  dapat dimanfaatkan sebagai sarana promosi dalam rangka pemasaran kue
  tradisional Kelayu.
kategori: riset_kajian_kuratorial
latar_belakang: "Berbagai jenis kue tradisional dapat dipakai sebagai salah satu ukuran tingginya kebudayaan dari daerah yang bersangkutan. Kue tradisional merupakan wujud budaya yang berciri kedaerahan dan jenis-jenisnya mencerminkan potensi alam daerah yang dimiliki. Kue tradisional tidak hanya sebagai sarana untuk pemenuhan kebutuhan gizi seseorang, tetapi juga berguna untuk mempertahankan hubungan antarmanusia, dapat pula dijual yang akan mendatangkan pendapatan keluarga dan dipromosikan untuk menunjang pariwisata yang selanjutnya dapat mendukung pendapatan suatu daerah. Terkait dengan kue tradisional, para perempuan Kelayu memiliki peranan penting dalam membuat dan menjual kue tradisional. Para perempuan Kelayu merupakan sosok-sosok inspiratif dalam mempertahankan budaya, dalam hal ini melestarikan kue tradisional. Perempuan Kelayu memiliki peran yang sangat penting dalam keluarga, seperti mengurus berbagai kebutuhan rumah tangga, mengurus anak dan suami, serta mengatur kondisi keuangan. Perempuan Kelayu tidak hanya berpangku tangan dan menunggu penghasilan dari suami, tetapi aktif dalam rangka meningkatkan ekonomi keluarga dan membantu suami dalam mencari nafkah dengan cara membuat kue tradisional dan menjualnya. Kue tradisional Kelayu dengan berbagai jenisnya tidak hanya menampilkan seni karya perempuan, tetapi dapat memenuhi gizi anggota keluarga yang ikut menikmati kue tradisional Kelayu. Selain itu, komunitas perempuan Kelayu yang membuat kue tradisional akan menciptakan Desa Kelayu sebagai destinasi wisata kuliner.\r\n\r\n"
masalah: >-
  Adapun pokok-pokok masalah yang akan diteliti terkait kue tradisional karya
  para perempuan Kelayu adalah sebagai berikut. 1. Sejarah kue tradisional karya
  para perempuan Kelayu.  2. Jenis-jenis kue tradisional karya para perempuan
  Kelayu. 3. Cara pengolahan kue tradisional karya para perempuan Kelayu. 4.
  Dokumentasi kue tradisional karya para perempuan Kelayu. 5. Bentuk-bentuk
  pemasaran kue tradisional karya para perempuan Kelayu. 
durasi: 6 bulan
sukses: "Indikator sukses dari riset tentang kue tradisional karya para perempuan Kelayu adalah sebagai berikut. 1. Sejarah kue tradisional dan jenis-jenis kue tradisional karya para perempuan Kelayu, akan lebih dikenal oleh masyarakat luar, baik masyarakat yang di luar daerah maupun luar negeri. 2. Dapat menjadi contoh sebagai desa industri rumahan yang berhasil bagi desa yang lain. 3. Diterbitkan buku khusus tentang kue tradisional Kelayu. 4. Peningkatan omset penjualan kue tradisional Kelayu. 5. Desa Kelayu sebagai destinasi wisata kulinert. 6. Menunjang pariwisata Nusa Tengggara Barat yang selanjutnya dapat mendukung pendapatan daerah. 7. Peningkatan gizi semua anggota keluarga. 8. Kemampuan seni para perempuan Kelayu dalam membuat kue tradisional semakin terasah. 9. Meningkatnya kesejahteraan masyarakat Kelayu. 10. Kebudayaan berupa kue tradisional akan tetap lestari.\r\nKesuksesan riset ini tentunya tidak hanya dari periset, namun keterlibatan semua pihak, baik itu narasumber, para perempuan Kelayu, pihak Cipta Media Ekspresi yang memberikan dukungan dana dalam pelaksanaan riset. Periset turut membantu para perempuan Kelayu untuk mengekspos kue tradisional yang mereka buat melalui riset ini. Periset berharap melaui riset ini akan menambah kepercayaan diri yang tinggi terhadap kualitas kue tradisional karya para perempuan Kelayu, Selong.\r\n"
dana: '47'
submission_id: 5ab7be0327a91c60a549168f
---
