---
permalink: /hibahcme/898
nomor: 898
nama: FA'IDA NURACHMAWATI
foto:
  filename: 10716240_10204186990547578_21396671_n.jpg
  type: image/jpeg
  size: 40320
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/be5add61-f486-492e-bacb-d5ee24875421/10716240_10204186990547578_21396671_n.jpg
seni: seni_rupa
pengalaman: 2 tahun
website: ''
sosmed: 'Instagram: @faidarachma'
file:
  filename: Daftar Karya - Cipta Media Karya.docx
  type: application/vnd.openxmlformats-officedocument.wordprocessingml.document
  size: 386134
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/41937cd5-3650-4928-b5bf-e8af782b79d0/Daftar%20Karya%20-%20Cipta%20Media%20Karya.docx
title: The Round Table
lokasi: DI Yogyakarta, Magelang, Solo, Bandung, Jakarta, dan Surabaya
lokasi_id: Q3741
deskripsi: The Round Table merupakan proyek jangka panjang yang saya kerjakan bersama dengan Alwan Brilian Dewanta dan Yonaz Kristy. Proyek ini berusaha untuk membentuk sebuah direktori kolektif muda yang bergerak dalam skena seni. Direktori yang akan direalisasikan dalam wujud website ini mencakup profil kolektif-kolektif muda dari Indonesia (yang diharapkan dapat meluaskan jangkauan hingga region Asia Tenggara) berikut dengan arsip karya mereka. Website ini dibuat sebagai wadah agar informasi tentang mereka lebih mudah diakses (accessible) bagi khalayak yang lebih luas. Keterjangkauan data-data tersebut juga kami harapkan dapat meningkatkan eksposur bagi beberapa kolektif yang mungkin masih membutuhkan. Selain itu, secara rutin situs ini akan memunculkan diskusi seputar isu pergerakan anak muda terkait dengan laku kolektif mereka dengan data-data yang akan diperoleh lewat metode wawancara. \r\n\tDalam jangka waktu sembilan bulan ini, kami bertiga juga berkesempatan untuk melakukan residensi di Tentacles Gallery, Bangkok, Thailand. Proyek ini rencananya juga akan kami bawa selama program residensi tersebut sebagai pembanding atas dinamika kolektif muda di Bangkok dengan di Indonesia. Pun kami juga berencana untuk berusaha mendapatkan program residensi dari berbagai daerah yang berada di Asia Tenggara untuk lebih memperluas cakupan direktori ini.
kategori: riset_kajian_kuratorial
latar_belakang: 'Pada bulan Oktober 2017 silam, RAR Editions (kolektif interdisipliner tempat kami bertiga bergabung) bersama dengan empat kolektif muda lainnya, berkesempatan untuk membuat proyek di Cemeti: Institut untuk Seni dan Masyarakat sebagai bagian dari proyek Dito Yuwono berjudul Ruang Politik Pertama Bernama Rumah. Proyek Dito tersebut berupaya untuk mendemistifikasi Cemeti sebagai institusi yang telah berusia panjang (dan sebagai institusi yang memiliki pengaruh kuat terhadap seni kontemporer di Indonesia), sehingga seniman-seniman dari kolektif muda ini ini dapat kembali menggunakan Cemeti sebagai ruang berkarya dan bereksperimen seperti Cemeti di awal mula berdirinya. \r\n\tSetelah pelaksanaan proyek tersebut, muncul beberapa pertanyaan seputar kolektif muda, terutama yang bergerak di bidang seni, misalnya: Mengapa seniman-seniman muda ini memutuskan untuk bekerja sebagai sebuah kolektif? Apakah cara kolektif seniman muda bekerja memiliki perbedaan dengan yang terjadi pada generasi sebelumnya? Seberapa lama mereka membayangkan diri mereka akan bekerja bersama sebagai sebuah tim atau kolektif? Bagaimana mereka melakukan pengaturan kerja tim dan manajemen? Bagaimana mereka mengatur pendanaan dan menyusun program? Bagaimana mereka mengarsipkan karya-karya mereka? Petanyaan-pertanyaan semacam ini membuat kami terpantik untuk memahami lebih jauh bagaimana kolektif-kolektif seniman muda ini bekerja. '
masalah: 'Selain pertanyaan-pertanyaan tersebut, kami juga menemukan permasalahan yang seringkali dihadapi kolektif-kolektif muda ini, yakni berkaitan dengan sumber daya. Beberapa kolektif muda yang kami temui, menghentikan aktivitasnya atau memutuskan untuk membubarkan diri karena kekurangan energi dan pendanaan untuk menyokong program-program yang dicanangkan\r\n\tDengan melakukan wawancara dan profiling, kami juga berharap proses pembentukan situs ini dapat membantu proses pengarsipan karya-karya mereka dan membuka kemungkinan kolaborasi di antara kolektif-kolektif muda ini. Selain itu, dengan adanya diskusi dan analisisis seputar isu pergerakan anak muda lewat wawancara yang dilakukan dengan masing-masing kolektif, diharapkan situs ini dapat terjadi pertukaran informasi yang dapat digunakan sebagai referensi untuk memecahkan permasalahan-permalahan dalam hal manajemen kolektif dan kekaryaan yang tidak dapat dipecahkan sendiri.\r\n\r\n*kolektif yang kami maksud di sini mengacu pada sekelompok orang yang berkumpul karena memiliki ketertarikan pada hal yang sama dan memusatkan kegiatan mereka pada ketertarikan tersebut. Usia yang menjadi batasan dalam projek ini adalah kolektif dengan anggota di bawah 30 tahun. '
durasi: 9 bulan
sukses: 'Indikator Sukses: Dalam waktu 9 bulan, kami menargetkan untuk meluncurkan website kami pada pertengahan tahun ini, dan menyelesaikan profiling kolektif-kolektif muda di area Yogyakarta, Magelang, Jakarta, Bandung dan Surabaya. Selain profiling, kami juga berencana untuk menuliskan artikel dan esai terkait dengan kolektivitas anak-anak muda ini di dalam situs tersebut. \r\nDana hibah akan digunakan untuk proses pembentukan dan pengelolaan website, desain, dokumentasi, transkrip wawancara, alih bahasa, pengolahan data, dan biaya operasional lainnya (transportasi, konsumsi, cetak, dsb.)'
dana: '60'
---
