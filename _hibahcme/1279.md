---
nomor: 1279
nama: rizka hidayatul umami
foto:
  filename: WhatsApp Image 2018-03-17 at 17.42.27.jpeg
  type: image/jpeg
  size: 369269
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/d1826453-36af-4d93-9198-8f8b1d6c3b54/WhatsApp%20Image%202018-03-17%20at%2017.42.27.jpeg
seni: sastra
pengalaman: 3 tahun
website: rizkatacin.blogspot.com / morfo biru
sosmed: >-
  FB: https://www.facebook.com/rizka.taciend IG: @Morfo_Biru twitter:
  @morfo_biru 
file:
  filename: DONGENG RUKMINI siap cetak.pdf
  type: application/pdf
  size: 3288934
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/1e716c13-6c72-4be0-83a4-3404bf45b729/DONGENG%20RUKMINI%20siap%20cetak.pdf
title: Perempuan dan Rewang dalam Kajian Etnografi
lokasi: kabupaten Tulungagung Jatim
lokasi_id: ''
deskripsi: >-
  Rewang merupakan salah satu kebiasaan masyarakat desa berupa bantuan tenaga
  dalam menyukseskan sebuah acara, seperti nikahan, slametan, tingkeban, dan
  beberapa acara yang khas dilakukan oleh masyarakat Indonesia, utamanya Jawa.
  Biasanya rewang didominasi oleh ibu-ibu yang merupakan tetangga sekitar, yang
  dengan sukarela memberikan jasa kepada si empunya hajat. Dalam praktik
  keseharian, rewang menjadi salah satu tradisi yang langgeng dilaksanakan dan
  menjadi kesadaran tiap individu untuk mengikutsertakan dirinya dalam berbagai
  kesempatan. Tradisi ini ternyata memiliki multihistoris dan keunikan yang
  tidak bisa dijelaskan, kecuali dengan pendekatan atau kajian etnografi. Selama
  kurang lebih enam bulan, riset terkait rewang akan dilaksanakan untuk
  menemukan keunikan-keunikan dari tradisi masyarakat yang ternyata juga mulai
  memudar ini. Setelah riset berhasil dilaksanakan, maka selaku penulis saya
  akan membukukannya sebagai novel etnografi.
kategori: riset_kajian_kuratorial
latar_belakang: >-
  Menjadi seorang jurnalis dalam majalah kampus yang memiliki jargon media
  pemikiran alternatif, menuntut saya untuk senantiasa bergumul dengan banyak
  masyarakat yang berbeda, baik dari segi agama, kepercayaan, suku, ras,
  termasuk budaya. Dalam pergumulan itu saya sering terpikat dengan
  tradisi-tradisi masyarakat desa pinggiran yang masih bertahan di tengah arus
  modernisasi seperti sekarang ini. Salah satu tradisi yang membuat saya gumun
  (dalam bahasa jawa) adalah rewang, mengingat saat ini masyarakat sangat
  individualis dalam berbagai hal. Di kota-kota besar, kebiasaan rewang sudah
  banyak bergeser, yang pada awalnya sukarela menjadi berbayar, yang pada
  awalnya dilakukan oleh para tetangga, justru dikerjakan oleh para pekerja yang
  di datangkan dari sebuah agen atau evet organizer. Sehingga ketika melihat
  tradisi ini masih berjalan dengan pakemnya, saya pun tergerak untuk
  mengabadikannya dalam bentuk tertulis. Selain itu, rewang yang didominasi oleh
  perempuan, membuat saya makin tergerak untuk melihat bagaimana posisi
  perempuan dalam tradisi tersebut. Adapun tujuan dari riset ini tak lain adalah
  ingin agar tradisi ini tidak punah dimakan zaman dan agar perempuan mengetahui
  bahwa sesungguhnya rewang (yang identik dengan dapur) bukan semata-mata
  diwajibkan untuk mereka, tetapi juga untuk  laki-laki. Selain itu manusia
  harus tetap mengedepankan asas gotong royong, yang salah satunya tercermin
  lewat tradisi rewang.
masalah: >-
  Saya ingin mengangkat tradisi rewang yang identik dengan perempuan dan yang
  saat ini mulai tergerus oleh modernitas zaman. Sebagaimana yang kita ketahui
  bahwa modernitas yang tidak dapat dihalau ini menghadirkan sikap-sikap
  antisosial beberapa golongan, sehingga membuat banyak tradisi terkubur,
  individualisme mengakar kuat hampir di seluruh lapis masyarakat. Rewang saat
  ini tidak bisa kita temukan di kota-kota besar, bahkan di desa-desa tradisi
  ini sudah lama bergeser dan tidak lagi seperti dulu. Saya tidak ingin turut
  andil dalam menggerus tradisi-tradisi yang mencerminkan gotong royong
  tersebut, maka dengan ini saya ingin membuat suatu karya yang kemudian dapat
  bermanfaat bagi kemaslahatan kita sebagai manusia yang punya jiwa sosial.
durasi: 8 bulan
sukses: >-
  Selama 6 bulan berhasil melakukan kajian etnografi di beberapa wilayah di
  kabupaten Tulungagung dan 2 bulan sisanya selesai menulis sampai membukukan
  hasil riset menjadi sebuah novel etnografi tentang rewang dan peran perempuan.
dana: '80'
submission_id: 5ab7cbf553f63e6e8f23359d
---
