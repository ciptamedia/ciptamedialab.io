---
permalink: /hibahcme/087
nomor: 87
nama: Suryana
foto:
  filename: 1513830504697.jpg
  type: image/jpeg
  size: 634430
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/f9469fe2-fb5e-4cb9-b852-1310b4b68cc9/1513830504697.jpg
seni: seni_pertunjukan
pengalaman: '4tahun '
website: ''
sosmed: 'Line : ryanafirmansyah  Ig : ryanafirmans/ryanamake_up'
file: ''
title: Workshop tari lintas perbatasan
lokasi: Kabupaten kepualauan anambas, propinsi Kepulauan Riau
lokasi_id: ''
deskripsi: Saya Suryana bekerja sebagai pelaku seni tari dan penata rias pertunjukkan. Saya memiliki proyek untuk melaksanakan workshop tari d Kabupaten Kepulauan Anambas dengan bahan ajar 1. Ketubuhan seorang penari 2. Penciptaan tari untuk entertaint 3. Tata rias pertunjukkan tari. Dalam hal ini sasaran saya adalah sekolah-sekolah serta komunitas seni tari untuk menghasilkan karya yg lebih baik lagi.  saya memilih kabupaten kepulauan Anambas, khususnya daerah Letung kecamatan Jemaja, Karena potensi masyarakat yg mencintai seni pertunjukkan sangat antusian namun tidak Ada acuan untuk memperdalam seni tari dengan teknik dan bentuk penyajian yang baik. Faktor utama ketidakstabilan ini adalah jarak anambas yang jauh di perbatasan perairan laut cina selatan dan jauh dari ibu Kota propinsi serta transportasi yang Masih kurang, mengakibatkan para pelaku seni tari membagi ilmu ke daerah tersebut. Tapi saya jatuh cinta dengan masyarakat dan tempat tersebut mendorong saya untuk melaksanakan workshop tari dan tata rias. Namun Kendala sekarang adalah, kecamatan jemaja kabupaten kepulaun Anambas sedang dilanda musibah banjir dan lonsong diakibatkan Karena angin utara dan guyuran hujan 3hari 3malam membuat jemaja sedikit teraniaya dengan keadaan, kalau tidak ada halangan saya ingin melaksanakan workshop pertengahan February, kalau sikon membaik, namun apabila tidak memungkinkan, saya Akan mengadakan akhir April. Terima kasih
kategori: riset_kajian_kuratorial
latar_belakang: Terbentuknya proyek ini dikarenakan kurangnya pengetahuan masyarakat atau pelaku seni d anambas tentang pertunjukkan tari. Proyek ini murni dari aspirasi pikiran saya untuk memajukan anambas Dalam bidang seni dan budaya khusus nya seni tari dan kebudayaan tradisi masyarakat melayu anambas. Anambas memiliki potensi untuk mengangkat tradisi dengan pengembangan seni perrunjukkan tari yang memiliki acuan bahwa tradisi masa lampau bisa dijadikan tradisi masa kini dengan tdak merubah tatanan melainkan memperindah pertunjukkan dengan konsep-konsep Yang dibutuhkan Dalam sebuah pertunjukkan Yang baik. Hal ini juga dilandasi agar keterbelkangan dan terpencilnya daerah anambas bukan menjadi penghalang sekelompok orang atau komunitas untuk melaksanakan pertunjukkan hingga keluar daerah, minimal membawa pertunjukkan tari yang apik dan pantas untuk di suguhkan Dalam acra-acra kedaerahan, dimana ibu Kota propinsi yang sangat jauh dari anambas bisa di jangkau dengan pembuktian bahwa seni tradisi anambas juga berhak diperhatikan dan do lestarikan, Salah satunya adalah mempersiapkan para pelaku seni tradisi dengan persiapan-persiapan pertunjukkan yang bisa menarik perhatian pemerintah.
masalah: Masalah Yang ingin saya atasi adalah kekurangan dana yang menjadi Kendala saya untuk melaksanakan proyek tersebut. Karena proyek tersebut murni pribadi dari keinginan saya. Saya tidak ingin masyarakat Di bebankan dengan adanya workshop ini agar terlaksana baik, apalagi sekarang bencana alam sedang melanda kabupaten kepulauan anambas tersebut
durasi: 1 bulan
sukses: Workshop ini akan sukses dengan beberapa Kali dilakukan. 1. Bisa dijadikan acuan apabila Ada acara daerah2 Yang membuat pelaku seni terlibat.\r\n2. Para pelaku seni bisa lebih baik lagi untuk pertunjukkan.
dana: '10'
---
