---
nomor: 1264
nama: Ingrid Fransisca Tambunan
foto:
  filename: Ingrid-2.jpg
  type: image/jpeg
  size: 1449261
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/8667aa77-355a-419b-9a61-d7ad03cc59a5/Ingrid-2.jpg
seni: audiovisual
pengalaman: 4 tahun
website: ''
sosmed: ''
file: null
title: Dokumenter OTK
lokasi: Jakarta Selatan
lokasi_id: ''
deskripsi: "Karya ini akan dituangkan dalam bentuk film dokumentar yang menggambarkan proses yang dijalani oleh penyintas kekerasan seksual untuk mencapai pementasan Ode Tusuk Konde. \r\n\r\nAda berbagai tantangan dan halangan dalam bentuk ekonomi, psikologis, maupun dukungan dari masyarakat sekitar yang harus dihadapi para penyintas.\r\n\r\nDalam tiap sesi, selalu saja ada trauma yang timbul dari pemain teater. Namun, penyintas lain yang bermain teater menguatkannya. Sehingga tujuan memberikan support system yang kuat tercapai. LBH Apik juga siaga menyediakan terapis dari Yayasan Pulih agar menguatkan mental pemain.\r\n\r\nSecara ekonomi, kebanyakan penyintas adalah perempuan kelas menengah bawah yang harus berjuang untuk hidupnya sehari-hari. Bahkan beberapa single parent. Sehingga, untuk berangkat latihan saja, kadang beberapa mengeluh tidak punya uang transportasi.\r\n\r\nLBH Apik mengusahakan beberapa solusi, seperti misalnya dengan berangkat latihan dari kantor LBH Apik agar transportasi bisa ditanggung kantor, maupun siasat lainnya.\r\n\r\nPara penyintas juga tak mendapatkan honoranium a. Sekalipun tiket terjual, namun biaya produksi dan penyewaan tempat jadi kendala. Sehingga penyintas yang ternyata berbakat di dunia akting tidak mendapat bayangan bahwa sebenarnya mereka bisa berkarya di dunia seni peran. \r\n\r\nDari segi teknis, dengan harapan film dokumenter ini akan dapat ditayangkan di berbagai festival fim di seluruh dunia, maka durasi film ini ditetapkan sepanjang 18-20 menit."
kategori: kerjasama_kolaborasi
latar_belakang: "Ode Tusuk Konde berawal dari inisiatif LBH Apik dalam membantu pemulihan penyintar kekerasan seksual, fisik, dan mental. Para pemeran Ode Tusuk Konde adalah perempuan penyintas dari mitra LBH Apik, termasuk diantaranya adalah para Pekerja Rumah Tangga (PRT) yang disiksa oleh majikannya. \r\n\r\nFormat Ode Tusuk Konde adalah dramaturgi dimana sebagian pemain memerankan diri sendiri. LBH Apik Jakarta sebagai mitra para penyintas mencari cara agar proses pemulihan dan pendampingan berjalan dengan baik lewat berbagai cara. Akhirnya, dibuatlah cara pemulihan lewat seni di samping pendampingan psikologis dari Yayasan Pulih dan metode healing lainnya.\r\n"
masalah: Menimbulkan kesadaran di masyarakat mengenai kekerasan seksual.
durasi: 6 bulan
sukses: "Kesuksesan tercapai disaat Unit Sekar telah memiliki sebuah film dokumentasi yang siap tayang, yang memiliki narasi yang jelas dan lugas tentang perjuangan para penyintas menuju pementasan Ode Tusuk Konde. Film dokumentasi ini seyogyanya akan dapat mengangkat cerita-cerita para penyintas secara faktual, dan memberikan pemahaman kepada penonton akan efek psikologis yang dihadapi penyintas kekerasan seksual, perjuangan mereka untuk pulih dari trauma, dan tantangan-tantangan yang mereka hadapi untuk dapat menyuarakan kisah mereka. \r\n\r\nIndikator kesuksesan berikutnya adalah tercapainya pemahaman dan empati dari para penonton mengenai dampak psikologis kekerasan seksual dan bagaimana seringkali korban justru menjadi pihak yang disalahkan. Harapan Unit Sekar adalah dengan adanya film dokumentasi ini maka terjadi diskusi yang meningkatkan kepedulian masyarakat terhadap penyintas kekerasan seksual. Selain itu diharapkan adanya peningkatan kesadaran dan kepedulian terhadap perlindungan perempuan, sehingga kedepannya jumlah kejadian kekerasan  seksual akan berkurang drastic atau malah hilang sama sekali.\r\n\r\nSelain itu diharapkan akan adanya peningkatan pengetahuan masyarakat terhadap Ode Tusuk Konde, sebagai salah satu sarana pemulihan trauma penyintas kekerasan seksual. Dengan adanya peningkatan pengetahuan dan kepedulian maka masyarakat akan lebih paham mengenai adanya trauma berkepanjangan setelah kejadian kekerasan seksual, sehingga masyarakat dapat memberikan sokongan dan dukungan lebih kepada para penyintas dalam menghadapi trauma mereka.\r\n \r\n"
dana: '150'
submission_id: 5ab7c716e0fafd7f196ed85f
---
