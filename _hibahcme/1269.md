---
nomor: 1269
nama: Linda Fitria
foto:
  filename: 2017-08-08 06.21.53 1.jpg
  type: image/jpeg
  size: 2085236
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/cc74dcaa-f2eb-41c3-b896-5a50970547e3/2017-08-08%2006.21.53%201.jpg
seni: penelitian
pengalaman: 2 tahun
website: www.sepertisirus.blogspot.co.id
sosmed: 'Instagram: @sepertisirus'
file:
  filename: LindaNegeriTiouwdanSinggasanayangKosong_Saparua_revisi.doc
  type: application/msword
  size: 769536
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/7871a49d-cfd7-400a-b018-a9ebb5f0eb04/LindaNegeriTiouwdanSinggasanayangKosong_Saparua_revisi.doc
title: Kartu Pos dari Banda
lokasi: 'Kepulauan Banda, Maluku'
lokasi_id: ''
deskripsi: >-
  Kepulauan Banda di Kabupaten Maluku Tengah, Provinsi Maluku seolah menjadi
  gudang cerita tentang kejayaan rempah masa lalu, keindahan alam, kelam cerita
  penjajahan hingga salah satu yang masih bertahan sampai saat ini: Pesta Rakyat
  Banda. Di 2018, ada momen unik dalam Pesta Rakyat Banda, yakni pembersihan
  sumur tua yang dianggap sakral dan hanya berlangsung 10 tahun sekali. Lewat
  proyek ini, saya ingin mendokumentasikan pra-acara-pasca Pesta rakyat Banda
  2018 yang kemudian akan memiliki output berupa catatan perjalanan dan photo
  story, yang akan dipamerkan dengan judul yang serupa dengan judul proyek ini.
  Selain itu, saya juga ingin menampilkan apapun yang nantinya diperoleh dari
  Banda dan tentang kepulauan ini, baik literatur maupun pernik hasil budaya
  yang berupa fisik dan berkaitan dengan Pesta Rakyat Banda 2018.
kategori: perjalanan
latar_belakang: >-
  Berawal dari catatan sejarah mengenai pengasingan Moh Hatta dan Sjahrir di
  Banda Neira, kemudian saya tertarik dengan cerita tentang segala sesuatu
  tentang Banda. Menurut saya, Banda menjadi salah satu tempat unik di
  Indonesia, dimana memiliki cerita dengan yang sifatnya seperti dua sisi mata
  uang, ada kisah menyenangkan dan adapula yang memilukan.
masalah: Mengangkat tentang kondisi Banda terkhusus pesta rakyat banda 201
durasi: 1 bulan
sukses: >-
  Terlaksananya pameran 'Kartu Pos dari Banda' yang menampilkan pernik pesta
  rakyat Banda 2018
dana: '25'
submission_id: 5ab7c7e904d9e120cd64f0f0
---
