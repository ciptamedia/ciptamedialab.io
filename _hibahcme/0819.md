---
permalink: /hibahcme/819
nomor: 819
nama: Elsa Izaty P
foto:
  filename: FOTO DIRI_Elsa Izaty Permatasari.jpg
  type: image/jpeg
  size: 2258193
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/880fdbb5-dd6e-4ad7-84d8-75434b71f912/FOTO%20DIRI_Elsa%20Izaty%20Permatasari.jpg
seni: seni_rupa
pengalaman: Baru Memulai
website: '-'
sosmed: Instagram @elsaizty
file:
  filename: Reconstruction of Beauty_Inspired by the cultures of Indonesia (Lombok, Dayak), Africa, Myanmar, India.jpg
  type: image/jpeg
  size: 2036695
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/22cde40f-185e-45aa-9d74-0b6203cc05bc/Reconstruction%20of%20Beauty_Inspired%20by%20the%20cultures%20of%20Indonesia%20(Lombok,%20Dayak),%20Africa,%20Myanmar,%20India.jpg
title: Pameran Tunggal 'The Reconstruction of Beauty'
lokasi: Kota Batu, Propinsi Jawa Timur
lokasi_id: ''
deskripsi: 'Dalam proyek ini saya ingin melakukan pameran tunggal dengan jenis karya berupa foto dan vidio. Karya foto dan vidio tersebut dihasilkan dari objek-objek perupaan yang saya ciptakan dari proses merias talent perempuan. Hasil riasan merupakan gabungan dari berbagai kontruksi kecantikan dari berbagai kebudayaan. Hasil riasan tersebut juga didapatkan dari studi riset dari banyak literatur yang mendukung. Karya foto dan vidio nantinya akan ditunjukkan saat pameran. Selain itu saya juga ingin menunjukkan talent lengkap dengan riasannya saat pembukaan pameran. Dalam eksekusi terakhir yang berupa pameran, saya ingin menunjukkan kepada publik mengenai ide gagasan saya mengenai isu permasalahan yang saya angkat melalui karya saya. '
kategori: akses
latar_belakang: 'Sebagai sarjana lulusan antropologi, tentunya saya tertarik dengan isu-isu yang berkaitan dengan kebudayaan. Selain itu saya selalu tertarik dengan karya-karya yang berhubungan dengan perempuan. Menurut saya, karya yang menggambarkan tentang perempuan selalu memiliki daya tarik lebih tinggi dari pada karya-karya yang menggambarkan sosok laki-laki. Dewasa ini, isu yang selalu ada dipikiran saya adalah bahwa perempuan saat ini berlomba-lomba untuk menjadikan diri mereka cantik sesuai dengan kontruksi baru yang diciptakan oleh media. Pikiran-pikiran mereka diracuni oleh wacana-wacana bahwa cantik harus berkulit putih, bertubuh tinggi, ramping, dan sebagainya. Dari sinilah ide gagasan saya muncul bahwa cantik itu tidak terbatas pada satu sudut pandang saja. Dalam berbagai kebudayaan, cantik bisa dimaknai secara bermacam-macam sesuai dengan kontruksi kebudayaan mereka. Hal inilah yang kemudian mendasari saya ingin menciptakan karya yang menyatukan banyak unsur budaya, bahwa setiap kebudayan memiliki pemaknaan sendiri atas kecantikan. Relativitas kebudayaan menunjukkan bahwa cantik itu tidak bisa diartikan secara general. Saya ingin menunjukkan bahwa ‘Beauty has truly no boundaries. Whoever you are and wherever you are come from, you are beautiful’. '
masalah: 'Saya mengangkat persoalan mengenai kontruksi tubuh perempuan dalam kebudayaan. Dalam studi tubuh sosial, tubuh merupakan produk budaya. Tubuh dibentuk, dikendalikan dan dimiliki secara koletif. Fokus yang saya ambil adalah pada penandaan atas tubuh perempuan sebagai hasil dari proses kultural. Misalnya tindik pada bayi perempuan. Hal tersebut dilakukan karena adanya pemaknaan kolektif bahwa bayi perempuan harus diberi ‘tanda’ untuk membedakannya dari bayi laki-laki. Ini menggambarkan bahwa tubuh menyimbolkan proses transisi sosial individu dalam masyarakat. Proses itulah yang menciptakan peranan-peranan baru dan tubuh baru bagi seorang individu. Pada hakikatnya tubuh fisik juga berperan sebagai tubuh sosial. Memang konstruksi pada setiap kebudayaan tidak selalu berhubungan dengan kecantikan, akan tetapi juga berhubungan dengan sistem religi, gender, politik, kelas sosial, dan sebagainya. Namun, disini saya melihat bahwa penandaan atas tubuh perempuan dalam berbagai kontruksi kebudayaan itu merupakan hal yang indah (estetis). Itulah mengapa saya mengangkat istilah ‘beauty’ atau keindahan sebagai pokok utama dalam karya saya. Selain itu saya menggunakan istilah ‘rekonstruksi’ adalah untuk menyatukan semua kontruksi-kontruksi atas tubuh perempuan dari banyak budaya menjadi satu. Saya melihat bahwa memaknai keindahan atas tubuh perempuan tidak bisa terbatas hanya pada satu sudut pandang saja. Setiap budaya memiliki kearifan lokalnya masing-masing dalam memaknai tubuh perempuan. '
durasi: 9 bulan
sukses: \r\nSaya rasa 80% adalah prosentase yang pas untuk menggambarkan indikator kesuksesan dalam proyek saya. Mengingat saya merupakan seorang yang baru saja terjun ke dalam dunia seni rupa, saya milihat proyek ini adalah sebuah tantangan besar yang harus saya lakukan. Mengawali iktikad baik dari sebuah mimpi untu melakukan pameran tunggal, saya rasa indikator kesuksesan proyek pameran ini adalah ketika banyak pihak yang mengapresiasi karya saya. Mulai dari seniman, media, ataupun dari kelompok-kelompok lain yang bukan berasal dari kalangan seni.
dana: '75'
---
