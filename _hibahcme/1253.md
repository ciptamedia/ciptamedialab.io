---
nomor: 1253
nama: febriana shinta sari S.Sos
foto:
  filename: foto.jpg
  type: image/jpeg
  size: 66410
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/7c50aced-d4c5-4b72-9684-46da7e0e28e0/foto.jpg
seni: audiovisual
pengalaman: 15 tahun
website: ' http://www.konde.co/2017/04/cerita-perempuan-di-selembar-kain-batik.html?m=1'
sosmed: ''
file:
  filename: FEBRIANA Features  HAK REPRODUKSI DIFABEL.mp3
  type: audio/mpeg
  size: 4651050
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/47533aa2-abfb-4339-abbc-d7873190768d/FEBRIANA%20Features%20%20HAK%20REPRODUKSI%20DIFABEL.mp3
title: Drama Radio Kehidupan Perempuan
lokasi: 'Yogyakarta dan Klaten, Jawa Tegah'
lokasi_id: ''
deskripsi: "Drama radio ini akan mengangkat kisah nyata tentang kehidupan lima perempuan  yang pernah saya tulis di media perempuan dan media berita radio.\r\n\r\nDrama ini akan bercerita tentang lima perempuan yang hidup di Jogja, Klaten (Jawa tengah), dan Jakarta. Antara lain bercerita tentang perempuan yang mengalami Kekerasan Dalam Rumah Tangga (KDRT), seorang anak perempuan yang bapaknya melakukan poligami, dan cerita perempuan dalam selembar kain batik.\r\n\r\nMasing - masing kisah akan dibuat satu cerita / drama radio  dengan dua bahasa, yaitu Bahasa Indonesia dan bahasa campur (Bahasa Indonesia dan Bahasa Jawa),  dan akan disiarkan dua radio di Jogjakarta. \r\n\r\n\r\n\r\n\r\n"
kategori: kerjasama_kolaborasi
latar_belakang: "Drama radio ini  berdasarkan kisah nyata kehidupan perempuan yang pernah saya tulis dan ditayangkan di media perempuan www.konde.co dan media online Kantor Berita Radio (KBR) www.kbr.id\r\n\r\nMengapa melalui drama atau sandiwara radio ?. Selama ini kita sangat jarang menemukan  program radio tentang perempuan melalui radio, apalagi dikemas dengan program sandiwara radio.  \r\n\r\nSelain itu saya ingin kembali menghidupkan sandiwara radio baik dalam bahasa Indonesia dan Jawa.\r\n"
masalah: "Masalah yang saya angkat adalah \r\n1. Perempuan korban Kekerasan Dalam Rumah Tangga (KDRT).\r\n2. Anak perempuan yang bapaknya melakukan poligami, \r\n3. Minimnya informasi kesehatan reproduksi bagai murid perempuan difabel. \r\n4. Kehidupan perempuan yang bekerja sebagai pembuat kain batik\r\n5. Perempuan penjaga Tradisi Endog Abang"
durasi: 9 bulan
sukses: "1. Adanya feedback pendengar di acara talks show radio dan setelah mendengarkan sandiwara diputar\r\n2. Pendengar terinspirasi ,tahu dan memahami hak asasi  perempuan\r\n3. Ditulis di media cetak dan online\r\n"
dana: '150'
submission_id: 5ab7c3ff1ebb0704ccceb6a4
---
