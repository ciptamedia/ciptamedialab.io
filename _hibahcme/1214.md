---
nomor: 1214
nama: NUNING ZAIDAH
foto:
  filename: FOTO NUNING.jpg
  type: image/jpeg
  size: 555755
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/ba80060c-fb4b-452a-8417-cfa3b594ae68/FOTO%20NUNING.jpg
seni: penelitian
pengalaman: 10 Tahun
website: >-
  http://www.koran-sindo.com/news.php?r=6&n=21&date=2016-01-15 dan
  http://jateng.tribunnews.com/2016/06/01/gadis-cantik-dari-jerman-ini-bermimpi-bisa-menikah-dengan-adat-jawa.
sosmed: >-
  http://berita.suaramerdeka.com/smcetak/tradisi-begalan-sarat-nilai-filosofis/.
  dan
  https://seminarpbjuns.files.wordpress.com/2016/02/prosiding-semnas-pbj.pdf.
file:
  filename: PAMERAN MODEL BUSANA PENGANTIN DAN TARI ANAK.jpg
  type: image/jpeg
  size: 2146266
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/30de81a4-777d-46a1-91bf-b5e7eb26a6b3/PAMERAN%20MODEL%20BUSANA%20PENGANTIN%20DAN%20TARI%20ANAK.jpg
title: 'Model  Perkawinan  Adat  Corak “Kamajaya-Kamaratih”  '
lokasi: Jawa Tengah
lokasi_id: Q3557
deskripsi: >-
  Penelusuran tentang upacara perkawinan adat corak “Kamajaya-Kamaratih” di Desa
  Onggosoro Borobudur Jawa Tengah,  sebagai salah satu genre  tata upacara
  perkawinan di Jawa Tengah. Penelitian melalui pendekatan historis,
  antropologi, fenomenologi dan ekspresi seni  dengan output seminar, peragaan 
  busana, tata upacara, buku model, publikasi dan  hak paten sebagai karya cipta
  akan keberadaan corak “Kamajaya-Kamaratih”. Penelitian di lakukan sebagai
  upaya menggali dan memunculkan corak baru sebagai inovasi akan keberagaman
  budaya khususnya pada upacara perkawinan adat di Jawa Tengah. 
kategori: riset_kajian_kuratorial
latar_belakang: >-
  Jawa Tengah jagad  reproduksi berbagai teritori kebudayaan, di wilayah inilah
  fakta, fiksi, mitos, kenyataan, peristiwa, sakral dan profan masih bersemuka,
  bersimpang bahkan bernegosiasi. Contoh adanya upacara perkawinan adat di
  Surakarta dan di Jogyakarta  yang memiliki stratifikasi sosial bersumber pada
  kraton sebagai pusat kebudayaan. Berbeda dengan ritual perkawinan adat
  masyarakat Onggosoro Borobudur yang masih teguh mempertimbangkan tradisi
  sebagai pembelaan integrasi seni, budaya, daya cipta, dan kebebasan
  berekpresi.  Desa Onggosoro berada diatas Gunung Menoreh 8 KM dari candi
  Borobudur Jawa Tengah, memiliki sanggar seni budaya membatik, menari, dan
  membuat perlengkapan upacara perkawinan. Corak tersebut memiliki keunikan
  pada  busana, tata urutan upacara, sesaji, bahasa dalam upacara perkawinan.
  Keunikan ditengarai sebagai ajaran budi pekerti perkawinan dari leluhurnya
  Mataram Kuno sebelum Perjanjian Giyanti yang memisahkan Surakarta dan
  Yogyakarta. “Kamajaya-Kamaratih” adalah mitos dewa-dewi cinta asmara yang
  divisualisasikan dalam ekpresi seni dan masih dipercayai hingga saat ini.
  Kepercayaan adanya Kamajaya-Kamaratih  menurut  pemangku budaya Sarjono (67th)
  adalah personafikasi dewa asmara yang masih hidup di alam lain dan manjing
  (masuk) dalam diri pengantin, menyebabkan prosesi upacara  berbeda dengan 
  prosesi pada umumnya. Prosesi perkawinan corak Kamajaya-kamaratih belum
  diketahui dan tersebar secara umum di wilayah sekitar karena keterbatasan
  sumber daya dalam mempublikasikan budaya yang dimiliknya serta hambatan
  geografis yang menjadi kendala. 
masalah: >-
  Penelusuran masalah yang menjadi latar belakang diatas menjadi penting, jika
  Jawa Tengah memiliki corak upacara perkawinan yaitu corak Surakarta dan
  Jogyakarta, sedangkan Desa Onggosoro Borobudur memiliki upacara corak
  “Kamajaya-Kamaratih”. Melalui  kajian yang komprehensip untuk menggali,
  mendredah, mengangkat dan mengembangkan keberadaan upacara perkawinan 
  tersebut sebagai salah satu genre upacara perkawinan adat  Jawa Tengah serta
  wujud pelestarian seni budaya Indonesia. 
durasi: 7 bulan
sukses: "Hasil cipta karya Model prosesi perkawinan  adat  corak “Kamajaya-Kamaratih” masyarakat Onggosoro Borobudur Jawa Tengah adalah terwujudnya  genre budaya asli yang diturunkan pada jaman Mataram Kuno sebelum  keberadaan budaya kraton  ada sebagai wujud keberagaman budaya Indonesia. Langkah yang harus di tempuh adalah  1). Membuat kajian tentang busana dan  tata upacara perkawinan serta pendokumentasian sebagai dasar model prosesi perkawinan  adat  corak “Kamajaya-Kamaratih” masyarakat Onggosoro Borobudur Jawa Tengah. 2). Membuat Buku Model sebagai petunjuk Prosesi Perkawinan  Adat  Corak “Kamajaya-Kamaratih” Jawa Tengah. 3).Seminar dan peragaan busana model prosesi perkawinan  adat  corak “Kamajaya-Kamaratih” masyarakat Onggosoro Borobudur Jawa Tengah untuk di sosialisasikan kepada HARPI (Himpunan Ahli Rias  Pengantin Indonesia)  Jawa Tengah dan PERMADANI (Persaudaraan Masyarakat Budaya Nasional Indonesia)  serta masyarakat stageholder. 4). Mengusulkan Hak Karya Cipta. 5). Publikasi melalui media dan jurnal ilmiah.  \r\n\r\n"
dana: '195.250'
submission_id: 5ab7b3f3bcf7ae09f4935d8a
---
