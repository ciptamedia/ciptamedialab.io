---
nomor: 1280
nama: Mega Lovrina
foto:
  filename: IMG20180225131140.jpg
  type: image/jpeg
  size: 4115226
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/28fa1a21-87a6-4dd3-9915-5baa92d224ab/IMG20180225131140.jpg
seni: kriya
pengalaman: 3 bulan
website: Potretnagari.blogspot.co.id
sosmed: 'Ig: therealmegalovrina atau oneupon.outfit'
file:
  filename: IMG20180308190325.jpg
  type: image/jpeg
  size: 3457126
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/c4e140f5-7824-431f-bfe0-3bad5f87e87f/IMG20180308190325.jpg
title: Perjalanan mengkaji pembuatan tapis di Negeri Katon
lokasi: 'Desa Negeri Katon, Pesawaran, Lampung'
lokasi_id: ''
deskripsi: >-
  Saya akan melakukan perjalanan ke Negeri Katon bersama satu teman perempuan
  saya. Kami akan tinggal di desan tersebut selama 4 hari untuk mengenal
  kebudayaan dan cara membuat tapis. Lalu,  akan saya komunikasikan kepada
  masyarakat luas melalui sosial media (secara tertulis dan audiovisual).
  Selanjutnya saya akan berkolaborasi dengan pengrajin Negeri Katon (yang
  mayoritas sudah senior)  untuk membuat 10 baju kombinasi tapis. Lalu akan saya
  pamerkan melalui sosial media serta membuat pameran sederhana. 
kategori: perjalanan
latar_belakang: >-
  Masyarakat Indonesia masih belum banyak mengenal tapis, khususnya generasi
  muda. Oleh sebab itu dengan adanya proyek ini, saya berharap masyarakat lebih
  mengenal tapis, dan anak muda lebih mencintai tapis.
masalah: >-
  Generasi muda masih belum banyak mengenal tapis Lampung. Oleh sebab itu,
  melalui video dan desain baju yang selaras dengan selera anak muda zaman
  sekarang saya berharap tapis dapat dikenal oleh masyarakat luas khususnya
  generasi muda. 
durasi: 1 bulan
sukses: 99%berhasil
dana: '11'
submission_id: 5ab7cd60b52f721260e25342
---
