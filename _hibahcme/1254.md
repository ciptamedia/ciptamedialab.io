---
nomor: 1254
nama: Sugar Nadia Azier
foto:
  filename: Screen Shot 2018-03-23 at 11.58.15 AM.png
  type: image/png
  size: 4489876
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/d9b1fa37-48ba-4021-b3a1-0fbb84fb05f2/Screen%20Shot%202018-03-23%20at%2011.58.15%20AM.png
seni: kuratorial
pengalaman: 6 tahun
website: 'https://www.youtube.com/watch?v=6UV0UBEZKB4'
sosmed: '@shoosugar'
file:
  filename: Image Porgram Book.jpeg
  type: image/jpeg
  size: 121139
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/47363efa-ab8b-4d9e-af5f-34b74b723342/Image%20Porgram%20Book.jpeg
title: Perempuan Dunia Madani
lokasi: DKI Jakarta
lokasi_id: ''
deskripsi: "Proyek ini merupakan program kuratorial khusus dalam bentuk pemutaran film dan diskusi. Program kuratorial ini ditempatkan sebagai bagian dari Madani Film Festival yang rencananya akan dilaksanakan pada bulan Oktober 2018. Madani Film Festival adalah festival film pertama di Indonesia yang khusus menampilkan film-film dari negara-negara Islam, atau yang bertemakan masyarakat Islam. Sebagaimana namanya, festival ini mendedikasikan diri untuk menunjukkan bahwa Islam bukanlah penanda pembeda yang memisahkan pemeluknya dari nafas bersama umat manusia di dunia.\r\n\r\nProgram ‘Perempuan Dunia Madani’ merupakan program khusus dalam Madani film festival yang mengkaji dan menampilkan isu masyarakat Madani dari sudut pengalaman perempuan. Proyek kuratorial ini akan menampilkan bagaimana isu dan situasi kaum perempuan (di dunia Islam) muncul dalam karya-karya filmmaker perempuan, serta juga menampilkan bagaimana perjuangan/kisah hidup karakter perempuan tertentu tergambarkan dalam film. Selain menayangkan film dari sutradara perempuan, dan film khusus tentang tokoh perempuan, program ini akan dilengkapi dengan satu sesi diskusi yang rencananya akan mengundang Aida Begic, sutradara perempuan asal Bosnia Herzegovina sebagai salah narasumber diskusi. Sejak karya pertamanya Snijeg, kisah perempuan dalam perang Bosnia yang memenangi penghagaan Critics Weeks Grand Prize di Cannes Film Festival 2008, Aida Begic terus membuat karya yang peka terhadap kekisahan dan sudut pandang perempuan. "
kategori: riset_kajian_kuratorial
latar_belakang: "Perempuan dalam Islam memiliki posisi yang penting sebagaimana digambarkan beragam kisah dalam Qur’an dan hadis. Hampir semua literatur Islam sepakat bahwa Islam menjunjung tinggi harkat dan martabat perempuan. \r\n\r\nPeran penting perempuan dalam sejarah Islam juga banyak tercatat. Dalam sejarah Islam, perempuan mengajar kaum laki-laki dalam hal keagamaan bukanlah hal aneh. Perempuan dalam sejarah Islam juga tercatat mendirikan perguruan tinggi pertama. Bahkan wanita Muslim juga banyak tercatat memainkan peran paling penting dalam kehidupan publik: menjadi kepala negara.\r\n\r\nNamun, pandangan ideal Islam soal keseimbangan antara perempuan dan laki-laki seringkali tidak terealisasi dalam dunia nyata. Pelanggaran HAM, pelecehan seksual, dominasi laki-laki, bahkan penyangkalan terhadap hak-hak dasar kaum perempuan masih banyak tercatat terjadi pada masyarakat dan komunitas Muslim. Kurangnya akses pendidikan, akses lapangan pekerjaan, serta kurangnya eksposur bagi para perempuan di negara-negara Islam, memiliki berbagai alasan dan sebab: sosial, geografis, politik maupun ekonomi. Tak ada jawaban mudah bagi situasi yang masih menyelimuti banyak kaum perempuan ini. Film dapat menjadi sebuah medium efektif untuk masuk ke dalam isu-isu ini. Keinginan untuk mengkaji, mediskusikan dan menampilkan perjuangan, harapan, ketakutan, dan keindahan hidup dari perempuan yang beragam dari seluruh dunia ini yang menjadi latar ide untuk proyek ini.\r\n"
masalah: "1. Isu perempuan hampir selalu berada di pusat perdebatan maupun pusat konflik. Namun lebih sering isu perempuan dibicarakan tidak dari sudut pandang perempuan itu sendiri. Program ’Perempuan Dunia Madani’ ingin melihat bagaimana pembicaraan tentang Islam, tentang masyarakat Madani ditampilkan dan dibicarakan sepenuhnya dari sudut pandang pengalaman perempuan.\r\n\r\n2.Kurangnya akses terhadap film-film karya atau tentang perempuan dari negara negara Islam. Kurasi film-film ’berkualitas’ tentang perempuan dari berbagai kebudayaan akan bisa menampilkan mozaik masalah terkini yang dihadapi kaum perempuan.\r\n\r\n3.Filmmaker perempuan memiliki cara pandang tersendiri dalam memotret masalah dalam masyarakatnya. Filmmaker seperti Aida Begic di Bosnia Herzegovina misalnya mulai berkarya dalam situasi negaranya yang tercabik peperangan, dan itu menjadikan karyanya peka terhadap penderitaan. Nadine Labaki di Lebanon juga lahir dan tumbuh di negara yang tercabik perang bertahun-tahun, namun ia memilih humor dan komedi untuk memotret nasib perempuan biasa dalam kompleksitas konflik politik. lalu juga ada Yasmin Ahmad di Malaysia yang harus bereaksi terhadap meningkatnya inklusifitas kaum muslimin dalam masyarakat multi etnis dan agama. Masih banyak nama lainnya seperti Samira Makhmalbaf dan lainnya. Film-film mereka, ditambah dengan film tentang kisah hidup tokoh perempuan seperti Malala Yosefzai dan Tjut Nya Dhien ingin diangkat menjadi bagian dari program ’Perempuan Dunia Madani’ ini.  "
durasi: 6 bulan
sukses: >-
  Diharapkan kurang lebih 2,000 penonton hadir dalam keseluruhan program ini.
  Menayangkan 5-10 Film bertemakan Perempuan Madani, Indikator sukses program
  ini adalah jika film, dan diskusi yang menyertainya menjadi topik hangat yang
  dibincangkan oleh media dan publik. Ukuran langsung yang bisa dijadikan acuan
  adalah dengan melihat dan menghitung jumlah terpaan media, pengikut di sosial
  media serta tagar yang berhubungan dengan program ini. Selain itu, diharapkan
  juga program ini memicu perbincangan lebih lanjut dan menghasilkan program
  atau karya yang mengangkat lebih banyak lagi isu perempuan khususnya di
  Indonesia. 
dana: '258'
submission_id: 5ab7c4d504d9e14ed064f0ef
---
