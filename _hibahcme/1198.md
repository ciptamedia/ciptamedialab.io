---
nomor: 1198
nama: intan ristarini
foto:
  filename: aoki profil for waft.jpg
  type: image/jpeg
  size: 836372
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/8566ac08-65ca-416a-99de-fcd0ea2d2765/aoki%20profil%20for%20waft.jpg
seni: seni_rupa
pengalaman: 7 tahun
website: 'www.rasvanaoki.,com / instagram : aokichan'
sosmed: >-
  instagram : @hyperallergic.sub / soundcloud: hyperallergicsub / youtube:
  hyperallergicsub 
file:
  filename: nusaswara pilgrim by hyperallergic.docx
  type: application/vnd.openxmlformats-officedocument.wordprocessingml.document
  size: 3309683
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/84036038-255a-4a2f-956c-e6f50983ce96/nusaswara%20pilgrim%20by%20hyperallergic.docx
title: NUSASWARA PILGRIM
lokasi: >-
  Jawa Timur (malang, Mojokerto, Banyuwangi, Ponorogo, Yogyakarta, Bandung,
  Bali.)
lokasi_id: Q3586
deskripsi: "Menelisik kembali peran penting wanita dalam kesenian tradisi, budaya lokal dan, praktik –praktik keilmuan tradisional. Kami merasa kontribusi kaum wanita dalam praktik seni dan terciptanya kebudayaan tradisional, menjadi pengingat bagi kita untuk melihat kembali esensinya dan berpikir ulang tentang isu eksistensi kaum wanita di dunia kesenian yang selalu di perbincangkan saat ini.Hyperallergic akan melakukan perjalanan di beberapa titik di Indonesia untuk berusaha menggali karakter musik tradisi beserta kultus spiritualitas terkait dengan fungsi musik pada pola-pola masyarakat tradisional, mencari kemungkinan kolaborasi dengan pelaku seni, penduduk lokal melalui praktik interdisipliner dan mandiri.Dalam pengarsipannya, dokumentasi, kolaborasi dan interaksi langsung dengan para pelaku kesenian merupakan inti dari proses perjalanan Nusaswara Pilgrim ini. Perjalanan ini melibatkan dan menelisik lebih dalam keterlibatan kaum wanita dalam proses pencarian karya kolaborasi. Perjalanan tur ini kami  membuka kemungkinan kolaborasi dengan berbagai macam praktisi seperti musisi, penyanyi,seniman visual, paranormal, pembuat instrumen musik, penari, petani. semua perjalanan akan didokumentasikan dalam karya jurnal ilustrasi, musik, video dan karya rupa. Semua dokumentasi akan dipublikasikan ke publik, dalam bentuk pameran atau showcase. \r\nHasil akhir dari proyek ini adalah pementasan karya kolaborasi, dengan melibatkan para kolaborator dalam perjalanan tur Nusaswara.Mengkonstruksi ruang interaktif yang hidup untuk saling berbagi pengalaman dan pengetahuan."
kategori: perjalanan
latar_belakang: "\r\n\tMenelisik kembali peran penting wanita dalam kesenian tradisi, budaya lokal dan, praktik –praktik keilmuan tradisional. Kami merasa kontribusi kaum wanita dalam praktik seni dan terciptanya kebudayaan tradisional, menjadi pengingat bagi kita untuk melihat kembali esensinya dan berpikir ulang tentang isu eksistensi kaum wanita di dunia kesenian yang selalu di perbincangkan saat ini.\r\n\r\nHyperallergic adalah sebuah proyek musik eksperimental dari surabaya, jawa timur, indonesia. Proyek yang digagas oleh sebuah kolektif seni bernama Waft Lab. Hyperallergic mengusung konsep spiritualitas dan meditatif dalam lantunan musik yang dibalut dengan eksplorasi ritmis etnik dalam performancenya. Proyek ini digawangi oleh Intan Rista Rini (aoki) dan Tuwis Yasinta (uncletwis). Laku eksplorasi dalam proyek ini terus dikembangkan seiring kesadaran para pelakunya akan keragaman budaya dan kekayaan musik tradisional di Indonesia.\r\n\r\n\tWaft Lab adalah inisiatif interdisipliner kolaboratif dari Surabaya. Berlandaskan semangat diy (do it yourself) melalui praktik seni, sains, teknologi dan budaya. Sejak 2011 waft lab telah memulai berbagai kegiatan seperti workshop, diskusi, pameran, festival, dll yang memiliki tujuan untuk menggali ide, eksplorasi gagasan, menyebarkan semangat berbagi dan membangun jaringan kolektif yang berkelanjutan.\r\n"
masalah: "- Menjelajahi kemungkinan kolaborasi antara kultus tradisional dengan musik eksperimental dengan tujuan untuk meregenerasi semangat kearifan lokal.\r\n- Mengingat peristiwa historis budaya dan kearifan lokal melalui pengarsipan baik berupa teks ataupun audio visual.\r\n- Mengingatkan kembali akan pentingnya peran wanita dalam pengembangan budaya.\r\n- Menjangkau informasi sekaligus mendistribusikan pengetahuan melalui penelitian dan pengarsipan lapangan dengan membuat ruang berbagi interaktif, untuk belajar, menggali, mengakali dan mengkaji praktik swakarya lintas disiplin.\r\n"
durasi: 9 bulan
sukses: "Mampu membawa kembali kesadaran masyarakat akan kekayaan kebudayaan tradisional dan kearifan lokal. Menimbulkan kebanggaan dan kecintaan terhadap budaya tradisional. Semua praktek kerja yang dilakukan tim Nusaswara Pilgrim mampu melibatkan banyak kolaborator dan keterlibatan banyak pihak, khususnya masyarakat lokal.\r\n"
dana: '210'
submission_id: 5ab7ab481ebb076838ceb6a4
---
