---
nomor: f-057
nama: Genevieve Duggan (Belorgey) untuk Ice Sarlince Tere Dara
foto:
  filename: f-057-duggan-tara.jpg
  type: image/jpeg
  size: 109,843
  url: /hibahcme/profile-pict/f-057-duggan-tara.jpg
seni: Kriya 
pengalaman: tenun 12 tahun, celup asli 6 tahun
website: tidak ada
sosmed: tidak ada
file: 
  filename: f-057-duggan-tara.jpg
  type: application
  size: 3524272
  url: /hibahcme/contoh-karya/f-057-duggan-tara.pdf
title: Pusat Tekstil Tradisional Tewuni Rai Sabu NTT
lokasi: Hawu Mehara, Mesara, Sabu NTT (85391)
lokasi_id: 
deskripsi: >-
  Membangun pusat tekstil dengan semua alat-alat dan kursus supaya berfungsi dengan baik. Kelompok Tenun Tewuni Rai belum pernah dibantu oleh pemerintah daerah walaupun beberapa proposal pernah dimasukkan pada tahun 2013 dan 2016   
kategori: Kerjasama/ Kolaborasi
latar_belakang: >-
  Proposal ini dibuat oleh Dr. Geneviève Duggan, antropolog dan spesialis budaya Savu, dan Ibu Ice Tede Dara, seorang guru sekolah menengah dan penenun terampil di pulau Savu. Yang menarik adalah fakta bahwa Geneviève Duggan dan Ice Tede Dara membawakan sebuah makalah di Konferensi TEXILE ASEAN di Chiang Mai pada bulan Januari 2016. (Lihat berkas terlampir).
  Sesudah akhir abad 19 desa Pedèro di Pulau Savu telah terkenal dengan keahlian penenunnya dan kualitas pewarna alami: nila untuk semua nuansa biru menjadi hitam, dan morinda atau mengkudu untuk warna merah. Raja local (Ama Doko Ju) membuka rumah tenun sekitar tahun 1890, tetapi sudah lama tidak ada lagi. Kelompok Penenun yang disebut Tewuni Rai atau 'Plasenta Bumi' di desa Pedèro, Kabupaten (Kabupaten) Savu Raijua, di Provinsi Nusa Tenggara Timur (NTT) dibentuk pada tahun 2006 dan bertujuan mentransmisikan pengetahuan budaya mereka yang unik dan teknik.
  Semua anggota Tewuni Rai adalah wanita yang tahu persis pola yang diciptakan oleh nenek moyang perempuan mereka. Tekstil yang diwarisi dari leluhur mereka secara tradisional disimpan di keranjang pusaka mereka. Para wanita mengetahui silsilah nenek moyang mereka dan pola yang terkait dengannya, dan mereka sangat berhati-hati dalam melestarikan pengetahuan ini. Pengetahuan sejarah yang unik ini, yang diteliti oleh antropolog Geneviève Duggan yang telah mempelajari tradisi tekstil di Indonesia selama hampir tiga dekade, tidak ditemukan di tempat lain di Indonesia atau Asia Tenggara.
  Tujuan Grup Penenun Tewuni Rai adalah untuk melestarikan pengetahuan luar biasa ini. Sayangnya, pihak berwenang setempat tampaknya tidak memprioritaskan dukungan untuk bagian penting dari budaya Savu ini. Penenun Tewuni Rai fokus pada melestarikan keterampilan dan pengetahuan tradisional, namun juga dalam menyesuaikan teknik dengan zaman modern tanpa melontarkan apa yang membuat budaya Savu begitu istimewa. Aspek halus dalam seni dan budaya ini sering kali terbengkalai. Tidak seperti penenun lainnya di pulau ini, penenun Tewuni Rai tidak menggunakan pewarna kimia dan tidak menghasilkan potongan dengan motif dekoratif murni; semua ciptaan baru terkait dengan budaya lokal. Mereka fokus pada tenunan berkualitas tinggi dengan pola yang bermakna. Demi budaya Savu dan pengakuan internasionalnya yang semakin meningkat, pengetahuan ini perlu dilestarikan dan kelompok penenun ini perlu didukung. Ina Raseuki, alias Ubiet, anggota dewan juri, mengatakan bahwa proyek Media Cipta Ekspresi memenuhi keinginannya untuk melihat wanita lokal memiliki akses dan dapat berpartisipasi dalam seni tradisional budaya mereka sendiri.
  Struktur Grup penenun Tewuni Rai
  Kelompok ini saat ini memiliki selusin anggota aktif dan selusin anggota pendukung atau pengikut. Kepala kelompok adalah anggota tertua dan memenuhi fungsi kehormatan: Bendelina Buki, alias Ina Nara. Pemimpin angkatan adalah Ice Tede Dara yang berusia 27 tahun, yang belajar ilmu ekonomi di Universitas Cendana di Kupang (Timor) dan sekarang menjadi guru sekolah menengah di desa tersebut, dan juga penenun terampil.
  Lokasinya
  Dusun Ledetadu di desa Pedèro masih memiliki pesona asli dengan bangunan kayu tradisional yang diikat dengan daun palem lontar. Kombinasi arsitektur warisan dan tekstil berkualitas tinggi menarik pengunjung lokal maupun mancanegara. Namun, penenun Tewuni Rai tidak memiliki bangunan yang memadai untuk kebutuhan mereka, seperti: rapat kelompok, penyimpanan bahan, pengunjung yang menerima, melakukan pencelupan dan demonstrasi menenun, dan menampilkan tekstil tenun siap dijual. Karena lokasinya, rumah penenun harus dibangun dari kayu dan dengan atap jerami menghormati tradisi lokal.
  Tujuan rumah penenun (rumah tenun [In] atau èmu mane [Sa])
  Rumah penenun akan menjadi bangunan balai atau pendopo yang terbuka dan tertutup, ditutup di salah satu ujungnya untuk menyimpan bahan. Ini akan memiliki banyak tujuan:
  1. Memegang rapat untuk memutuskan tugas dan strategi.
  2. Mendidik anggota dalam keterampilan bisnis seperti pembukuan.
  3. Mengajar anak-anak sekolah dan keterampilan menenun remaja sehingga pengetahuannya tidak tersesat.
  4. Mengajarkan tenunan bagaimana cara membuat aksesoris fashion, seperti tas, ikat pinggang dan perhiasan, dari kain yang rusak dan tidak bisa dimakan.
  5. Belajar bagaimana menggunakan mesin jahit untuk membuat asesoris.
  6. Menyimpan bahan yang dibutuhkan untuk menenun demonstrasi.
  7. Menyambut pengunjung dan tamu.
  8. Menampilkan tekstil dan produk untuk dijual.
  9. Memegang pemintalan, ikating, demonstrasi menenun untuk pengunjung, sekaligus memberi workshop kepada mereka yang ingin mengalaminya dengan menggunakan teknik ini.
  Yunirusia Riwu (alias Nebu) menghadiri Sekolah Tinggi Teknik di Kupang (SMK Busana) dan saat ini sedang magang di Jakarta untuk mengembangkan ketrampilannya membuat aksesoris tekstil. Setelah selesai magang, dia akan bisa mengajari para anggotanya bagaimana menggunakan mesin jahit dan untuk memotong bahan untuk membuat asesoris.
  Biaya yang terlibat:128 juta
  Pembangunan rumah penenun pada musim kemarau (Juni sampai Oktober 2018).
  Pembelian bahan untuk menyimpan dan menunjukkan karya penenun.
  Pembiayaan kelas yang diselenggarakan oleh Ibu Es Tede Dar  a dan Yunirusia Riwu.
  Pembelian 2 mesin jahit untuk digunakan oleh kelompok.
  Pembelian laptop untuk digunakan oleh Ibu Ice Dara untuk komunikasi dan akuntansi.
  Rincian untuk evaluasi biaya dapat ditemukan di lembar terpisah.
masalah: >-
  Kebutuhan penenun Tewuni Rai
  1. Penenun membutuhkan "infrastruktur sendiri, yang sebagiannya merupakan kebutuhan uang atau dana" seperti yang disebutkan dalam teks Cipta Media Ekspresi. Dalam hal ini, itu berarti bangunan untuk pertemuan, untuk menampilkan aktivitas dan pekerjaan, untuk menyimpan materi dan untuk mengajar generasi muda. (Untuk rinciannya, lihat di bawah 'Tujuan rumah penenun').
  2. Penenun membutuhkan bantuan keuangan untuk mempresentasikan karyanya di pameran di Indonesia dan luar negeri, serta mendidik masyarakat tentang arti pekerjaan mereka, karena mereka tidak dapat membiayai perjalanan melalui penjualan tekstil mereka. Sejauh ini, karyanya telah dipamerkan di pameran "Meet the Makers" di Jakarta dan di Singapura, dengan dukungan "Meet the Makers" dan bantuan keuangan dari Geneviève Duggan. Tewuni Rai tidak pernah mendapat dukungan dari Kabupaten Savu Raijua (Kabupaten) karena merupakan kelompok swasta yang tidak diciptakan oleh Departemen Kebudayaan. Koperasi ini diabaikan oleh pemerintah daerah, meski kreasi Tewuni Rai telah dipamerkan oleh museum, galeri dan kolektor swasta internasional.
durasi: Tujuh bulan
sukses: 100 persen
dana: '128'
---
