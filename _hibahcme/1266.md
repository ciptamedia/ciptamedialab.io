---
nomor: 1266
nama: Tisa Granicia
foto:
  filename: BCEBD28A-9B94-4401-8AEA-8A0671CBAA8A.jpeg
  type: image/jpeg
  size: 274530
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/003a1ab9-a6ab-4208-86eb-6306f8ac9e2c/BCEBD28A-9B94-4401-8AEA-8A0671CBAA8A.jpeg
seni: seni_rupa
pengalaman: 'Lebih dari 3 tahun '
website: 'http://tisagranicia.blogspot.co.id/'
sosmed: 'https://www.instagram.com/rapu___/'
file: null
title: 'Produk Rajutan dari Kantung Kresek Bekas '
lokasi: 'Sindangjaya, mandalajati Bandung '
lokasi_id: ''
deskripsi: "Merajut kantung plastik (kresek) bekas, memberdayakan ibu- ibu di lingkungan sekitar.\r\nMembuat produk dari kantung kresek bekas tersebut, membuka lapangan kerja bagi para perempuan yang tinggal di sekitar tempat saya tinggal. Menciptakan produk yang  ramah lingkungan, sustainable, mengurangi sampah plastik. "
kategori: kerjasama_kolaborasi
latar_belakang: >-
  diawali dari kegemaran Ibu saya sendiri mengumpulkan kantung kresek bekas
  untuk kemudian dirajut menjadi taplak., tadinya Ibu yang mengerjakan sendiri,
  sekarang Ibu sudah memiliki 2 asisten lepas. Adalah Ibu-ibu rumah tangga yang
  tinggal di kampung dekat tempat tinggal kami, mereka rajin bertandang ke rumah
  kami untuk belajar bagaimana mengolah kantung plastik bekas menjadi lembaran
  pita plastik panjang yang siap dirajut. Saya merasa hal ini punya keunikan
  tersendiri dan pasti potensial. Lalu saya memutuskan untuk membagi konsentrasi
  usaha keramik dengan juga mendesain tas tas kecil untuk bahan rajutan Ibu dan
  teman-temannya, lalu saya mulai membuat brand nya, dan menjual kecil- kecilan
  di craft market di Bandung. Satu mimpi saya dan Ibu, suatu saat nanti pekerja
  rajutan ini semakin banyak, dan dari hasil merajut ini dapat menaikkan taraf
  hidup mereka, kelak.
masalah: "\r\nSeperti yang telah diketahui bersama bahwa limbah berupa kresek adalah limbah yang berbahaya, dan butuh waktu yang sangat sangat lama, untuk kantung plastik yang memiliki tebal sepersekian milimeter butuh waktu 1000 tahun untuk dapat benar benar terurai, belum lagi platik yang berwarna hitam ternyata sangat sangat berbahaya bagi kesehatan jika digunakan langsung sebagai pembungkus makanan. Badan Pengawas Obat-obatan dan Makanan RI (POM) telah mengeluarkan \"Peringatan Publik/Public Warning\" Nomor: KH.00.02.1.55.2890 tentang Kantong Plastik \"Kresek\", di dalam suratnya itu telah disebutkan penyebab bahayanya kantung kresek hitam, serta larangan untuk memakainya sebagai pembungkus makanan.* Dari situ saya hanya membayangkan, mungkin saya bisa memperpanjang hidup si kantung kresek hitam sambil menunggu dirinya terurai.\r\n"
durasi: 6 bulan
sukses: "Jika produk bisa dijual di dalam dan di luar negeri secara berkesinambungan. \r\nBerhasil menjual produk tersebut secara on-line, dan juga off line, dikenal secara lokal dan internasional. Bisa menghidupi banyak ibu rumahtangga yang tinggal di sekitar rumah saya. "
dana: '50'
submission_id: 5ab7c74fb52f723b3ee2533e
---
