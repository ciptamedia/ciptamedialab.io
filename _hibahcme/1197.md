---
nomor: 1197
nama: Shinta Febriany S
foto:
  filename: IMG_0353.JPG
  type: image/jpeg
  size: 865901
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/e4627254-fb29-4a00-971b-e21bd27ce377/IMG_0353.JPG
seni: seni_pertunjukan
pengalaman: 20 TAHUN
website: 'https://www.youtube.com/watch?v=3k0EMNm6dJQ'
sosmed: Instagram kalateater. Facebook kala teater.  Twitter @kalateater
file:
  filename: IMG_0975.JPG
  type: image/jpeg
  size: 732880
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/84c10140-07e4-4f41-88da-ac0c1494a71e/IMG_0975.JPG
title: Pertunjukan Keliling Kota dalam Teater
lokasi: 'Palu, Manado, Jakarta, Bali'
lokasi_id: ''
deskripsi: >-
  Kota dalam Teater mengangkat isu-isu kota yang dihadapi Makassar, yakni
  reklamasi pantai, meningkatnya jumlah orang gila, dan aksi bunuh diri yang
  terjadi . Saya ingin melakukan pertunjukan keliling Kota dalam Teater ke
  kota-kota yang mengalami isu yang sama yang dihadapi oleh Makassar. Ada 3
  pertunjukan yang berlangsung secara berturut-turut di 3 titik lokasi yang
  berbeda.
kategori: perjalanan
latar_belakang: "Inisiasi proyek Kota dalam Teater dimunculkan di tahun 2015 setelah mengamati gejala minimnya partisipasi warga kota terhadap persoalan-persoalan yang dihadapi kota tempatnya bermukim. Partisipasi warga terhadap kota dapat diwujudkan salah satunya dengan cara menyatakan pandangannya atas proses pembangunan kehidupan kota yang tidak sehat yang berpeluang merusak ekosistem hidup bersama. \r\nProyek Kota dalam Teater merupakan proyek pembacaan isu-isu kota melalui riset terhadap warga kota. Warga kota dari berbagai latar belakang mengungkapkan persepsi mereka perihal persoalan yang dihadapi kota di mana mereka bermukim. Data dari riset terhadap warga kota tersebut dielaborasi dan diwujudkan ke atas panggung teater. Proyek ini juga berkolaborasi dengan penyair-penyair asal Makassar. Puisi-puisi mereka tentang Makassar dihadirkan untuk melihat pandangan puitik warga terhadap kotanya. Proyek Kota dalam Teater adalah proyek jangka panjang yang direncanakan berlangsung selama 10 tahun, dimulai tahun 2015 dan berakhir pada tahun 2025. \r\nTahun 2015 dipentaskan 3 pertunjukan yang berdasar pada 3 isu utama yang dihadapi kota Makassar, yakni kemacetan lalu lintas, sampah dan baliho, serta banjir yang terjadi setiap tahun. Tahun 2017 dipentaskan pula 3 pertunjukan yang berangkat dari 3 isu utama yang dihadapi kota Makassar, yaitu reklamasi pantai Losari, meningkatnya jumlah orang gila, dan aksi bunuh diri yang terjadi.\r\n"
masalah: >-
  Kota dalam Teater mengangkat isu-isu yang dialami kota Makassar yaitu
  reklamasi pantai Losari, meningkatnya jumlah orang gila, dan aksi bunuh diri
  yang terjadi. Isu-isu ini diangkat berdasarkan riset terhadap warga kota
  Makassar perihal pandangan mereka atas kota Makassar.
durasi: 1 bulan
sukses: "1.\tPenonton tergerak untuk menyatakan pandangannya terhadap isu-isu yang dihadapi kota yang ditinggalinya. \r\n2.\tPenonton menyadari pentingnya partisipasi warga kota atas berbagai persoalan yang dihadapi kota di mana ia bermukim.\r\n"
dana: '150'
submission_id: 5ab7aae91ebb0758ffceb6a4
---
