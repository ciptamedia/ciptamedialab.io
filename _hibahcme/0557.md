---
permalink: /hibahcme/557
nomor: 557
nama: SITI BADRIYAH, S.SN, M.HUM
foto:
  filename: Screenshot_2018-03-15-22-07-29.png
  type: image/png
  size: 495729
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/6ee3178f-e060-4fab-bc2f-de65bf496b97/Screenshot_2018-03-15-22-07-29.png
seni: seni_rupa
pengalaman: 2 Tahun
website: ''
sosmed: ''
file:
  filename: contoh karya siti badriyah.pdf
  type: application/pdf
  size: 2684719
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/078283d7-9841-4904-b03c-38f6be48f719/contoh%20karya%20siti%20badriyah.pdf
title: Pemberdayaan Potensi Tenun Lurik Pedan
lokasi: Pedan, Troketon, Klaten, Jawa Tengah
lokasi_id: ''
deskripsi: 'proyek ini merupakan cara untuk memberikan koneksi antara pengrajin tenun Pedan dengan pihak hotel yang membutuhkan karya seni tentang hasil olahan limbah tenun lurik Pedan. kegiatan dilakukan dengan cara memberikan pelatihan dan meregenerasi skill ibu-ibu dan wanita-wanita muda di sana untuk bisa melestarikan tenun lurik Pedan. '
kategori: riset_kajian_kuratorial
latar_belakang: 'proyek ini didasari atas kondisi tenun lurik Pedan yang hampir punah, hal ini terbukti dengan hanya tinggal dua perusahaan tenun lurik asli Pedan yang masih bertahan. hal ini menimbulkan rasa prihatin dan semangat saya sebagai orang Pedan untuk ikut melestarikan tenun lurik Pedan melalui proyek ini. '
masalah: 'ibu-ibu dan wanita-wanita muda dilatih secara regeneratif melalui inovasi desain art work yang dibutuhkan owner (hotel di public space). hasil produk inovasi merupakan wujud promosi potensi lokal sebagai upaya mengkonstruksikan masyarakat Indonesia untuk cinta produk tradisional Indonesia. '
durasi: 9 bulan
sukses: 'proyek ini akan sukses jika pelatihan dapat dilakukan dengan baik sehingga menghasilkan karya-karya yang inovatif dan kersama dengan hotel dapat terjalin secara berkelanjutan. '
dana: '250'
---
