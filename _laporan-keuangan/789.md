---
nomor: 789
auth_name: rena asyari
nama: Rena Amalika Asyari
proyek: Pendokumentasian Perempuan Intelektual Sunda Lasminingrat
date: '2018-12-11'
kode: A3
anggaran: Transportasi
reimburse: 'no'
pelakuTransaksi: ''
title: Biaya penggantian peminjaman mobil untuk transport ke Sumedang+Garut
biaya: '2000000'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5c7b303bbc704862472abf0d
submitter_id: 5b7a13abb4d41cc74c71a4e31f808aec
nota: "/uploads/nota/789.JPG"
---
