---
nomor: 1275
auth_name: Wawa ghotix
nama: Wawa Saptarini
proyek: Pasar Tari Dua Ribu Lima Ratus
date: '2018-12-10'
kode: C1
anggaran: Konsumsi
reimburse: 'yes'
pelakuTransaksi: Anis Harliani Kencana Ekaputri
title: Jagung Pedas, Berlian Guling Keju, Berlian Bidaran Manis, Kelinci Rengginang
  Udang, dll
biaya: '91800'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5cd786949289e5d1692e7f33
submitter_id: 7c5a361e2a7014ce85b6f517e2c61b09
nota: "/uploads/nota/1275.jpg"
---
