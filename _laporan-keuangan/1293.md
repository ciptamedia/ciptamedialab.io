---
nomor: 1293
auth_name: Wawa ghotix
nama: Wawa Saptarini
proyek: Pasar Tari Dua Ribu Lima Ratus
date: '2018-12-22'
kode: C1
anggaran: Konsumsi
reimburse: 'yes'
pelakuTransaksi: Anis Harliani Kencana Ekaputri
title: Ayam Bakar Halilintar, Ayam Goreng Kremes, Ayam Bakar Granat, Ayam Bakar Madu,
  dll
biaya: '175000'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5cd792cd5ea074073af76b78
submitter_id: 7c5a361e2a7014ce85b6f517e2c61b09
nota: "/uploads/nota/1293.jpg"
---
