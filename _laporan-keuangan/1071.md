---
nomor: 1071
auth_name: Anggia nasution
nama: Lia Anggia Nasution
proyek: Penelitian Sejarah Pers Perempuan Sumatra Utara
date: '2019-03-10'
kode: G1
anggaran: honor
reimburse: 'yes'
pelakuTransaksi: Desi P
title: Honor editor penulisan jurnal ilmiah
biaya: '1000000'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5cbb0a5922f5eea950e10c61
submitter_id: 3ccd51155510c8223fb0801d2e447a36
nota: "/uploads/nota/1071.jpg"
---
