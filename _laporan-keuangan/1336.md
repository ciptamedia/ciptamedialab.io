---
nomor: 1336
auth_name: sartika sari
nama: Sartika Sari
proyek: Puisi dan Gerakan Perempuan
date: '2018-11-05'
kode: E4
anggaran: Cetak Buku
reimburse: 'no'
pelakuTransaksi: ''
title: Cetak Buku 260 halaman, 200 eks, bookpaper 72 gram, softcover
biaya: '10500000'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5cde7f4681cbb2d8dab120eb
submitter_id: c40ee4217a399fd78175049f5af6adf6
nota: "/uploads/nota/1336.jpg"
---
