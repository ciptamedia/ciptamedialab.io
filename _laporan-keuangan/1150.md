---
nomor: 1150
auth_name: Relaksasi Beragama
nama: Anastha Eka
proyek: Bukan Perawan Maria
date: '2018-10-27'
kode: E2
anggaran: Infrastruktur Pameran
reimburse: 'yes'
pelakuTransaksi: Riyana Rizki
title: Alat-alat bangunan. Kabel, parang, solder, timah, dll.
biaya: '241300'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5cbd880d9e4dea1ded6d66d3
submitter_id: 6b12a0ee097b7051ac6663896c433059
nota: "/uploads/nota/1150.jpg"
---
