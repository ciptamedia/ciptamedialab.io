---
nomor: 132
auth_name: Raisa Kamila
nama: Raisa Kamila
proyek: 'Merekam Ingatan Perempuan: Kumpulan Cerita Pendek'
date: '2018-08-06'
kode: D3
anggaran: Konsumsi peserta
reimburse: 'yes'
pelakuTransaksi: Amanatia Junda, Armadani
title: 10 Juni 2018, Konsumsi rapat persiapan workshop pra riset di rumah Dani, Kotagede
biaya: '127000'
tanggalpelunasan: ''
submission_id: 5bec63c98849b46c8a2ff69d
submitter_id: a0bb14f96c641fdd6ab06b73b89c4cca
nota: "/uploads/nota/132.jpg"
---
