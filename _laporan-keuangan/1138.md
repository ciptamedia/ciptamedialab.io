---
nomor: 1138
auth_name: Anggia nasution
nama: Lia Anggia Nasution
proyek: Penelitian Sejarah Pers Perempuan Sumatra Utara
date: '2019-03-06'
kode: X1
anggaran: konsumsi
reimburse: 'yes'
pelakuTransaksi: Bu Minah Catering
title: kue kotak untuk diskusi hasil penelitian
biaya: '240000'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5cbd4cd74b21664b2084c18e
submitter_id: 3ccd51155510c8223fb0801d2e447a36
nota: "/uploads/nota/1138.jpg"
---
