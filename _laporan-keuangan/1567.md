---
nomor: 1567
submission_id: miqfuhz10y
auth_name: ''
nama: Raisa Kamila
proyek: 'Merekam Ingatan Perempuan: Kumpulan Cerita Pendek'
date: '2018-07-15'
kode: E2
anggaran: Honor riset pustaka
reimburse: 'no'
pelakuTransaksi: ''
title: Honor riset pustaka untuk Astuti Kilwouw
biaya: '300000'
tanggalpelunasan: ''
upload_nota: >-
  https://gitlab.com/ciptamedia-upload/upload/uploads/464ed9f2a08bc56b0228f5065562d62b/honor_riset_pustaka_astuti-done.jpeg
upload_notapelunasan: ''
submitter_id: a0bb14f96c641fdd6ab06b73b89c4cca
---
