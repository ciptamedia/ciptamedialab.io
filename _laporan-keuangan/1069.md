---
nomor: 1069
auth_name: Anggia nasution
nama: Lia Anggia Nasution
proyek: Penelitian Sejarah Pers Perempuan Sumatra Utara
date: '2018-12-10'
kode: F1
anggaran: Pengadaan bahan penelitian
reimburse: 'yes'
pelakuTransaksi: PUSIS UNIMED
title: Pembelian literatur tambahan
biaya: '853000'
tanggalpelunasan: ''
referrer: https://www.ciptamedia.or.id/formkeuangan
submission_id: 5cbb05b4f1ed7976ea63b303
submitter_id: 3ccd51155510c8223fb0801d2e447a36
nota: "/uploads/nota/1069.jpg"
---
