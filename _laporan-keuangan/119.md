---
nomor: 119
auth_name: Raisa Kamila
nama: Raisa Kamila
proyek: 'Merekam Ingatan Perempuan: Kumpulan Cerita Pendek'
date: '2018-06-04'
kode: A1
anggaran: Transportasi panitia
reimburse: 'yes'
pelakuTransaksi: Amanatia Junda
title: Transportasi ke Sanggar Larik-Lurik, Pakem
biaya: '17000'
tanggalpelunasan: ''
submission_id: 5bec4b44875d7d2791c827ac
submitter_id: a0bb14f96c641fdd6ab06b73b89c4cca
nota: "/uploads/nota/119.png"
---
