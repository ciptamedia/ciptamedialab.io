---
nomor: 50
nama: Nanik Endarti
proyek: Aku Perempuan Unik
title: Shooting Video Aktivitas
date: '2018-09-29'
jam: 13.00-16.00
lokasi: Langgeng Art Foundation
alamat: Jl Suryodiningratan Yogyakarta
tujuan: Pengambilan gambar untuk bahan konten pertunjukan
img:
  filename: shooting 2.jpg
  type: image/jpeg
  size: 1507305
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/665d8368-a40c-48c1-90ce-b0904e7874da/shooting%202.jpg
img_caption: Persiapan sebelum take video
img2:
  filename: kordinasi sebelum take.jpg
  type: image/jpeg
  size: 1567014
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/f273d67e-6da4-4188-a8b1-33c84fc87f7c/kordinasi%20sebelum%20take.jpg
img2_caption: Kordinasi sebelum take video
hadir: "Nanik Indarti\r\nShinta Kusuma\r\nNunung Deni Puspitasari\r\nHanung\r\nAndy\r\nGrace\r\nGandez\r\nCitra Pratiwi\r\nJarot Pracahyaadi"
hasil: Video bahan materi pertunjukan
evaluasi: ''
rekomendasi: ''
categories: laporan
submission_id: 5c20539037c213bb569b71d0
---
