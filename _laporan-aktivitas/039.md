---
nomor: 39
nama: Nanik Endarti
proyek: Aku Perempuan Unik
title: Mengintip Rumah Ninit Ungu
date: '2018-08-16'
jam: 10.00-17.00
lokasi: rumah tinggal
alamat: Jalan Monjali Yogyakarta
tujuan: 'Mengetahui  sisi kehidupan seorang Ninit Ungu '
img:
  filename: IMG_20180816_172403.jpg
  type: image/jpeg
  size: 1105377
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/f2226d09-b340-4ac3-b294-d2fafd463896/IMG_20180816_172403.jpg
img_caption: Foto bersama Ninit Ungu sebagai narasumber
img2: null
img2_caption: ''
hadir: "Nanik Indarti\r\nGandez Sholikah\r\nNinit Ungu\r\nMailani Sumelang"
hasil: >-
  mengetahui kebiasaan si narasumber ketika berinteraksi di lingkungan tempat
  tinggalnya
evaluasi: >-
  kegiatan ini sangat menyenangkan apalagi ketika perempuan bertemu dan ngobrol
  dengan sesama perempuan
rekomendasi: tidak ada
categories: laporan
submission_id: 5c1a1e7c567db8a3640e59b5
---
