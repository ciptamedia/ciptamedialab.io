---
nomor: 111
nama: Ata Ratu
proyek: Perempuan Sumba dan Musik Tradisional
title: 'Rekaman, Kolaborasi, Riset Musik Tradisional'
date: '2018-06-18'
jam: ''
lokasi: Rumah Tinggal
alamat: Desa Palanggay
tujuan: "The second collaboration in Desa Palanggay involved recording additional Rice Harvest songs with a different group of five female singers selected by Ata Ratu and based in Desa Palanggay. A series of songs was recorded in the traditional call response format (two groups of two singers alternating) Ata Ratu performed a well known Sumbanese popular song “Eri Rambu Balu” accompanied by the accapella female group and to close the event two male dancers accompanied the singers with a traditional Rice harvest dance used to separate the grains from the chaff after the harvest (Ludu Parinna/Lagu Injak Padi).\r\n\r\nKolaborasi kedua di Desa Palanggay melibatkan perekaman lagu panen padi dengan kelompok yang berbeda dari lima penyanyi wanita yang dipilih oleh Ata Ratu dan berbasis di Desa Palanggay. Serangkaian lagu direkam dalam format respons panggilan tradisional (dua kelompok yang terdiri dari dua penyanyi bergantian) Ata Ratu membawakan lagu populer Sumba \"Eri Rambu Balo\" yang diiringi oleh grup wanita accapella dan untuk menutup acara, dua penari pria mengiringi penyanyi dengan tarian panen padi tradisional digunakan untuk memisahkan biji-bijian dari sekam setelah panen (Ludu Parinna / Lagu Injak Padi).\r\n"
img:
  filename: tim palanggay 2.jpg
  type: image/jpeg
  size: 1918161
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/c2696b41-ba00-41e2-a3c4-805e8a7108c7/tim%20palanggay%202.jpg
img_caption: Perempuan yang rekaman Lagu Lagu Panen
img2:
  filename: palanggay 2 tim_small.jpg
  type: image/jpeg
  size: 1038826
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/1742f53a-5598-48e5-a3b3-186a7f54131d/palanggay%202%20tim_small.jpg
img2_caption: ''
hadir: "Ata Ratu\r\nJoseph Lamont\r\nPerempuan Perempuan Senior dari Desa Palanggay"
hasil: "Dokumentasi, Kolaborasi dan riset tentang Lagu Panen\r\nhttps://www.youtube.com/watch?v=d0zygvIj1iw"
evaluasi: ''
rekomendasi: ''
categories: laporan
referrer: 'https://www.ciptamedia.or.id/formaktivitas'
submission_id: 5c6fe67847690df8899d1cc3
---
