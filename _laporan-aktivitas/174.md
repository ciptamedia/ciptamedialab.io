---
nomor: 174
submission_id: k7j8ppejio9
nama: Raisa Kamila
proyek: 'Merekam Ingatan Perempuan: Kumpulan Cerita Pendek'
title: Ruhaeni Intan Riset Pustaka di Yogyakarta
date: '2018-08-06'
jam: 10:00 - 16:00
lokasi: Ruhaeni Intan Riset Pustaka di Yogyakarta
alamat: Jalan Malioboro No. 175, Sosromenduran, Gedong Tengen, Kota Yogyakarta, Daerah
  Istimewa Yogyakarta 55271
tujuan: Mencari tahu peristiwa yang terjadi di Jawa Tengah pada waktu menjelang, saat,
  dan sesudah reformasi 1998.
img_caption: Potongan berita tentang pasar murah di Semarang menjelang Mei 1998.
img2_caption: ''
hadir: Ruhaeni Intan
hasil: Memperoleh informasi tentang adanya pasar murah serentak di berbagai tempat
  di kota Semarang yang diadakan menjelang Mei 1998.
evaluasi: Informasi yang didapatkan sangat berharga karena menjadi salah satu ide
  dalam cerpen yang saya kerjakan untuk proyek ini.
rekomendasi: Sudah cukup.
categories: laporan
submitter_id: a0bb14f96c641fdd6ab06b73b89c4cca
img: "/uploads/img/174.jpeg"
---
