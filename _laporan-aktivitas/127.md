---
nomor: 127
nama: Sonia Piscayanti
proyek: 11 Ibu 11 Panggung 11 Kisah
title: >-
  Pementasan Teater Dokumenter, Publikasi dan Dokumentasi, Focus Group
  Discussion
date: '2018-07-24'
jam: 19.00-21.00 WITA
lokasi: Rumah tinggal
alamat: Jalan Pantai Penimbangan Gang Balbo no F8 Singaraja
tujuan: >-
  Mementaskan kisah hidup seorang Ibu dari 11 Ibu yaitu Bu Erna sehingga cerita
  nyata yang dikemas dalam pentas teater bisa menginspirasi perempuan lainnya.
  setelah pementasan diadakan diskusi, evaluasi dan refleksi. Penonton juga
  memberikan respon teater berupa tulisan, ataupun gambar visual tentang
  pementasan. Dalam proses tersebut seluruh upaya dokumentasi dilakukan.
  Publikasi di media juga dilakukan secara online maupun offline. Setelah pentas
  1, dilanjutkan dengan pentas kedua dan seterusnya hingga pentas ke 11 dan
  seluruh rangkaian ini ditutup pada acara Focus Group Discussion pada tanggal
  30 Desember 2018.  
img:
  filename: pentas bu erna.jpg
  type: image/jpeg
  size: 199723
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/a7d95657-44d7-48cc-afc3-d80696734b7e/pentas%20bu%20erna.jpg
img_caption: Pentas Ibu Erna Dewi dalam pentas 11 Ibu 11 Panggung 11 Kisah
img2: null
img2_caption: poster pementasan pertama
hadir: "Putu Yogi Agustia Pratama\r\nMega Brahwija\r\nWidya Kusuma\r\nPutu Kerti Nitiasih\r\nKadek Sonia Piscayanti\r\nHerlin Pramesti Susanti\r\nAyu Dasrini\r\nTini Wahyuni\r\nWatik\r\nHermawati\r\nYanti Pusparini\r\nMeilani\r\nWiwik Widayanti\r\nEndang Hari Rizki\r\nPuji Astuti\r\nAstry\r\nArlingga\r\nAndi\r\nDayu Istri\r\nArpin Rarekual\r\nLily Suprianti\r\nLaba Jayanta\r\nMami Sisca\r\nRika Swandewi\r\nKartina Dewi\r\nRatna\r\nIstacy Rosree\r\nNyoman Ari Purnaya\r\nAgodha\r\nManik Sukadana\r\nTantri\r\nMade Adnyana Ole\r\nEka Prasetya \r\nLilik Surya Aryani\r\nAgus Wiratama\r\nYanti Pusparini\r\nKetut Simpen\r\n\r\n\r\n"
hasil: >-
  11 Pementasan berlangsung sukses dengan hasil positif. Adanya iklim berproses
  dengan baik di antara 11 ibu, dimulai dengan belajar mendengar, belajar
  mengapresiasi dan menghargai orang lain. Juga pembelajaran lain adalah bahwa
  dengan teater dokumenter ini para Ibu menjadi lebih ringan bercerita, lebih
  merasa dihargai dan diapresiasi. Apalagi mereka bukanlah aktor. Tantangan
  berteater menjadi terapi bagi beban psikologis para Ibu. Dari sudut saya
  sebagai sutradara dan penulis naskah, bagi saya project ini berhasil karena
  dalam kondisi yang menantang, dinamis dan status sosial beragam, project ini
  terjadi dan hadir memberi alternatif baru di dunia teater. Keragaman para Ibu
  termasuk kondisi fisik dan psikologis yang tak sama, bahkan ada yang tuna
  rungu dan wicara, justru mempersatukan.  Dari segi budaya, latar belakang 11
  Ibu juga beragam; ada Bali, Jawa, China dan ada juga campuran di antaranya.
  Hal ini membuat rasa kebangsaan menebal. Project ini adalah model mini tentang
  pluralisme yang bersatu dalam harmoni.  Sedangkan hasil dari segi penonton,
  karena setiap usai pementasan saya mengumpulkan respon, dari sana saya
  mendapat masukan bahwa pentas teater kami dihargai dan memberi inspirasi. 
evaluasi: >-
  Evaluasi terjadi untuk memastikan bahwa setiap proses adalah pembelajaran
  untuk membuat kita lebih baik dalam hal pengelolaan manajemen produksi,
  manajemen audiens, manajemen publikasi dan dokumentasi. Semua itu kita
  diskusikan secara santai namun serius demi upaya perbaikan ke depan. Hasil
  evaluasi menunjukkan bahwa proses teater dokumenter harus terus dilanjutkan
  dan dikembangkan.
rekomendasi: >-
  Diperlukan sebuah riset untuk mengembangkan teater dokumenter di Indonesia.
  Saya sudah memulainya, namun jika dirancang penelitian lebih lanjut maka
  project ini pasti akan meningkat kualitasnya. 
categories: laporan
referrer: 'https://www.ciptamedia.or.id/formaktivitas'
submission_id: 5c77e46a2a6928fa8983e659
---
