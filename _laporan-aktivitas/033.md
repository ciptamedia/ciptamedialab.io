---
nomor: 33
nama: Nanik Endarti
proyek: Aku Perempuan Unik
title: Meeting bersama Flying Balloons Puppet
date: '2018-07-14'
jam: 15.00-18.00
lokasi: Studio Flying Balloons Puppet
alamat: Sembungan Bantul
tujuan: Pada kegiatan ini kami membicarakan gambaran konsep pertunjukan
img:
  filename: nanik & Fbp.jpg
  type: image/jpeg
  size: 578265
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/5697953d-a394-4aca-8b34-9e69ba344e0d/nanik%20&%20Fbp.jpg
img_caption: Senang sekali mendapatkan banyak peluang gagasan dalam pertemuan ini
img2: null
img2_caption: ''
hadir: "Nanik Indarti\r\nRangga\r\nMeyda Bestari\r\nCumin\r\nGandez Sholikah"
hasil: 'Gambaran konsep pertunjukan dalam karya '
evaluasi: >-
  Kesulitan menentukan waktu untuk bertemu bersama karena kesibukan komunitas 
  ini
rekomendasi: Membutuhkan waktu yang intens untuk membicarakan banyak konten
categories: laporan
submission_id: 5c1a028c6b3758770b155064
---
