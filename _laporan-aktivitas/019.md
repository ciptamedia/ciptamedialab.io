---
nomor: 19
nama: Sanchia Hamidjaja
proyek: Seragam Merah Jambu
title: Mulai Proses Inking
date: '2018-08-21'
jam: '9:00-17:00'
lokasi: Studio
alamat: 'Jl. Ciputat Raya no. 5, Pondok Pinang Lt 2'
tujuan: >-
  Setelah porses sketsa final berakhir, dilanjutkan dengan proses inking yang
  berlangsung hingga Januari 2018 
img:
  filename: inking.jpg
  type: image/jpeg
  size: 626757
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/f3649de4-bfce-4905-8229-d7e1ea9108d7/inking.jpg
img_caption: Beberapa contoh halaman yang sudah di inking
img2: null
img2_caption: ''
hadir: "Sanchia hamidjaja\r\nBobby Satya"
hasil: >-
  Proses inking dilakukan secara manual, lalu akan di warnai secara digital
  untuk mempercepat waktu.
evaluasi: ''
rekomendasi: ''
categories: laporan
submission_id: 5be2c2c8e1e4d73a5630bca6
---
