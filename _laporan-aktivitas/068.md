---
nomor: 68
nama: Sri Harti
proyek: Wanita Kusumayuda
title: Studi literer
date: '2019-01-04'
jam: 12.00-14.00
lokasi: Perpustakaan Prodi Pedalangan Fakultas Seni Pertunjukan ISI Ska
alamat: 'Jl ki Hajar Dewantoro no 19, Kentingan, Jebres Surakarta'
tujuan: >-
  Mencari naskah pakeliran (naskah pertunjukan wayang) sebagai sumber acuan
  untuk penyusunan naskah "Wanita Kusumayuda" dan mencari sumber literatur
  karakter tokoh2 perempuan dalam pewayangan.
img:
  filename: 20190104_120707.jpg
  type: image/jpeg
  size: 3079879
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/d1cc5068-3f3a-45b3-aeb4-4389042b4120/20190104_120707.jpg
img_caption: Menarik kesimpulan dan mencatat karakter tokoh Srikandhi
img2:
  filename: 20190104_120720.jpg
  type: image/jpeg
  size: 2753645
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/3eb660f5-cd14-4f90-9062-076ff3e25928/20190104_120720.jpg
img2_caption: Mencari sumber pustaka yg menjelaskan karakter tokoh Srikandhi
hadir: "Ika Laksmiwati (pustakawati)\r\nDwi Adi Nugroho\r\nEko Prasetyo\r\nKevin meinandoval\r\n"
hasil: >-
  Dari kegiatan ini ditemukan penebalan karakter tokoh Srikandhi dan
  Mustakaweni.
evaluasi: >-
  Tidak ditemukan buku atau naskah pertunjukan wayang berjudul Mbangun Candi
  Saptaharga dan Srikandhi Mustakaweni  di perpustakaan ini. Di mana kedua judul
  itu sebagai bingkai lakon dalam penyusunan naskah wanita kusumayuda. Sumber
  dari you tube banyak ditemukan dalang yg menyajikan kedua lakon tersebut,
  namun blum ada yg menulisnya menjadi sebuah naskah atau buku yg bisa dibaca
  oleh umum. Naskah yg dibuat dalang biasanya untuk koleksi pribadi saja. Begitu
  banyak buku yg mengupas tokoh  Srikandhi, namun untuk tokoh Mustakaweni tak
  ada yg mengupasnya, padahal tokoh ini pun punya fenomena yg menarik untuk
  ditampilkan
rekomendasi: >-
  Saya bersyukur mendapatkan hibah ntuk menyusun naskah wanita kusumayuda. Saya
  berharap naskah ini nanti bisa menambah koleksi naskah2 pakeliran di
  perpustakaan prodi Pedalangan, sehingga mahasiswa di Fakultas Seni pertunjukan
  terkhusus prodi pedalangan bisa mendapatkan sumber acuan dalam penggarapan
  karya terkhusus dalam penyusunan Tugas Akhir. Beberapa kali di sela2 jam
  mengajar saya sering berkunjung ke perpustakaan ini. Hal-hal penting sering sy
  temukan demi terselesaikannya proyek ini.
categories: laporan
submission_id: 5c335ba8a4f6070a9050ffe7
---
