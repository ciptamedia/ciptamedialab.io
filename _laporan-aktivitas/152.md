---
nomor: 152
nama: Mona Sylviana
proyek: Narasi Selembar Kain
title: Pengamatan Aktivitas Menenun
date: '2018-07-05'
jam: '10:00 - 15:00'
lokasi: Rumah para penenun
alamat: Beberapa Desa di Saumlaki
tujuan: Mengetahui detil aktivitas para penenun.
img:
  filename: 4 (1).JPG
  type: image/jpeg
  size: 492094
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/bfc97c72-4f84-4aac-bd19-b5d7d35b7c45/4%20(1).JPG
img_caption: 'Mama Tina di Desa Tumbur, Saumlaki'
img2:
  filename: 4 (2).JPG
  type: image/jpeg
  size: 486599
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/dab4e4e5-2e59-4dea-a723-d71c0368f779/4%20(2).JPG
img2_caption: Penenun di Desa Tumbur
hadir: "Mona Sylviana\r\nPara penenun"
hasil: Menambah beberapa detil mengenai proses menenun.
evaluasi: ''
rekomendasi: ''
categories: laporan
referrer: 'https://www.ciptamedia.or.id/formaktivitas'
submission_id: 5ccf2f2b5047547ff73b038b
---
