---
nomor: 29
nama: Rena Amalika Asyari
proyek: Pendokumentasian Perempuan Intelektual Sunda Lasminingrat
title: Wawancara Narasumber
date: '2018-12-11'
jam: 11.00-18.00
lokasi: 'Rumah Tinggal, Garut'
alamat: Jl. Proklamasi Garut
tujuan: 'Menggali data lebih jauh dari Narasumber tentang Raden Ayu Lasminingrat. '
img:
  filename: IMG_20181211_151915.jpg
  type: image/jpeg
  size: 2040054
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/cfc4e72e-fc9c-4bc1-b404-b089c071c45e/IMG_20181211_151915.jpg
img_caption: 'Foto urutan dari kiri (asisten peneliti, narasumber, peneliti,informan)'
img2: null
img2_caption: ''
hadir: "Rena Amalika Asyari\r\nDyah Murwaningrum (asisten peneliti)\r\nDeddy Effendie Tp.M.Hs (narasumber)\r\nDicky Lukmana (informan)"
hasil: Mendapatkan data yang tidak ada pada sumber pustaka
evaluasi: >-
  Saat itu cuaca kurang mendukung (hujan deras) sehingga waktu untuk berkunjung
  sangat terbatas. Narasumber kedua tidak berhasil ditemui karena ada acara
  mendadak.
rekomendasi: >-
  Harus menyiapkan waktu lebih lama lagi untuk ke observasi lapangan, karena
  situasi di lapangan tidak bisa diprediksi. Harus mempertimbangkan kondisi
  kesehatan narasumber agar wawancara dapat berjalan lancar. 
categories: laporan
submission_id: 5c1839979a82048b35e35af0
---
