---
nomor: 154
nama: Rhidian Yasminta Wasaraka
proyek: Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai
title: Bedah buku di Museum Lokabudaya Uncen Jayapura
date: '2019-02-16'
jam: '15:00-18:OO'
lokasi: Museum Lokabudaya Uncen -abepura-jayapura
alamat: Jln Raya Abe-sentani -Jayapura Papua
tujuan: Mempublikasikan dan melihat respon dari publik tentang buku Perempuan Perkasa
img:
  filename: WhatsApp Image 2019-03-16 at 20.23.36.jpeg
  type: image/jpeg
  size: 124204
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/67133461-563e-4f78-8af8-70c99cd8e7e0/WhatsApp%20Image%202019-03-16%20at%2020.23.36.jpeg
img_caption: 'berphoto bersama moderator, penanggap dan undangan'
img2: null
img2_caption: ''
hadir: "Erni Wais,\r\n Anna Baransano,\r\n Fien Jarangga\r\nDaniel Randongkir,\r\nVanwi subiat, \r\nSteven rumagit, \r\nKelly Mandosir \r\ndan 25 orang yang lain"
hasil: >-
  Kegiatan ini menarik minat banyak pihak, namun disaat yang bersamaan Jayapura
  terjadi banjir bandang, hingga yang datang hanya 25 orang dari 50 undangan
  yang ditargetkan. Namun mereka yang datang adalah para aktivis perempuan,
  antropolog, wartawan dan mereka sangat tertarik dengan isu ini. Diakhir
  kegiatan ibu Erni Weis selaku moderator mengatakan bahwa kedepan mungkin
  Korowai bisa menjadi semacam lab yang nanti ya semua orang bisa belajar disana
evaluasi: >-
  Bencana alam tidak bisa dicegah, dan makanan yang sudah dipesan tak mungkin
  dibatalkan, maka cara untuk menyiasatinya adalah dengan tetap menempatkan
  photo-photo dari hasil penelitian didalam ruang museum Lokabudaya Unce sampai
  sekarang 
rekomendasi: >-
  Banyak yang meminta agar mencetak ulang buku perempuan perkasa, semoga bisa
  ada donor yang mau biayai ini
categories: laporan
referrer: 'https://www.ciptamedia.or.id/formaktivitas'
submission_id: 5cd3a426efed8b7637338c5d
---
