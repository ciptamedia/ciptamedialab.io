---
nomor: 13
nama: Sanchia Hamidjaja
proyek: Seragam Merah Jambu
title: Pertemuan dengan narasumber
date: '2018-06-26'
jam: '10:00-17:00'
lokasi: 'Solong Cafe, Ulee Kareng'
alamat: 'Jl.T. Iskandar no. 13 Ulee Kareng, Banda Aceh'
tujuan: >-
  Memahami sejarah konflik Aceh, sesuai dengan latar belakang cerita di balik
  grafis novel
img:
  filename: Kak Matan.jpg
  type: image/jpeg
  size: 256492
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/1819f7fa-85a1-4242-a9d4-d44d1c395175/Kak%20Matan.jpg
img_caption: 'Interview dengan narasumber Kak Matan, seorang mantan kombatan'
img2: null
img2_caption: ''
hadir: "Ezra Silitonga\r\nIbu Matan\r\nBasrie (NGO)"
hasil: >-
  Lebih memahami sejarah konflik dari prespektif kombatan, yang akan di
  refleksikan dalam penulisan naskah Jilid 2 dari novel grafis. 
evaluasi: >-
  Dari pertemuan-pertemuan dengan narasumber-narasumber, saya merasa naskah dan
  plot cerita dalam novel saya akan berubah cukup drastis, menjadi lebih baik,
  dan lebih detil,
rekomendasi: ''
categories: laporan
submission_id: 5be2afc072b7f93734ae0011
---
