---
nomor: 45
nama: Nanik Endarti
proyek: Aku Perempuan Unik
title: Take Video Testimoni
date: '2018-09-29'
jam: 19.00-21.00
lokasi: Gatak Art Space
alamat: Jalan Parangtritis
tujuan: ' Rukman Roasadi adalah seorang dosen teater di ISI Yogyakarta. Selain sebagai dosen, ia pendiri komunitas teater SAC Yogyakarta. Penggagas juga terlibat dalam komunitas tersebut.  Pada kegiatan ini, penggagas bertujuan untuk  menghasilkan video sebagai bahan materi pertunjukan. Selain itu, ngobrol-ngobrol kepada beliau mengenai kesan beliau  sebagai dosen yang mengajar Muchlis Mustafa dan Didik Saputro sebagai mahasiswanya. '
img:
  filename: DSC09596.JPG
  type: image/jpeg
  size: 981024
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/eebf27d0-dbc6-4806-b277-9fe083ebc6fb/DSC09596.JPG
img_caption: Penggagas bersama Rukman Rosadi
img2:
  filename: DSC09599.JPG
  type: image/jpeg
  size: 1911787
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/37282ea2-6d53-4608-a540-5a8244742375/DSC09599.JPG
img2_caption: Penggagas sedang menjelaskan konsep karya
hadir: "Nanik Indarti\r\nRukman Rosadi\r\nShinta Kusuma\r\n"
hasil: Kegiatan ini menghasilkan materi berupa video sebagai bahan konten pertunjukan
evaluasi: ''
rekomendasi: ''
categories: laporan
submission_id: 5c1da50a9d7cb8c1e9535e13
---
