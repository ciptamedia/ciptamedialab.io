---
nomor: 180
submission_id: gcl72qwt7y
nama: Raisa Kamila
proyek: 'Merekam Ingatan Perempuan: Kumpulan Cerita Pendek'
title: Maria Margareth R.F
date: '2019-03-21'
jam: 15:00-19:00
lokasi: Rumah tinggal
alamat: Gang Wuluh No.21C Papringan, Depok, Sleman
tujuan: 'Mengevaluasi penulisan cerpen. Memastikan lagi kesesuaian data dan alur cerita.
  Memastikan logika kalimat dan diksi sesuai dengan kaidah yang berlaku. '
img_caption: Editan pertama dari editor
img2_caption: ''
hadir: Maria Margareth R.F
hasil: Segala aspek tekstual dan konstekstual bisa dipastikan pemakaiannya dengan
  tepat dalam cerpen
evaluasi: Komunikasi dengan editor agak tersendat karena kesibukan masing-masing
rekomendasi: Lebih detil membicarakan proses editing ini dengan editor
categories: laporan
submitter_id: 5137e006199e775e8fdf6b96596e57e5
img: "/uploads/img/180.jpg"
---
