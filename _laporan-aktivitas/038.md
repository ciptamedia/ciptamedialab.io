---
nomor: 38
nama: Nanik Endarti
proyek: Aku Perempuan Unik
title: Shooting di gereja
date: '2018-10-01'
jam: 08.00-13.00
lokasi: Gereja Aletheia
alamat: Jalan Magelang
tujuan: Pengambilan video dokumenter dan testimoni
img:
  filename: 7.jpg
  type: image/jpeg
  size: 950880
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/e18ff22e-2621-4c73-a62c-d8543bf9320f/7.jpg
img_caption: testimoni bersama Ninit ungu
img2:
  filename: 8.jpg
  type: image/jpeg
  size: 1072825
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/0d330872-827f-446b-9ff6-c3522f3257d1/8.jpg
img2_caption: Shooting
hadir: "Nanik  Indarti\r\nNinit Ungu\r\nShinta Kusuma\r\nkaryawan 1\r\nkaryawan 2\r\nSatpam"
hasil: Video dan foto kegiatan
evaluasi: Pengambilan gambar berjalan lancar tanpa hambatan apapun
rekomendasi: keterbatasan alat kamera yang tidak dimiliki si penggagas
categories: laporan
submission_id: 5c1a1cb3f1c2ae62d4ebca88
---
