---
nomor: 157
nama: Sartika Sari
proyek: Puisi dan Gerakan Perempuan
title: Pengumpulan data
date: '2018-05-25'
jam: 09.00-16.00
lokasi: Perpustakaan Nasional Republik Indonesia
alamat: 'Jalan Salemba Raya 28A, Jakarta Pusat'
tujuan: >-
  Mencari, mengumpulkan, dan menggandakan surat kabar yang terbit di Sumatra
  Utara Tahun 1990-an sampao 1940-an
img:
  filename: IMG20180525110422.jpg
  type: image/jpeg
  size: 1759556
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/20b2f38e-c3fa-4d2f-ae6c-c2ecaaf8aec0/IMG20180525110422.jpg
img_caption: Pengecekan katalog surat kabar
img2:
  filename: IMG20180525120929.jpg
  type: image/jpeg
  size: 1509444
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/f898d283-cf0c-44ff-9099-361b8619620d/IMG20180525120929.jpg
img2_caption: Bersama Yuliana Sari
hadir: "Sartika Sari\r\nYuliana Sari"
hasil: >-
  Surat kabar yang terbit di Sumatra Utara Tahun 1990-an sampao 1940-an yang
  sudah digandakan
evaluasi: >-
  Masih banyak koran yang tidak ditemukan (padahal di katalog tersedia). Selain
  itu, karena sudah banyak koran lama yang rusak sehingga tidak bisa dijangkau
  pembaca, dan sayangnya belum didigitalisasi, maka pencarian data tidak dapat
  menjangkau semua data yang ingin direncanakan.
rekomendasi: >-
  Pencarian data di perpustakaan lain, yang saya tahu, Perpustakaan di Leiden,
  misalnya.
categories: laporan
referrer: 'https://www.ciptamedia.or.id/formaktivitas'
submission_id: 5cf49f53548e6d992da25035
---
