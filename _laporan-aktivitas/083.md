---
nomor: 83
nama: Nanik Indarti
proyek: Aku Perempuan Unik
title: Presentasi karya pentas Sepatu Yang Sama kisah Jiwa dan Angka
date: '2018-11-16'
jam: 19.00-22.00
lokasi: Pendhapa Art Space
alamat: Ring Road Selatan
tujuan: Presentasi
img:
  filename: _DSC5308.jpg
  type: image/jpeg
  size: 1007305
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/02cb7b5c-e8fd-4bf3-b4ff-a8e9c76da7bb/_DSC5308.jpg
img_caption: bagian Pertunjukan
img2: null
img2_caption: Bagian akhir pertunjukan
hadir: "Marhaban_mua\r\nMathori Brilyan\r\nBroto Wijayanto\r\nDaniel Nainggolan\r\nDwi Handoko Putro\r\nYunita\r\nFebrinawan Prestianto\r\nTiara Arianggi\r\nDesi Puspitasari\r\nUtroq Trieha\r\nAyu Permata Sari\r\nGhalib\r\nMaulana Mas\r\nAzka\r\nVandy Rizaldi\r\nYoga Tri Anjar W.\r\nFitri Andono Warih\r\nRahman Yaasin Hadi\r\nRisao Pambudi\r\nNurul Jamilah\r\nTotok\r\nFahlan\r\nIke\r\nIntan Meliana N.\r\nNur Chorimah\r\nHandayani Pertiwi\r\nPutri Desyanawati\r\nSyakirina Rahmatuzahra Utami\r\nShafira Rahmatunnisa Utami\r\nAleng Yanti\r\nAsita\r\nLusi\r\nGirra\r\nAirul\r\nSyahirul Alim\r\nHeribertus Diwyacitra\r\nSaskia\r\nMarieska\r\nOni Anugra Roszita\r\nByta Indrawati\r\nLita Pauh Indrajaya\r\nYopi Hendrawan\r\nTedy\r\nBrigitta Isabella\r\nOyi \r\nIrna Nj\r\nBirgita Yuniarti\r\nAgnes\r\nKarel\r\nAstri Adzhani\r\nJamaluddin Latif\r\nArini Rachmatika\r\nViola Alexsandra Putri \r\nEka Sri Hayati\r\nAzis Nurzaen\r\nRadinda Nabila\r\nAmalia Rachma Annisa\r\nSofie Andre Yefana Bachren K.\r\nRiska Ayu Wulandari\r\nDian\r\nInayatul Khoiroh\r\nHenricus Benny Hendriono\r\nEka Nusa Pertiwi\r\nPrasetya Yudha DS\r\nYuni Catur Putri\r\nDhani Nur Permadi\r\nShinta Amelia\r\nSekar Ayu Maharani\r\nFebriyanti Pratiwi\r\nLusi Kurniawati\r\nHenokh Sean Christiawan\r\nBanyu Bening\r\nKristanto\r\nAnggit Dwi Prasetyo\r\nBinti Wasingatul Sya'adah\r\nBinti Dewi M.\r\nBagus Ariyanto\r\nElnani Yuliana \r\nHayati Mar Atussholihah\r\nRatna Siyem\r\nElinda\r\nBasudewa\r\nPulung\r\nKamil\r\nD Dhyah Permonosari\r\nJuna Hagana Sembiring Meliala\r\nGraciela Glori Nadanta\r\nTimothy Tri Wahyu Jati\r\nAmalia Rizqi F.\r\nMega Dwi Afriyani\r\nAyu Christine\r\nOni\r\nRahmafany M.A.\r\nClara Sinta\r\nChandra Nilasari\r\nDinda Bekti Nugraheni\r\nPenny Sylvania\r\nSulis\r\nTitin\r\nGradhina Melya\r\nLaksito Aji\r\nMuhammad Dinu Imansyah\r\nThomas Bima Dita Megantara\r\nHariyanto\r\nIndah Ayu Fitria\r\nWahono Simbah\r\n"
hasil: Presentasi
evaluasi: >-
  Pementasan ini merupakan karya inovatif yang mengkolaborasikan lintas
  disiplin seni
rekomendasi: ''
categories: laporan
submission_id: 5c417e6d16d97b5f63203697
---
