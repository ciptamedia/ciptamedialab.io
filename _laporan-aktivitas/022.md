---
nomor: 22
nama: Sanchia Hamidjaja
proyek: Seragam Merah Jambu
title: Program Magang Slot ll
date: '2018-10-02'
jam: '10:00-17:00'
lokasi: Studio
alamat: Jl.Ciputat Raya no.5 Pondok Pinang
tujuan: >-
  Membuka kegiatan internship / magang bagi mahasiswa ilustrasi / seni rupa,
  untuk membantu mempercepat proses yang pada umumnya akan memakan waktu jauh
  lebih lama. Yang sekaligus membagi pengalaman proses berkarya. 
img:
  filename: Intern2.jpg
  type: image/jpeg
  size: 111247
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/9e63d0cb-737c-4fbd-8e5a-9a7880d1d29c/Intern2.jpg
img_caption: Kehadiran intern ll Arifa RIzka Utami
img2:
  filename: intern 2b.jpg
  type: image/jpeg
  size: 69451
  url: >-
    https://353a23c500dde3b2ad58-c49fe7e7355d384845270f4a7a0a7aa1.ssl.cf2.rackcdn.com/23fda382-0d26-423b-9e1b-080fd0663729/intern%202b.jpg
img2_caption: Bobby dan Arifa saling diskusi pekerjaan masing2
hadir: "Sanchia Hamidjaja\r\nArifa Utami\r\nBobby Satya\r\n\r\n"
hasil: >-
  Sangat membantu mempercepat proses pengerjaan proyek,  sekaligus membagi
  pengalaman proses berkarya. Program magang slot ke 2 berlangsung sampai
  Desember 2018
evaluasi: ''
rekomendasi: ''
categories: laporan
submission_id: 5be2d4b0875d7d6c06a3a8e0
---
