---
title: Tutorial Tata Cara Sholat Menggunakan Augmented Reality
date: 2014-04-08 11:08:00 +07:00
nomor: 203
foto: /static/img/hibahcms/203.png
nama: Aditya Zulkarnain
lokasi: Malang, Jawa Timur
organisasi: NA
judul: Tutorial Tata Cara Sholat Menggunakan Augmented Reality
durasi: 8 Bulan
target: Siswa TK dan SD di daerah Malang Raya
sukses: Banyaknya pengguna yang menggunakan aplikasi ini kira2 4000 orang dalam
  3 bulan
tipe: Aplikasi mobile interaktif
strategi: |-
  Google playstore

  Promosi produk ke jejaring sosial
  Promosi ke sekolah TK dan SD
  Youtube
  Dari mulut ke mulut
kuantitas: Mendesign semenarik mungkin user interface dari aplikasi tersebut. Merancang
  tampilan yang sederhana tetapi interaktif sehingga mudah dipahami oleh pengguna
  baru sekalipun
dana: Rp. 250 Juta
deskripsi: Aplikasi ini dijalanka dengan platform android. Konten yang terdapat didalamnya
  berisi tata cara solat, tata cara wudu, doa setelah solat.
masalah: Untuk membuat aplikasi tutorial solat yang menarik dan interaktif.
solusi: Dengan memanfaatkan tim yang ada sesuai job deskripsi masing2 anggota. Anggota
  sudah dipilih sesuai kemampuan keahlian masing2.
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
