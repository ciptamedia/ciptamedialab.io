---
title: PPMDes – Pusat Pengaduan Masyarakat Desa
date: 2014-04-08 11:08:00 +07:00
nomor: 188
foto: /static/img/hibahcms/188.png
nama: Kristian Poto
lokasi: Kupang, NTT
organisasi: Komunitas Peduli Masyarakat Pulau Semau
judul: PPMDes - Pusat Pengaduan Masyarakat Desa
durasi: 24 Bulan
target: masyarakat Pulau Semau
sukses: masyarakat penggun seluler akan melakukan share penyelesaian masalah
  melalui HP, kemduian mencari solusi melalui tim yang ada
tipe: akan dibuat brosur dan dibagikan kepada masyarakat
strategi: membagikan melalui kantor desa, pemuda gereja, siswa-siswa SMA dan komunitas
  yang ada
kuantitas: SMS sebanyak 1000 nomor HP/hari
dana: Rp. 25 Juta
deskripsi: PPMDes adalah komunitas yang merupakan kelompok inisiatif masyarakat untuk
  melihat berbagai persoalan sosial dan ketidak adilan di desa, agar masyarakat bisa
  menyampaikan aspirasi dengan bebas, yang bertujuan untuk menyelesaikan persoalan
  yang ada dengan melihat prinsip musyawarah untuk mufakat, kebebasan menyampaikan
  pendapat dengan melihat kembali kearifan lokal masarakat
masalah: penggunaan ADD di desa, pemerintah tidak transparan kepada masyarakat, dan
  perencanaan yang tidak berbasis masyarakat
solusi: membangun komunikasi dengan pemerintah desa, jika ada penyelewengan akan di
  adukan melalui ombudsman
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
