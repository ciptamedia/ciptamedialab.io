---
title: Platform Aplikasi Berbasis SMS
date: 2014-02-14 11:08:00 +07:00
nomor: '075'
foto: /static/img/hibahcms/075.png
nama: Anton Akhiar
lokasi: Tangerang, Banten
organisasi: NA
judul: Platform Aplikasi Berbasis SMS
durasi: 12 Bulan
target: Masyarakat yang belum mengenal koneksi data dan masih bergantung pada SMS
  diseluruh Indonesia
sukses: Digunakan oleh 1000 orang dalam waktu 3 bulan.
tipe: Server yang meng-host aplikasi/content, aplikasi web untuk penyedia content,
  dan SMS gateway untuk menerima request dari user.
strategi: |-
  Menawarkan platform ini kepada penyedia content dan bekerjasama dengan penyedia content untuk mempromosikan content mereka.

  Dengan memperbanyak jumlah penyedia content. Semakin banyak penyedia content maka akan semakin bermanfaat platform ini, dan semakin banyak pula masukan untuk perbaikan.
kuantitas: NA
dana: Rp. 500 Juta
deskripsi: Platform ini mirip Google Play pada Android atau App Store pada iPhone,
  hanya saja menggunakan SMS sebagai medianya.
masalah: Masih banyak segmen masyarakat yang masih mengandalkan SMS dan belum bisa
  menikmati koneksi data. Dengan demikian tidak bisa menggunakan berbagai aplikasi
  yang dapat menyampaikan content yang bermanfaat.
solusi: Solusinya adalah dengan membuat platform berbasis SMS untuk mendistribusikan
  aplikasi. Platform ini mirip Google Play pada smartphone Android, atau App Store
  pada iPhone.
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
