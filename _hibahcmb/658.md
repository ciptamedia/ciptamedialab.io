---
title: Virus Menulis di Sekolah
date: 2011-09-16 11:08:00 +07:00
nomor: 658
foto: /static/img/hibahcmb/658.jpg
nama: Ficky Ubaidillah
lokasi: Jawa Tengah/Kudus
dana: 150 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: NOVEMBER 2011 – OKTOBER 2012
deskripsi: Ini merupakan sebuah proyek untuk menumbuhkan minat dan daya kretifitas
  para pelajar sekolah di bidang menulis. Dibentuk dalam wadah komunitas menulis di
  masing-masing sekolah, para pelajar tersebut akan menerbitkan sebuah produk majalah.
  Berisi tentang kondisi keseharian yang dekat dengan mereka, tentang cita-cita dan
  keinginan para pelajar.
masalah: Selama ini media-media yang ada (khususnya di Kabupaten Kudus) tidak memiliki
  ruang khusus untuk komunitas pelajar atau remaja. Padahal dunia remaja adalah dunia
  yang dinamis yang juga rentan akan pengaruh-pengaruh dari luar.
solusi: |-
  Memberikan pelatihan menulis dan jurnalistik, yang selanjutnya akan diterbitkan dalam majalah di sekolah masing-masing. Di dalamnya akan berisi tentang kondisi keseharian yang dekat dengan mereka dan komunitasnya yang akan mengasah nalar kritis dan kesadaran mereka atas informasi yang berkembang

  Yang diuntungkan:
  Target pelajar di 10 Sekolah Menengah Atas di Kabupaten Kudus
sukses: |-
  Terbentuknya jaringan lingkar media yang berisi komunitas majalah sekolah.
  Para pelajar mampu berpikir kritis sejak dini, sadar media, dan mampu menyuarakan aspirasi mereka melalui media tulisan.
organisasi: Lingkar Media
target: memberikan pelatihan menulis dan jurnalistik, yang selanjutnya akan diterbitkan
  dalam majalah di sekolah masing-masing. Di dalamnya akan berisi tentang kondisi
  keseharian yang dekat dengan mereka dan komunitasnya yang akan mengasah nalar kritis
  dan kesadaran mereka atas informasi yang berkembang.
---


### {{ page.nohibah }} - {{ page.title }}

---
