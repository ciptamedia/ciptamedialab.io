---
title: Visualisasi Informasi Lalulintas Berdasarkan Informasi Berbagi dari Masyarakat
  dengan menggunakan Semantic Web Teknologi
date: 2011-09-16 11:08:00 +07:00
nomor: 177
foto: /static/img/hibahcmb/177.jpg
nama: Handri Santoso
lokasi: Depok, Jawa Barat
dana: Rp 346.120.000,00
topik: Meretas batas - kebhinekaan bermedia
durasi: Januari - Desember 2012
deskripsi: Januari - Desember 2012
masalah: Sebuah proyek visualisasi informasi dari text ke peta dengan menggunakan
  semantic web technology untuk mengatasi kelemahan pada penyampaian informasi lalu
  lintas terkini melalui jaringan sosial, radio atau layanan SMS
solusi: Informasi mengenai kondisi lalu lintas seringkali tidak tersedia atau tidak
  dimengerti oleh penggunanya
sukses: Meningkatnya partisipasi masyarakat untuk berbagi informasi bagi kepentingan
  bersama, serta sistem informasi ini sudah terintegrasi dengan layanan darurat
target: Masyarakat pengguna jalan, institusi seperti kepolisian dan dinas jalan
  raya, dan pemerintah
organisasi: STKIP Surya
---


### {{ page.nohibah }} - {{ page.title }}

---
