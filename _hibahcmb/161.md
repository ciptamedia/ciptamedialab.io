---
title: Jurnalis Warga Cerdas Membangun NKRI
date: 2011-09-16 11:08:00 +07:00
nomor: 161
foto: /static/img/hibahcmb/161.jpg
nama: I Wayan Adi Aryanta
lokasi: Denpasar, Bali
dana: 1, 5 Miliar Rupiah
topik: Meretas batas - kebhinekaan bermedia
durasi: Januari - Juni 2012
deskripsi: Sebuah proyek yang bertujuan untuk mewadahi buah pikiran para anak-anak
  bangsa yang akan dipublikasikan dalam sebuah media teknologi informasi atau internet
  yang saat ini terbukti sanggup 'mempersatukan dunia'. Jurnalis Warga berharap bisa
  membina para akademisi, praktisi dan ahli dibidangnya masing-masing guna menjadi
  jurnalis warga yang cerdas sehingga mereka bisa menyampaikan buah pikirannya dalam
  sebuah artikel yang nantinya akan dipublikasikan di www.jurnaliswarga.com. Diharapkan
  buah pikiran mereka bisa menjadi masukan bagi penyelenggara bangsa dan negara, demi
  kemajuan NKRI tercinta
masalah: Buah pikiran jutaan pemikir handal, akademisi, praktisi, dan para ahli yang
  tersebar di seluruh pelosok Nusantara terkadang sulit untuk dibagikan dan disampaikan
  kepada penyelenggara bangsa dan negara
solusi: Menempatkan pembina (koresponden) di 33 ibukota provinsi, yang akan bertugas
  merangkul dan membina akademisi, praktisi dan ahli menjadi jurnalis warga cerdas
  dan sanggup menuangkan buah pikirannya dalam sebuah tulisan
sukses: 10 ribu artikel yang bersifat membangun dalam enam bulan, dimana penuliasnya
  adalah dosen, guru besar, dan profesor dari 33 ibukota provinsi di Indonesia
target: Para dosen, guru besar, dan profesor di 33 provinsi.
organisasi: CV Jurnalis Warga Network
---


### {{ page.nohibah }} - {{ page.title }}

---
