---
title: Sisihkan Komisi Untuk Pakir Miskin & Menerapkan Tehknologi Pedesaan
date: 2011-09-16 11:08:00 +07:00
nomor: 429
foto: /static/img/hibahcmb/429.jpg
nama: Lintasarea
lokasi: Garut
dana: 75 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: Selama-lamanya
deskripsi: Sebuah proyek penjualan produk & jasa kepada masyarakat dengan memakai
  teknologi tepat guna serta dibantu oleh berbagai komunitas radio dan pemuda. Keuntungan
  dari penjualan disisihkan utk disumbangkan kembali kepada masyarakat dengan kategori
  layak menerima sumbangan.
masalah: 'Masalah yang sering muncul dilapangan: sebagian masyarakat pedesaan belum
  bisa memahami dan belum bisa memanfaatkan teknologi yang sudah ada, mereka memilih
  dengan cara tradisional'
solusi: |-
  Pendekatan serta sosialisasi melalui media internet, jaringan radio komunitas, sebar brosur, survey oleh jaringan pemuda pemudi dan mengadakan pertemuan langsung kepada masyarakat.
  Pihak yang diuntungkan melalui proyek ini adalah  masyarakat itu sendiri yang menggunakan fasilitas tekhnologi tepat guna, karena dari setiap terjadi transaksi penjualan atau pembayaran keuntungan kami kembalikan kepada masyarakat khususnya pakir miskin dan 100% tidak mampu serta untuk membantu pembangunan mesjid-mesjid serta pondok pesantren
sukses: Kesuksesan yang terjadi disaat masyarakat sadar akan tehknologi dan
  menggunakan tekhnologi secara tepat guna, sadar terhadap sesama untuk membantu dalam
  kesulitan.
organisasi: Lintasarea
target: Yang diuntungkan adalah masyarakat itu sendiri yang menggunakan fasilitas
  tekhnologi tepat guna, karena dari setiap terjadi transaksi penjualan atau pembayaran
  keuntungan kami kembalikan kepada masyarakat khususnya pakir miskin dan 100% tidak
  mampu serta untuk membantu pembangunan mesjid-mesjid serta pondok pesantren
---


### {{ page.nohibah }} - {{ page.title }}

---
