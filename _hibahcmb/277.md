---
title: Membangun 100 content e-Learning Mata Kuliah Teknik Industri
date: 2011-09-16 11:08:00 +07:00
nomor: 277
foto: /static/img/hibahcmb/277.jpg
nama: Prodi Teknik Industri IT Telkom
lokasi: Bandung, Jawa Barat
dana: 500 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Januari – Desember 2012
deskripsi: Sebuah proyek pengembangan konten e-learning untuk meningkatkan mutu pendidikan
  dan menumbuhkan peran aktif mahasiswanya dalam proses belajar dan mengajar (students
  centered). Tujuan yang ingin dicapai antara lain terwujudnya sarana pembelajaran
  yang mampu mendeskripsikan dan membantu pemahaman materi yang bermutu untuk menunjang
  kegiatan belajar dan mengajar di Fakultas Rekayasa Industri IT Telkom, serta terwujudnya
  sarana pembelajaran yang menarik, interaktif, dan menumbuhkan minat belajar secara
  mandiri bagi peserta didiknya
masalah: |-
  1. Pembelajaran belum berorientasi pada pemenuhan kebutuhan peserta didik sehingga mampu mengembangkan kapabilitas intelektual peserta didik untuk pribadi yang bertanggung jawab dan mampu berkontribusi pada daya saing bangsa;
  2. Pembelajaran belum dapat menumbuhkan kegiatan penelitian dan program ekstrakulikuler sehingga terbentuk inkubator yang membantu pengembangan sistem ekonomi berbasis ilmu pengetahuan yang adaptif dan berkelanjutan dan integrasi teknologi terkini untuk memaksimalkan akses dan penerapan ilmu pengetahuan mutakhir;
  3. Pembelajaran belum mampu berkontribusi pada pengembangan masyarakat demokratis beradab, terbuka, dan memenuhi kriteria akuntabilitas publik;
  4. Pembelajaran belum memberikan kesempatan kepada peserta didiknya untuk mengikuti proses pembelajaran yang tak terbatas;
  5. Pembelajaran belum memberikan inspirasi dan memungkinkan peserta didik untuk mengembangkan dirinya sampai pada peringkat tertinggi sepanjang hidupnya. Dengan demikian, mereka dapat tumbuh secara intelektual dan emosional, terampil untuk bekerja, mampu berkontribusi kepada masyarakat, dan mampu memenuhi kebutuhan pribadinya
solusi: Reformasi akademik yang di dalamnya meliputi reformasi artikulasi nilai, integrasi
  deliveri, dan link and match dengan industri; implementasi metode student centered
  learning atau pembelajaran aktif dan partisifatif; dan implementasi e-learning.
  Proyek ini akan memberi keuntungan kepada civitas akademika dan masyarakat umum
  dan industri
sukses: Evaluasi proses apakah sudah sesuai dengan prosedur yang ditentukan
  dan melakukan evaluasi hasil apakah sesuai dengan target yang diharapkan
organisasi: Prodi Teknik Industri IT Telkom
target: Civitas akademika dan masyarakat umum dan industri
---


### {{ page.nohibah }} - {{ page.title }}

---
