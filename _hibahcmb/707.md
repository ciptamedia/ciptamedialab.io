---
title: SSYM (Semut Semut Yang Menyatu)
date: 2011-09-16 11:08:00 +07:00
nomor: 707
foto: /static/img/hibahcmb/707.jpg
nama: I Wayan Dwitanaya
lokasi: Bali
dana: 2 Miliar Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: 1 Nopember 2011 – 31 Desember 2012 (14 bulan)
deskripsi: Pembuatan media komunikasi dan bisnis bagi Usaha usaha kecil dan desa wisata
  di Tabanan dalam bentuk portal yang memberikan peluang promosi, edukasi dan koneksi
  antar anggota SSYM.
masalah: Publikasi usaha kecil dan dewa wisata di Tabanan belum maksimal. kalaupun
  ada yg memiliki website, itupun dengan pengembangan terbatas dan sendiri – sendiri.
  SSYM, memberikan wadah untuk kebutuhan ini serta mengurangi terjadinya persaingan
  tidak sehat.
solusi: |-
  SSYM mendata, mengedukasi, mempromosikan serta memberikan jaminan bagi industri kecil dan buyer (pembeli) akan kemudahan, mutu, dan garansi.

  Yang diuntungkan: 50 UKM ( minimal ) + 10 desa wisata di Kabupaten Tabanan (untuk pilot project).setelah berhasil, akan menyasar seluruh Bali
sukses: Terjadi transaksi bisnis melalui SSYM
organisasi: NA
target: 50 UKM ( minimal ) + 10 desa wisata di Kabupaten Tabanan (untuk pilot
  project).setelah berhasil, akan menyasar seluruh Bali
---


### {{ page.nohibah }} - {{ page.title }}

---
