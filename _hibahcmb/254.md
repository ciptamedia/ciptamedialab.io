---
title: Ratanika Graphic Novel / Comic
date: 2011-09-16 11:08:00 +07:00
nomor: 254
foto: /static/img/hibahcmb/254.jpg
nama: John Tarumanegara
lokasi: Jakarta
dana: 185 Juta Rupiah
topik: Pemantauan media
durasi: Sudah berjalan selama 1,5 tahun sejak 2009
deskripsi: Sebuah novel grafis setebal 200 halaman berwarna dengan konten yang cerdas,
  berwawasan dengan bumbu kritik sosial, kemajemukan individu, dan konspirasi hal
  kemanusiaan
masalah: Kurangnya media baru bagi generasi muda untuk menunjukkan karya dan kemampuan
  membangun industri kreatif dalam negeri yang bermutu dan cerdas
solusi: Dengan graphic novel ini diharapkan masyarakat dapat lebih kritis menanggapi
  polemik yang terjadi di Indonesia. Menilai sesuatu lebih objektif dan mampu menelaah
  lebih jauh lagi kecenderungan sosial terutama di negara berkembang. Proyek ini akan
  memberi keuntungan kepada kalangan anak-anak dan orang dewasa yang haus akan media
  baru sebagai kebanggaan industri kreatif dalam negeri yang kompetitif
sukses: Semakin banyak organisasi kecil yang tumbuh dan berani mempublikasikan
  karyanya untuk lebih dinamis, beragam, dan termotivasi
organisasi: NA
target: Kalangan anak-anak dan orang dewasa yang haus akan media baru sebagai
  kebanggaan industri kreatif dalam negeri yang kompetitif
---


### {{ page.nohibah }} - {{ page.title }}

---
