---
title: 'FM Radio Watch: Tampil Pertama Menjawab Berita, Tampil Utama Membawa Kabar'
date: 2011-09-16 11:08:00 +07:00
nomor: 078
foto: /static/img/hibahcmb/078.jpg
nama: Wihelmus Openg
lokasi: Kupang, NTT
dana: 1 Milyar Rupiah
topik: Pemantauan media
durasi: Januari 2012-Desember 2013 (2 tahun)
deskripsi: Sebuah proyek yang memadukan teknologi telepon seluler dengan aplikasi
  radio yang berada di dalamnya untuk menyiarkan berita terkini di berbagai daerah
  di NTT. Berita yang didapat kemudian akan dilansir ke berbagai media cetak setempat.
  Proyek ini akan merekrut beberapa orang di berbagai daerah di NTT untuk dijadikan
  wartawan HP Radio Watch
masalah: Media yang pada prinsipnya memberikan kebebasan berekspresi dan berpendapat,
  menjadi tertekan dan tidak bebas akibat kontrol uang dari para penguasa dan pengusaha
  sehingga berita yang didapat, terutama di daerah pedesaan, jarang bersifat benar,
  adil, relevan, dan tuntas.
solusi: Membuat stasiun radio yang berfungsi sebagai penjaring dan penyaring berita-berita
  utama yang disuguhkan di berbagai media. Berita akan disaring berdasarkan pengaruhnya
  dan relevansinya, serta akan dijaring melalui investigasi  langsung dari narasumber.
  Selain itu, juga akan disiarkan mengenai kisah sukses dan teknologi tepat guna dari
  berbagai daerah
sukses: Adanya jumlah tanggapan balik berupa; diskusi langsung radio, sms yang
  masuk dari desa-desa yang mendapat pelayanan HP RADIO WATCH, berita mengenai kisah
  sukses dan teknologi tepat guna
organisasi: Yayasan Gerbang Alam Timur (YAGAT)
target: Masyarakat di Kabupaten Kupang
---


### {{ page.nohibah }} - {{ page.title }}

---
