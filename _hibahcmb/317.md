---
title: Urbanisasi Informasi
date: 2011-09-16 11:08:00 +07:00
nomor: 317
foto: /static/img/hibahcmb/317.jpg
nama: Jaringan Radio Komunitas Sulawesi Utara
lokasi: Sulawesi Utara
dana: 760 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Februari – Juli 2012
deskripsi: Sebuah radio komunitas yang mendukung teknologi informasi berbasis jaringan.
  Tiap radio diberi kebebasan saling berbagi kejadian disekitarnya untuk diketahui
  pengambil kebijakan dan masyarakat luas
masalah: Arus informasi masih terbelenggu dan menjadikan media rakyat sebagai resapan
  intelektual kebutuhan informasi berkelanjutan
solusi: Pemberdayaan media rakyat yang telah ada. Pihak yang diuntungkan melalui proyek
  ini adalah masyarakat dalam jangkauan Radio Komunitas di 7 Kab/Kota di Sulut
sukses: Berdasarkan sebaran media rakyat
organisasi: Jaringan Radio Komunitas Sulawesi Utara
target: Masyarakat dalam jangkauan Radio Komunitas di 7 Kab/Kota di Sulut
---


### {{ page.nohibah }} - {{ page.title }}

---
