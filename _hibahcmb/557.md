---
title: Televisi Sehat untuk Memperbaiki Kualitas Hidup di 10 Desa Lereng Merapi
date: 2011-09-16 11:08:00 +07:00
nomor: 557
foto: /static/img/hibahcmb/557.jpg
nama: Perhimpunan Sakala
lokasi: DI Yogyakarta
dana: 1 Miliar Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: 1 tahun
deskripsi: Proyek pembangunan masyarakat di 10 Desa lereng Merapi menjadi sadar media
  yang mampu memnuhi kebutuhannya akan media alternatif yang sehat, khususnya media
  audio visual (televisi), dan bisa menjadi model bagi komunitas dan masyaraka lain.
masalah: Masyarakat telah dijadikan korban oleh praktik yang dilakukan para pengelola
  stasiun televisi atas nama hiburan dan informasi. Sementara, masyarakat tidak memiliki
  posisi tawar yang cukup kuat atas praktik-praktik itu. Sehingga media televisi memiliki
  andil dalam memperburuk kualitas hidup masyarakat Indonesia.
solusi: |-
  Melakukan 3 tahapan: meningkatkan ‘media literacy / melek media” (kesadaran wacana) masyarakat, meningkatkan ‘media awareness / sadar media” (kesadaran kebutuhan) masyarakat, dan memproduksi media sendiri oleh masyarakat.
  Pihak yang menerima manfaat dari proyek ini adalah komunitas Lajur Merapi dan masyarakat di 10 desa di Kawasan lereng Merapi.
sukses: Masyarakat 10 desa lereng Merapi memiliki kelompok masyarakat sadar
  media dan Komunias Lajur Merapi mampu memproduksi kebutuhannya sendiri
organisasi: Perhimpunan Sakala
target: komunitas Lajur Merapi dan masyarakat di 10 desa di Kawasan lereng Merapi
---


### {{ page.nohibah }} - {{ page.title }}

---
