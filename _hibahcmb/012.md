---
title: Digitalisasi & Broadcast Budaya Sunda Online
date: 2011-09-16 11:08:00 +07:00
nomor: '012'
foto: /static/img/hibahcmb/012.jpg
nama: A. Firman Juliansyah
lokasi: Bandung
dana: 300 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Minimum 1 tahun untuk waktu yang tak terbatas
deskripsi: Sebuah proyek yang berupaya untuk mengkoleksi budaya Sunda yang bernilai
  tinggi dan mempublikasikannya secara online dengan tujuan meningkatkan kunjungan
  wisata ke Pulau Jawa, meningkatkan solidaritas kemasyarakatan dan kecintaan terhadap
  budaya lokal, serta memungkinkan komersialisasi produk karya sehingga meningkatkan
  kesejahteraan rakyat
masalah: Musnahnya kekayaan budaya lokal, kurangnya kecintaan dan akses terhadap budaya
  lokal, berkurangnya rasa kemasyarakatan rakyat, dan terbatasnya akses untuk peningkatan
  kesejahteraan rakyat dengan pemanfaatan budaya lokal
solusi: Turut melestarikan budaya lokal secara digital, menyimpannya, dan menyebarkannya
  kepada masyarakat dunia sehingga akan meningkatkan kecintaan, solidaritas, dan peningkatan
  ekonomi rakyat. Yang diuntungkan dari proyek ini adalah masyarakat dunia karena
  mengenal budaya Sunda, masyarakat Sunda karena dapat mengenal dan menggali kembali
  kebudayaannya sendiri, serta meningkatnya kunjungan wisata dan pengrajin seniman
  karena mendapatkan pemasukan baru
sukses: Jumlah koleksi budaya lokal mencapai 500 konten dan jumlah minimum transaksi
  adalah Rp 300.000,00 per tahun
organisasi: Zigra Aptha Nirbhaya, CV
target: masyarakat dunia karena mengenal budaya Sunda, masyarakat Sunda karena
  dapat mengenal dan menggali kembali kebudayaannya sendiri, serta meningkatnya kunjungan
  wisata dan pengrajin seniman karena mendapatkan pemasukan baru
---


### {{ page.nohibah }} - {{ page.title }}

---
