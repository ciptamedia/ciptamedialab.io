---
title: Kabar Dari Kawan
date: 2011-09-16 11:08:00 +07:00
nomor: 676
foto: /static/img/hibahcmb/676.jpg
nama: Matahari Asysyakuur
lokasi: DIY
dana: 120 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Januari-Juli 2012
deskripsi: Kawan Dari Jauh merupakan suatu portal online dimana didalamnya merupakan
  upload-an video, berita dan sebagainya oleh penikmat berita feature yang berasal
  dari orang-orang yang tinggal di daerah (urban dan sub-urban). Cara peliputan berita
  feature ini dengan cara citizen journalism yang spesifik untuk awalnya akan diujicobakan
  di DIY dan Jateng. Dengan demikian akan banyak peliputan berita-berita feature tentang
  kejadian unik dan dirasa pantas untuk di upload di portal online ini untuk dinikmati
  para penggiat media interaktif. Tujuan dari program ini tentu saja dapat meningkatkan
  kebanggan terhadap Indonesia.
masalah: Banyaknya jenis peliputan berita-berita yang terpusat pada daerah tertentu
  dengan gaya bahasa yang relatif formal dan menggunakan bahasa yang sulit dimengerti
  oleh mereka byang berpendidikan rendah.
solusi: |-
  1. Mengajak masyarakat luas untuk mulai mengupload segala kejadian yang dirasa unik dan pantas (mengandung nilai berita) dengan bentuk bahasa mereka sendiri untuk segmentasi umum;
  2. Membiasakan lagi budaya menulis dan peduli lingkungan dengan membumikan citizen journalism;
  3. mengajari orang-orang (urban dan sub-urban) yang mau dan mampu melakukan perubahan hidup untuk pemerataan merita
  Yang diuntungkan:
  Orang-orang yang berkenan untuk tergabung dalam dinamika perubahan sosial dalam hal ini teknologi informasi, dan integrasi media serta semua orang penggiat media baru, terutama yang update pada berita-berita terbaru yang tergabung baik dalam jejaring sosial maupun forum, dan sebagainya.
sukses: |-
  1. Banyaknya pengunjung yang datang dalam portal online itu untuk membaca hingga berkomentar;
  2. Banyaknya mention, retweet hingga membagikan link di jejaring sosial;
  3. Banyaknya upload-an berita feature yang berkualitas di portal online tersebut.
organisasi: NA
target: 1. Mengajak masyarakat luas untuk mulai mengupload segala kejadian yang
  dirasa unik dan pantas (mengandung nilai berita) dengan bentuk bahasa mereka sendiri
  untuk segmentasi umum; 2. Membiasakan lagi budaya menulis dan peduli lingkungan
  dengan membumikan citizen journalism; 3. mengajari orang-orang (urban dan sub-urban)
  yang m
---


### {{ page.nohibah }} - {{ page.title }}

---
