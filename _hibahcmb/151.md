---
title: Lan Jalan Madura
date: 2011-09-16 11:08:00 +07:00
nomor: 151
foto: /static/img/hibahcmb/151.jpg
nama: Lulus Andika
lokasi: Bangkalan, Jawa Timur
dana: 15 Juta Rupiah
topik: Meretas batas - kebhinekaan bermedia
durasi: September 2011 - Februari 2012
deskripsi: Sebuah proyek pembuatan buku yang berisi tentang wisata, budaya, serta
  kuliner Madura. Komposisi buku sebgaian besar merupakan foto-foto yang disertai
  artikel. Dalam proses pengumpulan data juga akan dilakukan eksplorasi di Pulau Madura
masalah: Kurangnya pendanaan untuk proses pengumpulan data dan produksi buku, serta
  kurangnya referensi sehingga harus melakukan pengamatan secara langsung
solusi: Melakukan pengumpulan data langsung ke lapangan, serta melakukan kerja sama
  dengan pihak produksi. Buku juga dapat diterbitkan secara online (e-book)
sukses: Banyaknya respon terhadap buku tersebut dan jumlah penjualan/pengandaan
  buku
target: Masyarakat Madura dan calon wisatawan
organisasi: Madura for Photography (MFP)
---


### {{ page.nohibah }} - {{ page.title }}

---
