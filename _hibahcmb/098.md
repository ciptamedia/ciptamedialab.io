---
title: Pemberdayaan remaja berbasis teknologi komunikasi dan teknologi pengolahan
  lingkungan
date: 2011-09-16 11:08:00 +07:00
nomor: 098
foto: /static/img/hibahcmb/098.jpg
nama: Suryadi Kusniawan
lokasi: Gresik, Jawa Timur
dana: 150 Juta Rupiah
topik: Meretas batas - kebhinekaan bermedia
durasi: Oktober 2011-Agustus 2012 (10 Bulan)
deskripsi: Sebuah proyek yang ditujukan untuk mengembangkan soft skill remaja di bidang
  broadcasting yang berorientasi pada pengoptimalan lembaga penyiaran komunitas sebagai
  media dakwah dan sebagai fasilitas aktualisasi diri remaja yang sekaligus mampu
  memberikan penghasilan bagi mereka. Di samping itu, para remaja juga diajarkan untuk
  mengolah sampah
masalah: Keterbatasan masyarakat dalam memanfaatkan media sebagai sarana komunikasi,
  interaksi, dan media dakwah, serta adanya penumpukkan sampah rumah tangga
solusi: Mendirikan lembaga penyiaran komunitas untuk menyentuh berbagai lapisan masyarakat
  dan memberikan teknologi tepat guna untuk pengolahan sampah organik
sukses: Dengan keterlibatan para remaja dengan lembaga penyiaran komunitas yang
  didirikan kelak, dan bagaimana keberadaan lembaga tersebut mampu menyentuh secara
  langsung kepada masyarakat sebagai audiensnya. Untuk program pengolahan sampah,
  indikatornya dapat dilihat pada tingkat penanggulangan yang mampu dikoogtasi oleh
  program tersebut terhadap permasalahan sampah yang ada di masyararakat di wilayah
  Menganti-Gresik
organisasi: Yayasan Baitul Awwabin
target: Remaja dengan range usia 15 - 25 tahun yang tinggal di wilayah Kecamatan
  Menganti, Kabupaten Gresik,  warga masyarakat yang limbah rumah tangga dan limbah
  industri rumah tangganya mampu diolah di tempat pengolahan yang direncanakan
---


### {{ page.nohibah }} - {{ page.title }}

---
