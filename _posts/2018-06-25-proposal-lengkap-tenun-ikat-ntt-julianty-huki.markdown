---
title: Proposal Lengkap - Tenun Ikat Tradisional NTT - Julianty Huki
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/tenun-ikat-ntt
layout: proposal
author: Julianty Huki
categories:
- laporan
- CME
- Tenun Ikat Tradisional NTT
---

![0498.jpg](/static/img/hibahcme/0498.jpg){: .img-responsive .center-block }

# Julianty Huki - Tenun Ikat Tradisional NTT

**Tentang penerima hibah**

Julianti Huki adalah perempuan berusia 22 tahun yang merupakan seorang profesional muda di bidang pemberdayaan dan pengembangan kapasitas masyarakat selama 3 tahun terakhir ini. Menamatkan studi di Universitas Nusa Cendana dengan konsentrasi Pendidikan Matematika, ia tetap semangat untuk mengabdi bagi lingkungan sekitar khususnya untuk ibu-ibu penenun kain tradisional khas Nusa Tenggara Timur. Saat ini, ia sedang bekerja di salah satu perusahaan multifinance ternama di Indonesia, sembari mengukir mimpi menjadi pemimpin perusahaan sosial miliknya sendiri.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Kupang, NTT

**Deskripsi Proyek**

Upaya fasilitasi jejaring penjualan kain tenun ikat tradisional NTT yang berlokasi di Kecamatan Manutapen dan Kecamatan Penkase, NTT sebagai peningkatan mata pencaharian keluarga penenun dipercaya Julianty Huki, sebagai pelopornya, penting untuk dilakukan. Kelompok tenun beranggotakan para ibu rumah tangga ini diharapkan mampu meningkatkan kapasitas ekonomi serta mensosialisasikan dan menjaring kerja sama antar penenun tradisional di Kabupaten Kupang.

- **Tujuan**

  Memfasilitasi ibu–ibu penenun dalam aktivitas tenun serta penjualan kain tenun.

- **Sasaran**

     1. Menyediakan fasilitas yang layak digunakan oleh ibu–ibu penenun, sehingga meningkatkan produktivitas penghasilan kain tenun secara keseluruhan.

     2. Memperluas tingkat dan/atau jumlah konsumen agar penjualan meningkat, sekaligus mempromosikan kain tenun khas NTT.

     3. Mengedukasi ibu–ibu penenun dan keluarga akan pentingnya meneruskan cara tenun tradisional khas NTT.

- **Latar Belakang**

  1. Keterkaitan pada kategori: Akses

     Dengan memberikan fasilitas yang dapat menunjang produktivitas para penenun, proyek ini dapat pula menghubungkan ibu–ibu penenun dengan konsumen. Hasil tenunan pun dapat menjadi akses bagi mereka dalam memenuhi kebutuhan sehari–hari serta mempersiapkan kebutuhan bagi anak–anaknya demi masa depan keluarga. Pengembangan kapasitas ini dibuat dengan dua kelompok tenun sebagai objek utama dengan akses yang disediakan oleh Cipta Media melalui fasilitator. Dua kelompok tenun ikat tradisional NTT yang difasilitasi berlokasi masing – masing di Kecamatan Manutapen dan Kecamatan Penkase. Kelompok pertama berjumlah 13 orang dan kelompok kedua berjumlah 10 orang, dengan rata- rata usia diatas 40 tahun.

     Kedua kelompok ini dimulai oleh para ibu rumah tangga yang memiliki kemampuan menenun sejak remaja, dan berinisiatif untuk menjadikan kemampuan ini sebagai sumber mata pencaharian mereka dan keluarga. Awalnya kelompok ini dimulai secara manual dan individu serta dilakukan di masing–masing rumah. Pada tahun 2014, kelompok–kelompok ini pun diresmikan dengan bantuan yang diperoleh dari pemerintah pada masa itu. Karena kegigihan para ibu rumah tangga ini dan ketekunan mereka dalam menenun demi menghasilkan sebuah kain yang tidak hanya bernilai materi namun juga seni budaya, mereka berhasil menafkahi keluarga bahkan banyak pula yang berhasil menguliahkan putra/putri mereka. Pun begitu, para ibu pejuang ini tidak lupa untuk mewariskan keahlian menenun ini pada anak–anak mereka. Dua kelompok ini mempunyai cara pemasaran masing–masing, dimana kelompok pertama memasarkan kain tenun dan selendang di pasar–pasar tradisional sementara kelompok kedua sudah memiliki pelanggan tetap yaitu toko jahit. Kelompok pertama meyakini bahwa kualitas kain tenun yang baik adalah kain tenun yang warnanya dihasilkan dari pewarna alami.


  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Proyek kain tenun ini akan dijalankan bersama oleh dua kelompok tenun ikat tradisional NTT dalam jangka waktu 6 bulan. Untuk kelompok pertama, perlu dilakukan langkah pemasaran yang tepat yaitu dengan penempatan etalase dengan pengenalan basecamp kelompok sebagai sebuah toko kain tenun adat. Untuk kelompok kedua, alat tenun manual tidak efektif dalam pembuatan kain tenun sehingga harus diantisipasi. Akan lebih mudah untuk kain tenun diproduksi apabila mereka memiliki alat tenun permanen. Dengan bahan benang serta pewarna kain yang produksinya ditingkatkan untuk masing–masing kelompok, peningkatan produksi dan penjualan kain tenun secara signifikan dan berkepanjangan tentu bisa terwujud.

  3. Strategi

     Rencana bentuk pelaksanaan kegiatan adalah: (1) Pemberian alat dan bahan berupa benang dan pewarna alami serta alat tenun permanen yang disesuaikan dengan kebutuhan masing–masing kelompok; (2) Pencarian pelanggan tetap melalui promosi hasil tenunan dari kelompok, monitoring dan evaluasi.

  4. Aktivitas dan keterkaitan pada sasaran

     Dalam rangka meningkatkan produktivitas tenun, akan dilakukan pendistribusian benang dan pewarna alami pada kedua kelompok, kemudian dilakukan promosi masing–masing toko alat tenun oleh ibu–ibu penenun bekerja sama dengan pemerintah setempat. Dilakukan juga pendistribusian hasil tenun dan jejaring dengan calon pelanggan tetap, dan terakhir pengawasan dan evaluasi proyek.

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah perempuan dengan pengalaman memfasilitasi pengembangan kapasitas masyarakat lokal sebagai mitra LSM maupun individu selama 3 tahun (dari tahun 2015), 22 tahun dan baru saja menamatkan studi S-1 di Universitas Nusa Cendana.

  6. Demografi kelompok target

     Dua kelompok tenun tersebut adalah kelompok tenun ikat tradisional khas NTT beranggotakan ibu–ibu penenun asli NTT dengan rentang usia 40-50an tahun. Mereka adalah ibu–ibu rumah tangga yang masih memiliki semangat tinggi akan kain tenun adat dan masih berjuang untuk meningkatkan ekonomi keluarga.

  7. Hasil yang diharapkan & Indikator keberhasilan

     Melalui proyek ini, diharapkan ibu–ibu penenun dapat meningkatkan jumlah tenunan serta meningkatkan pasar/jumlah pelanggan pembeli tenun.

  8. Indikator keberhasilan

     Kegiatan kedua kelompok tenun ikat ini akan dikatakan sukses bila:

        1. Kedua kelompok berhasil mendapatkan peningkatan jumlah pelanggan

        2. Kedua kelompok berhasil meningkatkan jumlah produksi kain tenun

        3. Kedua kelompok berhasil mempromosikan kain tenun dengan penggunaan pewarna alami

        4. Kedua kelompok berhasil menambah anggota

        5. Setelah masa proyek 6 bulan, kedua kelompok dapat mandiri dalam pengelolaan

  9. Durasi Waktu Aktivitas dilaksanakan

     6 bulan.

  10. Total kebutuhan dana

      IDR 69.258.000 – IDR 393.200.000,-

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.68.000.000

  12. Sumber dana lainnya

      Tidak ada.

  13. Kontribusi perorangan

      Menyediakan dan mengedukasi ibu–ibu penenun dalam menghasilkan kain tenun yang berkualitas, serta menyediakan akomodasi antar konsumen dan para penenun.

  14. Kontribusi kelompok target

      Tidak ada.
