---
title: Laporan Naratif - Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai - Rhidian Yasminta Wasaraka
date: 2019-04-18
permalink: /ciptamediaekspresi/laporan-naratif/perempuan-perkasa
layout: proposal
author: Rhidian Yasminta Wasaraka
categories:
- laporan
- Laporan Naratif
- CME
- Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai
---

# Laporan Naratif - Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Belajar mengalahkan rasa takut dan dilema sebagai seorang ibu yang masih memberi ASI kepada anak saya. Saya belajar berbagi peran dan mengatur keuangan dengan baik.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> 1. Persiapan meyangkut pembelian alat-alat dan kelengkapan lain yang menunjang kegiatan penelitian, termasuk mengirim kabar melalui kurir pesan atau radio SSB kepada tuan dusun di Korowai
>
> 2. Turun lapangan untuk pengambilan data
>
> 3. Mengolah data
>
> 4. Editing dan Cetak Buku
>
> 5. Peluncuruan buku, pameran photo dan diskusi
>
> 6. Laporan

**Ceritakan hasil dari aktivitas tersebut.**

Setiap tahapan kegiatan mempunyai tantangan sendiri, misalnya pada poin satu dimana saya harus mencari jalan bagaimana menghubungi orang di Korowai karena radio SSB milik misionaris di Waena dan di Sentani rusak, sedangkan Pdt. Trevor dan Pdt. Jan De Vris sedang cuti ke negara masing-masing. Jadi, saya seperti harus mengirim pesan melalui kurir di tanah merah, dan begitu pun sebaliknya. Pada tahap turun ke lapangan saya berhadapan dengan bukan hanya situasi medan yang sulit sambil membawa bayi saya tetapi juga saya harus berhadapan dengan membengkaknya harga tiket pesawat akibat jalur yg memutar lewat tanah merah lalu ke Danuwage. Sementara hitungan saya yang awal adalah hanya dari Jayapura langsung ke Danuwage.Tahap mengolah data; tidak terlalu menjadi masalah karena isu Korowai bukan merupakan barang baru bagi saya. Saya hanya perlu seperti cross check data dan mewawancarai beberapa orang diluar orang Korowai, seprti Pdt. Trevor, Pdt. Piter Jan de Vris dan Dr. Rupert Stach melalui WA. Tahap editing dan cetak; proses inipun tidak mengalami kendala berarti sebab Dayu Rifanto selain memang berpengalaman dalam bidang edit-mengedit, tetapi juga sangat komunikatif dan paham isu-isu Papua sebab beliau lahir, besar, dan sampai saat ini bekerja di Papua. Tahap Peluncuran, Pameran dan Diskusi; menurut rencana kegiatan, saya akan melakukan pameran dan peluncurun buku di Galeri ANTARA Jakarta dan di 2 tempat di Jayapura (ISBI dan Museum Loka Budaya UNCEN). Dari tempat-tempat tersebut ternyata saya mendapatkan kendala. Namun, bisa dialihkan ketempat lain yakni di Kampus UI (Universitas Indonesia) Depok di Gedung Anak Nusantara. Antusias dari para perserta membludak. Pihak Antropologi UI sendiri menyambut dengan baik sebab bidang kajian visual antropologi sendiri merupakan hal yang baru dan sedang dirintis di UI. Pelaksanaan kegiatan yang kedua sebenarnya ingin diselenggarakan di ISBI ttetapi erkendala waktu dan ruangan sebab rektor ISBI Papua maunya dibuat saat bulan April pertengahan sementara April harusnya sudah masuk laporan kegiatan. Maka dengan terpaksa kegiatan dipindahkan ke kampus STIKOM Muhammadiyah Jayapura. Pada kegiatan tersebut dihadiri para pengurus PW Muhammadiyah Provinsi Papua, akademisi STIKOM Muhammdiyah dan semua ortom yang ada dibawahnya (NA, AISIYAH, PM, IMM).
Pada kegiatan tersebut PW Muhammadiyah menyarankan agar kegiatan serupa juga bisa diselenggarakan di kampus Muhammadiyah yang lain di seluruh Indonesia. Pada bagian akhir dari rangkaian kegiatan pameran dan diskusi di Jayapura, berhasil terselenggara di Museum LokaBudaya UNCEN Jayapura di Abepura. Sayangnya, saat kegiatan tersebut berlangsung terjadi banjir bandang di Kabupaten dan Kota Jayapura hingga hanya sedikit orang yang bisa datang (sekitar 20an orang). Namun, meski sedikit, yang hadir adalah dari kalangan yang cukup beragam.
Selain soal berpindahan lokasi kegiatan, hal lain yang juga membuat pusing adalah naiknya harga tiket dari Jayapura-Jakarta PP ditambah lagi dengan sistem bagasi berbayar. Hal ini tidak saya perhitungkan sebelumnya.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Bagaimanakah suku bangsa Korowai memandang posisi perempuan dalam kehidupan sehari-hari mereka?

**Ceritakan hasil dari mengatasi isu tersebut.**

Suku Korowai adalah suku yang unik dan menarik sekaligus membingungkan bagi banyak ahli-ahli sosial. Sebab mereka memang seperti selalu berada dalam kutub yang berlawanan. Di satu sisi mereka hidup terpisah, tidak bergabung satu dengan yang lain seolah-olah egois namun ternyata kalau ada pesta ulat sagu semua ikut bergabung dan saling bantu. Pun terhadap perempuan, mereka memang menganut budaya patriarki, mereka juga memang memandang bahwa perempuan yang sedang menstruasi tidak boleh ada di rumah pesta atau ada di dalam rumah tinggal atau makan daging binatang buruan. Tapi di sisi lain seorang ibu sangat dihormati di sini. Perempuan punya hak yang sama dalam hal harta warisan, dan kepemilikan barang. Yang menarik adalah mereka sangat menghormati sosok ibu mertua dari garis istri, saking hormatnya para pria mereka bahkan tak berani memandang wajah sang ibu mertua. Perempuan Korowai juga punya hak penuh atas diri mereka dan organ reproduksi mereka. Para perempuan Korowai juga punya hak untuk meminta cerai, memutuskan ikatan perjodohan, dan mereka pun punya hak atas tanah, uang, dan benda-benda berharga yang mereka hasilkan dari proses perdagangan dan ataupun warisan.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Terselenggaranya kegiatan penelitian dilapangan
>
> 2. Terdapat Buku tentang Budaya Suku Korowai
>
> 3. Terselenggaranya publikasi berupa pameran photo dan diskusi di tiga tempat (Galeri Antara, Museum Loka Budaya Uncen dan kampus ISBI tanah Papua)


**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Dalam proses ini saya mengalami beberapa kendala dan perubahan tempat kegiatan. Namun masih bisa diatasi dengan baik walaupun harus menunggu cukup lama agar bisa mengikuti pesta ulat sagu versi pesta adat dan bukan pesta turis. Harus juga mengalami pembengkakan biaya tiket pesawat karena beberapa alasan namun tetap bisa berjalan dengan baik dan berhasil mendokumentasikan pesta ulat sagu dan kehidupan di Korowai dengan baik sehingga dapat menghasilkan buku berjudul Perempuan Perkasa, Belajar Praktik Kesetaraan pada budaya Suku Korowai. Buku ini ternyata sangat diminati dan sejauh ini saya sudah menyebarkannya bukan hanya di Jayapura dan Jakarta saja tapi juga sampai di Flores dan Sulawes. Saya juga berupaya bertemu dengan sebanyak mungkin orang dalam diskusi dan bedah buku yang disponsori oleh Ford Foundation dan disponsori juga oleh pihak lain. Saya juga mendistribusikan buku ke perpustakaan nasional dan daerah. Pada tahap ini saya mengalami kendala tentang izin tepat di dua tempat yang pertama yaitu di Galeri Foto Antara dan di ISBI. Meskipun begitu, berkat kerjasama semua pihak kita bisa mencari alternatif tempat lain. Galeri Foto ANTARA di Jakarta diganti dengan di FISIP UI Depok. Di satu sisi ini juga menjadi keuntungan sendiri karena dengan dihadiri oleh para pakar dan akademisi maka kebenaran buku ini bisa di uji secara akademis. Sedangkan di kampus ISBI Tanah Papua terkendala masalah tempat dan waktu sebab rektor ISBI maunya kegiatan dilaksanakan pada bulan April  pertengahan sedangkan pada periode tersebut kita harus sudah masuk tahap laporan. Kegiatan tersebut akhirnya di pindahkan ke STIKOM Muhammadiyah Jayapura. Kegiatan terakhir adalah di Museum Lokabudaya UNCEN yang terkendala masalah cuaca. Saat itu sebagaian besar kota dan kabupaten Jayapura dilanda hujan deras dan banjir bandang hingga membuat peserta yang hadir hanya sebagian saja (20an orang).

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Saya sendiri merasa bagaimana jaringan yang saya miliki akhirnya bisa tumbuh dengan sangat pesat melalui berbagai diskusi dan pertemuan. Juga bagaimana minimnya pengetahuan tentang Korowai di satu sisi dan di sisi  yang lain antusiasme banyak orang untuk mengenal Korowai lebih dalam dan lebih detil menjadi terbuka. Keterbukaan ini berasal dari berbagai kalangan, bukan hanya sipil, akademisi, dan pemda namun juga politisi dan militer. Saya sangat bersyukur karenanya.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Dalam setiap kegiatan selalu ada tantangan dan hambatan. Namun saya kira ini untuk proses pendewasaan kita semua dan jika bisa dibagi maka saya ingin membagi dua tantangan yang saya hadapi itu  Yang pertama adalah tantangan dari dalam diri sendiri. Saya merasa sebagai seorang ibu yang sedang menyusui, saya menjadi sangat dilema dan galau kalau harus meninggalkan anak saya. Apalagi sebelumnya dia memang tidak pernah berpisah dari saya. Maka dengan segala konsekuensinya, saya memutuskan untuk membawa bayi saya ini ke Korowai. Namun, di sisi lain saya bersyukur dengan adanya kesempatan ini. Saya jadi punya banyak waktu dan kesempatan untuk bukan hanya mengembangkan diri tapi juga bagaimana dengan fleksibel bisa mengatur waktu antara kewajiban sebagai  peneliti, dosen, aktivis sekaligus juga sebagai ibu. Tantangan yang kedua berasal dari luar. Saya mendapat kendala dari naiknya harga tiket, bagasi berbayar, kesulitan hubungan komunikasi masyarakat di Korowai, sistem perhitungan tanggal yang berbeda dengan kita (orang Korowai menggunakan hitungan edar bulan), bencana alam, dan berpindahnya lokasi kegiatan dari yang semula yang direncanakan. Menghadapi semua itu membuat saya harus memutar otak bagaimana mencari jalan keluar misalnya dengan menghemat pengeluaran dan mencari sumber dana lain yang bisa menopang pembengkakan biaya

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Tidak bisa saya bayangkan bagaimana senangnya saya bisa mendapatkan hidah dari CME. Sebab, memang bagi saya sangat susah mendapatkan dana untuk penelitian di Korowai. Kebanyakan donor merasa itu terlalu jauh dan terlalu mahal. Namun, CME justru memberikan kepercayaan itu pada saya dan sekali lagi karena saya punya waktu yang sangat panjang untuk penelitian (dihitung dari diumumkannya pemenang sampai kepada laporan) maka saya punya kebebasan mengatur waktu saya. Bagi saya apa yang CME lakukan bukan hanya memperkaya keragaman ekspresi perempuan tetapi juga memperkaya sumber-sumber informasi dan sumber belajar bagi siapapun di seluruh dunia.
