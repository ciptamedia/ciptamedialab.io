---
title: 'SIARAN PERS: ETALASE PEMIKIRAN PEREMPUAN 2020'
date: 2020-07-16
categories:
  - CME
  - Publikasi
tags:
  - CME
  - Siaran Pers
  - Sekolah Pemikiran Perempuan
  - Etalase
  - Wikimedia Indonesia
  - Ford Foundation
author: Amalia Sekarjati
comments: false
---

**Siaran Pers**: untuk disebarluaskan<br/>
Jakarta 16 Juli 2020

# Etalase Pemikiran Perempuan 2020: Sebuah Festival<br/>
## 24-26 Juli 2020

Setelah [Festival Cipta Media Ekspresi 2019]({{site.baseurl}}/ciptamediaekspresi/festival-cme.html) terselenggara pertama kali di Yogyakarta pada 26-28 April 2019, para pengelolanya terus mengadakan ruang bagi pemikiran perempuan. Salah satunya dengan menyelenggarakan ‘Etalase Pemikiran Perempuan 2020: Sebuah Festival’ (Etalase) pada 24-26 Juli mendatang sebagai keberlanjutan. Etalase ini merupakan perjalanan lanjutan [Sekolah Pemikiran Perempuan (SPP)]({{site.baseurl}}/ciptamediaekspresi/spp-cme.html), sebuah ruang edukasi berdasarkan pengalaman perempuan dan pemikiran-pemikirannya. Landasan pikiran mengenai etalase untuk memajang pemikiran perempuan berasal dari pernyataan penulis/akademisi Intan Paramaditha dalam artikel [“Bongkar: Siasat Feminis dalam Seni dan Budaya di Indonesia”]({{site.baseurl}}/cme/kajian/2019/05/03/bongkar-siasat-feminis-dalam-seni-dan-budaya.html).
 
”Salah satu siasat yang perlu terus dilakukan adalah menyikapi forum diskusi sebagai etalase pemikiran perempuan. Etalase di sini kita lepaskan dari asosiasi feminin, semisal manekin dengan gaun modis, dan kita reka ulang sebagai tempat memamerkan gagasan kreator dan intelektual perempuan. Ruang etalase, baik yang sementara maupun permanen, perlu terus kita ciptakan,” tulis Intan.
 
Lisabona Rahman, salah satu pendiri SPP sekaligus Ketua Penyelenggara tahun ini menambahkan, “Dari landasan pikiran tersebut, kita perlu mengadakan (lebih banyak) kesempatan untuk memamerkan karya dan pemikiran perempuan. Salah satunya melalui forum diskusi yang juga bisa diarahkan menjadi tempat para perempuan saling berkenalan, berbagi gagasan dan kecakapan, serta saling menopang. Untuk itulah Etalase ini ada.”
 
Selama tiga hari, publik peserta dapat mengikuti kegiatan-kegiatan berupa rangkaian diskusi, cerita kisah hidup, dan pentas ceramah secara daring. Kegiatan-kegiatan ini berangkat dari beberapa tema/topik: membuka akses yang terhambat bagi perempuan, sejarah, metode kerja seni/budaya, kolaborasi lintas disiplin dan lintas generasi. Pada edisi kedua Etalase ini, penyelenggara tidak saja menghadirkan kembali beberapa alumni hibah Cipta Media Ekspresi (CME), tapi juga mengundang para penampil yang mempunyai pengalaman dan sumbangan menarik bagi perkembangan pemikiran perempuan di bidang seni budaya.
 
Etalase ini terbuka bagi para pelaku seni budaya, penonton dan pecinta seni, pemerhati persoalan kemasyarakatan termasuk isu-isu perempuan dan khalayak umum dengan pendaftaran karena kapasitas terbatas.

**\*\*SELESAI\*\***

---
**Tentang Sekolah Pemikiran Perempuan**<br/>
Sekolah Pemikiran Perempuan (SPP) adalah ruang untuk mengumpulkan dan mengasah pemikiran perempuan Nusantara dalam bidang seni budaya supaya bisa digunakan oleh masyarakat luas. Pemikiran perempuan Nusantara yang sangat dinamis kini masih tersebar dan berserakan, perlu dikumpulkan dan dijadikan pelajaran bersama.

Cipta Media Ekspresi, salah satu kegiatan SPP, adalah hibah dana tunai untuk merayakan keragaman pengetahuan dan ekspresi kreativitas perempuan di Indonesia, yang dilaksanakan tahun 2018/2019.

Keterangan lebih rinci mengenai jadwal dan acara dalam Etalase dapat diikuti melalui halaman [Etalase Pemikiran Perempuan 2020: Sebuah Festival]({{site.baseurl}}/ciptamediaekspresi/etalase-2020.html) dan berbagai kanal media sosial:
- Facebook	: [@ciptamedia](https://www.facebook.com/ciptamedia/)
- Twitter		: [@ciptamedia](https://twitter.com/ciptamedia)
- Instagram	: [@cipta.media.ekspresi](https://www.instagram.com/cipta.media.ekspresi/)
- YouTube	: [Cipta Media](https://www.youtube.com/channel/UCtShaHHIZrZ1iHa5bkH-Z_A)

**Kontak Media**<br/>
Sekar - 081212501033<br/>
<etalasepemikiranperempuan@gmail.com>







