---
title: Proposal Lengkap - Merekam Ingatan Perempuan - Raisa Kamila
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/merekam-ingatan-perempuan
layout: proposal
author: Raisa Kamila
categories:
- laporan
- CME
- Merekam Ingatan Perempuan
---

![0701.jpg](/static/img/hibahcme/0701.jpg){: .img-responsive .center-block }

# Raisa Kamila - Merekam Ingatan Perempuan

**Tentang penerima hibah**

Raisa Kamila lahir dan besar di Banda Aceh. Setelah menyelesaikan pendidikan tinggi di Fakultas Filsafat UGM, Raisa melanjutkan pendidikan pasca sarjana di jurusan Sejarah Kolonial dan Global Universiteit Leiden melalui skema beasiswa Encompass-COSMOPOLIS. Selain menulis cerita pendek dan essai, selama masa studinya, Raisa telah melakukan penelitian dan bekerja dalam kajian dekolonisasi, perempuan dan budaya populer.

**Kontak**

  - Instagram: [@perempuanmenulis](https://www.instagram.com/perempuanmenulis/)

**Lokasi**

Aceh, Jawa Tengah, Jawa Timur, Sulawesi Selatan, Nusa Tenggara Timur, Maluku Utara

**Deskripsi Proyek**

- **Tujuan**

  Untuk jangka pendek, proyek ini bertujuan untuk merekam ingatan, trauma, proses perubahan serta suara perempuan pada masa transisi dari Orde Baru hingga ke periode reformasi. Dalam jangka panjang, proyek ini dibayangkan dapat menjadi awal dari pembentukan forum penulis perempuan yang berada di luar medan sosial kebudayaan Indonesia.

  Proyek ini dimaksudkan untuk menyelami apa-apa yang berubah maupun tidak, di daerah-daerah luar ibukota dan dalam rentang waktu yang telah ditentukan, dengan menyoroti upaya, strategi dan berbagai hal yang dialami perempuan dalam menghadapi perubahan ini melalui riset di masing-masing daerah asal para penulis. Adapun narasi-narasi yang diperoleh melalui proses riset ini akan menjadi bahan untuk penulisan kumpulan cerpen.

- **Sasaran**

    - Mengumpulkan narasi-narasi di sekitar periode transisi Reformasi dari daerah-daerah di luar ibukota berdasarkan ingatan perempuan.

    - Menghimpun penulis perempuan berpotensi dari berbagai daerah di Indonesia.

    - Menjadi ruang alternatif bagi para penulis perempuan dari daerah untuk berjejaring, saling belajar, dan saling mendukung terkait proses kreatif serta produksi dan distribusi karya.

- **Latar Belakang**

  Ide mengenai proyek ini bermula dari diskusi awal saat kami bersepakat membentuk tim, yang berkutat di sekitar pertanyaan- pertanyaan seperti: menjelang dua dekade Reformasi, apa cerita-cerita yang luput dari pengetahuan publik? Bagaimana peran, posisi dan pengalaman perempuan dalam masa transisi ekonomi politik di Indonesia tersebut? Bagaimana para penulis sastra generasi pasca 1998 selama ini menuliskan perempuan dalam karya sastra? Pertanyaan-pertanyaan ini mejadi latar belakang inisiatif kami untuk membuat karya yang dapat mengisi narasi alternatif mengenai Reformasi.

  Momen keruntuhan Orde Baru yang kemudian melahirkan babak baru demokrasi di Indonesia, telah mengilhami banyak penulis untuk menghasilkan karya sastra (Saman- Ayu Utami, Pulang dan Laut Bercerita- Leila S. Chudori, Sekuntum Nozomi- Marga T, Notasi- Morra Quatro, Trilogi Soekram- Sapardi Djoko Damono, 1998- Ratna Iswandari Ibrahim, dlsb). Konflik sosial politik yang menjadi dampak dari perubahan besar ini, banyak dipakai sebagai titik pijak penceritaan karya-karya tersebut. Namun, narasi yang menjadi arus utama sebagian besar hanya mengambil latar di kota-kota besar yang menjadi pusat konflik, seperti Jakarta, Solo atau Yogyakarta dan cerita seringkali berkutat pada aktivisme mahasiswa.

  Padahal sebagai sebuah perubahan rezim yang bersifat struktural, dampaknya pun terjadi di berbagai wilayah di seluruh pelosok Indonesia, dengan konflik yang beragam pula. Alasan kami memilih merekam cerita-cerita dalam rentang waktu antara 1997-2004, adalah karena kami meyakini bahwa pada periode tersebut, dinamika sosial-politik Indonesia mengalami masa transisi yang sangat penting dan keberadaan perempuan, terutama dari luar arena utama, di tengah situasi tersebut penting untuk ditelusuri dan ditulis.

  1. Keterkaitan pada kategori: Kolaborasi

     Dalam proyek ini ada enam orang penulis perempuan yang terlibat, yaitu Raisa Kamila (Aceh), Ruhaeni Intan Hasanah (Jawa Tengah), Amanatia Junda Solikhah (Jawa Timur), Armandhany (Sulawesi Selatan), Maria Margareth R.F. (Nusa Tenggara Timur) dan Astuti N. Kilwouw (Maluku Utara). Terlepas dari latar belakang kebudayaan dan disiplin ilmu, tiap-tiap pelaku proyek memiliki pengalaman menulis dan berorganisasi, yang diharapkan dapat menjadi pengikat dalam mengerjakan proyek ini.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        1. Terbatasnya narasi mengenai proses transisi dari kejatuhan Orde Baru menuju Reformasi di luar kota-kota pusat ekonomi dan politik Indonesia, terutama yang bersinggungan dengan persoalan dan keseharian yang dialami perempuan di luar wilayah tersebut.

        2. Terbatasnya wadah interaksi kreatif bagi penulis dari luar medan sosial kebudayaan Indonesia, terutama perempuan, yang meskipun tidak memiliki latar belakang pendidikan sastra secara formal, namun berusaha untuk terlibat, baik secara aktif dan pasif, dalam dinamika wacana sastra di Indonesia maupun dalam proses penciptaan narasi yang mandiri.

  3. Strategi

     Tahap I Juli 2018 (4 hari), workshop yang mempertemukan keenam penulis dengan para pemateri yang terdiri dari peneliti, pengajar, dan penulis. Dalam workshop, penulis akan dibekali dengan materi terkait metodologi riset, perspektif sejarah dalam sastra, sejarah Indonesia modern, serta penulisan kreatif. Di samping itu, penulis juga akan mendiskusikan permasalahan mengenai perempuan dalam sastra Indonesia modern- kontemporer serta menggali ide potensial untuk keperluan menulis cerita pendek dalam focus group discussion bersama penulis yang lebih mapan. Melalui tahap pertama ini nantinya juga akan disepakati perspektif dan metode riset yang akan digunakan.

     Tahap II Agustus-November 2018, para penulis akan melakukan riset lapangan di daerah masing-masing dan juga melakukan eksplorasi arsip/bahan pustaka. Pada tahap ini, tiap penulis akan memulai proses penulisan berupa tiga buah cerita pendek.

     Tahap III Desember 2018, para penulis akan mengikuti post-writing workshop, untuk membahas temuan, kendala serta proses kreatif yang dialami selama proses riset.

     Tahap IV Januari 2019, proses penyuntingan naskah hingga naik cetak.

     Tahap V Februari 2019, penyusunan laporan pertanggungjawaban dan distribusi karya dalam bentuk buku cetak dan buku elektronik.

  4. Aktivitas dan keterkaitan pada sasaran

     Untuk mencapai sasaran kami melakukan:

        1. Riset untuk keperluan mengumpulkan narasi-narasi di sekitar periode transisi reformasi dari daerah-daerah di luar ibukota berdasarkan ingatan perempuan.

        2. Mengadakan workshop untuk mempertemukan penulis-penulis perempuan berpotensi dari berbagai daerah di Indonesia.

        3. Memanfaatkan media sosial sebagai sarana untuk mewujudkan ruang alternatif bagi para penulis perempuan dari daerah untuk berjejaring, saling belajar, dan saling mendukung terkait proses kreatif serta produksi dan distribusi karya.

  5. Latar belakang dan demografi pelaku proyek

     Raisa Kamila, 27 tahun, lahir dan berdomisili di Banda Aceh, pendidikan terakhir program pascasarjana Sejarah Kolonial dan Global Universitas Leiden. Saat ini tinggal di Aceh.

     Amanatia Junda, 27 tahun, lahir di Malang dan berdomisili di Sidoarjo, pendidikan terakhir program sarjana Ilmu Komunikasi Universitas Gajah Mada. Saat ini tinggal di Yogyakarta.

     Maria Margareth R. F., 26 tahun, lahir di Kupang dan berdomisili di Larantuka, pendidikan terakhir program sarjana Ilmu Sejarah Universitas Negeri Yogyakarta. Saat ini tinggal di Yogyakarta.

     Armadhany, 26 tahun, lahir dan berdomisili di Makassar, pendidikan terakhir program sarjana Sastra Inggris Universitas Ahmad Dahlan. Saat ini tinggal di Yogyakarta.

     Ruhaeni Intan Hasanah, 23 tahun, lahir dan berdomisili di Pati, pendidikan terakhir program vokasi Adminsitrasi Bisnis Politeknik Negeri Semarang. Saat ini tinggal di Yogyakarta.

     Astuti N. Kilwouw, 30 tahun, lahir di Ambon dan berdomisili di Ternate, pendidikan terakhir program pascasarjana Hukum Universitas Islam Indonesia. Saat ini tinggal di Ternate.

  6. Demografi kelompok target

     Pembaca sastra, aktifis, peneliti sejarah, kritikus sastra, dan masyarakat secara umum.

  7. Hasil yang diharapkan & Indikator keberhasilan

     Berhasil melakukan riset di wilayah yang telah ditentukan dan menuliskan serta menerbitkan hasilnya dalam bentuk kumpulan cerita pendek, secara cetak maupun digital (terdiri dari 18 cerpen).

  8. Durasi Waktu Aktivitas dilaksanakan

     Proyek ini berlangsung dari bulan Mei 2018 sampai dengan Februari 2019.

  9. Total kebutuhan dana

     135 juta rupiah.

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      100 juta rupiah.

  11. Sumber dana lainnya

      Belum ada.

  12. Kontribusi perorangan

      Menghasilkan cerita pendek sesuai dengan tema sebagai awal dari kerja kepenulisan yang lebih panjang.

  13. Kontribusi kelompok target

      Memberikan kritik, apresiasi, dan dukungan terkait penulisan fiksi yang berhubungan dengan sejarah, perempuan, dan daerah asal tiap-tiap pelaku proyek.
