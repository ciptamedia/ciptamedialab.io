---
title: Proposal Lengkap - Periuk Wanita Kalimantan Barat - Jelsy Meivira
date: 2018-12-30
permalink: /ciptamediaekspresi/proposal-lengkap/periuk-wanita-kalimantan-barat
layout: proposal
author: Jelsy Meivira
categories:
- laporan
- CME
- Periuk Wanita Kalimantan Barat
---

![0923.jpg](/static/img/hibahcme/0923.jpg){: .img-responsive .center-block }

# Jelsy Meivira - Periuk Wanita Kalimantan Barat

**Tentang penerima hibah**

Jelsy lahir dan besar di kota khatulistiwa, Pontianak. Ia merupakan seorang arsitek sekaligus pengusaha. Ia lulus dari Jurusan Teknik Sipil, Program Studi Arsitektur, Universitas Tanjungpura. Banyak hal yang telah dilalui dalam perjalanan hidupnya, ia sempat memiliki kepribadian yang anti sosial dikarenakan menjadi korban bullying di masa sekolah. Segala hal membaik ketika ia mengenyam pendidikan di bangku kuliah. Saat ini Jelsy turut serta dalam gerakan pemulihan mental anak korban pelecehan seksual.

**Kontak**

  - Instagram: [@jelsy_meivira](https://www.instagram.com/jelsy_meivira/)

**Lokasi**

Pontianak, Kalimantan Barat

**Deskripsi Proyek**

Periuk, sebuah kata yang merujuk kepada sebuah benda dan pada umumnya di jadikan sebagai salah satu alat untuk memasak. Periuk biasanya di gunakan untuk memasak nasi, yang merupakan makanan pokok di daerah Kalimantan barat. Di beberapa kabupaten/kota di Provinsi Kalimantan Barat masih banyak yang menggunakan alat ini untuk menanak nasi. Periuk pada umumnya terletak di dapur, dan di dapur sendiri adalah area memasak ibu. Dapur di mata saya adalah sebuah ruang tempat seorang ibu mengekspresikan cinta dan kasih sayangnya kepada suami dan anak-anaknya melalui masakkan, dan periuk merupakan salah satu symbol yang saya gunakan sebagai alat penyampai rasa cinta dan kasih tersebut. Melalui pandangan saya tersebut mendeskripsikan bahwa dapur adalah ruang otoritas seorang perempuan atau ibu di dalam rumah tangga dan rumahnya. Hal ini akan menjadi kebanggan tersendiri ketika sang ibu mampu menyajikan makanan dengan beragam menu dan di gemari oleh suami dan anaknya, dan akan menjadi sebuah boomerang ketika sang ibu tidak mampu memasak kembali dan akan di anggap “hilang” fungsi atau kuasanya di dalam sebuah rumah tangga. Ketika hal ini terjadi seolah-olah identitas perempuan yang bernama “ibu” tersebut akan hilang dan tidak mendapatkan posisi lagi di dalam ruangnya.

- **Tujuan**

     1. Membongkar kembali pehamaman bahwa keharusan perempuan di tuntut untuk bisa memasak dan ketika perempuan yang bisa memasak tidak dapat memasak kembali akan di anggap tidak seutuhnya menjadi perempuan

     2. Mengangkat hubungan ruang kasih antara Sang Ibu dan anak Perempuan dalam sebuah ruang dapur melalui sebuah objek yaitu periuk

        **Rumusan Masalah**

        Zona sebuah ruang yang menjadi penanda posisi dan identitas perempuan dalam lingkungan rumah tangga

- **Perencanaan proyek**

     1. Mewawancarai pemahaman dan sudut pandang setiap orang tentang kata “periuk”, “dapur” dan “ibu” yang kemudian saat ini di perkecil menjadi kata “periuk” dan “ibu”

        Target wawancara : anak perempuan dan ibu

     2. Mengkaji ulang kata periuk (selain dari KBBI) dan meninjau kembali hasil riset yang telah dilakukan sebelumnya

     3. Pemantapan konsep dengan riset dengan cara residensi salah satu alternatifnya

     4. Mencari referensi tambahan

     5. Pameran tentang periuk bisa berupa cerita dan arsip

     6. Mencari tahu siapa yang pernah melakukan pameran tentang periuk sebelumnya

     7. Menyusun timeline untuk melakukan riset

- **Output**

  1. Konsep catatan karya dan riset

  2. Konsep karya dari pengolahan hasil riset

- **Durasi Waktu Aktivitas dilaksanakan**

  4 bulan (Oktober 2018 - Februari 2019)

- **Total kebutuhan dana**

  Rp.20.000.000

- **Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi**

  Rp.20.000.000

- **Sumber dana lainnya**

  Belum ada
