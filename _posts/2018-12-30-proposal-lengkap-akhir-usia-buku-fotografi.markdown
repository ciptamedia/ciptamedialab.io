---
title: Proposal Lengkap - Akhir Usia Buku dan Pameran Fotografi - Azka Amalina
date: 2018-12-30
permalink: /ciptamediaekspresi/proposal-lengkap/akhir-usia-buku-fotografi
layout: proposal
author: Azka Amalina
categories:
- laporan
- CME
- Akhir Usia Buku dan Pameran Fotografi
---

![1155.jpg](/static/img/hibahcme/1155.jpg){: .img-responsive .center-block }

# Azka Amalina - Akhir Usia Buku dan Pameran Fotografi

**Tentang penerima hibah**

Azka adalah seorang fotografer dan ilustrator dari Yogyakarta. Lulus dari Institut Seni Indonesia jurusan fotografi pada tahun 2015, Azka kemudian mulai memotret pasangan & pernikahan sebagai pekerjaan utamanya. Terkadang Azka mengajar kelas fotografi untuk beberapa perkumpulan perempuan. Di rumahnya, ia membuat kelas seni rupa untuk anak-anak. Saat ini Azka sedang membuat buku cerita bergambar untuk anak dan juga mengerjakan proyek foto mengenai perempuan lanjut usia.

**Kontak**

  Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

- Pemotretan dilakukan di DI Yogyakarta

- Karya akan ditampilkan melalui sosial media: Instagram

**Deskripsi Proyek**

Pembuatan karya fotografi mengenai bagaimana perempuan lanjut usia memaknai dan menyikapi penuaan pada tubuhnya yang secara khusus adalah rambutnya

- **Latar Belakang**

  Selalu menjadi persoalan mengenai sejauh mana perempuan punya kuasa atas tubuhnya. Sistem dan tatanan kehidupan mengkonstruksi bagaimana tubuh perempuan harus terlihat, sehingga ketika selama ini perempuan ideal adalah kecantikan wajah, kulit yang mulus, maupun badan yang langsing, proses penuaan yang terjadi pada tubuh menjadi sesuatu yang tidak diterima. Pemikiran ini berulang-ulang diproduksi dan direproduksi yang mengakibatkan timbulnya perasaan dan keinginan untuk menghentikan penuaan, secara sadar maupun tidak. Padahal proses penuaan adalah mutlak dalam konteks tubuh. Salah satu proses penuaan yang paling jelas adalah berubahnya warna rambut menjadi putih atau beruban. Kemunculannya kemudian dianggap menjadi sebuah masalah yang ingin dihadapi. Menjadi menarik ketika ada berbagai macam respon-respon atas penuaan yang terjadi pada rambut perempuan lanjut usia, apakah itu sebuah penolakan, atau penerimaan, atau malah perayaan? Hal ini tentu juga dipengaruhi oleh banyak faktor lainnya seperti latar belakang pendidikannya, lokasi tempatnya tinggal, budaya, agama, dan lain sebagainya.

  Saya merasa proyek ini penting untuk saya kerjakan karena awal ketertarikan saya kepada perempuan lanjut usia, nenek saya, seseorang yang suatu hari adalah saya. Lalu ketertarikan tersebut berubah menjadi keingintahuan, yang lalu saya banyak mengamati bagaimanaperempuan lanjut usia menjalani hidupnya secara keseharian. Hal ini kemudian berkembang bersama seiring dengan aktifitas saya memotret mereka. Yang saya lihat bahwa ternyata banyak perempuan lanjut usia yang menggunakan penutup kepala atau hijab untuk menutupi ubannya, dimana keputusan ini memang muncul ketika ubannya mulai banyak, dan seringkali bukan karena faktor agama sebagai pertimbangan utama. Selain itu alternatif untuk menutupi ubannya yaitu adalah dengan mengecat rambutnya. Lebih menarik lagi karena tenyata strategi inipun juga dilakukan oleh perempuan yang memilih untuk menggunakan penutup kepala. Kenapa? Pertanyaan-pertanyaan memang akan selalu muncul, dan pertanyaan ini membantu untuk melihat sejauh mana kita menyadari kuasa kita atas sikap kita terhadap tubuh kita sendiri. Penerimaan adalah salah satu kata kunci yang muncul saat seorang perempuan memilih untuk merayakan ubannya, merayakan penuaannya.

  Kenapa menggunakan fotografi? Selain karena saya seorang fotografer, saya merasa bahwa aktifitas memotret sejalan dengan pendekatan yang dilakukan, yaitu mengamati dan berdialog. Dialog yang dilakukan selama proses pemotretan berlangsung merupakan salah satu faktor yang akan menjawab dan menentukan bagaimana mereka ingin terlihat dan dalam konteks ini bagaimana saya selaku fotografer memotretnya. Perencanaan atas bentuk visual dapat berkembang seiring dengan berjalannya percakapan.

  Penonton diharapkan dapat memaknai dan merefleksikan bagaimana mereka memandang tanda penuaan pada tubuh dan bagaimana menyikapi hal tersebut. Bagaimana hal yang tampak remeh dan ada dalam kesehari-harian dapat menjadi sebuah hal yang dipertanyakan ketika melihatnya dari sudut pandang kritis. Juga diharapkan dapat memunculkan diskusi-diskusi dan pertanyaan lain.

  Pembuatan karya ini dalam prosesnya secara tidak langsung akan mengajak perempuan lanjut usia kembali merefleksikan keputusannya mengenai sikap yang diambil atas penuaan yang terjadi pada tubuhnya.

  Selain itu karya ini merupakan sebuah kontribusi perempuan dalam ranah fotografi secara khusus terutama di indonesia, yang hingga kini masih didominasi laki-laki. Diharapkan adanya karya ini menambah wacana dan cara pandang terhadap perempuan dalam foto. Cara menatap merupakan salah satu poin utama yang perlu ditunjukkan.


- **Demografi Kelompok Target**

     - Perempuan lanjut usia (usia 65 tahun keatas) sebagai subyek. Akan dipilih atas dasar aspek-aspek seperti; apa yang tampak secara visual tentang bagaimana mereka menyikapi rambutnya; bagaimana latar belakangnya; pendidikan; budaya; lingkungan tempat tinggal; agama. Dari aspek-aspek tersebut dapat diambil beberapa agar dapat perbandingan dan variasi dari segi visual maupun kerangka berfikir individu. Akan dipilih kurang lebih 10 orang.

     - Perempuan indonesia berusia diatas 20 tahun sebagai target utama penonton

- **Strategi**

  Pembuatan karya terdiri dari proses pemilihan perempuan lanjut usia yang akan dan bersedia untuk difoto. Kemudian akan dilakukan pendekatan dengan observasi dan berdialog. Proses itu akan menentukan bagaimana masing-masing perempuan akan ditampilkan dalam foto. Pemotretan akan dilakukan kepada masing-masing subyek secara terpisah, dengan catatan setelah beberapa kali pertemuan, dan ketika konsep visual dan interpretasi masing-masing telah dirumuskan. Sehingga ragam waktu bisa berbeda tetapi kesemua akan dilakukan dalam waktu yang timpang tindih satu sama lainnya.

  Foto akan dibuat dengan format portrait. Semiotika dan simbol akan digunakan untuk menyusun narasi visual, juga aspek artistik dan teknis akan digunakan untuk menunjang karya.Setelah proses pemotretan selesai kemudian foto akan dipilih dan kemudian diedit.

  Hasil pembuatan karya fotografi ini akan dipamerkan melalui Instagram karena dirasa dapat menyasar target penonton dan juga dapat meraih penonton yang lebih banyak. Selain itu Instagram mempunyai fitur komentar yang dapat memberi ruang diskusi, juga fitur share agar persebaran narasi ini menjadi lebih mudah ketika dibutuhkan. Strategi untuk melakukan ini adalah dengan bekerjasama dengan beberapa akun instagram yang memiliki sekurang-kurangnya 10.000 pengikut yang termasuk dalam target penonton. Sehingga pembuatan teks caption merupakan elemen penting pada pendistribusian karya ini.


- **Durasi Waktu Aktivitas dilaksanakan**

  4 bulan (Oktober 2018 - Februari 2019)

- **Total kebutuhan dana**

  Rp20.000.000

- **Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi**

  Rp20.000.000

- **Sumber dana lainnya**

  Belum ada
