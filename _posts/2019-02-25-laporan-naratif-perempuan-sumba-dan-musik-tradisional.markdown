---
title: Laporan Naratif - Perempuan Sumba dan Musik Tradisional - Kahi Ata Ratu
date: 2019-02-25
permalink: /ciptamediaekspresi/laporan-naratif/perempuan-sumba-dan-musik-tradisional
layout: proposal
author: Kahi Ata Ratu
categories:
- laporan
- Laporan Naratif
- CME
- Perempuan Sumba dan Musik Tradisional
---

# Laporan Naratif - Perempuan Sumba dan Musik Tradisional

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Selama program, saya meneliti bentuk-bentuk kuno puisi tradisional Sumba yang disebut lawiti. Setiap generasi memiliki gaya lawiti yang berbeda, mereka juga berbeda dari satu daerah ke daerah lain. Diskusi, kolaborasi dan pertunjukan dengan spesialis bahasa ritual dan musisi generasi tua menggunakan gaya lawiti yang lebih tua ini untuk menginformasikan penciptaan karya-karya baru saya yang berkaitan dengan masalah-masalah kontemporer dan perjuangan sehari-hari kehidupan orang-orang di Sumba Timur.

Saya juga belajar pentingnya menciptakan peluang lebih lanjut untuk generasi muda pemain musik tradisional di seluruh Sumba Timur yang berpartisipasi dalam lokakarya dan pertunjukan yang didukung oleh program ini. Dengan terus memberikan dukungan untuk berbagi pengetahuan antar generasi, saya berharap masa depan musik Sumba tetap penting.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Aktivitas untuk sasaran 1 : Mencari orang yang mampu untuk menerjemahkan lagu–lagu saya dalam Bahasa Kambera (Bahasa Sumba), Bahasa Indonesia dan Bahasa Inggris.
>
> Aktivitas untuk sasaran 2 : Berkoordinasi dengan Dinas pendidikan Sumba Timur untuk dapat melibatkan anak–anak SD dan SMP dalam kegiatan lokakarya dan juga konser kolaborasi dengan mereka.
>
> Aktivitas untuk sasaran 3 : Melakukan rekaman audio dan video dengan seorang komposer.
>
> Aktivitas untuk sasaran 4 : Mengadakan konser tunggal di dua daerah yang berbeda untuk mempertunjukkan lagu–lagu lama yang sudah hampir punah itu sehingga orang–orang bisa mengenal kembali lagu–lagu lama Sumba Timur.

**Ceritakan hasil dari aktivitas tersebut.**

Semua aktivitas yang saya usung dalam program ini semuanya berhasil saya lakukan meskipun ada beberapa kendala yang saya hadapi dalam perjalanan yang cukup panjang ini. Tetapi hasil yang saya terima sangat memuaskan karena berkat kerja keras dan bantuan dari tim yang telah membantu saya untuk menyelesaikan proyek ini. Aktivitas dari sasaran 1 adalah mencari orang yang mampu menterjemahkan lagu-lagu yang telah saya ciptakan, hasilnya bisa lihat di wikimedia commons dalam buku yang telah dipublikasikan berjudul "Songs Of Ata Ratu". Di situ sudah tersedia terjemahan lagu-lagu saya dalam Bahasa Kambera, Bahasa Indonesia dan Bahasa Inggris. Untuk aktivitas dalam sasaran 2, bisa dilihat hasilnya dalam foto-foto dan juga video yang akan saya lampirkan dalam laporan saya ini dan di website ciptamedia. Untuk aktivitas sasaran 3, video lagu-lagu rekaman, riset dan kolaborasi dalam program yang sudah saya taruh dalam channel youtube saya (Channel: Ata Ratu) sebanyak 45 video dan juga di wikimedia commons. Di situ adalah hasil rekaman/shooting dengan seorang komposer/produser/dokumenter dari Australia yang sudah lama menekuni bidang ini lebh dari 20 tahun. Untuk aktivitas sasaran 4, konser yang saya adakan yaitu konser tunggal di Bentara Budaya Bali, Rumah Sanur Bali, Cafe Bohemia Ubud dan konser kolaborasi jungga di Taman Kota Sandalwood - Waingapu.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> 1. Publikasi yang terbatas.
>
> 2. Belum adanya pertunjukan yang secara khusus menampilkan karya–karya saya.
>
> 3. Belum ada pendokumentasian karya – karya saya.
>
> 4. Banyak lagu tua yang belum dikenal di generasi muda saat ini yang hampir punah.
>
> 5. Belum ada pertunjukkan yang mengkolaborasikan antara generasi tua dan generasi saya.

**Ceritakan hasil dari mengatasi isu tersebut.**

Dari isu yang saya angkat dalam prososal saya, akhirnya saya berhasil mengatasinya karena dengan proyek ini akhirnya lagu-lagu yang saya ciptakan sudah mulai terdokumentasi dengan baik dan juga mulai terpublikasi melalui buku lagunya saya ("Songs of Ata Ratu), 45 video Youtube/Wikimedia Commons (Kanal: Ata Ratu) dan juga rekaman audio (wikicommons). Saya bisa mengadakan konser tunggal yang bertajuk perempuan Sumba dan musik tradisionalnya dan akhirnya lebih banyak lagi orang - orang di luar sana yang mengenal instrumen Sumba yang dikenal dengan sebutan jungga. Juga dengan diadakan 2 kali lokakarya, akhirnya anak-anak generasi muda mendapatkan suntikan energi untuk lebih lagi mencintai musik tradisional jungga. Proyek ini juga menyatukan saya dengan beberapa pemain musik jungga sehingga kita bisa berkolaborasi bersama generasi tua dan muda dalam satu panggung konser jungga.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Riset dan kolaborasi dengan minimal 10 orang spesialis musik lagu dulu dari generasi tua dan juga seorang spesialis akan mengajarkan bahasa mendalam.
>
> 2. Ada minimal 3 pertunjukan (konser) musik tradisional Sumba dan lagu-lagu ciptaan saya di 3 lokasi untuk menampilkan lagu dulu yang sudah saya pelajari dan juga lagu yang baru saya ciptakan. Satu dari pertunjukkan ini akan dilaksanakan di Sekolah Dasar di Pahunga Lodu untuk mengajarkan atau melatih beberapa anak SD bermain musik tradisional dan akan melakukan konser bersama mereka.
>
> 3. Setiap pertunjukan dihadiri oleh 100 orang dari berbagai latar belakang pendidikan, status sosial, gender, dan usia.
>
> 4. Ada 300 booklet yang berisikan lagu-lagu yang saya ciptakan dan saya nyanyikan dalam konser. Saya menulis lirik dalam Bahasa Kambera (Bahasa Sumba), Bahasa Indonesia, dan Bahasa Inggris.
>
> 5. Ada dokumentasi audio video dengan semua kolaborasi para seniman jungga dan penyanyi dengan terjemahan Bahasa Sumba Kambera, Bahasa Indonesia dan Bahasa Inggris. (untuk contoh bisa lihat di situs web saya di www.ppp.virb.com).

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Saya telah berhasil untuk mencapai semua indikator sukses yang telah saya catumkan dalam proposal saya. Awalnya saya memulainya dengan melakukan riset terhadap lagu-lagu sumba dan juga mencari pemain - pemain jungga yang mahir memainkan alat musik jungga ini agar bisa berdiskusi mengenai lawiti dalam lagu - laguu sumba. Saya juga melakukan lokakarya dengan bekerja sama dengan pemerintah setempat gunanya agar lebih mudah melakukan pendekatan terhadap anak-anak generasi muda dalam membangun atau menciptakan kecintaan akan musik tradisional Sumba. Semua kegiatan yang saya lakukan dan juga rekaman-rekaman lagu dengan para seniman, saya dokumentasikan dalam bentuk video dan juga foto. Untuk video rekaman lagu-lagu instrumen musik jungga bisa dilihat dalam kanal youtube saya (Kanal: Ata Ratu) dan Wikimedia Commons.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Saya pikir program ini berhasil dalam tujuannya. Salah satu tujuannya adalah untuk meneliti lirik lagu yang lebih tua dan berkolaborasi dan mendokumentasikan pemain dari generasi yang lebih tua. Penelitian ini digunakan untuk membuat lagu-lagu baru yang menggabungkan puisi lama yang menanggapi situasi kontemporer dan keprihatinan orang-orang di Sumba Timur.

Perempuan sangat terwakili sebagai peserta dan penampil dalam program ini. Saya pikir program tersebut menunjukkan bahwa perempuan dapat menjadi contoh dan dapat memimpin cara untuk memfasilitasi komunikasi dan pertukaran antara generasi musisi yang lebih tua dan lebih muda di Sumba. Hubungan dan jaringan yang terbentuk selama program ini dapat digunakan di masa depan untuk mengembangkan hubungan ini lebih lanjut.

Terlepas dari masyarakat patriarkal yang kuat di Sumba, proyek Cipta Media ini telah membuktikan bahwa perempuan dapat menjadi contoh positif yang kuat dan memimpin cara untuk mempromosikan budaya musik Sumba Timur. Peserta perempuan dari rentang usia yang luas (10 hingga 73 tahun) berkontribusi pada proyek, khususnya untuk penelitian dan pertunjukan lagu panen lama. Program ini mengubah cara saya berpikir tentang masa depan musik Jungga Sumba. Di daerah kota seperti Waingapu dan daerah yang dekat dengan jalan utama di Sumba Timur anak-anak muda tidak begitu tertarik bermain musik Jungga lagi seperti di masa lalu. Dalam keluarga saya sendiri, tidak ada anak yang dapat saya bagi pengetahuan saya tentang musik Jungga dan Sumba. Untuk menemukan anak-anak kecil yang bisa bermain Jungga, perlu pergi ke daerah yang lebih terpencil (seperti Kauhung Eti dan Tabundung) di mana budaya musik tradisional lebih vital.

Setelah mengadakan kontak dengan para peserta lokakarya muda, pemain Jungga perempuan seperti Meyli, cucu perempuan Purra Tanya (yang juga tampil di Festival Jungga), dan para pemain muda Jungga seperti Jekson dan Ronaldo membuat saya menyadari pentingnya berbagi pengetahuan saya antar generasi sehingga musik Sumba tetap vital. Saya akan melanjutkan misi ini untuk berbagi pengetahuan yang saya miliki tentang Jungga. Harapan saya untuk masa depan adalah bahwa cucu-cucu saya dan lebih banyak anak kecil di Sumba akan tertarik, terinspirasi dan termotivasi untuk belajar menyanyi dan memainkan Jungga dan musik tradisional Sumba.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Tantangan yang dihadapi selama program termasuk mendapatkan terjemahan yang akurat dari ritual bait sajak orang Sumba. Spesialis yang bisa menjelaskan arti bait ini tidak berbicara bahasa Indonesia. Penerjemah bahasa Sumba tambahan terkadang diperlukan untuk menerjemahkan ke bahasa Indonesia. Selain bahasa Sumba ini bahasa Indonesia berbeda dari yang digunakan di Jawa, jadi pembacaan dan penyuntingan bukti tambahan diperlukan oleh penutur asli bahasa Indonesia dari Jawa.

Tantangan lain adalah menemukan wanita yang lebih tua yang bisa bermain Jungga dan yang bersedia atau mampu tampil. Banyak mantan pemain wanita telah berhenti tampil setelah mereka menikah. Karena banyak lagu Jungga Hau adalah lagu cinta, beberapa wanita yang sudah menikah tidak merasa nyaman atau pantas bagi mereka untuk menyanyikan lagu-lagu ini.

Sayangnya kami tidak dapat menemukan wanita selain Ata Ratu yang bisa memainkan gaya Jungga Humba yang lebih tua, ini adalah salah satu masalah utama yang kami identifikasi. Artis Jungga Humba perempuan dari generasi yang lebih tua sangat sulit ditemukan, kami berharap di masa depan upaya dilakukan untuk menemukan beberapa pemain perempuan yang tersisa di daerah yang lebih terpencil di Sumba Timur. Menemukan penyanyi wanita yang bisa membawakan lagu-lagu lama yang berkaitan dengan panen padi jauh lebih sukses dan penelitian dan pertunjukan lagu-lagu ini hampir secara eksklusif oleh wanita.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Saya pikir pertunjukan yang dibuat sebagai bagian dari program ini memperkenalkan banyak penonton di luar (dan di dalam) Sumba dengan bakat para pemain perempuan kami. Konser di Bali adalah pertama kalinya seorang seniman perempuan Sumba tampil di sana. Konser di Waingapu adalah festival Jungga pertama di Sumba Timur, dan yang pertama dikuratori oleh seorang wanita Sumba. Karena keragaman penonton konser, banyak orang terpapar pada perempuan yang melakukan dan mempromosikan musik tradisional, yang paling sering didominasi oleh laki-laki di Indonesia.
