---
title: Laporan Naratif - Pengadaan alat dan kostum grup Suluk Samudera - Dida Imada Maulina
date: 2019-01-17
permalink: /ciptamediaekspresi/laporan-naratif/suluk-samudra
layout: proposal
author: Dida Imada Maulina
categories:
- laporan
- Laporan Naratif
- CME
- Pengadaan alat dan kostum grup Suluk Samudera
---

# Laporan Naratif - Pengadaan alat dan kostum grup Suluk Samudera

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Selama menjadi penanggung jawab, saya tidak merasa bahwa menjadi beban sepenuhnya ditanggung oleh saya sendiri. Saya tidak merasa menjadi satu-satunya pemegang kendali. Namun, justru menyadari bahwa secara personal, kami saling mengisi dan menopang demi kemajuan grup ini. Memang para personil menjadikan saya sebagai sosok yang "dituakan", namun bukan berarti para personil grup tidak diberi hak untuk bicara dan bergerak. Intinya, dengan proyek ini, grup kami semakin hangat, kompak dan bersatu.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> 1. Pengadaan alat musik : alat musik disesuaikan dengan kebutuhan grup suluk samudera. Beberapa diantaranya dibuat secara custom untuk memaksimalkan kebutuhan dalam pembuatan karya. 
>
> 2. Pengadaan kostum : menjalin kerjasama dengan penata kostum di STSI Bandung 
>
> 3. Pembuatan karya : diadakannya latihan secara rutin kembali dengan proses setiap 2 kali dalam seminggu. Diharapkan dapat membuat suatu pertunjukan dengan sudah terpenuhinya alat musik dan kostum yang dibutuhkan grup suluk samudera.

**Ceritakan hasil dari aktivitas tersebut.**

Setelah mendapatkan dana, kami secepatnya mulai memperoses pemesanan/pembelian/pembuatan berbagai macam alat musik dan kostum yang kami butuhkan. Hal tersebut dapat diselesaikan dalam waktu beberapa bulan. Selama proses pengadaan alat musik dan kostum, kami pun tetap mengadakan pertemuan untuk berdiskusi ataupun latihan menggunakan alat seadanya. Atas dasar kontinuitas dan dukungan dari beberapa seniman senior, kami akhirnya berhasil mengaransemen serta menciptakan lagu-lagu khas grup kami. Secara perdana karya-karya tersebut telah dipentaskan dalam acara Seni Bandung di IFI Bandung. Kami bahagia karena di penampilan pertama setelah lama vakum kami diapresiasi dengan baik.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Grup Suluk Samudera adalah grup musik tradisi Sunda yang beranggotakan 10 orang wanita. Grup ini terbentuk pada tahun 2010, saat kami menjadi mahasiswa di Sekolah Tinggi Seni Indonesia (STSI) atau yang sekarang menjadi Institut Seni Budaya Indonesia (ISBI) Bandung. 
>
> Pada saat masih menjadi mahasiswa, grup yang seluruhnya beranggotakan wanita masih sangat jarang, karena wanita masih dianggap tabu atau belum lazim menjadi pemain alat musik, apalagi sebagai pembuat karya. Walaupun demikian, pada saat itu kami tetap aktif melakukan latihan dan membuat karya. 
>
> Pada prosesnya, status mahasiswa memudahkan kami untuk meminjam ruangan ataupun berbagai alat musik serta perlengkapan lainnya seperti sound ataupun kostum, dari pihak kampus. Namun kami mengalami masa vakum setelah kami lulus kuliah, karena selain kami sulit mendapatkan sarana prasarana, pernikahan dan kewajiban mengurus anak pun menjadi kendala. Selama mengalami masa vakum tersebut, kami merasa dikebiri karena potensi dan keinginan kami seolah dipaksa untuk dihentikan begitu saja. 
>
> Beberapa tahun kemudian (tahun 2017), akhirnya kami memutuskan untuk kembali mengadakan latihan walaupun harus sambil membawa anak-anak kami. Kami berpikir bahwa mengurus keluarga memang adalah keharusan, namun bukan berarti kami harus berhenti mengembangkan diri kami. Namun gagasan tersebut terhambat karena kami tidak memiliki alat musik dan kostum yang layak untuk proses latihan, berkarya, dan pentas.


**Ceritakan hasil dari mengatasi isu tersebut.**

Dengan mendapatkan Hibah dari CME, semangat dan keyakinan kami untuk tetap produktif berkarya menjadi semakin tinggi. Walaupun hingga saat ini masih jarang grup musik yang seluruhnya beranggotaan perempuan, tapi berkat kontinuitas dalam berlatih dan produktivitas dalam berkarya, membuat beberapa pihak (sesama seniman, akademis seni masyarakat umum, dsb.) menjadi tertarik untuk setidaknya melihat dan memperhatikan kami. Selanjutnya mereka mempertanyakan siapa kami dan apa yang kami lakukan. Dari situ kami memulai, hingga kemungkinan karya-karya kami diapresiasi. Atas dasar tanggapan positif dari sekeliling, kami berharap perempuan tidak hanya dihargai karena keunikannya tetapi juga karena karyanya.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> Proyek ini dikatakan sukses jika kami sudah bisa berproses secara rutin dan pentas dengan alat-alat musik dan kostum yang memadai.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Secara umum kami merasa sudah cukup sukses karena berhasil mengadakan pertemuan untuk berdiskusi dan berlatih secara berkala walaupun pada prosesnya ada kalanya tidak selalu 100% personil hadir, serta waktu pertemuan tidak selalu tetap karena kami harus selalu menyesuaikan dengan kepentingan pribadi kami, terutama menyangkut keluarga (suami dan anak kami). Namun walaupun begitu, kami selalu bisa menyediakan waktu untuk berlatih secara pribadi atau kelompok.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Yang kami rasakan saat ini, terdapat hal-hal yang tetap dan berubah. Hal-hal yang tetap seperti semula, misalnya keyakinan kami bahwa perempuan, terutama yang sudah menikah tak berarti dibatasi ruang geraknya hanya di ranah domestik saja namun juga berhak mengembangkan diri sesuai dengan minatnya. Selain itu, kami menjadi yakin bahwa kontinuitas dan produktivitas tidak dibatasi oleh gender. Adapun hal yang yang berubah yakni pada proses kami setelah mendapatkan dana. Setelah mendapatkan dana, kami menjadi mandiri. Kami benar-benar bekerja tanpa bergantung pada institusi karena kami sudah memiliki fasilitas yang kami rasa cukup memadai. Selain itu, berkat hal itu, kami pun dapat meyakinkan keluarga, terutama suami kami, bahwa kami menjadi istri yang produktif dan terus mengembangkan pengetahuan, namun tetap mandiri dan tidak merugikan kepentingan keluarga.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Tantangan/masalah yang kami hadapi di antaranya, pertama, berkaitan dengan keluarga, bagaimana pada awalnya cukup sulit meyakinkan suami agar mendukung segala kegiatan kami dan bersedia bergantian mengasuh anak-anak kami. Kendala selanjutnya dalam keluarga adalah kondisi anak. Ketika anak sakit maka kami tidak bisa meninggalkannya sehingga bolos untuk berlatih. Atau ketika tidak ada yang mengasuh anak, maka kami harus membawanya saat berlatih. Pada saat  kondisi itu kami harus ekstra kreatif untuk menenangkan anak agar dapat fokus berlatih. Kendala selanjutnya adalah ketika alat-alat musik sedang diproduksi tidak sesuai dengan kualifikasi kami sehingga memerlukan beberapa revisi dan membuang lebih banyak waktu. Kendala lainnya adalah tempat berlatih dari satu tempat ke tempat lainnya sampai-sampai kami sering berlatih di luar ruangan dan terganggu oleh cuaca ataupun bising dari lingkungan. Kendala terakhir adalah dana tambahan. Kami perlu mencari dana tambahan untuk memenuhi kebutuhan sarana dan prasarana grup termasuk dana untuk belajar.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Proyek ini menambah keragaman ekspresi perempuan yakni ekspresi kami para pemain alat musik tradisional Sunda yang biasanya identik dengan laki-laki namun kini dimainkan oleh kami para perempuan. Dan kami pun mencerminkan ekspresi para perempuan yang sudah menjadi seorang istri dan seorang ibu yang bebas mendapatkan hak untuk mengembangkan diri dan menyalurkan minat.
