---
title: Proposal Lengkap - Yang Liar Memberi Harapan - Monika Swastyastu
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/yang-liar-memberi-harapan
layout: proposal
author: Monika Swastyastu
categories:
- laporan
- CME
- Yang Liar Memberi Harapan
---

![0729.jpg](/static/img/hibahcme/0729.jpg){: .img-responsive .center-block }

# Monika Swastyastu - Yang Liar Memberi Harapan

**Tentang penerima hibah**

Monika Swastyastu memiliki latar belakang pendidikan S1 Antropologi UGM, merupakan anggota dari Bakudapan Food Study Group. Sebagai individu, Monika aktif melakukan beberapa penelitian tentang isu ketahanan pangan, identitas, migran dan memiliki fokus terutama pada isu makanan dan politik pangan.  Proyek “Yang Liar Membe- 729 - Monika Swastyastu - Yang Liar Memberi Harapan” merupakan proyek yang akan dikerjakan bersama antara Bakudapan dan Marcellina Dwi Kencana Putri serta  beberapa kontributor lainnya.

**Kontak**

  - Facebook: [facebook.com/bakudapan](https://www.facebook.com/bakudapan/)

  - Instagram: [@bakudapan](https://www.instagram.com/bakudapan/)

  - Website: [bakudapan.com](http://bakudapan.com/id/)

**Lokasi**

Yogyakarta

**Deskripsi Proyek**

Yang Liar Memberi Harapan” adalah proyek produksi (penulisan dan penerbitan) jurnal  yang mengangkat narasi pangan liar dari perspektif perempuan. Pemilihan judul ini, selain karena bahasan utamanya yang berupa pangan liar, juga karena pengetahuan yang berada di seputarnya sering dianggap bukan pengetahuan dalam terminologi ilmu yang formal. Jurnal akan membahas sejarah pangan liar, pangan liar hari ini dalam konteks pengetahuan dan politik pangan, serta masa depan beserta tantangannya sebagai alternatif pangan. Perempuan dari generasi yang berbeda akan dilibatkan untuk memperkaya perspektif dalam memahami konteks sejarah, sosial dan politik yang melatarbelakangi kondisi setiap generasi. Sebelumnya narasi tentang pangan liar dari perspektif perempuan jarang dituliskan, diarsipkan dan didistribusikan selain dalam buku resep. Upaya penerbitan jurnal dengan lisensi terbuka CC-BY dianggap penting dimana isu domestik menjadi terhubung dengan dimensi sosial politik yang luas.

- **Tujuan**

  Mengarsipkan dan mendistribusikan pengetahuan tentang pangan dan perempuan terutama dalam isu pangan liar, yang sering dianggap sebagai isu domestik dan dipandang sebelah mata, tetapi sesungguhnya terhubung dengan dimensi sosial politik yang luas.

- **Sasaran**

     1. Memperluas jaringan individu dan kolektif yang bergerak bersama terutama dalam isu pangan dan perempuan.

     2. Menuliskan data-data penelitian yang telah dilakukan terkait dengan isu pangan liar, menggunakan perspektif perempuan.

     3. Menempatkan wacana pangan dan perempuan dalam posisi yang terbuka, terlepas dari kungkungan domestik, untuk didiskusikan bersama.

- **Latar Belakang**

  Pada Februari 2016, Bakudapan mengikuti residensi Dapur Komunitas di Dusun Mendira, Jombang bersama Mantasa dan Komunitas Perempuan Sumber Karunia Alam yang mengedukasi bahwa tanaman liar yang banyak tumbuh di pekarangan dan hutan dapat dimanfaatkan. Para Ibu kemudian mendirikan kebun bersama untuk membudidayakan beberapa tanaman liar yang bisa diolah. Hasil dari residensi ini membekali kami dengan pengetahuan serta tantangan dalam melihat pangan liar di konteks perkotaan, tempat kami tinggal. Ketertarikan ini kemudian membawa kami pada narasi sejarah tentang para perempuan penyintas eks-tapol 65 yang juga memanfaatkan pangan liar sebagai strategi bertahan hidup semasa di kamp. Kami melihat, setiap masa dan setiap tempat memiliki pengetahuan lokal dan potensinya sendiri dalam soal keberadaan sumber pangan. Bagaimana peran perempuan dalam konstruksi nilai-nilai sosial budaya serta ekonomi dalam persoalan pangan merupakan hal utama yang menarik bagi kami. Melalui narasi-narasi kecil pangan liar, kami ingin menyasar pada persoalan politik pangan yang lebih luas, khususnya dari perspektif perempuan. Sebab, sejauh ini peran perempuan dalam perihal domestik (dapur) sering dikecilkan dan didepolitisasi hanya sebagai subyek yang memasak untuk memenuhi kebutuhan keluarga.

  1. Keterkaitan pada kategori: Riset / Kajian / Kuratorial & Lintas Generasi

     Keterkaitan pada kategori Riset kami pilih karena penelitian mengenai Pangan Liar telah dilakukan oleh Bakudapan dan kontributor jurnal jauh sebelum proyek ini dilaksanakan. Sebagai tindak lanjut dari pengumpulan data tersebut, diperlukan proses analisis data, lokakarya kepenulisan, hingga menjadi sebuah jurnal yang nantinya akan didistribusikan dan dapat diakses secara luas oleh publik.

     Keterkaitan pada kategori Lintas Generasi, karena dalam proyek produksi (penulisan dan penerbitan) jurnal, kami melibatkan beberapa perempuan dari generasi berbeda yang berasal dari beberapa lokasi dan memiliki latar belakang historis yang beragam. Anggota Bakudapan sendiri seluruhnya adalah perempuan dari generasi muda yang dalam proses riset akan bersinggungan dengan ibu-ibu dan perempuan yang diantara mereka terhadap kesenjangan pengetahuan yang perlu dijembatani melalui proses pengumpulan data dan dalam proses lokakarya jurnal tersebut.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Peran perempuan dalam membentuk pengetahuan dan politik pangan perlu dikaji lebih jauh. Misalnya bagaimana doktrin pemerintah tentang apa yang dianggap makanan baik, konstruksi budaya modern yang membuat perempuan melakukan pilihan konsumsi hingga persoalan apa yang disediakan oleh pasar seolah merupakan hal yang terjadi dan diterima begitu saja. Padahal dibalik itu ada kuasa yang bekerja dan patut kita sadari, salah satunya dengan mengetahui banyak narasi dan pengetahuan soal pangan di luar wacana besar tersebut. Berawal dari sejarah bagaimana sebuah tanaman pangan dibudidayakan, menjadi ketertarikan kami dalam kaitannya dengan ekonomi dan pasar. Selain itu melalui belajar langsung di Mendiro yang membudidaya apa yang secara umum dikategorikan pangan liar, menantang kami berpikir lebih jauh tentang potensinya. Juga keterkaitan pangan liar yang dimarjinalkan dalam ekonomi pangan, sama dengan yang terjadi pada penyintas eks-tapol 65, dimana mereka juga mengkonsumsinya sebagai metode bertahan. Kami melakukan riset dan uji coba dalam melihat kemungkinannya dalam latar belakang kehidupan kota, misalnya tentang akses dan ketersediaannya di pasar. Harapannya jurnal ini selain memuat beberapa tulisan pemaparan, refleksi dan kajian dari pangan liar juga supaya dapat meneruskan rantai pengetahuan pangan yang terputus antar generasi. Jurnal ini juga akan dilengkapi dengan resep, identifikasi tumbuhan liar beserta narasi sosial budaya yang menyertainya.

  3. Strategi

     Strategi yang dilakukan dalam proyek ini adalah menerbitkan jurnal pangan liar dari perspektif perempuan dengan konsep jurnal populer, gaya bahasa penulisan yang lugas namun mudah dipahami, pendekatan artistik yang menunjang, sehingga dapat mudah dibaca dan dimengerti oleh masyarakat dari latar belakang yang berbeda-beda.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Kontribusi untuk Sasaran A: Mengadakan diskusi awal terkait isu pangan liar dari sudut pandang perempuan yang melibatkan beberapa peneliti, seniman, aktivis perempuan, dan aktivis lingkungan, untuk bersama-sama merumuskan konten dari jurnal yang akan diproduksi.

        2. Kontribusi untuk Sasaran B: Mengadakan lokakarya penulisan bersama kontributor jurnal, editor, ilustrator, desainer untuk merumuskan tulisan, dan artistik dari jurnal yang akan diproduksi.

        3. Kontribusi untuk Sasaran C: Menerbitkan jurnal pangan liar dari perspektif perempuan dan mendistribusikannya secara luas sehingga dapat menimbulkan dialog dan diskusi terbuka antar individu maupun kolektif.

  5. Latar belakang dan demografi pelaku proyek

     Pelaku proyek terdiri dari sepuluh perempuan dan dua orang laki-laki. Sepuluh perempuan tersebut terdiri dari berbagai usia dengan rentang usia 24-55 tahun, memiliki latar belakang yang bervariasi yakni sebagai penulis, aktivis perempuan, aktivis lingkungan, seniman, peneliti, serta pekerja lepas. Sedangkan dua orang laki-laki yang terlibat di proyek ini memiliki rentang usia 35-60 tahun, memiliki latar belakang sebagai petani, pegiat konservasi keanekaragaman hayati dan aktivis lingkungan.

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman tiga tahun bergerak dalam isu pangan, memiliki pengalaman manajerial, serta  penelitian tentang isu sosial dengan pendekatan antropologi.

  7. Demografi kelompok target

     Masyarakat luas, kolektif dan aktivis pangan dan perempuan, yang terdiri dari usia, gender, latar belakang yang berbeda-beda.

  8. Hasil yang diharapkan & Indikator keberhasilan

     Harapannya jurnal ini selain memuat beberapa tulisan pemaparan, refleksi dan kajian dari pangan liar juga supaya dapat meneruskan rantai pengetahuan pangan yang terputus antar generasi. Jurnal ini juga akan dilengkapi dengan resep, identifikasi tumbuhan liar beserta narasi sosial budaya yang menyertainya.

  9. Indikator keberhasilan

        1. Jurnal populer dengan kajian ilmiah yang memuat bahasan persoalan pangan liar dari beragam perspektif serta komprehensif yang dapat terbit sesuai dengan garis waktu yang ditentukan. Sejauh ini kami merencanakan akan ada 7 sub-bahasan yang mengkaji persoalan pangan liar dari berbagai sudut pandang.

        2. Menghasilkan pengetahuan serta sudut pandang baru dan kritis dalam persoalan pangan, peran perempuan dalam politik pangan, dan menjembatani jarak pengetahuan soal pangan antara perempuan dulu dan sekarang.

        3. Dapat didistribusikan baik langsung (dengan acara peluncuran) maupun tidak langsung ke beberapa tempat di Indonesia.

        4. Menginspirasi proyek-proyek atau program-program sejenis yang berkelanjutan dan saling terhubung untuk membangun relasi dan pengetahuan yang berkelanjutan.

  10. Durasi Waktu Aktivitas dilaksanakan

      Mei 2018 - Februari 2019

  11. Total kebutuhan dana

      Rp.68.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.68.000.000

  13. Sumber dana lainnya

      Tidak ada

  14. Kontribusi perorangan

      Seluruh anggota tim dalam proyek ini akan bekerja dengan memaksimalkan sumber daya, pengetahuan, modal sosial yang dimiliki tiap individu untuk dapat menjalankan proyek ini dengan hasil yang maksimal.

  15. Kontribusi kelompok target

      Kelompok target dapat berkontribusi melalui proyek ini dengan membaca jurnal yang telah diterbitkan, yang harapannya dapat membuka wawasan, menimbulkan ruang-ruang diskusi terbuka untuk membicarakan dan menindaklanjuti hal-hal yang terkait dengan isu pangan dan perempuan yang sering kali dianggap sebagai wilayah domestik dan dipandang sebelah mata.
