---
title: Laporan Naratif - Aku Perempuan Unik - Nanik Indarti
date: 2019-01-14
permalink: /ciptamediaekspresi/laporan-naratif/aku_perempuan_unik
layout: proposal
author: Nanik Indarti
categories:
- laporan
- Laporan Naratif
- CME
- Aku Perempuan Unik
---

# Laporan Naratif - Aku Perempuan Unik

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Pada proyek ini saya belajar banyak hal selama menjadi penanggungjawab proyek ini. Saya belajar menjadi seorang leader (pemimpin) dan menjadi kendali utama. Saya belajar mengelola waktu, mengelola keuangan, mengelola manusia dan mengelola tempat.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> 1. Menjalin kerja sama dengan para narasumber lintas wilayah di Indonesia untuk menciptakan ruang kreatif bersama sebagai wadah ekspresi.
>
> 2. Menjalin network dengan penerbit
>
> 3. Berkomunikasi dengan narasumber yang dilakukan dengan wawancara, diskusi untuk mendalami konten-konten sebagai bahan karya

**Ceritakan hasil dari aktivitas tersebut.**

1. Saya telah menjalin kerja sama dengan para narasumber lintas wilayah, hal ini telah terwujud dan dapat terbukti dengan kolaborasi bersama 11 orang laki-laki dan perempuan yang bertubuh mini. Wujud kolaborasi yang dihadirkan para narasumber tidak  hanya pada 11 orang bertubuh mini tapi juga melibatkan narasumber selain bertubuh  mini. Hal ini saya hadirkan sebagai sumber untuk mengumpulkan data dan sebagai testimoni atas penghargaan terhadap 11 orang bertubuh mini. Lintas wilayah yang terwujud terbukti dengan kehadiran narasumber dari berbagai kota yang ada di wilayah Indonesia. Wilayah tersebut antara lain, Aceh Kalimantan, Banyuwangi, Surabaya, Malang, Blitar dan Yogyakarta.

2. Menjalin network dengan penerbit, hal ini pertama kali saya lakukan. Saya belajar banyak hal, mulai dari sistem produksi sampai percetakan. Selain itu juga saya belajar untuk melobi hal-hal yang terkait dengan buku. Tantangan ini benar-benar saya rasakan. Mulai  dari menghitung waktu cetak sampai menghitung jumlah lay out buku. Kerja sama ini akhirnya menjadi sebuah network yang menguntungkan bagi saya. Berkomunikasi dengan narasumber melalui wawancara, diskusi untuk mendalami konten-konten sebagai bahan karya : Bagi saya penting untuk mengetahui sisi dalam narasumber sebagai bahan pijakan saya untuk berkarya. Sehingga hal yang saya wujudkan bersama narasumber adalah wawancara dan diskusi. Dari pembicaraan-pembicaraan inilah saya gunakan data sebagai bahan untuk menulis dan menciptakan konten pertunjukan. 

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Karya Aku Perempuan Unik mengangkat persoalan-persoalan perempuan penyandang achondroplasia yang mengalami eksploitasi dan deskriminasi dalam kehidupan sosial masyarakat. Melalui media seni akan mengungkapkan peristiwa-peristiwa pengalaman hidupnya dan rahasia-rahasia perempuan tersebut yang akan dituangkan dalam sebuah buku untuk dibaca bersama dan akan divisualkan dalam bentuk pertunjukan. Dengan adanya karya ini harapannya mampu menginspirasi para penyandang achondroplasia lainnya untuk bangkit. Dengan adanya hibah dana ini diharapkan dapat memberi dukungan berupa fasilitas yang dibutuhkan demi mewujudkan proyek ini.

**Ceritakan hasil dari mengatasi isu tersebut.**

Hasil dari isu-isu tersebut adalah adanya pengetahuan baru bagi saya dan teman-teman mini serta masyarakat (penonton) mengenai kata Penyandang Achondroplasia. Hal yang baru ini berdampak pada isu persoalan hambatan umum  bagi penyandang  Achondroplasia dan disabilitas.

Dalam pertunjukan Sepatu Yang Sama: Kisah Jiwa dan Angka, sangat jelas telah dihadirkan melalui konten-konten yang ada di dalam pertunjukan tersebut. Sehingga isu mengenai diskriminasi bagi penyandang achondroplasia bisa disampaikan kepada penonton untuk dibaca bersama, dipikir ulang dan sebagai kritik sosial terhadap pemerintah untuk mencari solusi. Penonton akhirnya berpikir ulang dan memiliki sudut pandang lain terhadap isu yang disampaikan dalam pertunjukan.

Seperti beberapa tanggapan dari seorang penonton dia mengatakan bahwa:

1. Melihat foto-foto yang dipasang lebih tinggi daripada biasanya, dan kepala harus mendongak ke atas. Inilah yang dirasakan teman-teman ‘mini’ ketika melihat pameran atau apapun, pasti menyebalkan. Suara-suara rekaman itu sangat menyeramkan terdengar. Kisah teman-teman ‘mini’ membuatku tak kuasa menahan air mata. Aku menangis. Hai mbak Nanik, terima kasih sudah memberikan pementasan yang membuatku termotivasi untuk menjalani hidup mulai malam itu. Kamu tau, aku belum bisa melupakan pementasanmu. Bahkan aku bukan siapa-siapa, bila kamu menjelaskan tentang kisah hidupmu. Aku yakin menjadi dirimu dan teman-teman ‘mini’ tidak mudah hingga hari ini. Tapi aku percaya, kamu mempunyai kekuatan yang besar untuk memberikan dampak positif bagi orang-orang di sekelilingmu. Mbak...aku bersyukur bisa berkenalan denganmu, bisa melihat pementasanmu walau aku harus  datang jauh-jauh dari Surabaya. Kamu mengubah pikiranku tentang arti hidup. Sungguh. (Byta – Surabaya).
  
2. Pementasan ini sungguh mengharukan. Membuat emosi menjadi campur-aduk. Sangat menampar bagiku sebagai orang yang bertubuh normal. Jujur, aku meneteskan air mata apalagi saat Mbak Anggi membicarakan persoalan-persoalan maupun hambatan-hambatan yang dialami orang-orang bertubuh mini (Achondroplasia). Aku menjadi diingatkan kembali. (Wahono – Seniman).
  
3. Aku merinding. Momen yang sangat mengharukan. Tubuh kalian memang mini tapi semangat serta perjuangan kalian sangatlah besar. Dan aku salut dengan Mbak Nanik bisa menghadirkan orang-orang dari berbagai genre dan berbagai penjuru Indonesia  untuk berkesenian bersama menghadirkan pementasan yang epic. Itu aku akui sulit menghandle segalanya. (Hanna - Klaten).

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> Keberhasilan proyek ini akan tercapai ketika dapat menyelesaikan sesuai dengan waktu yang telah direncanakan. Perencanaan-perencanaan terlaksana dengan baik dan lancar dalam biaya yang efisien. Mampu mengungkapkan cerita ke dalam kemasan tertulis (cetak) maupun karya pertunjukan yang dapat mewadahi ide-ide, gagasan berdasarkan pengalaman pelaku dalam lingkungan seni dan sosial masyarakat. Cerita yang disampaikan dapat mengungkapkan pesan-pesan, atau isu-isu yang membuka sudut pandang audiens. Hibah dana untuk proyek ini hanya akan digunakan untuk menyelesaikan proyek ini hingga selesai.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Proyek Aku Perempuan Unik telah mencapai keberhasilan dan sukses, karena semua indikator sukses telah terwujud dan dapat direalisasikan dengan baik. Saya telah mewujudkan karya dalam bentuk tulisan yaitu launching buku Aku Perempuan Unik dan saya telah mewujudkan pertunjukan teater Sepatu Yang Sama, Kisah Jiwa dan Angka. Namun mengenai target waktu ada beberapa hal yang tidak tercapai. Hal ini disebabkan kondisi di lapangan yang tidak bisa dihindari. Seperti persoalan kesehatan pemain yang tidak bisa hadir latihan karena sakit dalam waktu yang lama. Kondisi seperti ini membutuhkan strategi baru untuk mencapai target yang diinginkan. Akhirnya strategi yang saya munculkan adalah pendekatan kepada pemain secara personal. Saya pun harus mendatangi rumahnya untuk melakukan diskusi. Beberapa hal lain yang tidak tercapai yaitu pendokumentasian baik tulisan maupun rekaman video. Hal ini dikarenakan saya tidak memasukkan anggaran berupa alat seperti kamera yang dapat mendukung proses ini.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Tujuan proyek ini telah berhasil saya capai. Wujud pencapaian itu adalah pada kerja sama kolaborasi saya dan teman-teman mini serta tim kerja  yang mendukung karya ini. Saya telah berhasil menghadirkan teman-teman mini dari berbagai kota. Meskipun terdapat satu orang dari Aceh yang tidak bisa terlibat karena ada hal yang berkaitan dengan tugas yang tak bisa ditinggalkan. Kerja kolaborasi ini merupakan perubahan cara kerja saya sebagai penggagas. Perubahan cara berpikir saya yang mau mengajak orang lain untuk mewujudkan gagasan saya. Melalui  cara-cara kreatif, berdiskusi membicarakan banyak hal mengenai gagasan lalu berlatih, bertemu dan pentas bersama dalam satu panggung. Selain itu keberhasilan yang lainnya adalah terwujudnya karya tulis " Aku Perempuan Unik"  dalam bentuk sebuah buku yang berhasil ditulis oleh tujuh perempuan bertubuh mini. Ini juga  sebagai wujud kolaborasi, karena saya bekerja sama dan berkolaborasi bersama dua orang editor buku serta lay outer serta  yang bersedia terlibat untuk mewujudkan gagasan saya. Proyek ini telah membawa perubahan cara berpikir saya maupun cara kerja.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Proyek Aku Perempuan Unik ini merupakan proyek yang besar bagi saya. Banyak tantangan ataupun masalah yang saya hadapi. Masalah yang pertama adalah persoalan menulis. Proses menulis yang dimulai sejak bulan April - Agustus dengan harapan sudah selesai. Tetapi tidak demikian, beberapa penulis mengalami kendala kesehatan sehingga proses menulis menjadi tertunda. Untuk mengatasi hal tersebut, hal yang saya lakukan adalah memaksimalkaan tulisan yang telah terkumpul dan memasukkan ke editor untuk diedit. Persoalan kedua adalah, beberapa penulis menambahkan materi ke dalam tulisannya sehingga saya harus membatasi.  Persoalan ketiga adalah menentukan konten (materi) dari bahan tulisan, ini menjadi terkendala karena tulisan yang masuk (selesai) hanya beberapa sehingga langkah yang diambil untuk menentukan konten (materi) adalah tulisan yang ada. langkah ini tidak mengurasi materi pertunjukan karena saat mendekati pentas semua materi konten terpenuhi seiring berjalannya proses berlatih. Persoalan keempat saat menentukan judul pertunjukan. Beberapa kali mengganti judul karena ada beberapa pihak yang tidak menyukai kata " Penyandang Achondroplasia" sehingga kata ini dihilangkan, tidak jadi dipakai untuk menjadi bagian dari judul pertunjukan. Persoalan kelima  adalah perubahan naskah yang selalu berkembang hingga 8 kali berubah. Bahkan saat pentaspun ada beberapa bagian naskah yang ditambahkan oleh sutradara. Persoalan yang lain adalah pengelolaan terhadap MANUSIA, banyak sifat dan karakter yang berbeda-beda. Sehingga hal yang saya lakukan adalah mengajak berbicara secara personal. Persoalan lainnya adalah WAKTU setiap orang yang terlibat dalam proses kolaborasi ini merupakan orang-orang yang memiliki kesibukan tinggi. Sehingga saya butuh strategi lain untuk mewujudkan semuanya menjadi lancar. Hal yang saya lakukan adalah meeting per divisi. Dalam proyek Aku Perempuan Unik ini, saya benar-benar belajar menjadi pemimpin untuk mengelola 100 manusia yang terlibat. Tantangan saya sebagai penggagas, pemimpin, aktor, penulis dan penanggungjawab terhadap proyek ini, benar-benar kompleks. Saya ingin melakukannya lagi.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Bagi saya proyek ini benar-benar telah/dapat mewujudkan proyek perempuan yang telah/dapat memperkaya ekspresi perempuan melalui proyeknya masing-masing perempuan. Hal ini dapat dilihat dari keberagaman generasi lintas usia yang terlibat, keberagaman wilayah (di berbagai wilayah Indonesia)  keberagaman suku, keberagaman isu ataupun gagasan yang diangkat,  keberagaman  profesi tanpa batas. Bagi saya hibah ini sangat menarik. Karena banyak mewujudkan keberagaman ekspresi perempuan di Indonesia.
