---
title: Proposal Lengkap - Takjir - Indah Fikriyyati
date: 2018-12-30
permalink: /ciptamediaekspresi/proposal-lengkap/takjir
layout: proposal
author: Indah Fikriyyati
categories:
- laporan
- CME
- Takjir
---

![0092.jpg](/static/img/hibahcme/0092.jpg){: .img-responsive .center-block }

# Indah Fikriyyati - Takjir

**Tentang penerima hibah**

Indah Fikriyyati merupakan santriwati Pondok Pesantren Al Munawwir Komplek Q Krapyak Yogyakarta, perempuan kelahiran Cirebon 06 April 1995 ini menekuni dunia Seni Rupa sejak 2012 sampai sekarang. Ketertarikannya pada dunia Seni Rupa bermula dari Ayahnya yang hobi melukis, dari situlah ia akhirnya memutuskan untuk berkuliah mengambil konsentrasi Seni Rupa, dan sekarang Indah masih dalam proses penyelesaian studi S2 di Universitas Negeri Yogyakarta. Indah sempat aktif berpameran seni bersama teman-teman kampusnya dan karyanya pernah terpajang di beberapa Gallery Seni di Jogja. Dan kini ia banyak menghabiskan waktu di Pondok Pesantren untuk mengabdi sebagai Wakil Ketua Pondok di Komplek Q dan mengajar Seni Budaya di SMK Maarif Al Munawwir Krapyak.

**Kontak**

  - Instagram: [@indahfikriyyati](https://www.instagram.com/indahfikriyyati/)

**Lokasi**

Bantul, Yogyakarta

**Tujuan**

  1. Membuat Pameran Kolektif yang bertempat di sekitaran komplek Pondok Pesantren dengan mengajak teman-teman santriwati untuk ikut berkolaborasi di dalam prosesnya. Mengangkat isu-isu yang terjadi dalam kehidupan sehari-hari di Pesantren Putri, seperti  isu sosial, kesetaraan berkarya di dunia pesantren, kebebasan membuat karya seni, batasasan, larangan, serta hukuman (Takjir) apabila melaggarnya.

  2. Memproblematisasi atau mempertanyakan batasan, larangan, hukuman, yang dialami oleh santriwati yang melakukan kegiatan seni rupa di pondok pesantren. Serta menantang diri sendiri untuk berani melakukan pameran Kolektif di sekitaran pondok pesantren.

  3. Project ini nantinya akan dimulai dengan melakukan diskusi kecil dengan beberapa santriwati mengenai pemikiran perempuan pesantren yang divisualisasikan nantinya pada goal akhir di pameran, diskusi tersebut diadakan rutin setiap bulannya guna mengasah ketajaman berpikir dan kepekaan merasa mengenai permasalahan perempuan yang ada di pesantren putri, diskusi bisa dilaksanakan sembari mencicil membuat karya untuk dipamerkan nantinya.


**Sasaran**

  1. Membuat karya seni  dengan  mengangkat permasalahan sehari-hari yang terjadi di pondok putri (batasan, larangan, Takjir) melalui sebuah ilustrasi yang di divisualisasikan ke dalam sebuah medium yang berbeda-beda, seperti lukisan, desain komunikasi visual, kerajinan, patung, dan lain-lain.

  2. Melakukan pengamatan visual terhadap isu yang terjadi di beberapa pondok putri yang ada di Krapyak.

  3. Mencari ruang pamer yang berada di sekitaran Pondok Pesantren, agar semua santri bisa ikut menikmati karya yang kelak akan dipamerkan.

  4. Mengajak seluruh santri putri yang juga bergelut di dunia seni khususnya rupa agar lebih berani menampilkan dan menyuarakan aspirasinya melalui sebuah karya, dan harapannya dunia seni rupa di pesantren putri dapat lebih dihargai serta mendapat ruang dan tempat yang layak.


**Latar Belakang**

  1. Keterkaitan pada kategori: Akses

     Pameran Kolektif  bertemakan“Takjir” ini berangkat dari keresahan pribadi dan juga mungkin beberapa teman-teman santriwati lain yang berkecimpung di dunia seni rupa di pesantren akan sebuah batasan untuk melakukan proses pengkaryaan seni, melihat belum adanya ruang berkarya yang layak bagi perupa perempuan yang tinggal di pesantren serta banyak karya yang hanya dibuat kemudian terbuang  begitu saja karena kurang mendapat perhatian yang baik dan seringnya dianggap sebagai sesuatu yang tidak penting.

     Keterbatasan ruang berkarya, peratutan yang mengikat dan hukuman apabila tidak patuh akan peraturan yang sudah ada, membuat semangatnya terus terbakar untuk dapat menyuarakan pendapat dan ekspresinya dalam berkarya seni, agar mendapatkan ruang berkarya yang nyaman dan layak, serta mendapat perhatian khusus atas protes terhadap permasalahan, peraturan, hukuman yang nantinya akan divisualisasikan dalam sebuah karya seni lintas medium.

  2. Masalah yang ingin diangkat dalam pelaksanaan pameran seni:

        1. Pameran Seni ini mengangkat permasalahan yang ada di sekitar kehidupan di Pesantren putri, batasan dalam berkarya, larangan membuat karakter gambar makhluk bernyawa, dan kebebasan dalam mengikuti/melakukan kegiatan seni yang didominasi oleh laki-laki, serta punishment  yang didapat ketika melanggarnya.

        2. Masih sedikit/bahkan belum ada santriwati yang berani untuk menyuarakan pendapat dan ekspresinya sebagai perupa perempuan yang tinggal di pesantren  agar  suaranya didengar dan mendapat perlakuan yang layak sama halnya seperti perupa (laki-laki) lainnya yang ada di pesantren.

        3. Kesusahan dalam membuat project karya pameran seni, dikarenakan tidak adanya ruang untuk mencipta  karya seni. Harapannya project hibah seni ini dapat memfasilitasi kegiatan pameran agar bisa terwujud dengan baik.

  3. Strategi

     Menyewa ruang untuk melakukan proses pengkaryaan dan penyimpanan karya setelahnya, tidak perlu studio mewah dengan peralatan lengkap, ruang kost bulanan bisa dijadikan alternatifnya. Bekerja sama dengan para santriwati yang juga berkecimpung di dunia seni rupa untuk membantu proses pameran seni agar dapat terwujud sesuai harapan.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Aktivitas untuk sasaran A: Menjalin hubungan dengan para santriwati dari berbagai macam komplek di pesantren yang juga terjun di dunia seni rupa, berdiskusi langsung mengenai problem-problem apa sajakah yang terjadi seputar perupa perempuan dan yang dialami oleh perupa perempuan yang ada di pesantren. Melakukan pengamatan  terhadap isu-isu tersebut, yang nantinya akan divisualisasikan menjadi sebuah karya seni lintas media.

        2.  Aktivitas untuk sasaran B: Diharapkan pameran kolektif  ini dapat mempelopori lahirnya pameran-pameran seni yang dilakukan oleh perupa perempuan pesantren kedepannya, serta menjadi wadah awal terbentuknya komunitas perupa perempuan pesantren.

  5. Latar belakang dan demografi pelaku proyek

     Pencetus proyek pameran ini adalah santriwati yang berkecimpung di dunia seni rupa sejak 2012, pengalaman berkarya selama 6 tahun  dalam dunia seni rupa membuatnya sedikit banyak menjadi mengerti akan dunia seni rupa yang ada di pesantren saat ini. Proyek ini akan bekerjasama dengan para perupa perempuan yang juga tinggal di pesantren, dimulai dari proses perancangan pameran, kepenulisan, kuratorial, publikasi dan diskusi karya dengan memasukkan pemikiran perempuan di dalamnya dari sebelum proses pengkaaryaan di mulai sampai pameran tersebut terlaksana.

  6. Demografi kelompok target

     Santriwati, Santri laki-laki maupun masyarakat umum berusia 17-45 tahun dengan ketertarikan pada dunia seni rupa pesantren khususnya pada pesantren putri.

  7. Hasil yang diharapkan & Indikator keberhasilan

        - Terbentuknya mental Santriwati untuk berani menyuarakan haknya terhadap segala perlakuan tidak adil akan dirinya dan sekitarnya dalam hal batasan, larangan serta hukuman dalam kegiatan kreatif berkarya seni di pesantren

        - Memperkenalkan ke masyarakat luas bahwasannya dalam keterbatasan ruang dan waktu serta peraturan yang mengikat seorang Santriwati mampu produktif dan berkarya setara dengan seniman-seniman di luar sana

        - Mencoba memasukkan instrumen pemikiran perempuan dalam karya seni yang kemudian dapat dipahamkan ke masyarakat akan pemahaman  terbatasnya membuat sebuah karya seni di Pondok Pesantren (tidak boleh melukis makhluk hidup dan sebagainya) adalah sebuah kekeliruan yang harus diluruskan

        - Setelahnya bisa lahir generasi Santriwati atau kelompok-kelompok seniman perempuan pesantren dengan karya yang lebih baik lagi dan berani menyuarakan aspirasinya melalui karya seni rupa

  8. Indikator keberhasilan

     Proyek ini dinamakan berhasil apabila berjalan sesuai konsep yang sudah dibuat, proses pengkaryaan terselesaikan dengan baik dan tepat waktu, isu-isu yang akan disampaikan ke publik melalui karya dapat dicerna oleh para penikmat seni yang datang ke pameran, tentunya dihasilkan dari pengamatan panjang di sekitar pesantren dengan tidak membuat karya yang nantinya akan merugikan orang lain.

  9. Durasi Waktu Aktivitas dilaksanakan

     Memakan waktu 4 bulan lamanya, dimulai bulan November sampai Februari

  10. Total kebutuhan dana

      Rp. 51.800.000,-

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp. 20.000.000,-

  12. Sumber dana lainnya

      Belum ada

  14. Kontribusi perorangan

      Pemohon hibah seni  terus produktif berkarya dan melakukan pameran kolektif yang dibantu oleh para perupa perempuan lainnya yang ada di pesantren

  15. Kontribusi kelompok target

      Kontribusi para santriwati dalam melihat persoalan yang ada di sekitar tidak hanya dari kejadian yang dialami dalam kehidupan kesehariannya saja, namun juga dengan cara membaca secara visual dari sebuah karya seni.
