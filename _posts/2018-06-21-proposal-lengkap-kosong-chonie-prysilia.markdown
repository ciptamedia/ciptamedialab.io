---
title: Proposal Lengkap - KOSONG - Chonie Prysilia
date: 2018-06-21
permalink: /ciptamediaekspresi/proposal-lengkap/kosong
layout: proposal
author: Chonie Prysilia
categories:
- laporan
- CME
- KOSONG
---

![0062.jpg](/static/img/hibahcme/0062.jpg){: .img-responsive .center-block }

# Chonie Prysilia - KOSONG

**Tentang penerima hibah**

Chonie Prysilia adalah seorang perempuan yang, pada tahun 2011, meninggalkan pekerjaannya di bidang bisnis untuk mengeksplorasi ketertarikannya pada seni dan budaya. Dalam ekplorasinya, ia menemukan tempat dalam seni & budaya, yaitu pada kebebasan dan dinamis-nya produksi film animasi independen.
 
Di tahun 2015-2016 Chonie memproduseri film pendek animasi berjudul 'Roda Pantura' karya Hizkia Subiyantoro yang sukses menembus beberapa festival film dan animasi internasional. Perjalanan mempromosikan film 'Roda Pantura' memberinya banyak pengetahuan tentang pasar film animasi independen di dunia.
 
Pengetahuan tersebut berbuah keresahan untuk menyediakan panggung bagi film-film animasi independen yang sesuai dengan karakter produksi nusantara, yakni yang berbasis kerajinan tangan. Karena itu, bersama Piotr Kardas (Direktur Festival Animasi Polandia) dan Hizkia Subiyantoro , Chonie mewujudkan gelaran dua-tahunan CRAFT International Animation Festival pada tahun 2017.
 
Di samping kegiatan memproduksi film dan mengelola festival film animasi, Chonie juga aktif berkontribusi dalam komunitas ANIMASI CLUB yang, setiap bulannya, rutin mengadakan pemutaran film, diskusi dan lokakarya di bidang animasi di Yogyakarta.

**Kontak**

  - Facebook: [facebook.com/filmkosong](https://www.facebook.com/filmkosong/)

  - Instagram: [@hizarts](https://www.instagram.com/hizarts/)

**Lokasi**

Sleman

**Deskripsi Proyek**

'KOSONG' adalah film dokumenter panjang animasi, karya Chonie Prysilia dan Hizkia Subiyantoro, dengan lisensi CC-BY. Film ini mengangkat isu pernikahan tanpa keturunan, yang melibatkan pemikiran dan kesaksian lima perempuan dari berbagai profil dan latar belakang pernikahan dalam isu ini. Dengan teknik-teknik animasi, 'KOSONG' memvisualisasikan ingatan luput dokumentasi dan menyimbolisasikan pemikiran subyek-subyeknya.

- **Tujuan**

  Menjadi bahan evaluasi dan diskusi masyarakat atas respon-respon mereka bagi pernikahan tanpa keturunan dan mengetahui dampaknya terhadap hak-hak atas tubuh dan pemikiran perempuan.

- **Sasaran**

  1. Sebuah generasi perempuan dengan kebebasan dan keberanian dalam mengungkapkan pemikiran mereka tentang pernikahan keluarga, pada umumnya, dan dalam merencanakan aktifitas reproduksi, pada khususnya.

  2. Ruang-ruang pemutaran dengan diskusi mengenai hak-hak perempuan atas tubuhnya sendiri.

- **Latar Belakang**

  1. Keterkaitan pada kategori: Akses

     Pengalaman menjadi produser film Roda Pantura adalah kesempatan bagi Chonie untuk menyaksikan dan mempelajari proses penyutradaraan sebuah film animasi. Pengalaman berada dalam pernikahan tanpa keturunan memberi ide untuk mengangkat isu ini ke dalam sebuah film animasi dokumenter. 'KOSONG' adalah karya penyutradaraan pertama bagi Chonie.


  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Kehamilan yang terjadi segera setelah pernikahan, semua pasutri menginginkan hal yang sama, adalah stereotip yang berkembang dalam masyarakat nusantara. Penundaan atau perencanaan waktu kehamilan pertama tidak populer. Setelah pernikahan, jika kehamilan tak kunjung terjadi, kehidupan pernikahan dipertanyakan.

     Urusan kehamilan pertama kemudian menjadi urusan publik dan intens dibahas secara terbuka, tanpa mengindahkan privasi. Dalam masyarakat tradisional, pembahasan lama-kelamaan berubah menjadi intimidasi, hingga pelecehan verbal. Oleh sebab itu atau bukan, banyak pasangan mencoba berbagai cara; dari yang bersifat medis, spiritual, hingga alternatif, untuk segera memiliki keturunan. Sebagaimana khasnya masyarakat patrilineal, obyek utamanya adalah perempuan (istri).

     Perempuan yang menua tanpa keturunan dianggap malang, cacat dan tak jarang, diceraikan. Setiap kita mengetahui setidaknya satu pernikahan tanpa keturunan. Jadi dapat dipastikan, dalam satu komunitas, setidaknya satu perempuan sedang menghadapi tekanan dan intimidasi sosial, juga pelecehan verbal, lalu menyerahkan tubuhnya pada resiko sejumlah eksperimen medis dan non-medis. Meski topik ini berhubungan langsung dengan tubuhnya, kehendak pribadinya tidak pernah penting, apalagi tersampaikan.

     Masalah diatas adalah masalah yang populer. Oleh karena itu pendekatan yang efektif adalah melalui media yang populer juga. Film adalah salah satu media populer.

  3. Strategi

        - Membuat sebuah film dokumenter yang merangkum kesaksian dan pemikiran beberapa perempuan yang mengalami atau pernah mengalami pernikahan tanpa keturunan.

        - Membuka akses bagi masyarakat untuk menonton dan berdiskusi mengenai isi film tersebut.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Aktivitas untuk sasaran 1:

           Memproduksi sebuah film dengan bentuk visual rangkaian adegan animasi yang artistik, simbolik, dan representatif, sebagai alat untuk mengarahkan fokus penonton pada pemikiran dan kesaksian para subyek, yang dihadirkan dalam bentuk audio.

        2. Aktivitas untuk sasaran 2:

              - Mempromosikan film ke berbagai festival film nasional dan internasional, untuk membangun ketertarikan kelompok target akan isi film.

              - Mengadakan, memfasilitasi, dan atau memberi akses bagi sejumlah pemutaran publik film ini, guna menghadirkan ruang-ruang diskusi mengenai isu yang diangkat.

              - Mengunggah film pada salah satu plattform publik di internet, dengan lisensi CC-BY agar siapapun dapat mengakses dan menonton film.

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah perempuan yang berkesenian selama lima tahun (sejak 2013) dan aktif di bidang animasi selama tiga tahun (sejak 2015), baik sebagai produser film, organizer kegiatan, maupun networker. Kolaboratornya adalah laki-laki yang telah lebih dari lima belas tahun menggeluti bidang seni rupa dan aktif di bidang animasi game dan film selama sepuluh tahun (sejak 2008), baik sebagai pemimpin komunitas, sutradara film, inisiator kegiatan dan networker. Keduanya berumur 30-an dan dari kelas menengah.

     Staf lain yang terlibat adalah 10 (sepuluh) orang. Terdiri dari perempuan dan laki-laki, usia 20-an, kelas menengah, yang merupakan lulusan atau mahasiswa sekolah seni jurusan multimedia, televisi dan animasi.

  6. Demografi kelompok target

     Perempuan dan laki-laki usia 15-40 tahun, semua kelas sosial.

  7. Hasil yang diharapkan dan Indikator keberhasilan

     Film ini diharapkan dapat menjadi bahan referensi bagi diskusi-diskusi mengenai hak-hak perempuan atas tubuhnya sendiri. Film ini juga diharapkan agar dapat menumbuhkan keberanian perempuan dalam mengungkapkan pemikiran mereka tentang pernikahan keluarga, pada umumnya, dan dalam merencanakan aktifitas reproduksi, pada khususnya.

  8. Durasi Waktu Aktivitas dilaksanakan

     Mei 2018 – Mei 2019

  9. Total kebutuhan dana

     IDR 460.000.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      IDR 250.000.000

  11. Sumber dana lainnya

         - IDR 30,000,000 (Sumbangan Pribadi Penerima Hibah / Sutradara 1)

         - IDR 30,000,000 (Sumbangan Pribadi dari Hizkia Subiyantoro/Sutradara 2)

         - Masih dicari sumbangan untuk sisa kekurangan dana.

  12. Kontribusi perorangan

      Penerima hibah / Sutradara 1 menyediakan 1 kali/hari konsumsi untuk semua kru selama 10 bulan masa produksi animasi.

      Masih dibuka kesempatan untuk kontribusi & kerja sama dalam bentuk:

         - Menerjemahkan subtitle film

         - Mengadakan ruang pemutaran dan diskusi, serta

         - berbagai bentuk kontribusi lainnya.

  13. Kontribusi kelompok target

         - Meluangkan waktu untuk hadir dalam pemutaran dan diskusi film Kosong, atau

         - Meluangkan waktu untuk menonton film Kosong pada plattform online lalu memberi apresiasi 'like & share'. Agar lebih banyak lagi yang menonton film Kosong.

         - Membeli produk-produk turunan film Kosong seperti CD lagu-lagu sountrack, DVD Tutorial Pembuatan Film dan berbagai jenis merchandise film Kosong.

         - Menghormati hak-hak rumah produksi dan pembuat film seperti yang diatur dalam lisensi CC-BY.
