---
title: Proposal Lengkap - Pasar Tari Dua Ribu Lima Ratus - Wawa Sapta Rini
date: 2019-02-28
permalink: /ciptamediaekspresi/proposal-lengkap/pasar-tari-2500
layout: proposal
author: Wawa Sapta Rini
categories:
- laporan
- CME
- Pasar Tari Dua Ribu Lima Ratus
---

![0647.jpg](/static/img/hibahcme/0923.jpg){: .img-responsive .center-block }

# Wawa Sapta Rini - Pasar Tari Dua Ribu Lima Ratus

**Tentang penerima hibah**

Wawa mulai menari sejak masuk SMKN 10 Bandung yang fokus mengajarkan kajian dan teknik tari. Ia kemudian melanjutkan kuliah di STSI Bandung program studi Seni Tari dan berhasil meraih predikat cumlaude dengan penciptaan tari “Balukarna”. Ia ikut mementaskan karya tari berjudul “Lara” dalam salah satu ujian akhir mahasiswa pascasarjana ISBI Bandung. Ia kini aktif di sanggar tari yang bergerak di bidang jasa pernikahan dan mengajar tari di Santo Aloysius Bandung.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Bandung & Jakarta

**Deskripsi Proyek**

Proyek ini adalah proyek pertunjukan kolaborasi yang melibatkan penari/koreografer perempuan yang melihat tari sebagai tempat mencari uang dan sebagai tempat mengekspresikan diri dan lingkunganya. Di dalam proyek ini saya akan mengundang 4 penari dari lulusan akademisi seni, di Jawa Barat, Jawa Tengah dan Jawa Timur, yang terlibat dalam jasa pertunjukan tari bagi acara-acara tertentu yang sifatnya selebrasi atau perayaan, seperti nikahan, khitanan, promosi produk, peresmian, sampai ke club malam. Saya bersama dengan 4 penari dari lulusan akademisi seni - bertempat di Bandung akan terlibat dalam pencarian (penelitian dan laboratorium gerak) dan presentasi (pertunjukan) koreografi selama dalam Pasar Tari Dua Ribu Lima Ratusan.

- **Tujuan**

  Pasar Tari Dua Ribu Lima Ratusan mengadopsi pemikiran dan kebiasaan transaksi jasa di dalam karya atau bentuk pesanan tari dan kerja eksplorasi koreografi (tema, komposisi, penataan, presentasi) yang lazim digunakan di setiap kami membuat suatu pertunjukan. Dua kebiasaan ini saya anggap memiliki nilai kemanusian dan nilai artistik masing-masing yang tidak bisa kami lepaskan karena salah satunya lebih baik atau buruk. Dua kebiasaan ini saya gunakan sebagai salah satu cara yang paling mungkin dari kenyataan situasi kami - untuk membangun diri dan memberi suara pada wacana tari kontemporer Indonesia, dan kenyataan kami sebagai seniman perempuan dan perempuan dalam kenyataan sosial kami kehidupan sehari-hari.

- **Sasaran**

  Proyek ini akan melalui riset sebelum proyek, publikasi proses gali sumber, dan publikasi dari hasil pertunjukan atau presentasi proyek dalam bentuk buku.

- **Latar Belakang**

  Proyek ini berangkat dari pengalaman saya dan beberapa teman lain sebagai koreografer lulusan akademisi tari yang punya keinginan membuat dan mengkomunikasikan sesuatu melalui karya tari, tapi di sisi lain juga mengharuskan diri untuk tetap bertahan demi kehidupan sehari-hari yang bukan hanya saja persoalan kebutuhan sandang, pangan, dan papan, tapi juga persoalan yang menyangkut tubuh kami sebagai perempuan yaitu perihal kecantikan, pernikahan dan gaya hidup. Pengalaman ini membawa kami pada suatu kondisi yang sepertinya banyak pemerhati menilai hal ini membuat ‘kemajuan’ tari kontemporer Indonesia dan pergaulan internasionalnya terganggu, tidak produktif dan sebagainya. Kenapa tari kontemporer Indonesia? karena memang pengalaman ini  dimiliki oleh teman-teman koreografer lain di luar Jawa Barat, khususnya pada lingkup Pulau Jawa.

  1. Keterkaitan pada kategori: Kolaborasi

     Keterkaitan pada kategori Riset kami pilih karena penelitian mengenai Pangan Liar telah dilakukan oleh Bakudapan dan kontributor jurnal jauh sebelum proyek ini dilaksanakan. Sebagai tindak lanjut dari pengumpulan data tersebut, diperlukan proses analisis data, lokakarya kepenulisan, hingga menjadi sebuah jurnal yang nantinya akan didistribusikan dan dapat diakses secara luas oleh publik.

     Keterkaitan pada kategori Lintas Generasi, karena dalam proyek produksi (penulisan dan penerbitan) jurnal, kami melibatkan beberapa perempuan dari generasi berbeda yang berasal dari beberapa lokasi dan memiliki latar belakang historis yang beragam. Anggota Bakudapan sendiri seluruhnya adalah perempuan dari generasi muda yang dalam proses riset akan bersinggungan dengan ibu-ibu dan perempuan yang diantara mereka terhadap kesenjangan pengetahuan yang perlu dijembatani melalui proses pengumpulan data dan dalam proses lokakarya jurnal tersebut.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Ketika wacana regenerasi tari kontemporer Indonesia tidak bisa keluar dari problem yang sama  dan itu-itu juga terutama tentang kesinisan pada mental ‘peye-an’ atau pesanan dan sebagainya, kenapa kita tidak menganggap kenyataan tersebut sebagai kemungkinan lain dari tari kontemporer Indonesia? Dengan kata lain, kita bisa mengangkat isu dan topik bagaimana kenyataan ekonomi adalah kenyataan pada nilai dan bagaimana seorang penari dan koreografer bertahan dalam kehidupan sehari-hari dan itu sedang berlangsung lama, seperti sejarah Ronggeng dari seputaran ritus kesuburan (representasi dewi), penghibur, seksualitas/tidak beradab tapi sekaligus eksotis (cara pandang kolonial), hingga dampak dari politik negara (Orde Baru). Dalam dinamika sejarah tersebut saya melihat, selain sisi ekonomi dan ekologi (lingkungan/kosmis) terdapat pula pertaruhan lain terkait persoalan politik, yaitu bagaimana tubuh (perempuan) diposisikan dan bagaimana kami sebagai penari dan koreografer memposisikan diri.

  3. Strategi

     Pasar Tari Dua Ribu Lima Ratusan akan dilakukan – pertama kali dengan cara membeli koreografi per satu gerakan dan satu bagian tubuh yang akan dilakukan secara interaktif di lingkungan ‘jual-beli’ bernama pasar, baik ‘pasar modern’ dan ‘pasar tradisional’ yang semuanya terdokumentasikan melalui video dan foto. Kedua, menghitung dan menghimpun data koreografi yang terbeli di situs pasar dan situasi pernikahan/perkawinan, yang akan dilakukan di ruangan khusus seperti kerja di dalam studio. Ketiga, transparansi dan transformasi koreografi, yaitu menunjukan semua hasil pembelian dan perhitungan di panggung Black-Box - yang diambil dari bagaimana situasi peristiwa kesenian rakyat dahulu yang tidak terpisah dengan peristiwa jual-beli di sekelilingnya (kacang rebus, kopi, angkringan, dsb) – situasi di acara pernikahan, yang akan diperbincangkan dengan data-data sejarah tari sebagai hiburan – baik di masa pra-kolonial, kolonial, hingga di masa kini – di lingkungan kota.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Gali Sumber: Juni – Agustus. 

              - Riset sejarah Ronggeng bertujuan untuk menelusuri keterkaitan antara yang lalu dan yang sekarang tentang posisi penari perempuan melalui sejarah Ronggeng di pulau (Jawa).

              - Riset jenis dan bentuk koreografi perayaan dan hiburan untuk menelisik dan memilih  jejak Ronggeng (Penari Perempuan) melalui gerak ketubuhannya di dalam nikahan, khitanan, peresmian, promosi produk, klub malam. 

              - Memilih satu jenis dan bentuk koreografi  perayaan dan hiburan yang akan dijadikan dasar proyek. 

              - Pendalaman jenis dan bentuk yang dipilih dengan mengurai setiap pola gerak dan setiap gerakan. 

        2. Publikasi (simultan) via website

           Merupakan laporan proses dan perkembangan berkala berupa catatan hasil temuan di lapangan selama projek ini berlangung yang disajikan kepada publik melalui website. 

        3. Diskusi keliling kampus seni kolaborator:  September

           Diskusi hasil riset dan gagasan proyek yang akan dibawa berkeliling ke masing-masing kampus seni – terfokus berada di Pulau Jawa – yang memiliki Jurusan Tari/Pendidikan Seni Tari, seperti  (ISBI dan UPI Bandung), Jawa Tengah (ISI Yogyakarta dan ISI Surakarta), dan Jawa Timur (STKW dan UNESA Surabaya). 

        4. Residensi (Studio Gerak):  September-Oktober

              - ‘Workshop’ bersama di Bandung – dengan seorang dramaturg, dan empat kolaborator yang terundang – untuk memetakan terlebih dahulu titik tolak awal dan tahap-tahap proses bersama. 

              - Transaksi gerak di masing-masing kota kolaborator.

              - Kerja Studio di Bandung: memanggil kembali para kolaborator untuk melakukan komposisi dari hasil transaksi gerak dan merancang  pertunjukan.

        5. Pertunjukan: Bandung dan Jakarta

        6. Katalog pasca-proses

           Pencatatan keseluruhan proses panjang ini dalam satu katalog dokumentasi – yang berisi:

              - Rangkuman Gali Sumber

              - Kisah proses

              - Testimoni penonton, 

              - Undangan penulis luar untuk menuliskan pengalaman amatan/kepenontonannya.

  5. Latar belakang dan demografi pelaku proyek

        - Koreografer:

          Wawa Saptarini

          Seorang koreografer dan penari di sanggar Nyentrik Production yang bergerak di bidang jasa kesenian di Bandung. Lulusan ISBI Bandung tahun 2016. Kegiatan sehari – hari mengajar tari di sekolah santo Aloysius bandung. Karya Tari Balukarna (2016)

        - Kolaborator:

             1. Puri Senja (Surabaya)

                Koreografer dan penari yang berdomisili di Surabaya. Lulus dari jurusan sendratasik UNESA. Mempunyai bisnis jasa yang berhubungan dengan acara perayaan atau hiburan. Membuat bisnis jasa make up untuk wisuda, party, prewed, photo session. Terlibat sebagai koreografer dan penari dari Surabaya dance collective dan sawung dance studio

             2. Ramifta Ayu Aerodila (Solo)

                berdomisili di kota surakarta basic tari yang dimiliki hip hop dan jathil Ponorogo. Tertarik pada jalur penciptaan tari dan sudah memiliki 3 karya tari, Yama (2016), kekang (2017), kendali (2017). Seorang koreografer dan penari di mahadance, lulusan ISI Surakarta tahun 2017 jurusan seni tari. 
            
            3. Putri Maylani Pamungkas (Yogyakarta)

               Berdomisili di Yogyakarta, lulusan ISI Yogyakarta. Berkegiatan sehari hari sebagai pengajar tari dan koreografer di salah satu sekolah menengah atas di Yogyakarta.
 
        - Tim Riset: Meytha Arta Putri, Dhea Mirzanadya, Riyadhus Salihin, Ganda Swarna

        - Koor. Dokumentasi: Mega Noviawulandari

        - Koor. Publikasi: Hilmi Zein

        - Stage Manager: John Heryanto

        - Koor. Artistik: F. Noufal

        - Dramaturg: Taufik Darwis

        - Music Director: Romy Jaya Saputra

        - Artistic Director: Puji Koswara

        - Make-up & Kostum: Anis Harliani

        - Finance: Rahmah Fitriani

  6. Demografi kelompok target

        - Target interaksi dan transaksi koreografi: Penjual dan pembeli di pasar modern dan tradisional

        - Target diskusi hasil riset dan gagasan proyek: Mahasiswa/i kampus seni di Pulau Jawa

  7. Indikator keberhasilan

     Terdapat tiga kategori yang menjadi indikator dalam proyek ini. Pertama, indikator interaksi dan transaksi koreografi yang sangat bisa dihitung melalui jumlah akumulasi uang dari penonton yang memesan satu bentuk koreografi dari satu bagian tubuh tertentu.  Kedua, nilai dari bagaimana nilai ekonomi  koreografi ‘peye-an’ mempunyai nilai atau kualitas artistik yang bisa dilihat bagaimana presentasi di pertunjukan di setiap harinya yang telah melalui berbagai macam proses di studio. Ketiga nilai sosial yang kemudian menjadi perbincangan topik lebih lanjut yang akan disingkap dan didorong melalui publikasi proses riset sejarah ekonomi dan politik tubuh penari perempuan dalam koreografi pesanan melalui situs web. Dan penerbitan buku (dokumentasi fotografi dan wawancara penonton baik yang terprovokasi ataupun tidak).

  8. Durasi Waktu Aktivitas dilaksanakan

     5 bulan

  9. Total kebutuhan dana

     Rp.162.000.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.97.000.000

  11. Sumber dana lainnya

      -

  12. Kontribusi perorangan

      -

  13. Kontribusi kelompok target

      -