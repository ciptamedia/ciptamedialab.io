---
title: Proposal Lengkap - Suara Sinden Ginonjing - Nur Handayani
date: 2018-10-10
permalink: /ciptamediaekspresi/proposal-lengkap/suara-sinden-ginonjing
layout: proposal
author: Nur Handayani
categories:
- laporan
- CME
- Suara Sinden Ginonjing
---

![0727.jpg](/static/img/hibahcme/0727.jpg){: .img-responsive .center-block }

# Nur Handayani - Suara Sinden Ginonjing

**Tentang penerima hibah**

Nur Handayani adalah perempuan kelahiran Sukoharjo 29 Januari 1985. Lahir dan besar dari keluarga seniman tradisi, membuat dirinya sejak kecil familiar dengan dunia seni, khususnya karawitan. “Genus” seni dalam dirinya mengalir sejak kecil. Tak ayal, potret tersebut menghantarkannya menjadi seniman perempuan yang potensian saat ini. Debutnya sebagai seniman, dimulai ketika belajar di SMKN 8 Surakarta tahun 2001-2003 mendalami seni karawitan, sebelum akhirnya melanjutkan studi S1 di ISI Surakarta tahun 2003-2009, dan meraih gelar sarjana seni di bidang seni karawitan. Pengalaman kesenimanannya diukir dari penggung ke panggung, baik itu panggung pewayangan, karawitan, campursari, hingga musik kontemporer dan musik populer.

Kini dia dikenal sebagai pesindhen yang cukup moncer, karena menguasai berbagai macam gaya sindhenan, seperti: gaya Surakarta, Sunda, Banyuwangi, Banyumasaan, serta Jawa Timuran. Selain bersuara merdu, dia juga piawai memainkan alat musik tradisi seperti: rebab, gendher, dan kendhang. Atas talentanya itu, beberapa penghargaan pernah diraih olehnya, seperti: Jura I lomba Macapat se-Kabupaten Sukoharjo; Juara I lomba nyanyi campursari se-Kabupaten Sukoharjo; Juara I lomba nyindhen se-Solo Raya, serta terlibat dalam Festival Musik dan Tari Muhibah di Turki. Melihat kiprahnya sebagai seniman perempuan yang multitalent, membuat banyak kreator seni melibatkan dirinya sebagai pemusik, terutama untuk vokal, seperti: dalang Ki Manteb Soedarsono, Ki Enthus Sumono, Ki Purbo Asmoro. Selain itu dia juga acap didaulat sebagai bintang tamu dalam acara musik di berbagai wilayah: Jawa Tengah, Jawa Timur dan Jakarta.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Kota Surakarta

**Deskripsi Proyek**

- **Tujuan**

  Tujuannya utamanya adalah ingin menunjukkan kekuatan keperempuanan lewat seni, khsusunya sindhenan. Lewat karya ini, saya memiliki peran penting terhadap keberpihakan perempuan dalam menyikapi kehidupan sosial. Orkestra sindhenan adalah sebuah simbol kekuatan wanita dalam dimensi estetika. Peran perempuan dalam dunia seni khususnya sinden, yang selama ini memiliki stigma negatif, karya ini berusaha menepis semua itu. Perempuan Jawa dengan produk budayanya mampu memberikan serta mempersembahkan karya berbeda.

  Selain itu, tujuan lainnya adalah memberikan energi positif kepada perempuan-perempuan supaya memiliki kepercayaan diri di tengah-tengah masyarakat luas, bahwa perempuan memiliki nilai tawar dalam kehidupan bersosial. Lebih dari itu, memperkuat gerakan-gerakan feminisme agar semakin kuat dalam menyadarkan kepada publik tentang keadilan terhadap perempuan.

  Lewat orkestra sindhenan ini, diharapkan memicu kreativitas seniman-seniman lain dengan mengangkat paradigma keperempuanan. Serta tidak menutup kemungkinan menjadi acuan model penciptaan seni dengan sudut pandang feminisme.

- **Sasaran**

  Objek sasarannya adalah masyarakat umum, pemangku kebijakan, aktivis perempuan, penggiat seni, wartawan, akademisi serta perempuan generasi tunas supaya menjadi stimulan untuk memupuk rasa percaya diri dalam menghadapi kehidupan sosial.

  Capaian Jangka Panjang

  Agar karya ini tidak berhenti sampai pada tahapan pementasan, maka pasca pementasan akan dilakukan kajian mengenai implikasi dari terlehatnya karya ini. kajian ini berfungsi untuk mengukur sejauh mana karya ini dapat berpengaruh atau merubah paradigma penonton, lingkungan atau siapapun tentang  perempuan. Langkah yang dilakukan untuk mengetahui itu adalah sebagai berikut:

     1. Mengadakan diskusi atau seminar yang diselenggarakan secara umum  mengenai responsi atas karya yang telah digelar. Diskusi akan dilalukan dengan narasumber yang berkompetensi di bidangnya, baik itu musik, kajian keperempuanan, dan lain sebagainya.

     2. Membuat tim riset untuk mengetahui responsi masyarakat dalam pandangannya tentang perempuan pasca karya ini digelar. Penelitian dilakukan secara kuantitatif dan kualitatif. Kuantitatif ditempuh dengan cara menyebarkan angket, dengan sampling 100 responden. Sementara kualitatif ditempuh dengan wawancara mendalam terhadap orang-orang yang menonton karya ini.

     3. Pembuatan semacam buku  yang memuat tentang intisari dari karya ini yang disampaikan secara deskriptif kepada masyarakat. Dengan demikian, keberlangsungan informasi yang dituangkan lewat karya, tidak berhenti meskipun pementasan sudah usai. Cara itu dianggap solutif agar propaganda tentang keadilan perempuan yang ada dalam karya musik tersebut berlangsung secara terus menerus lewat buku tersebut.

- **Latar Belakang**

  Karya ini diilhami oleh pemikiran Kartini ketika menilai sebuah gendhing Ginonjing. Penilaian itu tertuang lewat surat yang dia tujukan kepada sahabat Belandanya yang bernama Estelle Zeehandelaar pada Januari 1900.

  “Gamelan kaca di pendopo itu dapat bercerita lebih banyak daripadaku. Mereka sedang memainkan lagu kesukaan kami bertiga. Itu tepat bila dikatakan bukan lagu, bukan melodi, hanya nada dan bunyi, begitu lundak dan begitu lembut, bertingkah dan menggetar campur aduk tanpa tujuan, membumbung, tetapi betapa mengharukan, betapa mengharukan indahnya! Tidak-tidak itu bukan bunyi-bunyian dari gelas, atau kuningan, atau kayu yang membumbung di sana itu; itu adalah suara yang keluar dari jiwa manusia, yang berbicara kepada kami itu, sebentar mengeluh-ngeluh, kemudian merapat, dan kadang saja tertawa. Dan Jiwaku sendiri melayang bersama dengan gemericik suara peraknya yang suci itu ke atas, ke langit biru, ke mega kapas, ke bintang-gemintang gemerlap, bunyi-bunyi bas yang membumbung ke langit, dan bunyi-bunyian itu membawa aku menelusuri lembah-lembah dan lurah-lurah gelap dan dalam, melewati hutan-hutan  yang lengang, menerobosi belantara yang tak tertembus! Sedang jiwaku gemetar dan meriut ketakutan, kesakitan dan sukacita!” begitu ungkap Kartini dalam surat tersebut,

  Resepsi Kartini tentang gendhing tersebut seakan dia membicarakan tentang dirinya yang terkngkung oleh budaya feodal Jawa pada masa itu. Kisah tentang genhing Ginonjingpun berlanjut dalam surat berikutnya yang ditujukan kepada Nyonya Abendanon pada 12 Desember 1902.

  “Sudah pada nada-nada permulaan dari bagian pembukaan itu aku sudah hilang lenyap tenggelam, bila ginonjing itu terdengar olehku. Tak mau aku dengarkan lagu yang sayu itu, namun aku harus, harus dengarkan pada suara-suaranya yang berbisik-desah, yang bercerita padaku tentang masa lalu, tentang hari depan, dan seakan nafas suara-suara keperak-perakan yang menggetar itu meniup lenyap tabir yang menutup kegaiban hari depan. Dan sejelas halnya dengan masa kini beraraklah gambaran-gambaran hari depan di hadapan mata batinku. Maka menggigillah aku, bila sampai olehku gambaran-gambaran sayu-gelap muncul di hadapanku. Tak mau aku melihatnya, tapi mataku itu tinggal terbeliak lebar, dan pada kakiku menganga ke dalam jurang yang terbitkan gamang, tapi kalau kutebarkan pandangku ke atas, terbentanglah di sana langit biru muda di atasku dan cahaya matahari yang keemasan bermain-main dengan mega-mega putih dan di dalam hatiku kembali terbit terang!” begitu tulis Kartini.

  Kisah Ginonjing seolah merepresentasikan kegelisahan Kartini tentang hidup seorang perempuan pada masa itu. Ia menafsir gendhing Ginonjing dengan sangat muram. Kartini seolah tidak ingin melihat masa lalu dan masa depan. Ginonjing seakan membelenggu hidupnya dan nyaris tidak dapat bergerak. Kisah lain, Gendhing Ginonjing adalah gendhing pengahntar tidur yang setiap malam diperdengarkan oleh  Ngasirah, yaitu ibu Kartini. Bisa dibayangkan betapa akrapnya kartini dengan gendhing tersebut.

  Anggapan muram tentang gendhing tersebut, seolah mengungkapkan kegentingan dan kegelisahan perempuan di masa itu. Kendati menginginkan perubahan, nyatanya tidak memiliki kemampuan dalam mengupayakannya, seolah yang terlintas hanya mimpi dan batin yang berkecamuk. Lantas kepasrahan yang akhirnya yang dapat dilakukan. Mimpi Kartini tentang perempuan, agaknya terkisah oleh curahan Kartini lewat tafsir muram Gendhing Ginonjing. Kisah yang sama, saya alami di dalam dunia pesindhen yang selama ini saya geluti. Banyak kisah muram yang harus saya ungkapan ke publik lewat sebuah karya.

  Namun sebelum mengarah terlalu spesifik, membahas tentang perempuan adalah tema yang selalu menarik. Bangsa ini besar dicapai dengan susah payah dan tentu di dalamnya melibatkan perempuan-perempuan hebat. Kisah perempuan Indonesia selalu digambarkan dengan sosok Kartini, meskipun mayoritas perempuan Indonesia tidak sekuat dan setangguh Kartini dalam membela keadilan. Berbicara Kartini sama dengan berbicara keberpihakan, dimana perempuan juga harus memiliki nilai tawar di dalam kehidupan sosial.

  Hingga kini, perjuangan itu masih berlangsung dengan berbagi dalil dan tendensi yang melingkupinya. Tidak hanya soal keadilan sosial, tetapi juga tentang bagaimana peranan perempuan dalam ikut andil dalam menyelenggarakan keadilan. Oleh karena itu, muncullah kebijakan-kebijakan bangsa ini tentang keterlibatan perempuan dalam berbagai organisasi, termasuk salah satunya keterlibatan 30% perempuan dalam partai politik. Lebih dari itu, srikandi-srikandi pilihan bangsa ini sudah berada di lingkungan istana membantu pemerintahan, seperti Sri Mulyani sebagai menteri keuangan, Susi Pudjiastuti menteri kelautan dan perikanan, Retno Lestari menteri luar negeri, Puan Maharani menko bidang pembangunan kemanusiaan dan kebudayaan, Serta Rini Mariani Soemarno menteri BUMN. Mereka adalah bukti bahwa perempuan Indonesia bisa menjadi pilar-pilar yang kokoh untuk membantu penyelenggaraan negara.

  Pemikiran perempuan itu lebih kompleks dibandingkan dengan laki-laki. Pengetahuan laki-laki itu berada dalam logikanya, sementara pengetahuan perempuan menyatu dalam hidupnya. Penderitaan laki-laki adalah penderitaan karena kekurangan hak. Sementara penderitaan perempuan adalah kulminasi dari semua jenis penderitaan, termasuk  penderitaan harapan akan masa depan, begitu kata Rocky Gerung (Dosen Filsafat Universitas Indonesia). Misalnya tentang naiknya harga daging, laki-laki akan berpikir jika harga daging naik, mereka akan mengatakan beli daging setelah harga turun. Tetapi tidak demikian dengan perempuan, harga daging yang naik akan menentukan masa depan anak-anak mereka, tentang  kekurangan gizi bagi anak-anaknya termasuk suaminya, dan lain sebagainya.

  Kenyataan di atas, rupanya belum cukup untuk merubah pandangan terhadap perempuan, yang mayoritas masih menganggap bahwa perempuan itu adalah konco wingking, yaitu tugasnya di dapur, di kasur dan di sumur. Perspektif itu yang selama ini tertanam dalam kehidupan. Anggapan lain muncul, perempuan tidak harus mengenyam pendidikan, perempuan tidak harus bekerja, dan lain sebagainya. Stigma itu yang akhirnya membuat perempuan tidak memiliki nilai tawar dalam kehidupan sosial, yang kemudian memicu lahirnya gerakan-gerakan perempuan, seperti feminisme, partai perempuan, dan lain sebagainya.

  Gejala itu juga muncul di dalam dunia kesenian. Di Jawa, penari perempuan selalu dikaitkan dengan isu yang negatif, pesinden juga memiliki stigma negatif di luar pentas. Rumor-rumor negatif itu seolah menjadi kebenaran yang nyata, padahal pesinden itu merupakan daya tarik utama di dunia karawitan utamanya jika berada di panggung. Pandangan miring di luar panggung, sebagian masyarakat menganggap rendah serta murahan,  hal itu yang membuat gelisah para pesinden terutama saya pribadi sebagai seorang pesindhen.

  Atas dasar itulah, saya ingin merubah stigma tentang perempuan Jawa khususnya yang bergelut di dunia kesenian terutama pesinden. Bahwa perempuan juga memiliki kekuatan-kekuatan yang layak untuk diangkat ke permukaan, supaya khalayak tahu tentang energi perempuan Jawa. Rencana saya tentang ini adalah mencipta karya yang bisa mengangkat martabat perempuan di mata publik yang akan disampaikan lewat orkestra sindenan yang dilakukan oleh para perempuan. Sindhenan adalah vokal tunggal puteri yang dilantunkan oleh pesindhen. Sementara pesindhen adalah solois puteri dalam karawitan Jawa.

  Orekstra sindhenan, sejauh pengamatan saya belum pernah dilakukan di manapun. Idealnya sindhenan dilakukan secara solo, seperti yang biasa dilakukan oleh para pesindhen umumnya. Saya berpikir jika sindhenan dilakukan secara orkestra memiliki estetika bunyi yang khas. Jika orkestra pada umumnya suara terbagi menjadi sopran, alto, tenor dan bass, orkestra sindhenan tidak akan menggunakan pembagian suara tersebut. Melihat sindhenan adalah bagian dari seni karwitan, pembagian suara yang akan digunakan adalah suara yang ada di dunia karawitan.

  Lebih lanjut teknik ini berusaha menemukan celah-celah nada yang kemudian dipadukan sehingga menimbulkan estetika nada yang khas. Kekhasan itulah yang nanti menjadi temuan. Dan tidak menutup kemungkinan terdapat garap lain dalam perjalanan penggarapan.

  1. Keterkaitan pada kategori: Kerjasama/Kolaborasi

     Proyek ini akan menampilkan orkestra sindhenan, yang akan melibatkan sembilan pesindhen. Pada umumnya, sindhenan dilakukan oleh solois puteri dalam karawitan Jawa. Orkestra sindhenan, belum pernah dilakukan dan merupakan hasil tanggapan kehidupan pesindhen di dalam pertunjukan wayang kulit, di mana sindhen seringkali menjadi obyek kelakar seksual oleh para dalang maupun penontonnya.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Masalah yang ingin diatasi adalah keberpihakan kepada perempuan, khususnya para pekerja seni seperti sinden dan sejenisnya. Selain keberpihakan, juga ingin memunculkan isu tentang konsep feminisme kepada publik, agar martabat perempuan dapat setara dengan realitas sosial.

  3. Strategi

     Tidak ada strategi khusus, yang dilakukan hanya melibatkan segala aktivitas hibah dengan para perempuan, baik secara artistik maupun managemen. Strategi untuk menyampaikan konsep karya dengan cara mencetak catalog, dialog dengan audiens. Relasi antara pengkaraya dan pendukung sekaligus stakeholder pergelaran hibah dijalin hingga hari ini.

  4. Aktivitas dan keterkaitan pada sasaran

     Aktivitas yang akan dilakukan adalah pagelaran seni. Pergelaran ini akan menampilkan orkestra sindhenan, yang akan melibatkan sembilan pesindhen. Dalam karya ini, sindhenan akan dibagi menjadi beberapa bagian suara, tetapi dengan sudut pandang karawitan, seperti suara pesindhen dengan nada seleh nem (6), seleh ro (2), seleh lu dan seleh ji (1). Nada-nada tersebut dalam dunia karawitan jarang sekali dipadukan. Dalam karya ini, perpaduan antar nada tersebut berusaha disatukan dan  akan menimbulkan celah nada atau anak nada akibat benturan nada-nada tersebut, dalam istilah karawitan disebut dengan ngeng. Lebih lanjut karawitan yang digunakan sebagai pengiring adalah karawitan puteri.

     Karya ini diberi judul “Ginonjing” kata tersebut berasal dari kata “gonjing” yang berarti goyang karena tidak seimbang, mendapat sisipan “in” menyatakan ketidak sengajan. Ginonjing berarti goyah tanpa tahu siapa yang membuat posisi tidak seimbang . Atau ginonjing bisa berarti pengalaman kehilangan gravitasi sehingga orang tidak lagi bisa mengendalikan dirinya (St. Sunardi, dalam Jurnal Kalam, nomor. 21, 2004: 23). Secara implisit, kata ginonjing dapat merepresentasikan kegelisahan saya tentang fakta-fakta muram tentang perempuan khususnya pesindhen. Karya ini berdurasi 60 menit pertunjukan.

     Tempat yang dipilih untuk pergelaran pertama ini adalah Pendapa Institut Seni Indonesia (ISI) Surakarta pada  Juli 2018. Pergelaran kedua ditampilkan dalam post fest yang digelar di Institut Kesenian Jakarta (IKJ) pada tahun 2018.

  5. Latar belakang dan demografi pelaku proyek

     Pelaku proyek adalah Nur Handayani, seorang sindhen yang berasal dari Kabupaten Sukoharjo, Jawa Tengah. Dia besar di lingkungan seni, khusunya seni tradisi. Intensitasnya di dalam seni tradisi sebagai pesinden memberikan banyak pengaruh kepada kehidupannya. Seolah, dunia karawitan sudah menubuh pada dirinya. Pengalamannya menjadi sindhen dan berbagai kompleksitasnya di dalamnyalah yang lantas melatarbelakangi karya ini tercipta.

     Pimpinan projek hibah adalah Ayu Mita Radila, seorang musisi (pianis) asal kota Solo. Dia aktif dalam dunia musik populer dan sempat menjadi bagian dari management koreografer Sardono W  Kusumo dan beberapa event festival di Kota Solo.

     Tim Produksi Tugas Akhir

     Pimpinan Produksi: Ayu Mita Radila

     Asisten Pimpro: Nanang Musha

     Bendahara: Sri Wulandari

     Sponsorship: Fany

     Sekretaris: Tri Ratnasari

     Komposer: Nur Handayani

     Musisi pendukung:

        - Eka Suranti

        - Suci Ofitadewi

        - Siswati

        - Riski Handayani

        - Anis

        - Santi

        - Riski

        - Fitri

        - Ema

        - Vidi

        - Leni

        - Kartika

        - Prasasti

     Artistik: Ali Maksum

     Crew:

        - Bangkit

        - Risky

     Stage Manager: Suroto

     Kostum & Rias: Dwi Sruni

     Dokumentasi:

        - Videografer: Arief Budianto

        - Fotografer: Arief Team

     Konsumsi: Andri Saputro

     Sound director: Merwan Ardhi Nugroho

     Crew: Candra

     Publikasi: Sigit Yellow Box

     Perizinan: Andri

     Pembantu umum: Wahyono

  6. Demografi kelompok target

     -

  7. Hasil yang diharapkan & Indikator keberhasilan

     Hasil yang diharapkan adalah merubah stigma atau pandangan masyarakat terhadap para pelaku seni khususnya perempuan yang berkiprah sebagai seorang pesinden atau sejenisnya. Meruntuhkan pandangan patriarki yang selama ini menjadi faham lazim dalam masyarakat. Mengupayakan keadilan dan keberpihakan realitas sosial terhadap perempuan.

  8. Indikator keberhasilan

     Indikator keberhasilannya adalah, memicu karya-karya baru dengan perspektif perempuan, utamanya di lingkungan seni. Isu feminisme menjadi santer sebagai konsepsi seni. Lebih dari itu, pengkarya beberapa kali menjadi narasumber dalam diskusi dalam forum musik.

  9. Durasi Waktu Aktivitas dilaksanakan

     3 bulan

  10. Total kebutuhan dana

      IDR 50.000.000

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      IDR 50.000.000

  12. Sumber dana lainnya

      Tidak ada

  13. Kontribusi perorangan

      -

  13. Kontribusi kelompok target

      -
