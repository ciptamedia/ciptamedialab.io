---
title: Proposal Lengkap - Narasi Selembar Kain - Mona Sylviana
date: 2018-05-17
permalink: /ciptamediaekspresi/proposal-lengkap/narasi-selembar-kain
layout: proposal
author: Mona Sylviana
categories:
- laporan
- CME
- Narasi Selembar Kain
---

![0049.jpg](/static/img/hibahcme/0049.jpg){: .img-responsive .center-block }

# Mona Sylviana - Narasi Selembar Kain

**Tentang penerima hibah**

Telah menulis cerita pendek, puisi, dan esai sejak kuliah. Beberapa pengalaman dalam bidang penulisan di antaranya adalah menjadi peserta “International Literary Biennale” (2005), peserta program residensi prosa di Ubud Writers & Readers Festival (2009), peserta Program Residensi Penulis Komite Buku Nasional (2017). Tulisan-tulisannya telah diterbitkan berbagai media massa cetak, media online, dan sejumlah buku, di antaranya adalah Pesan *Ombak Padjadjaran* (Pustaka Sastra, Yogyakarta, 1993), *Improvisasi X* (Bentang, Yogyakarta, 1995), *Angkatan 2000 dalam Sastra Indonesia* (Grasindo, Jakarta, 2000), *Dunia Perempuan: Antologi Cerita Pendek Wanita Cerpenis Indonesia* (Bentang, Yogyakarta, 2002), *Living Together* (Kalam, Jakarta, 2005), *Wajah Terakhir* (Gramedia Pustaka Utama, Jakarta, 2011), *Upacara Adat di Jawa Barat* (Dinas Pariwisata dan Budaya Pemerintah Provinsi Jawa Barat, Bandung, 2014), *A Tale of Redemption & Other Stories* (A Trilingual edition in English, German, and Indonesia), (BTW Books, Jakarta, 2016).

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Propinsi Maluku (Pulau Ambon, Kepulauan Tanimbar, Kepulauan Kei)

**Deskripsi Proyek**

- **Tujuan**

  Mendeskripsikan keseharian para penenun perempuan dalam sebuah narasi perjalanan yang berbentuk catatan perjalanan/novel/cerita pendek. Narasi perjalanan tersebut diharapkan akan mencungkilkan kaitan kegiatan menenun yang personal dengan konteks sosial-historis-budaya. Saya merasa berkepentingan menuliskan hal tersebut di tengah kehidupan berbangsa sekarang ini—menggarisbawahi tulisan Benedict Anderson, “komunitas imajiner”—kehidupan berbangsa harus dipahami sebagai bukan terberi (*given*), tetapi suatu kondisi yang harus dipelajari dan dipahami oleh siapa yang menjadi warga negara. Pemahaman tersebut akan menjadi pijakan masing-masing diri mengetahui posisi sebagai warga negara serta hubungannya dengan diri-diri yang lain. Dengannya kita bisa keluar dari jebakan stereotipe dan kesalahpahaman; menghidari dari kesalahan-kesalahan yang pernah terjadi: perselisihan antar etnis, pertikaian antar agama, ketimpangan gender.

- **Sasaran**

  1. Kegiatan menenun kain adalah pekerjaan perempuan di ranah domestik yang penting untuk ranah publik. Di banyak tempat di Indonesia, termasuk di Maluku, kegiatan menenun dilakukan oleh para perempuan dan merupakan semacam keahlian yang melekat. Menenun adalah kegiatan sendiri para perempuan yang dilakukan di rumah sama halnya dengan pekerjaan domestik lain, tetapi tenunan—tenun ikat disebut tais di Tanimbar, Kabupaten Maluku Tenggara Barat, Maluku—adalah bagian penting dari adat. Tais memegang peran sentral dalam kehidupan warga; mulai dari kelahiran, upacara kematian, simbol perlindungan, penegas jati diri pemiliknya, hingga pendamai konflik antardesa. Dalam menenun para perempuan itu menggunakan keseluruhan dirinya, tubuh dan imajinasi. Maka, dari kegiatan domestik tersebut sangat mungkin akan mencungkilkan pikiran dan perasaan yang khas mengenai kehidupan bersama. Pikiran dan perasaan yang mungkin muncul dalam gerak tubuh, bahasa, atau respons yang belum dinamai oleh bahasa verbal yang ada.

  2. Dibandingkan dengan wilayah lain di Indonesia, pembangunan fisik dan non-fisik bisa dikatakan baru saja dimulai hingga masih terus harus berpacu dengan wilayah-wilayah lain di Indonesia. Dan cara melihat yang menempatkan perempuan sebagai kelas kedua, saya kira, masih dominan. Maka, untuk perempuan di wilayah timur Indonesia, dilihat/melihat dan memiliki kesadaran mengenai keperempuan menjadi relevan.

  3. Keterkaitan kehidupan para penenun di tengah meleburnya batas-batas, termasuk batas negara-bangsa, karena teknologi komunikasi. Teknologi komunikasi tidak bisa dimungkiri telah mengangkat banyak aspek kebudayaan di wilayah Indonesia, termasuk tais. Tais menjadi populer dan dikeluarkan dari pagar-pagar wilayah serta peran awalnya. Ia menjadi komoditas bahan etnis di dunia fashion, elemen tambahan di dunia pariwisata, bahkan menjadi alat bantu dalam dunia politik. Kesemuanya sudah pasti melibatkan dan memberi peran lain bagi para penenun.


- **Latar Belakang**

  1. Keterkaitan pada kategori: Prejalanan

     “Narasi Selembar Kain” merupakan sebuah catatan perjalanan yang tidak sekadar menuliskan perjalanan fisik dari kota asal (Bandung) ke desa-desa di beberapa pulau di Propinsi Maluku tapi suatu perjalanan yang melampauinya; pengalaman lain sebagai perempuan dan sebagai warga sebuah negara-bangsa.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Pengalaman para perempuan penenun itu ruang diskusi. Saya berharap ruang diskusi tersebut tidak berhenti antara saya dan para perempuan penenun, tetapi meluas kepada para pembaca “Narasi Selembar Kain”. Ruang diskusi itu diharapkan memberikan kesempatan merefleksikan pengalaman masing-masing sehingga bisa menempatkan diri secara proporsional dalam konteks komunitas maupun konteks berbangsa-negara. Untuk menuliskan pengalaman para perempuan penenun itu idealnya memiliki kemampuan menghadirkan bahasa serta kompleksitas pengalaman mereka yang sangat mungkin tidak keluar dalam bahasa verbal serta tidak memiliki cara melihat orientalis atau mengandung bias stereotipe.

  3. Strategi

        - Studi literatur, termasuk bahan-bahan lisan, mengenai konteks sosial-budaya desa yang akan disinggahi dan aktivitas menenun.

        - Mengadakan pertemuan dengan sejumlah narasumber.

        - Menjalani keseharian para perempuan penenun.

  4. Aktivitas dan keterkaitan pada sasaran

     Untuk menghasilkan narasi perjalanan dengan melibatkan pengalaman perempuan penenun saya melakoni perjalanan dan tinggal di sejumlah desa di Propinsi Maluku. Selain akan mengamati dan mencatat, saya juga akan mempelajari proses menenun yang dalam prakteknya saya akan berbagi, berinteraksi, dan berdiskusi dengan para perempuan penenun.

  5. Latar belakang dan demografi pelaku proyek

     Penulis adalah perempuan dengan pengalaman menulis lebih dari 25 tahun, juga seniman teater dan aktif di organisasi—Institut Nalar Jatinangor sebuah organisasi nirlaba yang bergerak di kegiatan-kegiatan literasi yang berfokus pada keragaman dan kesetaraan—sejak 2001.

  6. Demografi kelompok target

        - Pembaca buku kelas menengah; sastrawan, budayawan, aktivis, mahasiswa/i, siswa/i SMA, kritikus, akademisi.

        - Berusia di atas 17 tahun.

        - Berkomitmen sosial, peka, & nasionalis.

  7. Hasil yang diharapkan

     Dari riset & residensi akan dituliskan sejumlah catatan perjalanan yang akan dterbitkan di media massa cetak atau media daring, juga dalam bentuk buku. Alternatif penerbitan buku: Gramedia Pustaka Utama, KPG, Marjin Kiri, atau Baca Baca; yang kesemuanya telah memiliki editor untuk menjadikan buku tersebut kelak layak dibaca/terbit.

  8. Indikator keberhasilan

     1. Diterbitkan dalam media massa cetak/media daring dengan jumlah pembaca minimal 500 orang.

     2. Diterbitkan dalam bentuk buku dengan jumlah terjual antara 500-1000 eksemplar.

     3. Diadakan sejumlah diskusi buku atau pembahasan buku di media massa.

  9. Durasi Waktu Aktivitas dilaksanakan

     Juni – November 2018

  10. Total kebutuhan dana

      IDR 50.000.000

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      IDR 50.000.000

  12. Sumber dana lainnya

      Tidak ada

  13. Kontribusi perorangan

      Selama mengerjakan program ini saya akan bekerja penuh termasuk dengan mengadakan perjalanan dan tinggal di sejumlah desa di Propinsi Maluku.

  14. Kontribusi kelompok target

      1. Membeli buku/media massa (koran atau majalah).

      2. Membahas dalam bentuk tulisan (resensi/kritik) atau dalam diskusi (makalah).
