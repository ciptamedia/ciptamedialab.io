---
title: Proposal Lengkap - Perempuan Dalam Semesta Lurik - Ciptaningrat Larastiti
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/perempuan-dalam-semesta-lurik
layout: proposal
author: Ciptaningrat Larastiti
categories:
- laporan
- CME
- Perempuan Dalam Semesta Lurik
---

![0734.jpg](/static/img/hibahcme/0734.jpg){: .img-responsive .center-block }

# Ciptaningrat Larastiti - Perempuan Dalam Semesta Lurik

**Tentang penerima hibah**

Pada 2007-2015 menempuh pendidikan sarjana dan pascasarjana di Jurusan Antropologi Budaya Universitas Gadjah Mada. Sejak 2008 mengikuti hibah penelitian mahasiswa setingkat fakultas, universitas sampai nasional. Pernah menjadi asisten peneliti di Pusat Studi Asia Pasifik UGM pada 2010-2013. Pernah menjadi peneliti Sajogyo Institute Bogor pada 2015-2017 untuk mengelola program belajar perempuan di lokasi krisis sosial ekologis akibat tambang dan perkebunan kelapa sawit, [http://pejuangtanahair.org/](http://pejuangtanahair.org/). Saat ini sedang aktif merintis kerja dan kehidupan kolektif bernama Samadhya Institute di Yogyakarta, [http://www.samadhya.org/](http://www.samadhya.org/). Penelitian terbaru bersama Emancipatory Rural Politics Initiative dapat dilihat di [https://www.tni.org/en/profile/ciptaningrat-larastiti](https://www.tni.org/en/profile/ciptaningrat-larastiti)

**Kontak**

  - Instagram: [@larastiti](https://www.instagram.com/larastiti/)

  - Website: [ciptahningrat.blogspot.co.id/](http://ciptahningrat.blogspot.co.id/)

**Lokasi**

Kecamatan Pedan, Kabupaten Klaten, Provinsi Jawa Tengah

**Deskripsi Proyek**

Kecamatan Pedan Kabupaten Klaten dikenal sebagai sentra produksi tenun lurik –kain warisan budaya yang bermotif garis. Usaha ini dimiliki pengusaha lokal selama dua sampai tiga generasi dan menyerap buruh terampil di sekitar Pedan. Sayangnya, saat ini, produksi lurik tengah mengalami krisis regenerasi pengetahuan dan reproduksi tenaga kerja terampil. 

Oleh karenanya, kami ingin belajar bersama komunitas buruh perempuan lurik tentang relasi produksi dan proses produksi lurik. Bentuk dari kegiatan ini adalah pengorganisasian belajar antara kami dan generasi muda lokal agar mau belajar kepada komunitas buruh lurik berusia lanjut. 

Pertama, di proses awal, kami melakukan pendekatan kepada satu pengusaha lurik agar ruang produksinya bisa digunakan sebagai ruang belajar. Kami juga melakukan pendekatan kepada setidaknya dua generasi muda lokal agar mau bekerja sama mempelajari proses produksi dan relasi produksi lurik. Kedua, selama lima bulan, kami mengajak kelompok muda belajar memproduksi satu lembar kain lurik ukuran 30-50 meter dengan cara melihat langsung dan mempraktikkannya. Ketiga, di sela-sela praktik, kami dan kelompok muda akan melakukan diskusi tentang bagaimana relasi produksi dan proses produksi lurik dilakukan? Apa persoalan mendasarnya?

- **Tujuan**

     - Mendorong partisipasi kelompok muda untuk terlibat dalam proses belajar tentang proses produksi lurik dan relasi produksi lurik.

     - Mendorong partisipasi kelompok muda untuk mempraktikkan produksi tenun lurik.

- **Sasaran**

  peneliti seni, seniman, akademisi, masyarakat umum

- **Latar Belakang**

  Sebagian besar buruh tenun lurik ialah perempuan berusia lanjut –di atas 60 tahun. Kondisi ini menandakan kemandegan generasi muda dalam memahami seluk beluk produksi lurik. Sementara itu, angkatan kerja muda lebih banyak terserap di industri skala besar seperti pabrik tenun lurik mesin, pabrik garmen sampai pabrik pakan ternak. Klaten menjanjikan konversi lahan persawahan berbiaya murah dan efisiensi produksi melalui upah buruh murah. 

  Di tengah pasar tenaga kerja yang kompetitif, pengetahuan dan pengalaman perempuan tentang lurik cenderung dikesampingkan. Secara perlahan, lurik mengalami krisis regenerasi pengetahuan dan tenaga kerja terampil. Pada akhirnya, usaha tenun lurik akan disamaratakan dengan pabrik besar dalam hal penyerapan buruh murah. Maka penting untuk mempelajari kembali proses produksi dan relasi produksi lurik sebagai kerja rintisan untuk mewariskan pengetahuan dari kelompok buruh lurik berusia lanjut kepada generasi muda.

  1. Keterkaitan pada kategori: Riset / Kajian / Kuratorial

     Kelompok peneliti muda dari Yogyakarta mendekati kelompok muda lokal untuk melakukan riset tentang relasi produksi dan proses produksi lurik yang selama ini tidak menarik bagi mereka. Pintu masuk dari riset ini ialah melakukan praktik menenun lurik di salah satu ruang produksi pengusaha lurik. Selama praktik berlangsung, entitas di dalamnya juga melakukan pengamatan dan wawancara melalui obrolan sehari-hari yang selanjutnya dimaknai melalui proses diskusi terbatas.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Riset ini ingin mengatasi masalah regenerasi pengetahuan tentang relasi produksi dan proses produksi lurik kepada generasi muda lokal. Kami ingin belajar untuk memahami, bagaimana relasi produksi lurik selama ini berlangsung? mengapa pengetahuan tentang lurik mengalami kemandegan ke generasi penerus? Pada ruang seperti apa pengetahuan tentang proses produksi lurik bisa diwariskan –kaitannya dengan ujicoba praktik produksi lurik oleh generasi muda?

  3. Strategi

        - Melakukan pendekatan kepada satu pengusaha lurik agar ruang produksinya berkenan dijadikan sebagai ruang belajar kelompok muda.

        - Melakukan pendekatan kepada kelompok muda melalui salah satu tenaga kerja muda lurik yang berusia 18 tahun. Dia menjadi pintu masuk untuk mengajak teman-temannya.

        - Memanfaatkan uang pertemuan dari dana CME sebagai uang kas kelompok agar proses belajar ini bisa berkesinambungan.

  4. Aktivitas dan keterkaitan pada sasaran

        - Mendokumentasikan dan menuliskan proses produksi lurik tahap demi tahap, siapa saja yang terlibat? Berapa lama waktu yang dibutuhkan untuk menuntaskan setiap tahap produksi? Serta kategorisasi motif-motif lurik.

        - Mendokumentasikan dan menuliskan relasi produksi lurik bersama-sama dengan kelompok tenaga kerja muda.

        - Mempraktikkan proses menenun lurik, mulai dari pewarnaan alami, penyisiran benang, penenunan sampai finishing bersama tenaga kerja muda dan kelompok tenaga kerja berusia lanjut.

  5. Latar belakang dan demografi pelaku proyek

     Perempuan berpendidikan Pascasarjana Antropologi yang terbiasa melakukan riset etnografi dan menggemari material kain artisan. Ia bersama kawannya, bernama Rembulan Lintang, menginisiasi ruang kerja dan kehidupan kolektif bernama Samadhya Institute. Rembulan Lintang adalah perempuan, kelas menengah berpendidikan Sarjana Arsitektur. Ia berpengalaman selama empat tahun bekerja sebagai fasilitator lapangan untuk membantu memfasilitasi ruang hidup kaum miskin perkotaan yang tinggal di bantaran kali.

  6. Pemimpin proyek

     Perempuan dengan pengalaman mengerjakan penelitian kualitatif etnografi selama sembilan tahun, sejak tahun 2008, berumur 28 tahun dari kelas menengah  dan berpendidikan Pascasarjana Antropologi. Secara pribadi menggemari material kain artisan termasuk kain lurik.

  7. Demografi kelompok target

     Angkatan kerja muda berusia 18-30 tahun, dimana salah satunya sudah ada yang bekerja sebagai tenaga kerja finishing kain lurik.

  8. Hasil yang diharapkan & Indikator keberhasilan

  9. Indikator keberhasilan

        1. Kelompok belajar muda mampu menjelaskan tahapan proses produksi pembuatan lurik dan relasi produksi lurik.

        2. Kelompok belajar muda mampu mempraktikkan proses produksi lurik dengan menghasilkan 30-50 meter gulung kain lurik.

  10. Durasi Waktu Aktivitas dilaksanakan

      Enam bulan

  11. Total kebutuhan dana

      Rp.135.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.25.000.000

  13. Sumber dana lainnya

      Belum ada

  14. Kontribusi perorangan

      Peneliti melakukan riset dan praktik bersama kelompok muda untuk memproduksi tenun lurik.

  15. Kontribusi kelompok target

         1. Kontribusi kelompok muda untuk mengalokasikan waktu dalam melakukan praktik menenun.

         2. Pengusaha lurik menyediakan satu mesin tenun untuk dimanfaatkan sebagai alat belajar.

         3. Kontribusi pengetahuan kelompok tenaga kerja berusia lanjut sebagai guru dalam melakukan praktik menenun dan menjelaskan relasi produksi maupun proses produksi lurik. 
