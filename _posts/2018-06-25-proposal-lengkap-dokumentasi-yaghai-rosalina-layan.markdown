---
title: Proposal Lengkap - Mendokumentasi Nyanyian Tradisi suku Yaghai - Septina Rosalina Layan
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/dokumentasi-yaghai
layout: proposal
author: Septina Rosalina Layan
categories:
- laporan
- CME
- Mendokumentasi Nyanyian Tradisi suku Yaghai, Auyu, Wiyagar
---

![0322.jpg](/static/img/hibahcme/0322.jpg){: .img-responsive .center-block }

# Septina Rosalina Layan - Mendokumentasi Nyanyian Tradisi suku Yaghai

**Tentang penerima hibah**

Septina Rosalina Layan adalah seniman musik/pesuara, pianis, komposer berbasis di Papua, Indonesia. Lahir di Merauke Papua pada tanggal 15 September 1989. Ia memperoleh gelar S1 Jurusan Musik/ komposisi musik dari Institut Seni Indonesia Yogyakarta tahun 2014.

Septina bekerja di Institut Seni Budaya Indonesia (ISBI) Tanah Papua, dan sebagai salah satu pendiri komunitas Action Papua yang bertekad untuk menggali, melestarikan dan mengembangkan budaya Papua. Septina menekuni karirnya dibidang komposisi musik yang tertarik pada nyanyian-nyanyian tradisi Melanesia yang dimulai dari suku-suku di Papua.  Karya-karyanya mengangkat nilai-nilai tradisi berupa nyanyian-nyanyian yang mencerminkan Ratapan, keindahan alam, kekayaan alam, keunikan dan kritik terhadap masalah-masalah sosial di Papua. Tahun 2014 Septina menggelar konser di Jogjakarta dengan judul karya Ihin Sakil Somalae (Ratapan Cendrawasih). Pada tahun 2017 Septina memperoleh Hibah Cipta Seni dari Yayasan Kelola Indonesia untuk mementaskan karya komposisi musik berjudul Sagu VS Sawit. Saat ini, Septina aktif berkarya dan aktif dalam berbagai kegiatan seni di Papua dan Indonesia, menetap di Jayapura bersama Suami dan anaknya.

**Kontak**

  - Instagram: [@ince_layan](https://www.instagram.com/ince_layan/)

**Lokasi**

Mappi, Papua

**Deskripsi Proyek**

Upaya pendokumentasikan nyanyian-nyanyian tradisi suku Yaghai, berupa rekaman audio, visual (video), dan teks notasi musik yaitu notasi angka/balok (dibukukan) berupa kumpulan nyanyian-nyian tradisi suku Yaghai. Upaya ini mengkaji peran perempuan Yaghai melalui lantunan nyanyian tradisi tersebut dengan harapan literatur audio visual dan tertulis yang dapat digunakan sebagai referensi arsip budaya pembelajaran di SD, SMP, SMA, Perguruan Tinggi dan menambah literatur tentang nyanyian tradisi di Papua.  Kurangnya dokumentasi yang terkaji mengenai nyanyian suku Yaghai,dan proses asmilasi budaya suku lain (luar Papua) yang tinggal di wilayah Mappi dimana  nyanyian-nyanyian dari luar semakin berkembang, budaya rap (hiphop) semakin marak digemari oleh kaum muda, sedangkan nyanyian-nanyian tradisi seakan menjadi tanggung jawab orangtua saja. Lantunan tradisi akan mengkaji bagaimana suku Yaghai menghargai perempuan dan memberi tempat/ penghargaan terhadap perempuan.

- **Tujuan**

  1. Mendokumentasikan nyanyian tradisi suku Yaghai berupa rekaman audio, visual (video), dan teks notasi musik; notasi angka/balok (dibukukan) berupa kumpulan nyanyian-nyanyian tradisi suku Yaghai. 

  2. Mengkaji peran perempuan Yaghai melalui lantunan nyanyian tradisi tersebut dengan harapan literatur audio visual dan tertulis dapat digunakan sebagai referensi arsip budaya pembelajaran di SD, SMP, SMA, Perguruan Tinggi dan menambah literatur tentang nyanyian tradisi di Papua.

- **Sasaran**

  1. Proses Pendokumentasian Nyanyian di wilayah suku Yaghai Timur di kampung Kepi, Emete, Soba, Dagemon.

  2. Proses Pendokumentasian Nyanyian Wilayah Yaghai Barat di kampung Katan, Yatan, Linggua, Mur, Agham, Ima.

  3. Menyelesaikan projek berupa; audio rekaman, video rekaman dan buku hasil tulisan teks musik (notasi angka dan notasi balok) serta deskripsi nyanyian hasil Kajian.

  4. Mengadakan sosialisasi hasil dokumentasi audio, video dan buku kumpulan nyanyian-nyanyian tradisi suku Yaghai kepada masyarakat di Kabupaten Mappi.

- **Latar Belakang**

  1. Keterkaitan pada kategori: Riset / Kuratorial

     Nyanyian di Papua memiliki keragaman sesuai dengan suku dan wilayahnya. Terdapat tujuh wilayah adat dan lima wilayah budaya yang terbagi antara pantai dan pesisir, lumpur dan rawa, gunung dan lembah serta daratan. Kemajemukan suku, bahasa dan wilayah merupakan suatu kekayaan yang perlu didokumentasikan, diteliti dan dikaji dengan tepat agar diperoleh literatur berupa audio, video (visual), teks musik (notasi angka dan balok).

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        1. Projek ini penting dilakukan karena kurangnya dokumentasi yang terkaji mengenai nyanyian suku Yaghai, sementara proses asmilasi budaya semakin marak terjadi, toleransi terhadap budaya suku-suku lain (luar Papua) yang tinggal di wilayah Mappi semakin tinggi, nyanyian-nyanyian dari luar semakin banyak dan berkembang, budaya rap (hiphop) semakin marak digemari oleh kaum muda, sedangkan nyanyian-nanyian tradisi seakan menjadi tanggung jawab orangtua.

        2. Secara adat, masyarakat sudah mempunyai padangan tersendiri terhadap keberadaan perempuan secara keseluruhan. Seorang laki-laki menginginkan perempuan yang dapat bekerja keras dan dapat melahirkan banyak anak, baik anak laki-laki maupun anak perempuan, bahwa perempuan memiliki tempat di belakang atau mengurus dapur (rumah tangga saja), sehingga hal ini menjadi penting bahwa melalui lantunan nyanyian tradisi yang berkaitan dengan makna dan fungsi, maka akan dikaji bagaimana suku Yaghai memberikan tempat/penghargaan kepada perempuan.

        3. Pendidikan seni budaya di sekolah-sekolah perlu memiliki literatur tentang nyanyian-nyanyian tradisi dari suku Yaghai.

        4. Kurangnya literatur tentang nyanyian/musik Papua berkaitan dengan kurangnya SDM yang bersedia memberikan waktu dan kerelaan untuk menghadapi tantangan lapangan yang cukup kompleks untuk mendokumentasikan, mengkaji secara tepat salah satu kekayaan budaya yang jika dibiarkan akan semakin sulit untuk ditemukan, diharapkan hibah dana dapat memberi dukungan berupa fasilitas yang dibutuhkan demi mewujudkan proyek ini.

  3. Strategi

     Pendekatan secara keluargaan sesuai dengan budaya dan tradisi, melakukan diskusi, dialog dengan para pelaku budaya, pelantun nyanyian. Menjalin network di Mappi untuk perjalanan ke kampung-kampung di wilayah suku Yaghai.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Aktivitas untuk sasaran 1: Menjalin network di Mappi untuk perjalanan ke kampung-kampung di wilayah suku Yaghai. Mengadakan perjalanan ke kampung-kampung di wilayah Yaghai Timur. Bertemu dengan narasumber/kepala suku, para pelantun nyanyian tradisi di setiap kampung. Mengadakan diskusi dan dialog berkaitan dengan isi nyanyian-nyanyian tradisi yang berkonteks pada perempuan Yaghai serta mendokumentasikan nyanyian-nyanyian tersebut.

        2. Aktivitas untuk sasaran 2: Mengadakan perjalanan ke kampung-kampung di wilayah Yaghai Barat. Bertemu dengan narasumber/kepala suku, para pelantun nyanyian tradisi di setiap kampung. Mengadakan diskusi dan dialog berkaitan dengan isi nyanyian-nyanyian tradisi yang berkonteks pada perempuan Yaghai serta mendokumentasikan nyanyian-nyanyian tersebut.

        3. Aktivitas untuk sasaran 3: Proses mengolah data audio, mentranskip nyanyian-nyanyian yang telah di rekam kedalam notasi angka dan notasi balok.  Bekerjasama dengan penterjemah bahasa Yaghai kedalam bahasa Indonesia. Mengurutkan dokumentasi rekaman audio dan video sesuai dengan konteks budaya suku Yaghai (nyanyian-nyanyian bercerita tentang ratapan, ucapan syukur, sukacita dan lainnya). Mengkaji peran perempuan Yaghai melalui lantunan nyanyian tradisi tersebut. Mencetak hasil dokumentasi yang telah di transkip (notasi angka dan notasi balok) berupa buku yang isinya adalah kumpulan nyanyian dan deskripsi tentang nyanyian yang berkaitan dengan peran perempuan Suku Yaghai.

        4. Aktivitas untuk sasaran 4: Mengadakan pertunjukan Musik, sekaligus menyampaikan hasil dokumentasi kepada masyarakat yang berpusat di kota Kepi.

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah perempuan dengan pengalaman berkarya 10 tahun terakhir, khususnya komposisi musik tentang nyanyian-nyanyian dan ensambel musik Papua, yang mengerti betul setiap tahap prosesnya. Anggota tim lainnya videografer yang akan merekam video nyanyian-nyanyian, penterjemah dari bahasa Yaghai ke bahasa Indonesia dan pedamping yang akan mengantar dan mendapinggi proses pendokumentasian nyayian.

  6. Demografi kelompok target

     Perempuan dan laki-laki pelantun nyayian tradisi suku Yaghai dari setiap kampung sasaran.

  7. Hasil yang diharapkan & Indikator keberhasilan

     Menghasilkan dokumentasi berupa, audio, video dan buku kumpulan nyanyian-nyian tradisi suku Yaghai yang dapat digunakan sebagai arsip budaya, literatur pembelajaran di sekolah-sekolah SD, SMP, SMA, Perguruan Tinggi dan  menambah literatur tentang nyanyian tradisi di Papua. Menghasilkan sebuah kajian tentang peran perempuan suku Yaghai yang dikaji melalui lantunan nyanyian-nyanyian tersebut.

  8. Durasi Waktu Aktivitas dilaksanakan

     Durasi waktu proses pendokumentasian sampai selesai adalah 7 bulan

  9. Total kebutuhan dana

     Rp80.300.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp60.000.000

  11. Sumber dana lainnya

      belum ada

  12. Kontribusi perorangan

      Usaha untuk melanjutkan pendokumentasian nyanyian-nyanyian tradisi diwilayah Mappi pada suku Wiyagar dan  Auyu pada empat tahun kedepan dengan bantuan hibah  dana dari sumber-sumber lain. Proses pendokumentasian nyanyian-nyanyian ini sekaligus memberikan pegetahuan dan ruang kepada pribadi untuk megkomposisikan nyanyian ordinarium (lagu misa inkulturatif) bergaya Suku Yaghai yang selama ini menjadi kerinduan masyarakat setempat.

  13. Kontribusi kelompok target

      Memicu masyarakat pendukung budaya untuk mempertahankan nyanyian-nyanyian tersebut dalam kondisi dan perkembangan saat ini. Memicu kaum muda suku Yaghai untuk belajar dan turut bertanggung jawab mempertahankan nyanyia-nyanyian tradisinya.
