---
title: Laporan Naratif - Suara Sinden Ginonjing - Nur Handayani
date: 2019-05-16
permalink: /ciptamediaekspresi/laporan-naratif/sinden-ginonjing
layout: proposal
author: Nur Handayani
categories:
- laporan
- Laporan Naratif
- CME
- 'Suara Sinden “Ginonjing”'
---

# Laporan Naratif - Suara Sinden “Ginonjing”

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Pelajaran pertama yang didapatkan adalah tentang disiplin dalam hal administratif dan pentingnya dunia kearsipan. Selain itu, hal lain yang membuat saya belajar banyak adalah terbangunnya relasi antar seniman. Melalui kegiatan hibah ini, membuat saya berinteraksi dengan berbagai seniman, dan vendor-vendor yang bergerak di bidang event organizer. Lebih dari itu, proyek ini menjadikan saya lebih pengalaman dalam menangani produksi pentas musik berskala besar.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Aktivitas yang akan dilakukan adalah pagelaran seni. Pergelaran ini akan menampilkan orkestra sindhenan, yang akan melibatkan sembilan pesindhen. Dalam karya ini, sindhenan akan dibagi menjadi beberapa bagian suara, tetapi dengan sudut pandang karawitan, seperti suara pesindhen dengan nada seleh nem (6), seleh ro (2), seleh lu dan seleh ji (1). Nada-nada tersebut dalam dunia karawitan jarang sekali dipadukan. Dalam karya ini, perpaduan antar nada tersebut berusaha disatukan dan akan menimbulkan celah nada atau anak nada akibat benturan nada-nada tersebut, dalam istilah karawitan disebut dengan ngeng. Lebih lanjut karawitan yang digunakan sebagai pengiring adalah karawitan puteri.
>
> Karya ini diberi judul “Ginonjing” kata tersebut berasal dari kata “gonjing” yang berarti goyang karena tidak seimbang, mendapat sisipan “in” menyatakan ketidak sengajan. Ginonjing berarti goyah tanpa tahu siapa yang membuat posisi tidak seimbang . Atau ginonjing bisa berarti pengalaman kehilangan gravitasi sehingga orang tidak lagi bisa mengendalikan dirinya (St. Sunardi, dalam Jurnal Kalam, nomor. 21, 2004: 23). Secara implisit, kata ginonjing dapat merepresentasikan kegelisahan saya tentang fakta-fakta muram tentang perempuan khususnya pesindhen. Karya ini berdurasi 60 menit pertunjukan.
>
> Tempat yang dipilih untuk pergelaran pertama ini adalah Pendapa Institut Seni Indonesia (ISI) Surakarta pada Juli 2018. Pergelaran kedua ditampilkan dalam post fest yang digelar di Institut Kesenian Jakarta (IKJ) pada tahun 2018.

**Ceritakan hasil dari aktivitas tersebut.**

Konser Karya Ginonjing digelar di Tetaer Besar ISI Surakarta, sekaligus merupakan Ujian Terbuka Thesis Karya Seni Jurusan Penciptaan Seni Program Pascasarjana ISI Surakarta Saudari Nur Handayani. Pertunjukan tersebut dihadiri oleh berbagai lapisan audiens meliputi: mahasiswa seni, media masa, akademisi, seniman, budayawan, aktivis, serta masyarakat karawitan. Konser Karya Ginonjing, berkisah tentang keberpihakan perempuan terhadap konsep feminisme. Artinya pertunjukan tersebut lebih banyak berbicara tentang keadilan keperempuanan melalui surat kartini. Oleh sebab itu audiens yang hadir kebanyakan adalah aktivis perempuan dan mahasiswi di dengan berbagai disiplin ilmu. Karya ini juga dipentaskan dalam acara Postfest pada 10 Juni 2018 di Kompleks Taman Ismail Marzuki, di hadiri oleh seniman dari berbagai disiplin.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Masalah yang ingin diatasi adalah keberpihakan kepada perempuan, khususnya para pekerja seni seperti sinden dan sejenisnya. Selain keberpihakan, juga ingin memunculkan isu tentang konsep feminisme kepada publik, agar martabat perempuan dapat setara dengan realitas sosial.

**Ceritakan hasil dari mengatasi isu tersebut.**

Pasca Karya Ginojing ini dihelat,  dalam hal ini Nur Handayani sering diundang sebagai narasumber untuk berbicara tentang feminisme dan gender equality di berbagai diskusi. Dampak kongkret dari isu feminisme di wilayah seni pertunjukan khususnya sindhen adalah: memupuk rasa percaya diri di antara sesama pesinden bahwa stigma negatif itu mampu dijawab dengan kreativitas yang baik. Dampak lain setelah isu tersebut dilontarkan melalui karya Ginonjing, hari ini mulai muncul karya-karya seni selain musik yang juga mengangkat tema tentang keperempuanan. Melihat hal itu, artinya karya ini berhasil menjadi stimulan kepada seniman lain untuk tetap gencar menyarakan konsep feminisme di tengah masifnya konsep patriarki.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> Indikator keberhasilannya adalah, memicu karya-karya baru dengan perspektif perempuan, utamanya di lingkungan seni. Isu feminisme menjadi santer sebagai konsepsi seni. Lebih dari itu, pengkarya beberapa kali menjadi narasumber dalam diskusi dalam forum musik.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Menurut hemat saya, jika melihat indikator di atas, termasuk dalam kategori berhasil, mengapa, karena indikator di atas secara kongkret benar-benar berimbas kepada seniman lain yang mengangkat tentang feminisme seperti: penggiat teater Luna Karisma, koreografer Otniel Tasman, dan seniman lain khusunya di wilayah Kota Solo.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Ya, proyek ini telah banyak berpengaruh terhadap diri saya pribadi dalam dunia mengorganisasi pertunjukan. Saya secara pribadi mendapatkan peningkatan mutu kerja di dalam bidang administratif sekaligus pengelolaan keuangan. Cermat, teliti dan tepat sasaran adalah perubahan yang kongkret dalam etos kerja saya.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Selama perjalanan penciptaan karya ini, tentu banyak sekali kendala yang dihadapi, terutama mengatur jadwal latihan teman-teman seniman. Karena mereka yang membantu dalam karya ini juga memiliki kesibukan masing-masing. Jadi, untuk menentukan jadwal latihan butuh energi tersendiri. Namun semua berjalan dengan baik, karena semua seniman yang membantu, wajib menyerahkan kalender kegiatan berkesenimannya selama bulan Mei dan Juni 2018. Dari situ kami dapat mengatur jadwal latihan dengan baik.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Menurut saya, proyek ini merupakan upaya perempuan dalam memposisikan diri di era digital sebagai insan yang memiliki keadilan yang sama di antara kaum laki-laki. Selain memposisikan diri, juga sebagai usaha untuk mendorong lahirnya feminist legal theory. Agar kehidupan dunia perempuan memiliki hak dan keadilan yang seadil-adilnya di mata dunia.
