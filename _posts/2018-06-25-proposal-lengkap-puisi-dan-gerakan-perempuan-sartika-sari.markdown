---
title: Proposal Lengkap - Puisi dan Gerakan Perempuan - Sartika Sari
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/puisi-dan-gerakan-perempuan
layout: proposal
author: Sartika Sari
categories:
- laporan
- CME
- Puisi dan Gerakan Perempuan
---

![0619.jpg](/static/img/hibahcme/0619.jpg){: .img-responsive .center-block }

# Sartika Sari - Puisi dan Gerakan Perempuan

**Tentang penerima hibah**

Sartika Sari adalah pegiat sastra yang lahir di Medan, 1 Juni 1992. Selain menulis puisi, cerpen, esai, di media cetak lokal, nasional, dan media daring, Sartika fokus meneliti karya sastra perempuan, misalnya dalam skripsi di Universitas Negeri Medan dan tesisnya di Universitas Padjadjaran Bandung. Kini, ia aktif berkegiatan di Laboratorium Sastra Medan, sejumlah komunitas sastra, dan ruang-ruang diskusi.

**Kontak**

  - Facebook: [facebook.com/softauwag](https://www.facebook.com/softauwag)

**Lokasi**

Sumatera Utara

**Deskripsi Proyek**

“Puisi dan Gerakan Perempuan” merupakan proyek penelitian terhadap tulisan-tulisan perempuan dalam bentuk puisi yang diterbitkan di surat kabar tahun 1900-an dan 1930-an. Penelitian ini dilakukan untuk mengungkap pemikiran-pemikiran dan gerakan perempuan dalam menggugat hak-hak dasarnya yang disampaikan melalui puisi. Di samping itu, proyek ini berusaha untuk merekam, mendokumentasikan, dan menghadirkan wacana tandingan terhadap sejarah kesusastraan Indonesia dari Sumatera Utara yang hingga saat ini, menurut saya, tidak berpihak pada perempuan. Termasuk di dalamnya adalah menemukan dan memaparkan bagaimana strategi perempuan mempertahankan eksistensinya di tengah situasi sosial budaya pada tahun 1900-an hingga 1930-an. Untuk memperkuat pembacaan terhadap konteks puisi, penelitian ini akan didukung dengan pendekatan New Historicism yang menyandingkan puisi dengan peristiwa-peristiwa sejarah yang terjadi pada masa yang bersamaan yang tertuang dalam tulisan berbentuk esai, iklan, dan berita. Dengan demikian, terkhusus pada surat kabar yang dikelola perempuan, saya akan merekam, mendokumentasikan, dan meneliti seluruh tulisan yang diterbitkan. Hal ini menurut saya, sangat potensial untuk menemukan bagaimana situasi yang melingkari keberadaan perempuan pada masa itu. Termasuk untuk menemukan narasi ketokohan perempuan di wilayah Sumatera Utara. Proyek ini memfokuskan penelitian pada surat kabar, mengingat pada tahun-tahun tersebut perempuan terbilang produktif menulis di surat kabar, meski jarang sekali tercatat dalam sejarah. Semua akan didokumentasikan dan dipublikasikan dalam bentuk buku, pameran, dan publikasi daring di media sosial, blog, dan web untuk menjadi media informasi bagi siapa pun. Terutama perempuan Indonesia.

- **Tujuan**

  Mengembangkan khazanah penelitian tentang kepengarangan dan pergerakan perempuan di Sumatera Utara sehingga dapat dijadikan referensi dalam pembacaan atas kedudukan dan peran perempuan Indonesia (dari Sumatera Utara) dalam kesusastraan maupun pergerakan nasional.

- **Sasaran**

     1. Meneliti karya sastra (puisi) yang ditulis oleh perempuan di surat kabar di wilayah Sumatera Utara dengan pendekatan new historicism untuk mengungkap gagasan-gagasan pergerakan yang berhubungan dengan berbagai persoalan sosial, politik, dan budaya yang disampaikan perempuan tahun 1900-an hingga 1930-an.

     2. Mengarsipkan karya-karya sastra dan tulisan perempuan yang merepresentasikan kehidupan perempuan pada tahun 1900-an hingga 1930-an dalam bentuk surat kabar (fotokopi atau microfilm) dan foto.

     3. Mengembangkan khazanah penelitian pada teks-teks yang ditulis oleh perempuan dan menyebarluaskan informasi hasil penelitian ini kepada publik, sehingga dapat dijadikan wacana pendukung dalam upaya reinterpretasi penulisan sejarah sastra Indonesia.

     4. Menempatkan teks-teks yang ditulis oleh perempuan (puisi atau teks lainnya) sebagai bagian dari wacana sejarah, sosial, dan kebudayaan.

- **Latar Belakang**

  Sumatera Utara menjadi salah satu kiblat penulisan jejak kepengarangan Indonesia. Hal ini dikarenakan banyak sastrawan besar lahir dari Sumatera Utara. Misalnya Sutan Takdir Alisyahbana, Amir Hamzah, Armijn Pane, Sanusi, Merari Siregar, dan Chairil Anwar. Maka, penulisan sejarah sastra Indonesia pun nyaris hanya menggaungkan nama-nama itu dan tidak mencantumkan nama perempuan karena tulisan-tulisan perempuan kerap dinilai tidak mengandung gagasan besar. Padahal pada tahun yang sama perempuan juga menulis di surat kabar. Misalnya dalam Perempoean Bergerak dan Soeara Iboe yang dikelola oleh perempuan. Pada dua surat kabar itu, saya menemukan puisi dan esai perempuan yang berisi gagasan-gagasan besar. Perempoean Bergerak merupakan surat kabar perempuan pertama di Sumatera Utara. Di tengah penanggalan perkembangan puisi mutakhir Indonesia yang menggunakan puisi karya penyair laki-laki, puisi perempuan yang terbit di Perempoean Bergerak hadir dengan ciri yang sama. Sayangnya, sejarah sastra Indonesia tidak merekam jejak itu. Selain dua surat kabar tersebut, masih ada beberapa surat kabar lain yang belum berhasil saya teliti karena keterbatasan dana dan sebagainya. Saya yakin, di tengah keterbatasan ruang yang dimiliki perempuan pada masa itu, perempuan tetap bergerak melalui tulisan. Di samping itu, sejarah sastra Indonesia yang belum berpihak pada surat kabar, menurut saya, sudah selayaknya diberi wacana tandingan.

  1. Keterkaitan pada kategori: Riset / Kajian / Kuratorial

     Proyek “Puisi dan Gerakan Perempuan” merupakan proyek penelitian yang berupaya memajukan pengetahuan mengenai praktik kebudayaan perempuan yang dibangun melalui literasi, secara spesifik melalui penulisan puisi dan sejumlah esai di surat kabar yang terbit di Sumatera Utara tahun 1900-an hingga 1930-an. Riset akan dilakukan pada surat kabar yang terkumpul dari sejumlah perpustakaan dan kolektor. Hasilnya akan dipublikasikan dalam bentuk buku berlisensi CC-BY. Selain itu, sejumlah tulisan-tulisan perempuan dari surat kabar yang diteliti akan dipamerkan bersamaan dengan acara peluncuran buku hasil penelitian.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Penelitian terhadap karya-karya penulis perempuan tahun 1900-an hingga 1930-an masih terbatas. Padahal sejak tahun 1900-an, perempuan telah menulis dan menyampaikan gagasan-gagasannya pada masyarakat.  Alhasil, kedudukan perempuan pada masa-masa itu seringkali dinilai tidak berarti. Sebagian besar kritikus sastra lebih dominan memperhatikan karya penulis laki-laki yang diterbitkan penerbit besar. Maka, proyek ini berusaha meneliti tulisan-tulisan perempuan yang terbit di surat kabar yang justru terbilang kecil dan cenderung belum terjamah namun mengandung gagasan-gagasan besar. Dengan demikian, produk dari proyek ini diharapkan dapat menjadi referensi baru yang juga berpotensi menjadi wacana tandingan dalam upaya reinterpretasi sejarah sastra Indonesia dari wilayah Sumatera Utara. Selain itu, proyek ini diharapkan dapat turut memperluas khazanah penelitian pergerakan perempuan di Indonesia.

  3. Strategi

     Mengumpulkan, membaca, dan menganalisis tulisan-tulisan perempuan atau yang berkaitan dengan isu perempuan yang terbit di surat kabar yang terbit di Sumatera Utara tahun 1900-an sampai 1930-an secara keseluruhan (meliputi surat kabar umum dan surat kabar perempuan). Pengumpulan surat kabar itu saya lakukan melalui proses pencarian di sejumlah perpustakaan dan kolektor. Selain itu, untuk menambah informasi seputar kondisi sosial, budaya, dan politik pada masa itu, saya turut mengumpulkan dan membaca buku-buku sejarah yang bersangkutan. Dalam proses penajaman analisis, saya juga akan melakukan diskusi dengan sejumlah sastrawan dan sejarawan di Sumatera Utara perihal persoalan yang saya teliti. Ditambah dengan diskusi bersama pakar feminis untuk memperdalam argumentasi dalam penelitian yang berkaitan dengan perspektif kajian dan penggunaan sejumlah teori. Mengingat tulisan-tulisan yang akan saya teliti ditulis pada tahun 1900-an, maka jika dibutuhkan saya akan berdiskusi dengan tokoh adat (terutama Melayu) untuk memperjelas makna idiom-idiom atau kosakata kedaerahan yang kurang saya pahami. Setelah melalui keseluruhan proses tersebut, hasil penelitian saya akan diterbitkan dalam bentuk buku yang akan diluncurkan pada bulan Oktober. Dalam peluncuran buku tersebut, akan diundang sejumlah tokoh sastrawan, sejarawan, dan mahasiswa Bahasa dan Sastra Indonesia di Sumatera Utara agar informasi terkait kesusastraan Indonesia ini tersebar luas dan dapat dimanfaatkan serta dapat memperbarui pengetahuan. Buku yang dicetak juga akan dikirimkan kepada beberapa kelompok literasi se-Indonesia. Pada saat yang bersamaan akan digelar pameran tulisan-tulisan perempuan yang terbuka untuk publik.

  4. Aktivitas dan keterkaitan pada sasaran

        - Mendokumentasikan dan menuliskan proses produksi lurik tahap demi tahap, siapa saja yang terlibat? Berapa lama waktu yang dibutuhkan untuk menuntaskan setiap tahap produksi? Serta kategorisasi motif-motif lurik.

        - Mendokumentasikan dan menuliskan relasi produksi lurik bersama-sama dengan kelompok tenaga kerja muda.

        - Mempraktikkan proses menenun lurik, mulai dari pewarnaan alami, penyisiran benang, penenunan sampai finishing bersama tenaga kerja muda dan kelompok tenaga kerja berusia lanjut.

  5. Latar belakang dan demografi pelaku proyek

     Kontribusi untuk sasaran A: Mengumpulkan, membaca, dan menganalisis puisi-puisi perempuan yang terbit di surat kabar tahun 1900-an hingga 1930-an untuk mengungkap gagasan yang terkandung di dalam puisi tersebut. Selain itu, untuk mempertajam penafsiran atas kemunculan gagasan-gagasan dalam puisi, digunakan pendekatan New Historicism dengan menyandingkan teks-teks puisi dengan teks-teks lain yang terbit pada masa yang sama. Dengan demikian, dapat diperoleh gambaran kondisi sosial, budaya, dan politik yang berpotensi memiliki keterkaitan dengan isu yang disampaikan dalam puisi perempuan.

     Kontribusi untuk sasaran B: Mencari dan mengumpulkan surat kabar yang terbit di tahun 1900-an hingga 1930-an di Sumatera Utara dari perpustakaan Nasional Republik Indonesia, Perpustakaan Ichwan Azhari, beberapa kolektor surat kabar, dan melalui perjalanan ke beberapa wilayah yang pada tahun-tahun tersebut menerbitkan surat kabar (misalnya Lubuk Pakam, Padang Sidempuan, Sibolga). Selain itu, untuk memperluas penggambaran konteks sosial, saya akan berusaha mengumpulkan sejumlah surat kabar dari wilayah lain. 

     Kontribusi untuk sasaran C:

        - Mengadakan diskusi dan bedah buku hasil penelitian terhadap puisi-puisi perempuan di surat kabar di Sumatera Utara tahun 1900-an hingga 1930-an yang menghadirkan sastrawan, kritikus sastra, budayawan, sejarawan, mahasiswa Bahasa dan Sastra Indonesia, serta masyarakat umum. 

        - Menyelenggarakan pameran tulisan-tulisan perempuan Sumatera Utara tahun 1900-an sampai 1930-an yang bertendensi menunjukkan gagasan pergerakan.

        - Mempublikasikan kegiatan diskusi dan pameran tersebut di sejumlah media lokal dan jika memungkinkan juga media nasional. 

        - Mengirim buku versi cetak kepada beberapa kelompok literasi di Indonesia dan dalam versi daring dapat diakses masyarakat luas dengan lisensi CC-BY.

     Kontribusi untuk sasaran D: Menyebarluaskan informasi secara aktif melalui tulisan-tulisan di surat kabar cetak maupun daring kepada masyarakat mengenai keberadaan tulisan-tulisan perempuan di surat kabar tahun 1900-an sampai 1930-an. Salah satunya adalah dengan membagikan teks-teks hasil pindaian surat kabar yang membuat tulisan perempuan.

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman mengerjakan penelitian terhadap teks-teks sastra (dalam skripsi, tesis, artikel ilmiah, esai di surat kabar), sastrawan, dan editor selama tujuh tahun, berumur 26 tahun. Satu kolaborator adalah perempuan, mahasiswa pascasarjana, dan penulis sastra.

  7. Demografi kelompok target

     Mahasiswa, sastrawan, budayawan, kritikus sastra, akademisi sastra Indonesia atau masyarakat umum yang menyukai aktivitas membaca dan menulis sastra.

  8. Hasil yang diharapkan & Indikator keberhasilan

     Kesuksesan proyek ini ditunjukkan dengan keberhasilan menemukan, mendokumentasikan, dan meneliti jejak kepengarangan perempuan di Sumatera Utara melalui surat kabar yang ada pada tahun 1900-an sampai 1930-an. Selain itu, keberhasilan penelitian ini ditunjukkan pula dengan keberhasilan memetakan keberadaan pengarang perempuan dalam kanon sastra Indonesia dan peta gerakan perempuan di Indonesia. Semua akan disampaikan dalam bentuk buku, pameran puisi-puisi dan tulisan perempuan tahun 1900-an sampai 1930-an serta publikasi daring di media sosial, blog, dan web.

  9. Indikator keberhasilan

        1. Terkumpulnya puisi-puisi dan teks lain yang ditulis perempuan atau tulisan yang tidak ditulis oleh perempuan tetapi berisi isu perempuan di surat kabar tahun 1900-an sampai 1930-an yang terbit di Sumatera Utara.

        2. Terbitnya buku hasil penelitian “puisi dan gerakan perempuan” di surat kabar tahun 1900-an sampai 1930-an yang terbit di Sumatera Utara.

        3. Terselenggaranya pameran tulisan-tulisan perempuan tahun 1900-an sampai 1930-an di Sumatera Utara.

        4. Adanya liputan dari media cetak atau daring baik lokal maupun nasional mengenai temuan penelitian dan kegiatan peluncuran buku serta pameran proyek puisi dan gerakan perempuan.

  10. Durasi Waktu Aktivitas dilaksanakan

      Mei 2018-Oktober 2018

  11. Total kebutuhan dana

      Rp 49.580.000.-

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp 49.580.000.-

  13. Sumber dana lainnya

      Tidak ada

  14. Kontribusi perorangan

      Saya akan berusaha semaksimal mungkin sampai masa yang tidak terbatas, dengan sumber daya yang sudah terbangun selama tujuh tahun terakhir.

  15. Kontribusi kelompok target

         1. Dukungan dari sastrawan, budayawan, kritikus sastra, sejarawan, akademisi, dan praktisi feminis yang selama ini memberi perhatian terhadap sastra Indonesia.

         2. Siaran pers dimuat oleh media massa lokal dan jika memungkinkan, nasional.
