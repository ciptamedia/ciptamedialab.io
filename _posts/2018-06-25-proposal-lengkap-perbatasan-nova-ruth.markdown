---
title: Proposal Lengkap - Perbatasan - Nova Ruth Setyaningtyas
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/perbatasan
layout: proposal
author: Nova Ruth Setyaningtyas
categories:
- laporan
- CME
- "Perbatasan: Catatan Perjalanan"
---

![0663.jpg](/static/img/hibahcme/0663.jpg){: .img-responsive .center-block }

# Nova Ruth Setyaningtyas - Perbatasan

**Tentang penerima hibah**

Nova Ruth bertumbuh dengan menyanyi di gereja, belajar membaca Al-Qur’an serta mempelajari budaya Jawa dan gamelan ketika menjadi murid Taman Siswa. Proyek musik pertamanya, Twin Sista, duo hiphop yang mengangkat isu sosial dan lingkungan dalam lirik-liriknya telah tampil di berbagai konteks seperti penjara wanita, mall, festival puisi dan punk gigs di Indonesia. Sebagai pelaku seni yang merdeka, Nova Ruth membawa lirik-lirik puitisnya dalam musik hiphop ke Australia di tahun 2008, memenuhi undangan dari Gang Festival. Sejak tahun 2009, Nova mulai berkolaborasi dengan Filastine, proyek audio-visual dari Barcelona, dan memproduksi lagu serta video klip maupun materi visual di atas panggung hingga sekarang. Di sela-sela kesibukannya, kolaborasi dengan LSM dan komunitas yang mengambil fokus lingkungan tetap ia jalankan. Selain bermusik, Nova juga membuka sebuah cafe yang merupakan ruang budaya yang menggelar konser kecil di hari Senin bernama Legipait di kota tempat lahirnya, Malang.

Saraswati Sunindyo adalah pelaku budaya dan mahasiswa sosiologi yang terpaksa pergi dari Indonesia di tahun 80’an. Suaranya yang vokal saat tergabung dalam Teater Bulungan, Teater Keliling Jakarta dan Bengkel Teater membuatnya ditempa ancaman rezim orba. Menjadi lulusan University of Wisconsin dengan gelar Master dan PhD, Saraswati berhasil menjadi dosen gender studies baik di universitasnya dan lalu diterima di University of Washington di Seattle, tempat dia tinggal sampai saat ini. Dengan profesinya tersebut, Saraswati mendapatkan gelar ‘Honorary Chair of Women Studies”. Saat ini Saraswati tetap menjadi sosok budayawati yang dihormati di Seattle dan bermain musik kentrungan sambil bercerita. Saraswati mulai mendapat panggilan kakek nenek moyangnya di Jawa dan melakukan tur musik yang berjudul “Mbarang Jantur”.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Buku ini akan ditulis oleh Nova Ruth dimanapun dia berada dan dibimbing oleh Saraswati Sunindyo PhD. yang bertempat tinggal di Seattle, Amerika Serikat. Proses rekaman dan pencetakan akan dilaksanakan di Malang, Jawa Timur, Indonesia.

**Deskripsi Proyek**

Perbatasan adalah sebuah buku musikal yang berisi kumpulan catatan perjalanan Nova Ruth dalam meretas berbagai batas di dalam perjalanannya bermusik. Batas-batas tersebut tidak hanya yang kasat mata namun juga yang imajiner dan diciptakan oleh pola pikir masyarakat dari berbagai latar belakang negara dan budaya. Catatan perjalanan dalam sebuah buku ini akan dilengkapi dengan album akustik yang menelanjangi karya-karya yang pernah diciptakan dan dinyanyikan oleh Nova Ruth dari genre hiphop, jazz, RnB sampai pentatonik jawa. Evolusi di dalam penulisan lirik dan aliran musik Nova Ruth juga menandakan perjalanannya menuju kematangan hidup dan bermusik. 

Menulis selain lirik merupakan tantangan besar bagi Nova Ruth. Dalam proses penulisan buku ini, Nova Ruth akan dibimbing oleh Saraswati Sunindyo PhD., seorang perempuan akademis Indonesia dari generasi sebelumnya yang khusus mempelajari dan sempat mengajar ilmu sosiologi "gender studies" di University of Washington. Saraswati juga seorang aktivis dan budayawati yang terpaksa pergi dari Indonesia karena ancaman rezim orba di tahun 80-an. Tantangan berbagai bentuk perbatasan juga dialami oleh Saraswati dan membuat bimbingannya menjadi sangat relevan dalam proses pengkaryaan ini.

- **Tujuan**

  Menuliskan perjalanan meretas batas oleh Nova Ruth dirasa perlu dengan harapan tulisannya dapat memberikan inspirasi bagi perempuan lain terutama yang bergerak di bidang seni. Selain itu minat membaca di Indonesia sebagai negara berbudaya tutur juga sangat kurang. Dengan mengemas bacaan yang lebih dari sebuah buku, Nova Ruth berharap strategi ini dapat ikut andil tidak hanya untuk menambah minat membaca, namun juga menambah daftar nama penulis perempuan Indonesia.

- **Sasaran**

     - Perempuan Indonesia.

     - Perempuan Indonesia yang bergerak di bidang seni.

     - Penonton beragam dan lintas generasi.

     - Pengguna sosial media di dalam dan luar negeri.

     - Pemegang kebijakan.

- **Latar Belakang**

  Dalam meniti karir musiknya, Nova Ruth sering bersinggungan dengan birokrasi perbatasan dunia dan mengalami berbagai penolakan serta kerumitan yang disebabkan oleh kekuatan paspor, jenis kelamin, status melajang yang dinilai terlalu lama, keterbatasan ekonomi dan juga kemandiriannya sebagai pelaku musik. Penolakan konstan tersebut lalu menimbulkan trauma yang cukup dalam sehingga berpengaruh pada kesehatan mental dan fisik. Tidak hanya itu, di dalam Indonesia sendiri perempuan yang memilih karir sebagai seniman khususnya penyanyi hampir dipastikan akan sering mendapat stereotip dan perlakuan yang kurang layak namun dilumrahkan karena negara ini semakin mengarah kepada budaya yang patriarki. Menolak untuk menjadi perempuan bentukan orde baru yang dapur-kasur-sumur, Nova Ruth-pun lalu dinilai sebagai perempuan yang terlalu berani dan menakutkan paling tidak di kota kecilnya, Malang.

  1. Keterkaitan pada kategori: Lintas Generasi

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        - Ketidaksetaraan gender di dunia seni.

        - Keterbatasan hak untuk berpindah-pindah sebagai perempuan Indonesia,

        - Keterbatasan dalam berekspresi sebagai perempuan.

        - Kurangnya kolaborasi antar perempuan dari berbagai latar belakang yang berbeda.

        - Kurangnya minat membaca di Indonesia.

        - Daftar pendek nama penulis perempuan Indonesia.

  3. Strategi

     Daring:

        - Karya tulis format pendek dalam format PDF dan mini album yang dapat diunduh melalui situs web Cipta Media Ekspresi.

        - Pendistribusian karya tulis lengkap dan album melalui portal-portal daring seperti Bandcamp atau melewati situs web khusus sebagai faktor pendukung pendanaan kreatif. 

        - Promosi melalui media sosial.

     Luring:

        - Pendistribusian versi cetak buku dan album di toko-toko mandiri seperti Ommunium Bandung dan Kata Kerja Makassar.

        - Pendistribusian versi cetak buku dan album saat melakukan konser keliling.

  4. Aktivitas dan keterkaitan pada sasaran

     Nova Ruth memiliki basis penggemar dan komunitas yang cukup luas meski tidak berada di arus utama. Konser solo gitarnya di ruang-ruang kecil dihadiri paling tidak 50 penonton atau lebih. Konser kolaborasinya dengan Filastine yang diadakan di seluruh dunia ditonton oleh ruang berkapasitas 200 penonton sampai ruang publik dengan penonton sampai 5000 orang. Penggemar Nova Ruth sangat beragam dan lintas generasi. Dilengkapi dengan Saraswati, yang memiliki basis penggemar dan komunitas yang sangat beragam juga. Dimulai dari yang muda sampai pada generasinya. Saraswati memiliki murid-murid yang setia mengikuti perkembangannya hingga kini dan mereka adalah yang percaya dengan kesetaraan gender. Baik Nova Ruth ataupun Saraswati berharap bahwa ada kebebasan berpindah bagi rakyat Indonesia. Mereka berdua berharap isu ini dapat muncul ke permukaan dan dapat menjadi perhatian mereka-mereka yang memegang kebijakan di level pemerintahan.

  5. Latar belakang dan demografi pelaku proyek

     Indonesia adalah negara yang sangat unik, menjadikan seni bagian dari nafas anak-anak kelahirannya. Di setiap jengkal, orang Indonesia akan berseni dalam berbagai bidang untuk memenuhi kebutuhan egonya namun juga berkarya dan berkontribusi kepada lingkungan sekitarnya. Begitulah yang Nova Ruth dan Saraswati rasakan sejak lahir. Menjalani hidup seni, menyerap apapun yang baik dari pengaruh budaya asing untuk kesenian mereka, membagi kearifan lokal sampai ke luar negeri, menjadi lokal di dunia global, dan menjadi global di lingkup kelokalan mereka.

  6. Pemimpin proyek

     Proyek ini dipimpin oleh Nova Ruth yang memiliki pengalaman bermusik selama 18 tahun. Nova Ruth memiliki latar belakang desainer grafis dan juga memiliki keahlian menyunting video. Dengan keahliannya tersebut, Nova Ruth telah terlibat di dalam pembuatan 6 album dan menjadi produser video klip dan video-video untuk penampilannya dengan Filastine.

  7. Demografi kelompok target

     Target penikmat album buku ini berada di dunia maya dan nyata. Album buku versi mininya akan dapat diunduh di situs web Cipta Media Ekspresi, dan versi lengkapnya akan menyusul dan dapat dibeli secara daring. Versi cetak juga akan didistribusikan di distro buku serta merchandise kecil di Indonesia. Nova Ruth akan membawa album buku ini dalam turnya ke kantong-kantong kecil seperti Legipait, Kineruku, Kata Kerja, dsb. Tidak menutup kemungkinan album buku ini akan dipromosikan ke festival-festival literasi seperti Ubud Writer Festival dan Makassar Writer Festival.

  8. Hasil yang diharapkan & Indikator keberhasilan

     Proses penulisan buku sampai launching berjalan lancar tanpa halangan apapun. Semua narasumber bersedia dan ikhlas untuk terlibat dalam proyek ini. Hasil karya ini diharapkan dapat memberikan inspirasi bagi orang-orang bertubuh mini lainnya.

  9. Indikator keberhasilan

        - 200 unduhan di situs web CME.

        - Penjualan yang mencapai 500 kopi.

  10. Durasi Waktu Aktivitas dilaksanakan

        - Penulisan buku : April 2018 - Januari 2019

        - Rekaman album : Juli 2018

        - Mixing dan mastering album : Juli - Agustus 2018

        - Proses penyuntingan buku : Februari - April 2019

        - Cetak buku dan album : April - Mei 2019

        - Pertunjukan keliling dan distribusi karya : Juni 2019 - selesai

  11. Total kebutuhan dana

      Rp 160.000.000,-

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp 50.000.000,-

  13. Sumber dana lainnya

         - Dana Pribadi

         - Keuntungan penjualan karya fisik

         - Penjualan merchandise

  14. Kontribusi perorangan

         - Dana dan waktu Nova Ruth

         - Kesediaan waktu secara sukarela oleh Saraswati Sunindyo

  15. Kontribusi kelompok target

         - Ikut andil dalam promosi

         - Mengunduh karya

         - Membeli hasil karya fisik

         - Merealisasikan Indonesia yang setara
