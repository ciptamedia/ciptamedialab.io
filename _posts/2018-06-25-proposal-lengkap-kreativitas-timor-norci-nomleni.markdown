---
title: Proposal Lengkap - Pengembangan Kreativitas dalam Budaya Timor - Norci Nomleni
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/kertivitas-timor
layout: proposal
author: Norci Nomleni
categories:
- laporan
- CME
- Pengembangan Kreativitas Dalam Budaya Timor
---

![0216.jpg](/static/img/hibahcme/0216.jpg){: .img-responsive .center-block }

# Norci Nomleni - Pengembangan Kreativitas Dalam Budaya Timor

**Tentang penerima hibah**

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Nusa Tenggara Timur (NTT)

**Deskripsi Proyek**

Memahami budaya sebagai anugerah, saya dan kelompok mencoba mengembangkan kreativitas dalam budaya seperti memodifikasi barang-barang bekas (sepatu, tas, dompet, sarung hp) dan saya juga dapat memodifikasi sarung atau selimut sehingga bisa dipakai ke tempat tertentu (pesta dan gereja). Kreativitas ini akan berdampak pada nilai ekonomi yang sangat tinggi karena ada banyak sekali warna budaya yang terlihat dalam rajutan-rajutan motif yang kedepannya akan dimodifikasi dengan berbagai bentuk yang akan dikreasi pada barang-barang bekas, seperti sepatu, tas, dompet sarung hp dll, sehingga mempercantik produk yang dihasilkan. Ini akan menarik perhatian konsumen sehingga barang yang diproduksikan terjual banyak. Produk yang telah dihasilkan berhasil menarik minat para konsumen yang ada di dalam daerah maupun di luar daerah, selain karena bentuknya yang cantik, juga karena ketertarikan mereka dengan motif-motif Timor. Selain produk-produk yang saya hasilkan, saya juga bersama teman-teman membuat suatu kelompok tari, sebagai suatu dukungan kelancaran kegiatan gereja maupun kegiatan kemasyarakatan. Karena keterbatasan modal maka saya melakukan kerajinan ini masih dengan cara manual. Pemesan membawa bahan bekas seperti sepatu/satu helai selendang, Tas/ satu helai selendang,  dompet dan sarung hp/satu helai selendang dan hanya membayar ongkos kerja.

- **Tujuan**

  Perkembangan globalisasi sangat mempengaruhi pola pikir masyarakat, khususnya kaum muda tentang gaya hidup yang serba kekinian dalam hal berpenampilan sehingga dari pengalaman belajar tentang kreativitas saya mencoba mengembangkan beberapa model bahan-bahan bekas seperti sepatu, dompet, tas dan sarung hp bekas. Disamping itu, saya mencoba untuk memotivasi masyarakat atau kaum muda untuk melestarikan budaya masing-masing. Saya juga memotivasi mama-mama di kampung untuk lebih giat dalam menenun dan juga memberikan pola pikir yang positif kepada anak-anak muda yang terarah pada masa depan sehingga mereka dapat memahami budaya sebagai anugerah  yang sekaligus memiliki nilai ekonomi yang sangat tinggi. Selanjutnya, diharapkan anak-anak muda termotivasi untuk mengembangkan budaya seperti rajin menenun, dan juga memiliki kreativitas seperti halnya yang saya lakukan. Jika semua memahami budaya sebagai anugerah maka akan banyak bermunculan warna budaya yang masih tersembunyi. Tujuan dari mengembangkan kreativitas ini juga untuk mengurangi kemiskinan dan  mengurangi tenaga kerja indonesia di luar negeri (TKI).

- **Sasaran**

     - Mengembangkan kreativitas dalam budaya Timor melalui bahan-bahan yang diperlukan dan dimodifikasi dengan balutan motif-motif tenunan.

     - Meningkatkan ketahanan ekonomi dalam budaya melalui kegiatan kreativitas dalam balutan motif tenunan.

     - Membantu kelompok tenunan di berbagai pelosok desa dalam meningkatkan semangat dalam menenun.

- **Latar Belakang**

  Budaya adalah suatu anugerah yang diberikan Tuhan yang dinyatakan melalui cara hidup yang berkembang dan dimiliki bersama oleh sebuah kelompok orang dan diwariskan dari generasi ke generasi, hal ini perlu dikembangkan dan dijaga sehingga generasi muda terutama kaum perempuan dapat menjaganya dengan baik. Hal yang sudah kelompok saya lakukan adalah dengan memodifikasi barang-barang bekas menjadi barang layak pakai dengan motif kain tenunan Timor. Ini menjadi daya tarik baru serta menjadi tren budaya yang di kembangkan.

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Proyek ini termasuk dalam kategori kolaborasi karena proyek yang dikerjakan ini akan bekerja sama untuk pembuatan baju pengantin menggunakan kain tenunan. Di samping itu juga, sangat membutuhkan lebih dari 2 orang untuk membantu dalam pembuatan sepatu,  tas,  dompet, dll.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Dalam keterkaitan dengan proyek yang saya lakukan ini, kurang adanya perhatian khusus untuk kreativitas dalam budaya oleh karena keterbatasan pemahaman tujuan dari budaya itu sendiri. Sebabnya saya mencoba untuk menjawab kebutuhan pasar masyarakat dengan memproduksi hasil karya melalui kreativitas pembuatan barang bekas dengan bahan motif.

  3. Strategi

     Kebutuhan konsumen terkadang berubah-ubah untuk itu kedepannya saya akan mendesain model-model dalam kreativitas yang baru misalkan pakaian pengantin, kemudian perlengkapan peminangan mulai dari aksesoris dan juga tempat atau dulan peminangan dengan bahan alami dengan menggunakan daun lontar dihiasi dengan motif-motif timor, ini yang belum diproduksi oleh masyarakat pada umumnya sehingga dengan demikian dipastikan masyarakat akan banyak yang meminati.

  4. Aktivitas dan keterkaitan pada sasaran

     Bersama dengan kelompok kerja, menghasilkan produk-produk yang unik dalam menjawab kebutuhan masyarakat. Di samping itu juga, memberi kesempatan kepada kami untuk dapat menjual produk-produk unik yang kami buat di tempat wisata.

  5. Latar belakang dan demografi pelaku proyek

     Hal yang sudah kelompok saya lakukan adalah dengan memodifikasi barang-barang bekas menjadi barang layak dipakai dengan motif kain tenunan timor. Ini menjadi daya tarik baru serta menjadi tren budaya yang di kembangkan. Selain itu, kami juga menjaga budaya ini dengan baik dan memperkenalkan budaya Timor dengan tenunan motif-motif Timor.

  6. Pemimpin proyek

     Pemimpin Proyek adalah seorang perempuan dengan pengalaman memimpin proyek kreativitas dalam budaya. Dalam perkembangan di masa sekarang, masyarakat sangat meminati nuansa budaya dalam hal fesyen. Untuk itu, dengan pengalaman yang matang saya menuangkan ide-ide baru kreativitas dalam budaya Timor untuk mengembangkan aktivitas budaya dalam balutan-balutan Timor yang diekspresikan melalui bahan-bahan bekas seperti sepatu, tas, dompet, sarung hp dan juga ide baru yang dimunculkan untuk pembuatan gaun pengantin dengan motif Timor.

  7. Demografi kelompok target

     Masyarakat pada umumnya sangat tertarik dengan tren klasik. Dengan adanya ide-ide yang saya munculkan dalam pembuatan bahan-bahan bekas (tas, sepatu, dompet, sarung hp, dll.) dengan balutan motif akan meningkatkan produksi barang-barang yang akan kami pasarkan.

  8. Hasil yang diharapkan & Indikator keberhasilan

     Melalui kebutuhan pasar yang meningkat dengan tren budaya, saya mencoba menarik perhatian konsumen melalui kreativitas pembuatan barang-barang bekas, dalam hal ini sepatu, tas, dompet, sarung hp, dll. Dengan cara demikian sangat banyak peminat yang memesan, sekalipun dalam pembuatannya masih sangat manual (bahan ditanggung oleh pemesan misalnya sepatu dan satu buah selendang). Namun banyak yang tertarik dengan pembuatan barang-barang bekas dengan balutan motif-motif timor.

  9. Indikator keberhasilan

     Berhasil menarik perhatian konsumen dalam ide-ide yang dituangkan dalam kreativitas kami, tidak terbatas dengan hanya melakukan balutan motif Timor untuk tas, dompet, sepatu dll. Tapi lebih daripada itu kedepannya kami segera meluncurkan ide baru dalam pembuatan baju pengantin dan perlengkapan dulan masuk minta atau lebih tepatnya dulan peminangan dengan nuansa natural dengan menggunakan daun lontar dihiasi dengan motif-motif Timor.

  10. Durasi Waktu Aktivitas dilaksanakan

      6 bulan

  11. Total kebutuhan dana

      Rp.50.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.50.000.000

  13. Sumber dana lainnya

      Tidak Ada

  14. Kontribusi perorangan

      Tidak Ada

  15. Kontribusi kelompok target

      Tidak Ada
