---
title: Peluncuran 6 Proyek Seni-Budaya Peserta Sekolah Pemikiran Perempuan
date: 2018-10-19 13:00:00 +07:00
categories:
- CME
- Publikasi
tags:
- CME
- Siaran Pers
- Sekolah Pemikiran Perempuan
- Wikimedia Indonesia
- Ford Foundation
author: lisabona
comments: true
img: /uploads/blog/sekolah-pemikiran-perempuan.jpg
---

![sekolah pemikiran perempuan](/uploads/blog/sekolah-pemikiran-perempuan.jpg)

Sekolah Pemikiran Perempuan yang ditujukan untuk mematangkan perspektif perempuan dan menajamkan gagasan proyek seni-budaya para pesertanya sudah usai. Mereka yang telah mengikuti proses ini dengan baik mendapatkan hibah sebesar 20 juta rupiah untuk mewujudkan rencana kerjanya masing-masing dalam kerangka hibah Cipta Media Ekspresi.

Dengan bangga Cipta Media Ekspresi mengucapkan selamat mewujudkan karya-karya menarik kepada:

- [Indah Fikriyyati - 0092 Art Exhibition “Santriwati dan Giraffes Kingdom”](https://www.ciptamedia.org/ciptamediaekspresi/penerimahibah/0092)

- [Lia Anggia Nasution - 0365 Penelitian Sejarah Pers Perempuan Sumatra Utara](https://www.ciptamedia.org/ciptamediaekspresi/penerimahibah/0365)

- [Sri Harti, M.Sn - 0523 Wanita Kusumayuda](https://www.ciptamedia.org/ciptamediaekspresi/penerimahibah/0523)

- [Rena Amalika Asyari - 0529 Pendokumentasian Perempuan Intelektual Sunda Lasminingrat](https://www.ciptamedia.org/ciptamediaekspresi/penerimahibah/0529)

- [Jelsy Meivira - 0923 Periuk Wanita Kalimantan Barat](https://www.ciptamedia.org/ciptamediaekspresi/penerimahibah/0923)

- [Azka Amalina - 1155 Akhir Usia, Buku dan Pameran Fotografi](https://www.ciptamedia.org/ciptamediaekspresi/penerimahibah/1155)

Selamat bekerja dan berkarya bagi para peserta Sekolah Pemikiran Perempuan!
