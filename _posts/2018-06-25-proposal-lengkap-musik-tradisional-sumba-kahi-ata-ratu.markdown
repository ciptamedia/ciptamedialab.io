---
title: Proposal Lengkap - Perempuan Sumba dan Musik Tradisional - Kahi Ata Ratu
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/perempuan-sumba-dan-musik-tradisional
layout: proposal
author: Kahi Ata Ratu
categories:
- laporan
- CME
- Perempuan Sumba dan Musik Tradisional
---

![0995.jpg](/static/img/hibahcme/0995.jpg){: .img-responsive .center-block }

# Kahi Ata Ratu - Perempuan Sumba dan Musik Tradisional

**Tentang penerima hibah**

Kahi Ata Ratu adalah seorang seniman perempuan yang berasal dari desa Rindi, Sumba Timur. Ia berhasil bertahan dari tantangan patriarki dunia seni dimana dalam budaya Sumba Timur seorang wanita yang sudah menikah harus benar–benar mengabdikan dirinya hanya untuk suami dan keluarga. 

Ia memulai karirnya sebagai seorang seniman “jungga” pada usianya yang sangat belia yaitu 8 tahun bersama 3 orang teman perempuannya. Pada usia 13 tahun, ia pertama kali menciptakan lagu pertamanya untuk seorang pria yang dia cintai tetapi dia dipaksa untuk menikah dengan orang lain. Ia adalah salah satu dari sekian wanita yang menjadi korban kawin paksa dalam budaya Sumba.

Kahi Ata Ratu harus menjalani kehidupannya sebagai seorang istri muda dan menjadi istri kedua dalam pernikahannya. Tetapi itu tidak membuat ia menjadi patah semangat untuk tetap berkarya. Walaupun dalam budaya Sumba bahwa setelah menikah harus fokus kepada suami dan keluarga tetapi ia selalu mempunyai niat dan tekad yang kuat untuk tetap berkarya. Motto dari Ata Ratu adalah “Saya Mau main Jungga sampai mati“. 

Dengan motto itu sendiri ia mampu “lolos” dari patriarki dunia seni dimana ketiga orang teman perempuannya yang sudah mulai berkarya bersamanya di masa kecil itu memilih berhenti berkarya karena mereka sudah menikah dan fokus kepada suami dan keluarga.

Ata Ratu mempunyai 4 orang anak, 3 orang anak laki–laki dan seorang perempuan serta 2 orang cucu. Ia kehilangan suaminya pada usia 54 tahun dan sekarang ia berjuang untuk menghidupi keluarganya dengan bermain jungga. Ia sekarang tinggal dan menetap di Palangay bersama anak–anak dan cucunya.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Sumba Timur

**Deskripsi Proyek**

- **Tujuan**

     1. Saya ingin melakukan riset mengenai lagu–lagu dulu yang hampir punah dan juga melakukan kolaborasi dengan generasi tua untuk dapat mempertahankan kelestarian lagu–lagu dulu yang sudah tidak dikenal oleh generasi muda saat ini.

     2. Saya ingin mengatakan kepada dunia dan generasi muda saat ini bahwa wanita juga mampu untuk berkarya dan juga bisa memimpin para pria.

- **Sasaran**

     1. Menuliskan booklet untuk lagu–lagu yang sudah saya ciptakan dan nyanyikan.

     2. Bernyanyi bersama anak–anak dari tingkat SD–SMP dalam tahapan konser.

     3. Membuat rekaman video maupun visual untuk pendokumentasian karya–karya saya.

     4. Membuat konser di beberapa daerah.

- **Latar Belakang**

  Musik tradisional Sumba sudah mulai dilupakan bahkan menjadi barang langka di kampungnya sendiri. Musik tradisional tidak mendapat perhatian dari generasi muda saat ini karena era modern yang membawa generasi muda untuk lebih fokus kepada musik-musik modern. Persoalan ini terjadi karena salah satu alasan yaitu tidak banyaknya promosi dan pengenalan mengenai musik tradisional. Di Sumba, ada beberapa pemusik tradisional yang saat ini terus mengembangkan upaya pelestarian musik Sumba tetapi pemusik didominasi oleh laki–laki dan terbatas sekali perempuan yang fasih memainkan musik tradisional, sekaligus menciptakan lagu.

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Proyek ini akan menghubungkan kerja sama antar beberapa pemusik lintas generasi yang akan bekerja sama dalam meneliti lagu–lagu lama yang hampir punah.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        1. Publikasi yang terbatas.

        2. Belum adanya pertunjukan yang secara khusus menampilkan karya–karya saya.

        3. Belum ada pendokumentasian karya – karya saya.

        4. Banyak lagu tua yang belum dikenal di generasi muda saat ini yang hampir punah.

        5. Belum ada pertunjukkan yang mengkolaborasikan antara generasi tua dan generasi saya.

  3. Strategi

     Dibutuhkan waktu yang cukup lama untuk melakukan riset lagu–lagu dulu dan juga harus melakukan perjalanan riset yang mengharuskan berkeliling daerah Sumba Timur untuk menemukan beberapa orang tua yang masih tahu tentang lagu–lagu lama yang hampir punah itu. Dan juga membutuhkan waktu untuk melakukan pendekatan kepada kalangan muda untuk meyakinkan kepada mereka bahwa memainkan jungga bukanlah hal yang kuno sehingga mereka boleh mencintai musik tradisional.

  4. Aktivitas dan keterkaitan pada sasaran

        - Aktivitas untuk sasaran 1 : Mencari orang yang mampu untuk menerjemahkan lagu–lagu saya dalam Bahasa Kambera (Bahasa Sumba), Bahasa Indonesia dan Bahasa Inggris.

        - Aktivitas untuk sasaran 2 : Berkoordinasi dengan Dinas pendidikan Sumba Timur untuk dapat melibatkan anak–anak SD dan SMP dalam kegiatan lokakarya dan juga konser kolaborasi dengan mereka.

        - Aktivitas untuk sasaran 3 : Melakukan rekaman audio dan video dengan seorang komposer.

        - Aktivitas untuk sasaran 4 : Mengadakan konser tunggal di dua daerah yang berbeda untuk mempertunjukkan lagu–lagu lama yang sudah hampir punah itu sehingga orang–orang bisa mengenal kembali lagu–lagu lama Sumba Timur.

  5. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman berkarya 48 tahun yang profesional dalam memainkan jungga dan juga menciptakan lagu–lagu Sumba dengan lawiti yang baik dan benar. Anggota tim lainnya adalah komposer dan juga asisten yang mampu menerjemahkan lagu–lagu Sumba ke dalam Bahasa Indonesia dan juga Bahasa Inggris. Serta beberapa seniman yang profesional yang akan membantu dalam riset lagu–lagu lama yang sudah hampir punah sehingga mereka bisa membantu mempercepat dalam proses riset ini.

  6. Demografi kelompok target

     Semua seniman lintas generasi dengan kemampuan profesional dalam memainkan jungga serta mengerti bahasa mendalam Sumba Timur

  7. Hasil yang diharapkan & Indikator keberhasilan

  8. Indikator keberhasilan

        1. Riset dan kolaborasi dengan minimal 10 orang spesialis musik lagu dulu dari generasi tua dan juga seorang spesialis akan mengajarkan bahasa mendalam.

        2. Ada minimal 3 pertunjukan (konser) musik tradisional Sumba dan lagu-lagu ciptaan saya di 3 lokasi untuk menampilkan lagu dulu yang sudah saya pelajari dan juga lagu yang baru saya ciptakan. Satu dari pertunjukkan ini akan dilaksanakan di Sekolah Dasar di Pahunga Lodu untuk mengajarkan atau melatih beberapa anak SD bermain musik tradisional dan akan melakukan konser bersama mereka.

        3. Setiap pertunjukan dihadiri oleh 100 orang dari berbagai latar belakang pendidikan, status sosial, gender, dan usia.

        4. Ada 300 booklet yang berisikan lagu-lagu yang saya ciptakan dan saya nyanyikan dalam konser. Saya menulis lirik dalam Bahasa Kambera (Bahasa Sumba), Bahasa Indonesia, dan Bahasa Inggris.

        5. Ada dokumentasi audio video dengan semua kolaborasi para seniman jungga dan penyanyi dengan terjemahan Bahasa Sumba Kambera, Bahasa Indonesia dan Bahasa Inggris. (untuk contoh bisa lihat di situs web saya di www.ppp.virb.com)

  9. Durasi Waktu Aktivitas dilaksanakan

     9 Bulan

  10. Total kebutuhan dana

      Rp 150.000.000

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp 150.000.000

  12. Sumber dana lainnya

      Belum ada

  13. Kontribusi perorangan

      Perupa akan terus memberikan kursus jungga kepada generasi muda sebagai bentuk kepedulian terhadap kepunahan pemain jungga yang berkompeten.

  14. Kontribusi kelompok target

      Menginspirasi generasi muda untuk melanjutkan karyanya sebagai bentuk cinta terhadap daerah sumba Timur dan juga menginspirasi wanita lainnya untuk tetap berkarya walaupun sudah menikah.
