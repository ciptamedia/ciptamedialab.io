---
title: Proposal Lengkap - Penelitian Sejarah Pers Perempuan Sumatera Utara - Lia Anggia Nasution
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/penelitian-sejarah-pers
layout: proposal
author: Lia Anggia Nasution
categories:
- laporan
- CME
- Penelitian Sejarah Pers Perempuan Sumatera Utara
---

![0365.jpg](/static/img/hibahcme/0365.jpg){: .img-responsive .center-block }

# Lia Anggia Nasution - Penelitian Sejarah Pers Perempuan Sumatera Utara

**Tentang penerima hibah**

Lia Anggia Nasution, perempuan yang lahir dan berasal dari Medan. Aktif menulis cerpen dan puisi sejak duduk dibangku SLTA. Memiliki nama pena Anggia Djohan, beberapa karya telah dimuat di media cetak di Medan juga dimuat dalam buku Pesona Gemilang Musim, Kumpulan Puisi Penyair Perempuan Indonesia II (2004) serta buku Medan Sastra, Pumpunan Puisi, Cerpen, Drama, Esai, Sastrawan Sumatera (2007). Serius meniti karir menjadi jurnalis di Harian Sumut Pos (Jawa Pos Grup) hingga menjadi Redaktur (2004-2010). Pernah menjadi penulis pada bulletin Suara Perempuan dan Sorot Kemiskinan yang diterbitkan Yayasan Sada Ahmo di Medan. Menggagas lahirnya Tabloid pendidikan Gemilang dan Majalah Anak Beber (Belajar dan Bermain). Memiliki seorang putra yang kini duduk di bangku SMP. Saat ini masih menjadi kontributor di Harian Koran Sindo, aktif sebagai tenaga pendukung Humas dan Keprotokolan Pemprovsu, dipercaya sebagai Kabid Pendidikan dan Litbang Forum Jurnalis Perempuan Indonesia (FJPI) dan masih berbagi ilmu sebagai tenaga pengajar di Kampus Sekolah Tinggi Ilmu Komunikasi Pembangunan (STIK-P) Medan.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Medan, Sumatera Utara

**Deskripsi Proyek**

Penelitian Sejarah Pers Perempuan di Sumut

(Studi Analisis Isi dengan Perspektif Feminis dalam Konten Koran ‘Perempoen Bergerak’ di Sumut Periode 1919-1920)

Pers perempuan tertua di Sumatera Utara (Sumut) dari literatur yang diperoleh sementara untuk saat ini adalah Koran, ‘Perempoean Bergerak’, terbit tahun 1919 di Medan. Redaksi Perempoean Bergerak ini merupakan sekelompok perempuan dengan latar belakang setingkat SMP dan SMA.

Saat ini untuk sementara sudah ditemukan ada sebanyak delapan edisi Koran ‘Perempoean Bergerak’ dengan penerbitan sebulan sekali, mulai dari bulan Mei 1991 hingga Desember 1991. Saat ini masih belum ditemukan Koran tersebut yang diterbitkan tahun 1920. Namun berdasarkan sumber sejarah dari Roemah Sedjarah Ichwan Azhari di Medan disebutkan Koran Perempoean Bergerak ini terbit dari tahun 1919-1920. (Hal ini akan diverifikasi ulang apakah koran ini memang terbit hingga tahun 1920 atau hanya sampai Desember 1919).

Diketahui edisi pertama dari Koran Perempoean Bergerak ini terbit pada Mei 1919. Di mana Pemimpin Redaksi dari koran ini seorang lelaki yakni Parada Harahap. Dia adalah seorang jurnalis Indonesia yang lahir di Pargarutan, Sipirok, Tapsel. Ia dijuluki King of the Java Press karena kemauannya yang keras dan semangat belajarnya yang tinggi, baik secara otodidak maupun mengikuti kursus-kursus. (Wikipedia.org)

Selain Parada, ada beberapa nama redaksi yang tertera di halaman pertama koran tersebut. Mereka adalah redaksi Boetet Satidjah, Medewerkster; AS Hamidah, CH Bariah, Indra Bongsoe, Siti Sahara juga Rabiatoel Adwie. Selain itu ada juga Direksi T.A Bariah dan Administratur, Abdul Rachman. Nama Parada Harahap masih tertera hingga pada edisi kedua koran ini di bulan Juni 1919. Namun di bulan Juli hingga Desember nama Parada Harahap sebagai pemimpin redaksi, begitu juga nama Abdul Rachman sebagai administratur sudah tidak ada. Hal ini juga menjadi menarik untuk diteliti tentang bagaimana relasi perempuan dan laki-laki untuk membidani lahirnya dan berlangsungnya koran Perempoean Bergerak ini, hingga mengapa Parada Harahap dan Abdul Rachman hanya ada pada dua edisi koran tersebut.Hal menarik juga dalam delapan edisi koran Perempoean Bergerak ini ditemukan kata-kata yang mengajak perempuan untuk berjuang dan bergerak. Kata-kata agitasi ini ditemukan dalam tulisan redaksi maupun dalam tulisan kiriman yang dimuat di media tersebut.

Contohnya ; Perempuan memang mesti djadi goeroe nummer satoe dalam roemah, sebab itoelah sebaik-baiknya perempoean semoela anak patoet djoega di bersekolahkan sebagaimana saudara- saudaranja lelaki. Sesoenggoehnja di zaman doelokala hanja orang lelaki sadja jang pergi dari roemah boeat menoentoet ilmoe. (Koran Perempoean Bergerak, edisi Juli 1919).

Contoh lainnya ; Wahai kaoemkoe pehak perempoean, saja memperingatkan, djikalau boekan kita sendiri mesti memperbaiki nasib kita dengan sekeras-kerasnya dan bila kita masik masoek dengan kelemahan kita dan ketjantikan kita, soedah tentulah kita akan tinggal terjitjir selama- lamanja.

Berbagai tema juga ditemukan dalam koran Perempoean Bergerak ini, seperti tema mitos tentang mendidik anak, kesehatan reproduksi, pendidikan perempuan, kepedulian kaum perempuan terhadap kondisi sosial hingga persoalan bagaimana mendidik dan menjaga kesehatan anak dan lainnya.

Bahkan, semboyan koran ini di halaman depan selalu tertera ; “Diterbitkan oentoek penjokong pergerakan kaoem perempoean, sekali seboelan (boeat sementara) oleh pergerakan perempoean di Medan Deli.

Koran Perempoean Bergerak ini bukanlah satu-satunya koran perempuan yang terbit di Sumut. Terdapat juga surat kabar dan majalah yang teridentifikasi lainnya yakni; Parsaoelian Ni Soripada (Tarutung, 1972), Soeara Iboe (Sibolga, 1932), Beta (Tarutung, 1933). Keoetamaan Istri (Medan, 1937-1941), Menara Poetri (Medan, 1938), Boroe Tapanoeli (Padang Sidempuan, 1940), Dunia Wanita (Medan, 1949-1980 an) dan tahun 1951 terbit majalah Melati yang dipelopori oleh Julia Hutabarat dan tabloid ini lahir di Tarutung. (sumber ; Roemah Sedjarah Ichwan Azhari).
Namun untuk saat ini, saya masih menemukan data Koran Perempoean Bergerak. Ketersedian data inilah yang membuat saya memilih untuk fokus membuat penelitian isi konten Koran Perempoean Bergerak.

Metodologi penelitian yang akan digunakan nantinya adalah analisis isi kualitatif dengan perspektif feminis. Dimana dalam penelitian ini akan dimulai dengan kata kunci, perempuan, ayo perempuan, wahai perempuan ataupun kata-kata agitasi lainnya yang mengajak perempuan untuk bergerak dan berjuang.

Langkah yang akan dilakukan dalam penelitian ini akan dimulai dari meneliti teks Koran Perempoean Bergerak. Dari delapan edisi ada sebanyak 32 halaman koran yang akan diteliti. Penelitian dilakukan dengan melihat dari keseluruhan teks baik yang dituliskan oleh redaksi maupun direksi ataupun tulisan yang dikirim dari penulis yang berasal dari berbagai daerah. Selain teks juga akan diteliti diksi, makna simbolik dari kata, tanda baca hingga hubungan antar paragraf.

Kemudian teks yang memunculkan tentang ajakan perempuan untuk bergerak ataupun berjuang akan dikelompokkan seperti isu yang paling dominan dimunculkan, siapa saja penulisnya, apa saja temanya, dll.
Setelah meneliti teks, selanjutnya diteliti tentang dapurnya redaksi Perempoean Bergerak. Di sini akan diteliti mengenai siapa saja yang terlibat di dalam dapur redaksi, dalam langkah ini akan diverifikasi apakah nama-nama perempuan yang ada di jajaran redaksi merupakan benar perempuan atau malah sebenarnya adalah laki-laki yang menyamar menggunakan nama perempuan untuk permasalahan keamanan di masa itu. Apa saja latar belakang dari masing- masing personel di jajaran redaksi.

Selain itu juga diteliti mengenai bagaimana relasi perempuan dan laki-laki yang terlibat di koran tersebut. Apakah Parada Harahap atau pun lelaki lainnya yang terlibat di dalam koran Perempoean Bergerak memiliki relasi yang seimbang dengan perempuan yang ada di kolom redaksi seperti Boetet Satidjah dan lainnya. Termasuk mengapa Pemred, Parada Harahap hanya ada terlibat dalam dua edisi. Bagaimana alasan koran ini terbit maupun alasan kenapa tidak lagi terbit, dan lainnnya. Hal-hal seperti ini nantinya akan diverifikasi melalui data sekunder seperti hasil penelitian dan buku ataupun diverifikasi dengan sejarawan ataupun pengamat media.

Langkah terakhir juga akan dilakukan penelitian mengenai korelasi koran Perempoean Bergerak dengan gerakan perempuan di masa itu. Bagaimana hubungan redaksi dengan para penulis dari berbagai daerah di Indonesia. Seperti apa mobilisasi gerakan perempuan di masa itu, termasuk apakah koran Perempoean Bergerak ini menjadi alat propaganda untuk perjuangan ataupun pergerakan perempuan di masa itu.

Apalagi perkembangan wilayah serta moda transportasi pada masa itu sangat mudah dengan adanya kereta api Deli Maatschappij yang mengangkut hasil perkebunan dari satu daerah ke daerah lainnya di Sumut. Tentunya, transportasi ini memudahkan pengangkutan barang, termasuk Koran Perempoean Bergerak untuk disebarluaskan ke berbagai daerah.

Ditambah lagi dugaan kalau percetakan koran ini merupakan anak perusahaan dari Deli Maatschappij, karena koran ini dicetak pada percetakan N.V Handel Maatschappij ‘SETIA BANGSA’ Medan. Dalam sejarah Deli Maatschappij didirikan pada tahun 1869 oleh Jacob Nienhuys dan Peter Wihelm Janssen sebagai perusahaan budi daya tembakau dengan konsensi untuk Kesultanan Deli di Sumatera, Hindia Belanda, 50 persen saham Deli Maatschapij dibagi untuk Nederlandsche Handel – Maatschappij. (Wikipedia.org).

Proyek penelitian ini direncanakan hasilnya akan berbentuk jurnal ilmiah. Dengan harapan hasil penelitian ini nantinya dapat menjadi acuan informasi maupun data tentang sejarah pers perempuan di Sumut, khususnya mengenai Koran Perempoean Bergerak. Keputusan untuk membuat jurnal ilmiah ini juga disebabkan minimnya informasi mengenai pers maupun pers perempuan baik di Sumut maupun di Indonesia.

Untuk hasil jurnal ilmiah ini, sebelumnya akan dilakukan pencarian jurnal yang tepat dan dapat memuat hasil penelitian. Setelah memilih jurnal, maka strategi yang dilakukan adalah mempelajari sistematika penulisan dari ruang jurnal yang dipilih agar hasil penelitian ini nantinya dapat dimuat dalam jurnal tersebut. Adapun target dari pembaca dalam proyek penelitian ini nantinya adalah perempuan, jurnalis, sejarawan juga kalangan akademisi.

**Durasi Waktu Aktivitas dilaksanakan**

4 bulan (Oktober 2018 - Februari 2019)

**Total kebutuhan dana**

Rp20.000.000

**Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi**

Rp20.000.000

**Sumber dana lainnya**

Belum ada
