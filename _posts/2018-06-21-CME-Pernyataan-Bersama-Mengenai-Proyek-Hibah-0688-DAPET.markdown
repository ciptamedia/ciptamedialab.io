---
title: Pernyataan Bersama Mengenai Proyek Hibah 0688 DAPET
date: 2018-06-21 09:00:00 +07:00
categories:
- CME
- Publikasi
tags:
- CME
- Pernyataan Bersama
author: siska
layout: post
img: /static/img/landing/kategori.png
---

![CME Publikasi](/static/img/landing/kategori.png){: .img-responsive .center-block }

**Pernyataan Bersama Mengenai Proyek Hibah 0688 DAPET - Eksplorasi Kreatif Tentang Perempuan dan Tubuhnya**

Jakarta. 21 Juni 2018.

**Pembatalan hibah**

Pada tanggal 11 Juni 2018 penerima hibah Kartika Jahja dan Shera Rindra bersama dengan
Dewan Juri membuat pernyataan bersama pembatalan hibah yang berbunyi sebagai berikut:

<center><b>Pernyataan bersama pembatalan hibah untuk Proyek Hibah 0688 DAPET - Eksplorasi Kreatif Tentang Perempuan dan Tubuhnya</b></center>

Pemilik proyek DAPET - Eksplorasi Kreatif Tentang Perempuan dan Tubuhnya, Kartika Jahja dan Shera Rindra,
telah menimbang kembali perencanaan proyek dan membahasnya bersama Dewan Juri Cipta Media Ekspresi.
Setelah meninjau berbagai kemungkinan melangsungkan proyek dengan nilai hibah yang diberikan juri untuk
menjalankan proyek secara bertahap, kesimpulan yang didapat adalah bahwa proyek DAPET harus dijalankan
sekaligus, bukan bertahap agar mendapatkan hasil yang optimal dan tanpa mengubah bentuknya.
Karena itu pemilik proyek memutuskan untuk meminta pembatalan hibah CME dan terus menggali kemungkinan
pendanaan dari sumber-sumber lain.

Proyek ini menerima hibah Rp. 35 juta rupiah dan pembatalannya mengkoreksi nilai hibah total dari
Rp. 3,325,730,000,- (Tiga Milyar Tiga Ratus Dua Puluh Lima Juta Tujuh Ratus Tiga Puluh Ribu Rupiah) menjadi
Rp.3,290,730,000,- (Tiga Milyar Dua Ratus Sembilan Puluh Juta Tujuh Ratus Tiga Puluh Ribu Rupiah).
Sementara penambahan Rp. 35 juta dari proyek ini masuk pada sisa hibah yang sebelumnya ditulis sebagai
Rp.174,270,000 (Seratus Tujuh Puluh Empat Juta Dua Ratus Tujuh Puluh Ribu Rupiah) menjadi
Rp. 209,270,000,- (Dua Ratus Sembilan Juta Dua Ratus Tujuh Puluh Ribu).

<center><b>SELESAI</b></center>
