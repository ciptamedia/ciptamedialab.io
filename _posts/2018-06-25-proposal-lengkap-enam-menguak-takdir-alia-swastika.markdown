---
title: Proposal Lengkap - Enam Menguak Takdir - Alia Swastika
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/enam-menguak-takdir
layout: proposal
author: Alia Swastika
categories:
- laporan
- CME
- Enam Menguak Takdir
---

![0963.jpg](/static/img/hibahcme/0963.jpg){: .img-responsive .center-block }

# Alia Swastika - Enam Menguak Takdir

**Tentang penerima hibah**

Alia Swastika adalah seorang penulis, kurator dan organisator seni. Ia beberapa kali menginisiasi pameran dan kegiatan yang berkaitan dengan politik dan seni, feminisme dan seksualitas, serta budaya kaum muda. Ia bekerja di Ark Galerie (2008- sekarang), selain juga pernah bekerja di Biennale Jogja (2011-2015), serta menjadi bagian dari berbagai organisasi seni. Tulisannya terpublikasi di berbagai media, baik nasional maupun internasional, serta seringkali berkontribusi untuk beberapa penerbitan buku.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Yogyakarta, Jakarta, Bali

**Deskripsi Proyek**

Proyek ini merupakan upaya untuk mendokumentasikan, mendata, dan membaca kembali karya-karya seniman perempuan perupa yang berkarya terutama pada periode 1980an hingga awal 1990an. Presentasi karya akan menjadi buku dengan lisensi terbuka CC-BY sehingga dapat didistribusikan secara luas untuk membangun narasi mengenai seniman-seniman dengan kiprah khusus dalam pemikiran sejarah seni di Indonesia.

- **Tujuan**

    - menuliskan biografi dan pemikiran seniman perempuan yang berkarya pada akhir 1970an sampai awal 1990an.

    - membaca secara cermat dan kritis karya-karya yang dihasilkan pada periode tersebut untuk melihat bagaimana proses kreatif dipengaruhi oleh konteks sosial politik.

    - melihat bagaimana sejarah pergerakan perempuan mempengaruhi karya-karya yang lahir pada masa itu.

- **Sasaran**

  peneliti seni, seniman, akademisi, masyarakat umum

- **Latar Belakang**

  Pada awal 2017, saya mendatangi pameran Gerakan Seni Rupa Baru yang diselenggarakan di Galeri Katamsi ISI Yogyakarta. Sebagai gerakan penting yang menjadi tonggak lahirnya seni rupa kontemporer di Indonesia, hanya ada dua nama seniman perempuan yang tercatat di sana yaitu Siti Adiati dan Nanik Mirna. Selama ini tidak banyak pula tulisan yang secara khusus mengulas peranan kedua seniman tersebut dalam GSRB. GSRB selalu dituliskan dalam perspektif sang kurator maupun anggota seniman laki-laki yang lebih vokal. Karya kedua seniman perempuan ini sendiri menurut saya sangat menonjol. Siti Adiati melontarkan kritik yang sangat tajam berkaitan dengan proses pembangunan yang mengorbankan lingkungan hidup dan kemanusiaan, sementara Nanik Mirna menciptakan karya yang secara kritis mempertanyakan pembungkaman dan hilangnya aktivis-aktivis di masa awal Orde Baru.

  Dari penelusuran terhadap karya-karya mereka, saya melihat adanya peranan penting yang tidak terlihat dari seniman-seniman perempuan, dan bagaimana pemikiran mereka sering dikaburkan oleh peran laki-laki. Her(story) yang memuat perlawanan kaum perempuan banyak hilang dari kisah sejarah, sehingga generasi setelahnya seperti kehilangan narasi dan pengetahuan atas pemikiran tersebut. Situasi yang timpang dalam penulisan sejarah seni dan dalam konteks sosial politik secara lebih umum inilah yang kemudian mendorong saya untuk melaksanakan penelitian ini.

  1. Keterkaitan pada kategori: Riset / Kajian / Kuratorial

     Proyek penelitian tentang seniman perempuan yang merujuk pada satu periode tertentu, dianggap penting untuk melihat sejauh mana konteks sosial politik yang lebih luas memberi pengaruh pada pemikiran dan visi kekaryaan seniman perempuan. Riset ini mencakup biografi para seniman dan pembacaan atas karya-karya yang mereka hasilkan. Analisis terhadap karya-karya mereka akan membantu melihat lebih dalam pengaruh konteks zaman tersebut. Hasil riset akan diterbitkan dalam bentuk buku terbatas dan akan diadakan diskusi di beberapa tempat untuk membawa wacana ini pada publik yang lebih luas.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        - kurangnya catatan yang cukup komprehensif tentang kiprah dan pemikiran seniman  perempuan, terutama karena sejarah seni banyak didominasi oleh seniman laki-laki.

        - Adanya kesenjangan pengetahuan dari seniman senior kepada publik atau pelaku seni sesudahnya, sehingga pada pelaku ini sulit membaca fenomena berdasar sejarah.

  3. Strategi

        - membuat wawancara yang intensif dengan masing-masing narasumber.

        - bertemu dengan para pengamat sebagai narasumber sekunder.

        - membaca arsip dengan cermat dan teliti.

  4. Aktivitas dan keterkaitan pada sasaran

        - Aktivitas penelitian dengan tiga strategi di atas

        - Membaca kembali memori dan arsip narasumber (sasaran)

  5. Latar belakang dan demografi pelaku proyek

     Alia Swastika, Yogyakarta, 37 tahun, lulus S-1 Jurusan Ilmu Komunikasi UGM

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman kuratorial, penelitian dan penulisan serta pengorganisasian seni

  7. Demografi kelompok target

     Usia: 60-70 tahun

     Wilayah: Yogyakarta, Bandung, Jakarta, Bali

     Pendidikan: sebagian besar lulusan sarjana seni

  8. Hasil yang diharapkan & Indikator keberhasilan

  9. Indikator keberhasilan

        1. Terkumpulnya data dan arsip tentang seniman-seniman perempuan yang berkiprah tahun 1980an-1990an.

        2. Terumuskannya pemikiran-pemikiran penting dari tokoh-tokoh tersebut dan bagaimana pemikiran tersebut mempengaruhi sejarah seni rupa di Indonesia.

        3. Terdistribusikannya pengetahuan baru tentang seniman-seniman tersebut kepada generasi yang lebih luas melalui beragam cara presentasi dan penyebaran pengetahuan.

        4. Terjaganya estafet pengetahuan dan pemikiran perempuan antar generasi.

  10. Durasi Waktu Aktivitas dilaksanakan

      8 bulan

  11. Total kebutuhan dana

      Rp.125.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.90.000.000

  13. Sumber dana lainnya

      personal

  14. Kontribusi perorangan

      untuk sekarang belum ada.

  15. Kontribusi kelompok target

      komunitas seni dan masyarakat lebih luas hadir dan menanggapi presentasi mengenai hasil penelitian ini.
