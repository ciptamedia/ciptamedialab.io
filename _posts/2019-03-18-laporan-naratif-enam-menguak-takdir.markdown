---
title: Laporan Naratif - Enam Menguak Takdir - Alia Swastika
date: 2019-03-18
permalink: /ciptamediaekspresi/laporan-naratif/enam-menguak-takdir
layout: proposal
author: Alia Swastika
categories:
- laporan
- Laporan Naratif
- CME
- Enam Menguak Takdir
---

# Laporan Naratif - Enam Menguak Takdir

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Mendapatkan banyak bahan untuk melakukan kajian tentang seniman perempuan Indonesia dalam konteks 1980an, bertemu dengan para seniman secara intensif.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> 1. membuat wawancara yang intensif dengan masing-masing narasumber.
>
> 2. bertemu dengan para pengamat sebagai narasumber sekunder.
>
> 3. membaca arsip dengan cermat dan teliti.

**Ceritakan hasil dari aktivitas tersebut.**

Hampir semua aktivitas terlaksana tetapi karena keterbatasan waktu akhirnya hanya lima seniman yang menjadi subjek penelitian, bukan enam seniman seperti yang dibayangkan sebelumnya.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> 1. kurangnya catatan yang cukup komprehensif tentang kiprah dan pemikiran seniman perempuan, terutama karena sejarah seni banyak didominasi oleh seniman laki-laki.
>
> 2. Adanya kesenjangan pengetahuan dari seniman senior kepada publik atau pelaku seni sesudahnya, sehingga pada pelaku ini sulit membaca fenomena berdasar sejarah.

**Ceritakan hasil dari mengatasi isu tersebut.**

1. Adanya catatan yang cukup memadai tentang pemikiran seniman perempuan

2. terjadinya presentasi awal untuk mendistribusikan hasil pemikiran/penelitian

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Terkumpulnya data dan arsip tentang seniman-seniman perempuan yang berkiprah tahun 1980an-1990an.
>
> 2. Terumuskannya pemikiran-pemikiran penting dari tokoh-tokoh tersebut dan bagaimana pemikiran tersebut mempengaruhi sejarah seni rupa di Indonesia.
>
> 3. Terdistribusikannya pengetahuan baru tentang seniman-seniman tersebut kepada generasi yang lebih luas melalui beragam cara presentasi dan penyebaran pengetahuan.
>
> 4. Terjaganya estafet pengetahuan dan pemikiran perempuan antar generasi.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Secara umum, semua indikator projek tercapai, tetapi karena beberapa hal maka ada perubahan dalam hal pelaksanaan kegiatan sehingga pencetakan dan diskusi lebih lanjut untuk mendistribusikan hasil penelitian dapat dilakukan baru pada Februari 2019. Rencananya akan terus dilanjutkan sepanjang 2019.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Tujuan proyek untuk mendokumentasikan pemikiran perempuan secara luas bisa tercapai dan saya mendapatkan kesempatan bertemu dengan seniman-seniman perempuan pionir serta mempelajari arsip untuk pengkajian lebih jauh. Melalui penelitian ini, sebagai kurator saya dapat menanamkan pada diri saya untuk terus memperkuat perspektif feminis pada penyelenggaraan-penyelenggaraan pameran yang saya kerjakan.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Ada beberapa kendala terutama berkait dengan narasumber yang telah meninggal dunia (Nanik Mirna), karena arsipnya tidak tersimpan dengan baik dan keluarganya tidak mempunyai akses yang baik terhadap karya dan pemikiran narasumber. akhirnya nama ini harus saya hapuskan dan saya kemudian berkonsentrasi pada lima seniman lainnya. Saya tetap ingin melakukan proses penulisan tentang Nanik Mirna, tetapi membutuhkan waktu lebih panjang.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Saya kira proyek ini berharga untuk memperkuat peran dan posisi perempuan dalam sejarah seni, khususnya membicarakan konteks 1980an. sehingga dengan sendirinya memberikan keragaman pemikiran antar generasi.
