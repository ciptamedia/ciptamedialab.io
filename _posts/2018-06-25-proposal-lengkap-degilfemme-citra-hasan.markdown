---
title: Proposal Lengkap - DEGILFEMME - Citra Hasan
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/degilfemme
layout: proposal
author: Citra Hasan
categories:
- laporan
- CME
- DEGILFEMME
---

![1053.jpg](/static/img/hibahcme/1053.jpg){: .img-responsive .center-block }

# Citra Hasan - DEGILFEMME

**Tentang penerima hibah**

Citra Hasan adalah pemimpin proyek SIRKAM. SIRKAM merupakan komunitas dan media berupa zine yang bersifat kolektif dan bergelut di bidang seni dan literasi keperempuanan di wilayah kota Medan dan pulau Sumatra. Sebelumnya, Citra telah bergelut di media zine bernama Degilzine sejak Oktober 2017 yang mengulas tentang skena lokal Medan sebagai content editor sekaligus sekretaris redaksi.

**Kontak**

  - Facebook: [facebook.com/citra.hasannasution](https://www.facebook.com/citra.hasannasution)

**Lokasi**

Medan, Sumatera Utara

**Deskripsi Proyek**

SIRKAM (Sirkulasi Kreasi Perempuan) adalah komunitas dan media independen berbentuk zine di pulau Sumatera yang bersifat kolektif. Komunitas ini bergelut di dunia seni dan literasi perempuan untuk kota Medan dan pulau Sumatra. Kritik dilakukan dari sudut pandang yang berbeda, dan menjadi wadah kreasi khusus bagi para perempuan yang selama ini tidak cukup mendapatkan perhatian dari media arus utama. Dicetak sekali sebulan, dan membuka ruang kreasi dan kelas peningkatan kapasitas lintas bidang secara berkala, termasuk sebagai tempat penitipan anak bagi para perempuan yang ingin fokus berkreasi.

- **Tujuan**

  SIRKAM bukan hanya sebagai media informasi, tetapi juga sebagai wadah ekspresi dan fasilitasi produksi para pegiat seni dan literasi perempuan.

- **Sasaran**

     1. Membentuk media zine sebagai media alternatif bagi kesenian dan literasi keperempuanan.

     2. Memfasilitasi wadah ekspresi dan produksi para pegiat seni dan literasi perempuan.

- **Latar Belakang**

  Berawal dari berdirinya Degilzine (www.degilzine.com) yang bertujuan untuk menjadi media alternatif skena seni dan literasi kota Medan pada 30 Oktober 2017 lalu, animo pembaca dan peminat pun semakin meningkat, terutama pada artikel–artikel yang terkait isu maupun skena perempuan. Seiring dengan proses berjalannya Degilzine, begitu banyak pelaku dan penikmat seni dan literasi perempuan yang karyanya masih minim apresiasi. Karya–karya mereka masih menjadi konsumsi diri sendiri atau orang – orang terdekat. Begitu banyak pelaku seni dan literasi perempuan di kota Medan yang masih belum mendapatkan wadah dan apresiasi seperti yang kaum lelaki dapatkan, baik itu karena ketersediaan wadah, akses, media, bahkan mental rasa minder atau takut tidak diapresiasi. SIRKAM bertujuan untuk mewadahinya. Bukan hanya sebagai media informasi, tetapi juga sebagai wadah ekspresi dan fasilitasi produksi.

  1. Keterkaitan pada kategori: Akses

     Proyek ini cenderung berkategori Akses karena SIRKAM bertujuan sebagai pembuka jalan, sehingga perempuan yang mengalami hambatan dapat berkarya dengan lancar.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Karya–karya para seniman maupun penulis perempuan bukan hanya memiliki wadah yang minim, tetapi juga karya–karya tersebut merepresentasikan isu–isu perempuan yang terjadi di kota Medan maupun pulau Sumatra. Budaya patriarki yang begitu kuat yang disebabkan oleh budaya agama/relijius fundamentalis maupun adat istiadat menjadi isu genting. Berdasarkan data kekerasan pada anak dan perempuan di Sumatera Utara yang dikeluarkan Kementrian Pemberdayaan dan Perlindungan Anak, sampai dengan November 2017, ada 807 kasus dengan jumlah korban sebanyak 906 orang (sumber: Harian Analisa tanggal 19 November 2017). Begitu juga dengan banyaknya kasus penindasan hak perempuan dikarenakan adat seperti hak warisan yang hanya dimiliki anak laki–laki, marga hanya bisa diturunkan oleh anak laki–laki, hak berpendapat di dalam forum tidak diberikan kepada perempuan, bahkan sampai fenomena sinamot/mas kawin perempuan yang absurdnya ditentukan berdasarkan tingkat pendidikan si perempuan tersebut. Kondisi-kondisi mengerikan ini membuat  diperlukan bukan hanya sebagai wadah kreasi belaka tetapi juga sebagai wadah untuk menyuarakan hak–hak asasi perempuan dan sebagai akses edukasi bagi sesama perempuan dan gender lainnya.

  3. Strategi

        - Membangun jaringan para pegiat seni dan literasi perempuan di kota Medan dengan cara pendekatan personal ke individu maupun komunitas–komunitas yang ada.

        - Memetakan potensi kontributor dan narasumber yang mumpuni untuk media zine maupun kelas berbagi dan wadah kreasi terkait isu seni dan literasi di berbagai media, khususnya media sosial seperti Facebook, Instagram, maupun Twitter.

        - Menyediakan fasilitas tempat kreasi sebagai tempat produksi media zine, kelas berbagi, dan wadah diskusi yang menarik dan penuh sumber bermanfaat, seperti perpustakaan dan ruang kelas beserta fasilitas pendukungnya.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Printed Zine

              a. Interview: Pada bagian interview, tim SIRKAM mengulas bukan hanya pada “SIAPA”, tetapi lebih kepada “APA” yang dibahas pada proses wawancara. Pewawancara lebih fokus menggali informasi dan sudut pandang dari narasumbernya.

              b. Jejak Langkah: Koresponden mengulas tentang sejarah seni maupun sastra sesuai dengan tema yang diangkat, baik dari sosoknya, tempat, maupun benda bersejarah yang terkait.

              c. Karikatur: Ilustrator membuat karikatur yang menggugah paradigma sosial untuk meningkatkan awareness terhadap isu–isu tertentu.

              d. Review Cipta: Koresponden menulis tentang ulasan karya para seniman maupun sastrawan perempuan baik yang telah diluncurkan.

              e. Opini “Pedis”: Layaknya kata pedis/pedas, koresponden menuliskan opini terhadap isu – isu tertentu yang dianggap krusial dengan bahasa satir.

              f. Hotspot: Koresponden mengulas tentang tempat–tempat kreatif yang bisa dijadikan sebagai bahan berbagi inspirasi.

              g. Cerpen: Koresponden memuat cerita pendek tentang situasi sosial yang ada di sekitar.

              h. Puisi: Koresponden menulis puisi.

              i.Budaya: Dalam hal ini, koresponden menceritakan tentang hal–hal kebudayaan di sekitar yang telah hilang maupun masih ada.

        2. Kelas Berbagi: Kelas ini merupakan wadah bagi para pelaku seni dan sastra perempuan untuk saling berbagi ilmu dan inspirasi. Setiap kegiatan, SIRKAM mengundang pakar seni dan sastra di bidangnya untuk melatih dan membagi ilmu serta pengalamannya kepada para peserta. Kemudian, para peserta membuat Rencana Tindak Lanjut untuk diimplementasikan.

        3. Work Space: Wadah ini merupakan implementasi Rencana Tindak Lanjut yang disusun bersama–sama di Kelas Berbagi. Para peserta difasilitasi dan didampingi dalam berkreasi. Namun, bukan berarti peserta akan “diatur”, tetapi lebih cenderung didampingi jika peserta mengalami kendala–kendala selama proses kreasi.

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah perempuan berumur 30-an dari kelas menengah. Anggota lainnya adalah perempuan kelas menengah yang merupakan pegiat seni dan literasi.

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman mengerjakan media zine sebagai content editor dan sekretaris redaksi selama 6 bulan (sejak Oktober 2017).

  7. Demografi kelompok target

     Pegiat maupun penikmat seni, literasi, dan keperempuanan, mahasiswa/i, siswa/i sekolah, dan akademisi wilayah kota Medan dan pulau Sumatra, berusia di atas 15 tahun.

  8. Hasil yang diharapkan & Indikator keberhasilan

     SIRKAM diharapkan mampu memproduksi media zine terkait seni dan literasi perempuan secara berkala dan membuka ruang diskusi, tempat berbagi, dan wadah produksi bagi para perempuan untuk berkarya, khususnya perempuan di kota Medan.

  9. Indikator keberhasilan

        1. Penerbitan zine sekali sebulan.

        2. Ruang diskusi dan kelas berbagi ilmu dua kali sebulan.

        3. Wadah kreasi setiap hari (kapan saja tergantung seniman dan penulis).

  10. Durasi Waktu Aktivitas dilaksanakan

      9 bulan

  11. Total kebutuhan dana

      Rp.92.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.92.000.000

  13. Sumber dana lainnya

      Tidak Ada

  14. Kontribusi perorangan

      Sebagai kontributor ide, tulisan dan ilustrasi

  15. Kontribusi kelompok target

         1. Sirkam zine dibaca oleh 100 pembaca.

         2. Dukungan dari pegiat seni dan literasi perempuan dan laki–laki berupa keterlibatan dalam kegiatan–kegiatan SIRKAM dan media SIRKAM zine.

         3. Dukungan dari akademisi atau peneliti yang selama ini memberikan perhatian terhadap seni dan literasi.

         4. Dukungan dari komunitas seni dan literasi.

         5. Dukungan dari media arus utama.
