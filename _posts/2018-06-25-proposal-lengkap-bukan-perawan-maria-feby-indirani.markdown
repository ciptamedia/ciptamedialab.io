---
title: Proposal Lengkap - Bukan Perawan MAria - Wenda Maria Imakulata Tokomonowir
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/bukan-perawan-maria
layout: proposal
author: Feby Indirani
categories:
- laporan
- CME
- Bukan Perawan Maria
---

![0875.jpg](/static/img/hibahcme/0875.jpg){: .img-responsive .center-block }

# Feby Indirani - Bukan Perawan Maria

**Tentang penerima hibah**

Feby Indirani adalah pengarang dan jurnalis, buku fiksi terbarunya “Bukan Perawan Maria” (Pabrikultur 2017) akan diterbitkan dalam bahasa Inggris dan Italia, dan telah mendapatkan liputan media internasional seperti The Australian, BBC, Deutsche Welle dsb, Novelnya “Clara’s Medal” (Noura Publisher, 2011) menjadi bacaan wajib dalam kelas Bahasa Indonesia di salah satu sekolah internasional di Jakarta. Buku nonfiksinya “I can (not) Hear” meraih Anugerah Pembaca Indonesia 2010. Ia menginisiasi gerakan Relaksasi Beragama (Relax, It’s Just Religion) yang mendapat dukungan Menteri Agama RI, Lukman Hakim Saifuddin.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Bandung, Jawa Barat dan Mataram, NTB

**Deskripsi Proyek**

Pameran Tafsir Rupa dan Gerak "Bukan Perawan Maria" adalah upaya untuk menawarkan sudut pandang kritis dan segar atas pengalaman perempuan dalam beragama. Pameran yang direncanakan terjadi di Bandung dan Mataram diharapkan dapat memancing terbukanya ruang pemaknaan yang rileks dan pengalaman beragama yang beragam dengan keseharian beragama yang hangat, tepa selira, dan harmonis. Di tengah situasi yang tegang, seni diharapkan menjadi penengah yang dapat menyejukkan hati yang panas dan melembutkan perbedaan yang tajam.

- **Tujuan**

    - Mengubah energi keberagamaan dalam masyarakat dari penuh ketegangan dan konflik menjadi sebuah energi positif dan merekatkan masyarakat dalam kehidupan bersama yang menggembirakan melalui seni dan literasi.

    - Memberikan perempuan seniman/pekerja kreatif di tingkat lokal ruang yang lebih leluasa, untuk merespon persoalan sosial kemasyarakatan yang antara lain ditimbulkan interpretasi ajaran agama dan budaya yang patriarkis.

- **Sasaran**

  Bandung dan Mataram adalah dua kota yang pernah masuk ke dalam daftar kota intoleran di Indonesia berdasarkan penelitian dari Setara Institute. Kegiatan ini menyasar terutama anak muda di kedua kota ini yang diharapkan dapat lebih melek terhadap isu-isu toleransi dan keberagamaan melalui wahana seni dan kreativitas.

- **Latar Belakang**

  Antara 2015-2016, terjadi konflik SARA di Indonesia sebanyak 1.568 kejadian (Kompas, 16 Maret 2017). Dalam situasi seperti ini, sastra dan seni sebetulnya dapat berperan untuk menjadi penengah, menyejukkan hati yang panas, dan melembutkan perbedaan yang tajam. Sastra dan seni berpeluang mengajak semua orang untuk melihat masalah dari sudut pandang berbeda, berempati kepada pihak lain, dan menertawakan diri sendiri.

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Cerita-cerita dalam Bukan Perawan Maria menjadi pemantik gagasan bagi para perempuan seniman untuk menafsir dan merespon isu-isu yang terkait dengan kehidupan keberagamaan dalam berbagai medium seni yang mereka tekuni. Di dalam proses ini semua pihak berada pada posisi setara dan saling memfasilitasi antara inisiator/penulis cerita), seniman, kurator dan pekerja kreatif.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Intoleransi, minim empati dan ketegangan dalam kehidupan beragama.

  3. Strategi

     Proses pembuatan karya dan pameran bersifat organik, partisipatif/terlibat, tidak satu arah. Dalam proses seni terlibat ini, baik penulis sebagai pelempar gagasan, para seniman, kurator, tim produksi, dan khalayak saling menfasilitasi.

  4. Aktivitas dan keterkaitan pada sasaran

     Tahap pertama : Para perempuan seniman menafsir/merespon teks cerita dalam berbagai bentuk karya seni seperti grafis, instalasi, pantomim, tari sambil memasukkan pandangan, pemikiran dan pengalaman personal mereka terkait isu keberagamaan. Dalam proses ini telah terjadi pertukaran gagasan dan proses penciptaan di kalangan perempuan seniman yang diharapkan dapat menjadi bekal pula bagi perjalanan berkarya mereka selanjutnya.

     Tahap kedua : Karya ini ditampilkan di publik dan mengajak agar publik terlibat untuk dapat terpapar dengan gagasan relaksasi beragama sehingga bisa lebih peduli kepada isu terkait. Publik juga diajak untuk bisa untuk memiliki pengalaman yang berbeda melalui karya seni yang diharapkan dapat memantik diskusi lebih jauh terhadap topik terkait.

  6. Demografi kelompok target

     Usia 16-40 tahun, pelajar, mahasiswa dan masyarakat umum di usia produktif di Bandung dan Mataram SES ABC.

  7. Hasil yang diharapkan

     - Perempuan seniman di dua kota intoleran mendapatkan ruang untuk mengekspresikan pikiran, pengalaman dan perasaan terkait isu keberagaman dan keberagamaan.

     - Partisipasi publik di dua kota untuk hadir dan merespon acara ini sebagai pemantik untuk diskusi-diskusi terkait tema terkait.

     - Liputan media untuk menyebarluaskan gagasan.

     - Bertambahnya seniman dan pekerja kreatif yang peduli kepada isu keberagaman dan keberagamaan.

  8. Indikator keberhasilan

     -  Jumlah perempuan seniman dan pekerja kreatif yang terlibat

     - Jumlah hadirin.

     - Partisipasi warga setempat.

     - Liputan media massa.


  9. Durasi Waktu Aktivitas dilaksanakan

     - Mataram, Lombok 26 Agustus 2018 (melukis mural bersama warga Kampung Wisata Krisant mulai 23 Agustus)

     - Bandung, acara pembukaan/pertunjukan 8-9 September 2018 (Pameran rupa hingga 16 September 2018)

  10. Total kebutuhan dana

      Rp.200.000.000,- ++

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.200.000.000,-

  12. Sumber dana lainnya

      Sedang mencari pendanaan lain

  13. Kontribusi perorangan

      -

  14. Kontribusi kelompok target

      -
