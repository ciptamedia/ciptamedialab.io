---
title: Laporan Naratif - Bukan Perawan Maria - Feby Indirani
date: 2019-06-11
permalink: /ciptamediaekspresi/laporan-naratif/bukan-perawan-maria
layout: proposal
author: Feby Indirani
categories:
- laporan
- Laporan Naratif
- CME
- Bukan Perawan Maria
---

# Laporan Naratif - Bukan Perawan Maria

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Pertama, saya belajar untuk lebih 'membumi 'sebagai -sebut saja-- aktivis, karena banyak isu yang saya kira sudah sangat akrab ternyata masih jauh dari percakapan teman-teman di luar Jawa. Ada jurang yang cukup jauh antara teman-teman di Jawa dan luar Jawa, dan itu tidak hanya mengenai wacana, tapi juga cara pikir dan cara kerja sehari-hari. Saya merasa mesti lebih banyak membangun jejaring dengan komunitas lokal karena jika bicara tentang Indonesia, perubahan nyata juga harus terjadi di luar Jawa, yang nyatanya jarang saya temui dan dekati langsung karena perkara pendanaan dan akses. Kedua, saya belajar (dan masih belajar tentang seni mengelola keterlibatan dalam projek kolaborasi ini. Sejak awal saya ingin lebih memosisikan diri sebagai fasilitator, karena tidak ingin proyek-proyek ini menjadi Jakarta sentris. Namun, di sisi lain kemudian saya menyesal tidak maksimal turun tangan dalam menggarap seputar peliputan media massa. Bagaimanapun terlepas dari peliputan media massa yang saya rasakan kurang maksimal, saya puas melihat dinamika yang terjadi pada tim dan pertumbuhan para seniman, manager proyek dan pekerja kreatif yang terlibat, dan itu antara lain terjadi karena saya berusaha menahan diri untuk tidak hanya menggunakan 'cara saya'.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Tahap pertama : Para perempuan seniman menafsir/merespon teks cerita dalam berbagai bentuk karya seni seperti grafis, instalasi, pantomim, tari sambil memasukkan pandangan, pemikiran dan pengalaman personal mereka terkait isu keberagamaan. Dalam proses ini telah terjadi pertukaran gagasan dan proses penciptaan di kalangan perempuan seniman yang diharapkan dapat menjadi bekal pula bagi perjalanan berkarya mereka selanjutnya.
>
> Tahap kedua : Karya ini ditampilkan di publik dan mengajak agar publik terlibat untuk dapat terpapar dengan gagasan relaksasi beragama sehingga bisa lebih peduli kepada isu terkait. Publik juga diajak untuk bisa untuk memiliki pengalaman yang berbeda melalui karya seni yang diharapkan dapat memantik diskusi lebih jauh terhadap topik terkait.

**Ceritakan hasil dari aktivitas tersebut.**

Dari hasil karya, hasil para perempuan seniman ini sangat menggembirakan dan beragam. Dua manager projek di Bandung dan Lombok juga perempuan dan banyak perempuan yang terlibat di tim kreatif. Proses kerja sangat berarti karena banyak terjadi diskusi yang reflektif seputar isu kepercayaan dan keberagamaan. Para seniman dan pekerja kreatif yang terlibat banyak yang mengungkapkan bagaimana proses mereka mencerap cerita-cerita dari buku Bukan Perawan Maria, dan bagaimana mereka menghubungkan itu dengan pengalaman mereka, lalu menuangkan ke dalam karya/pertunjukan baik yang terencana maupun spontan. Menariknya adalah seniman-seniman yang terlibat dalam proyek di Lombok hampir seluruhnya berhijab dan cenderung konservatif. itu terlihat dari narasi-narasi yang mereka lahirkan saat menjelaskan karya mereka. Salah satu contoh menarik adalah narasi untuk Tari Perempuan yang Kehilangan Wajahnya menyebut tentang 'Perempuan modern urban yang arogan.... yang akhirnya harus berserah pada kekuasaan Tuhan', suatu interpretasi yang boleh dibilang cukup jauh dari semangat buku Bukan Perawan Maria. Namun sebagai proses kolaborasi karya, bagaimana pun telah terjadi dialog dan pertukaran gagasan. Ada pemantik pikiran dan sudut pandang berbeda yang telah diselundupkan ke dalam pandangan keseharian teman-teman seniman di Lombok. Dan saya percaya ini adalah langkah awal suatu proses yang terus berjalan. Setelah proyek berakhir, beberapa dari seniman ini sudah menampilkan karya yang sama di kesempatan lain atau melanjutkan topik yang sama untuk karya mereka berikutnya (misalnya tentang hijab.)

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Intoleransi, minim empati dan ketegangan dalam kehidupan beragama.

**Ceritakan hasil dari mengatasi isu tersebut.**

Proyek ini membuat saya makin optimistis bahwa sastra dan seni dapat menjadi jembatan untuk pihak-pihak yang berbeda paham dan pendapat. Sejauh pengamatan saya dalam proses di kedua kota, telah tercipta ruang-ruang berekspresi, berdiskusi, berpendapat yang membuat orang-orang yang berbeda paham dan pendapat bisa saling mendengarkan satu sama lain karena sedang mengalami proses bersama dalam menikmati karya seni ataupun berdiskusi.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Perempuan seniman di dua kota intoleran mendapatkan ruang untuk mengekspresikan pikiran, pengalaman dan perasaan terkait isu keberagaman dan keberagamaan.
>
> 2. Partisipasi publik di dua kota untuk hadir dan merespon acara ini sebagai pemantik untuk diskusi-diskusi terkait tema terkait.
>
> 3. Liputan media untuk menyebarluaskan gagasan.
>
> 4. Bertambahnya seniman dan pekerja kreatif yang peduli kepada isu keberagaman dan keberagamaan.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Menurut saya, kami telah berhasil mencapai hampir semua indikator sukses tersebut. Patut dicatat bahwa lebih dari separuh seniman yang terlibat di Bandung juga mengirimkan proposal ke CME dan kebetulan belum diterima, jadi saya sangat senang mereka bisa terlibat di dalam projek Bukan Perawan Maria dan mendapat kesempatan berkarya. Dari cerita mereka saat diskusi, mereka juga secara intens melakukan proses refleksi mendalam untuk menghasilkan karya-karya ini. Saya percaya pengalaman di dalam proyek ini akan tinggal tetap di dalam diri para seniman dan pekerja kreatif di Bandung dan Lombok, dan menjadi 'bekal' juga untuk karya-karya mereka berikutnya. Kemudian dilihat dari jumlah audiens yang hadir cukup menggembirakan, setidaknya lebih dari 800 orang untuk dua kota. Secara kualitatif di dua kota ini banyak yang datang dan ingin berdiskusi dengan kami, merasa terstimulasi untuk berdiskusi lebih jauh untuk tema-tema dan isu dari buku Bukan Perawan Maria. Seorang mahasiswi di Bandung datang dan bercerita di publik bahwa ketika ia melihat publikasi acara ini, ia berpikir acara ini seolah dibuat khusus untuknya. Dia sedang menimbang untuk membuka hijab dan dia takut dikucilkan oleh keluarga dan teman-temannya. Hari dimulainya acara Bukan Perawan Maria di Bandung adalah hari pertama ia membuka jilbabnya, dan dia dengan berani berbagi cerita itu pada saat diskusi. Yang menurut saya kurang terpenuhi pada indikator sukses ini adalah liputan media baik lokal maupun nasional yang tidak begitu maksimal. Dalam hal ini saya merasa seharusnya bisa lebih sigap menggarap peliputan media ini. Saya terlalu melepas semua proses pada tim lokal di dua kota dan lengah bahwa menggarap liputan di media butuh keahlian tersendiri yang belum dimiliki teman-teman tim Bandung dan Lombok.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Tujuan proyek yang berhasil dicapai adalah lebih banyak perempuan seniman dan perempuan pekerja kreatif yang dapat mengekspresikan pikiran, perasaan dan pengalaman mereka seputar isu-isu kepercayaan dan keberagamaan, terciptanya ruang-ruang dialog yang terbuka untuk membicarakan isu-isu yang dianggap tabu dan sensitif, lebih banyak orang yang peduli dan ikut memikirkan perihal ketegangan beragama di Indonesia dan bagaimana memulihkannya, serta bagaimana menumbuhkan empati melalui sastra dan seni. Saya merasa proyek ini juga membawa perubahan pada cara berpikir saya. Awalnya meskipun memosisikan diri sebagai fasilitator, namun penting bagi saya untuk menjaga spirit dari Bukan Perawan Maria seperti feminisme, HAM terkait kaum minoritas untuk muncul pada karya teman-teman seniman. Namun saya mesti berhadapan dengan kenyataan bahwa perempuan seniman di Lombok misalnya, punya seperangkat nilai dan gagasan yang berbeda, dan akan menjadi lompatan yang terlalu jauh bagi mereka jika saya 'memaksakan' nilai-nilai dan gagasan dari buku saya sekaligus. Saya belajar 'membumi' bahwa setiap komunitas punya proses mereka sendiri dan sebaiknya juga berkembang secara organik. Saya juga belajar konsisten dengan semangat dari Bukan Perawan Maria untuk menghargai interpretasi yang berbeda dari pihak lain. Sejumlah karya saya diinterpretasi dengan narasi yang tidak pernah saya bayangkan, yang membuat saya awalnya kaget, tapi kemudian saya sadar bahwa di situlah proses interpretasi dan kolaborasi mesti dihargai. Yang penting sudah terjadi dialog dan bagaimanapun 'penyelundupan' gagasan-gagasan yang mungkin masih relatif baru dan asing untuk teman-teman (terutama di Lombok), dan diharapkan dapat memantik diskusi dan proses berkarya selanjutnya.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

1. Sulit menemukan perempuan kurator, terutama di Lombok. Di Bandung juga tidak mudah, namun masih relatif lebih mudah ketimbang Lombok. Kurator di Bandung baru pertama kali melakukan kuratorial dan beragama Kristen, dan hal ini membuat ia dilarang keluarganya untuk disebut sebagai kurator tunggal atau utama. Persoalan kecemasan pada kelompok hardliners adalah sesuatu yang nyata dan menghantui. Solusinya adalah memasang nama saya sebagai penanggung jawab acara dan menulis nama saya juga sebagai posisi kurator. Solusi di Lombok, fungsi kurator akhirnya dirangkap oleh manager proyek, karena setelah dicari sekian lama akhirnya tetap tidak berhasil ditemukan. Hal yang sama terjadi pada perempuan seniman di Lombok, banyak sekali perempuan seniman yang berhenti berkarya setelah menikah dan punya anak sehingga sangat sulit menemukan perempuan seniman.

2. Dari segi jarak, Lombok cukup jauh dari saya sehingga saya tidak bisa sering datang untuk berinteraksi langsung dengan teman-teman di sana. Lebih dari itu ada gap wacana yang cukup jauh antara teman-teman Lombok dan teman-teman di Bandung, karena ternyata menurut aktivis perempuan di Lombok masih sangat jarang ada diskusi-diskusi kritis di Lombok. Saya misalnya memancing diskusi baik secara langsung maupun di grup Whatsapp mengajak para seniman untuk memperhatikan isu minoritas Ahmadiyah di sana, namun tidak ada respon sama sekali. Akhirnya saya merasa mesti berkompromi dan menyerahkan lebih banyak pada proses dan mekanisme tim lokal, karena bagaimanapun mereka pula yang akan menanggung risiko di sana jika terjadi sesuatu.

3. Ketika di Lombok terjadi gempa yang dashyat, mau tidak mau acara mesti ditunda. Ketika acara di Bandung, kami berusaha untuk menggalang dana kecil-kecilan juga untuk disumbangkan kepada seniman-seniman di Lombok yang terdampak langsung bencana tersebut. 

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Bukan Perawan Maria memperkaya keragaman ekspresi perempuan di area kepercayaan dan agama, yang di dalamnya banyak menyentuh hal-hal yang tabu dan sensitif untuk dibicarakan, seperti tubuh perempuan, poligami, peran perempuan sebagai ibu dan istri, dan isu-isu kepercayaan dan agama lainnya. Bidang kepercayaan dan agama secara umum masih sangat didominasi oleh laki-laki dan suara perempuan cenderung dianggap kurang otoritatif, terlebih perempuan yang tidak punya latar belakang keagamaan secara tradisional. Melalui proyek ini, perempuan bisa mengekspresikan pikiran, perasaan dan pengalamannya dalam menghidupi kepercayaan dan agamanya dengan caranya masing-masing.
