---
title: Laporan Naratif - Seragam Merah Jambu - Sanchia Hamidjaja
date: 2019-05-06
permalink: /ciptamediaekspresi/laporan-naratif/seragam-merah-jambu
layout: proposal
author: Sanchia Hamidjaja
categories:
- laporan
- Laporan Naratif
- CME
- Seragam Merah Jambu
---

# Laporan Naratif - Seragam Merah Jambu

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Pembelajaran yang saya dapat dalam proses pengerjaan proyek ini adalah diperlukannya fokus yang lebih dalam dan disiplin waktu yang harus lebih di perketat, karena selama tenggat waktu proyek ini sering terhambat dengan pekerjaan-pekerjaan komersil lain untuk mencari nafkah dan kepentingan-kepentingan domestik yang tidak mungkin di tunda seperti urusan sekolahan anak, dsb.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Aktivitas untuk sasaran A:
>
> Membuka kegiatan internship / magang bagi mahasiswa ilustrasi / seni rupa, untuk membantu mempercepat proses yang pada umumnya akan memakan waktu jauh lebih lama. Yang sekaligus membagi pengalaman proses berkarya. Juga kerjasama dengan penterjemah dan penyunting naskah komik.
>
> Aktivitas untuk sasaran B:
>
> Menjalin network di Aceh untuk dapat melakukan perjalanan kesana, untuk tahap riset visual yang sangat dibutuhkan sebagai referensi gambar, jilid ke 2 pada buku ini. Supaya terlihat betul perbedaan ilustrasi suasana jilid 1 yang berlatar belakang di jakarta dan jilid 2 yang bertempatkan di Aceh. Bertemu narasumber untuk membantu mendalami budaya dan keadaan pasca-konflik disana, sesuai dengan latar belakang cerita di balik grafis novel saya.
>
> Aktivitas untuk sasaran C:
>
> Diharapkan dengan menyelesaikan Jilid 1, hasilnya dapat dijadikan proposal untuk di presentasikan kepada calon penerbit.
>
> Aktivitas untuk sasaran D:
>
> Buku novel grafis dapat di distribusikan kepada berbagai kalangan komunitas komik maupun non-komik.

**Ceritakan hasil dari aktivitas tersebut.**

1. Saya mendapatkan pembelajaran yang penting dalam mendidik dan memberi peluang anak magang dalam praktek kerja yang baru, dan saling berbagi ide dan metode proses yang beragam.

2. Pentingnya pengalaman, perencanaan dan networking yang tepat sebelum riset itu di jalankan. Karena ini adalah proyek pertama saya yang perlu melakukan riset perjalanan dan bertemu sumber-sumber baru di Aceh lokasi yang belum pernah saya kunjungi. Untuk riset awal menurut saya sudah cukup puas mendapatkan banyak bahan visual namun, setelah disadari diperlukan pendekatan-pendekatan personal dengan para narasumber, yang idealnya memakan waktu yang lebih lama. Dalam hal ini untungnya dapat diatasi melalui hasil riset sekunder, yakni dari buku-buku. Untuk ke depannya sepertinya kurang maksimal kalau hanya mengedepankan visual saja. Semoga di lain waktu ada kesempatan berkunjung lagi, khususnya lokasi-lokasi pedalaman yang lebih sulit di jangkau dimana konflik-konflik itu terjadi.

3. Jilid 1 sudah mencapai 80 halaman, 30 halaman sudah selesai di bubuhi teks, tinta dan warna, 50 halaman sedang di tinta, kurang sekitar 20-25 halaman yang masih dalam bentuk sketsa. Disini saya belajar proses ketika teks dan gambar di satukan, kadang berhasil kadang tidak, hal seperti ini membutuhkan percobaan-percobaan yang makan waktu lama.

4. Yang tidak di sangka, dalam mendokumentasikan proses pengerjaan jilid 1 yang kemudian di unggah di media sosial, banyak menarik perhatian komunitas komik internasional, dengan tidak di sengaja terbentuk network sendiri. Semoga kedepannya menarik perhatian penerbit.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> 1. Novel grafis ini mengangkat berbagai masalah peranan seorang ibu dan istri, situasi politik Indonesia, berbagai ripple efek korupsi dalam kemanusiaan. Membuka sisi lain dari kultur kepolisian, kesehatan jiwa, Aceh pasca konflik dan tsunami. Meneliti peranan perempuan dalam kondisi rumah tangga dengan kelas dan subkultur yang berbeda beda. Juga mengangkat kontras budaya Jakarta dan Aceh beserta dampak-dampak konflik didalamnya.
>
> 2. Kurangnya karya novel grafis indonesia yang merepresentasikan cerita-cerita perempuan Indonesia.
>
> 3. Kesulitan meluangkan waktu untuk mengerjakan proyek ini secara intesif, dikarenakan kewajiban dalam rumah tangga serta pekerjaan sebagai ilustrator lepas, diharapkan hibah dana dapat memberi dukungan berupa fasilitas yang dibutuhkan demi mewujudkan proyek ini.

**Ceritakan hasil dari mengatasi isu tersebut.**

1. Dalam hal konten, belum bisa dinyatakan sukses atau tidak karena sampai hari ini belum dapat terselesaikan dengan tenggat waktu yang di tentukan.

2. Dalam hal biaya dapat dinyatakan efisien, karena hingga sekarang masih ada sisa. Keputusan mempekerjakan anak magang sangat berhasil dengan baik. Dan membuka peluang dan semangat mahasiswa untuk memilih komik menjadi media berkarya.

3. Dengan bantuan hibah dana sangat membantu melajukan/mempercepat, dalam waktu 6 bulan dapat menyelesaikan 70 sekian halaman tanpa gangguan apapun.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> Keberhasilan proyek ini tercapai ketika terselesaikan dalam waktu yang di rencanakan, dalam biaya yang efisien, konsistensi kualitas gambar, dan story telling yang baik, jelas dalam mengungkapkan pesan-pesan, atau isu-isu yang membuka sudut pandang para pembaca, baik di Indonesia maupun secara global.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Belum dapat dinyatakan sukses, karena belum selesai. namun secara artistik sudah baik, tinggal di kerjakan saja.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Proyek ini sangat merubah pemikiran saya, dimana saya menempatkan diri saya sebagai perempuan serta pilihan-pilihan politik saya. Ini sangat berpengaruh dalam ranah kesenian saya, setelah mengerjakan proyek ini saya merasa lebih memiliki tujuan sosial dalam berkarya.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

6 bulan pertama proyek berjalan dengan baik sesuai jadwal, 3 bulan terakhir terhambat dengan kepentingan-kepentingan domestik yang tidak dapat di tunda, kebetulan kontrak rumah habis, waktunya pindah, dan putri kami sudah harus daftar SD, dsb. Terpaksa harus menerima pekerjaan-pekerjaan komersil, karena kebutuhan menjadi lebih banyak. Disini saya mengalami kesulitan untuk kembali fokus ke proyek saya lagi.

Selain itu dalam segi konten, setelah melalui riset, tentunya banyak perkembangan-perkembangan yang banyak merubah jalur cerita yang kemudian dibutuhkan untuk lebih lagi waktu untuk bereksperimen visual dan penulisan. Ini proses yang sangat menyenangkan bagi pengembangan media komik saya.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Media komik perlu menjadi bagian dari keragaman ekspresi, karena mudah di cerna dan di distribusikan, melalui media sosial, maupun cetak. Semoga dengan melihat proses pengerjaan proyek saya akan semakin banyak yang memilih komik untuk dijadikan media berekspresi.
