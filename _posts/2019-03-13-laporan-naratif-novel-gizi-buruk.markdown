---
title: Laporan Naratif - Novel Wabah Campak dan Gizi Buruk di Asmat - Intan Andaru
date: 2019-03-13
permalink: /ciptamediaekspresi/laporan-naratif/novel-gizi-buruk
layout: proposal
author: Intan Andaru
categories:
- laporan
- Laporan Naratif
- CME
- Novel Wabah Campak dan Gizi Buruk di Asmat
---

# Laporan Naratif - Novel Wabah Campak dan Gizi Buruk di Asmat

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Saya banyak menemukan hal-hal baru selama riset menulis. Tidak hanya belajar tentang konsep sehat-sakit menurut masyarakat Asmat, tapi juga belajar mengaitkan faktor-faktor lain yang mempengaruhi peristiwa (wabah campak dan gizi buruk) seperti: faktor kepercayaan, kebudayaan, ekonomi, politik, dsb.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Melakukan interaksi, mengamati, dan wawancara pada pihak-pihak terkait serta studi literatur.

**Ceritakan hasil dari aktivitas tersebut.**

Riset langsung dengan berinteraksi dan wawancara sangat membantu saya dalam mengumpulkan data karena tidak banyak informasi tertulis dalam studi literatur. Misal: analisis mengenai hal-hal yang mempengaruhi peristiwa tersebut, pemikiran mereka tentang konsep sehat-sakit, dan kebijakan-kebijakan pemerintah dalam menyikapi masalah (yang menurut saya justru menambah masalah baru). Hal seperti itu saya dapatkan hanya dalam riset langsung.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Diharapkan Indonesia memiliki karya sastra yang mampu merepresentasikan kondisi kesehatan dan sosiokultural Indonesia Timur, khususnya tragedi kemanusiaan di Asmat Papua sehingga dapat mengenal lebih jauh akan kondisi negeri sendiri.

**Ceritakan hasil dari mengatasi isu tersebut.**

Saat ini, sedang dilakukan proses penulisan novel, maka hasil belum dapat dievaluasi.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Terbitnya novel bertema gizi buruk dan wabah campak serta kebudayaan Asmat tahun (2019-2020)
>
> 2. Terdistribusikan novel (dalam bentuk cetak) sebanyak 1000 eksemplar (satu tahun atau dua kali pelaporan royalti) atau 200 unduhan (bila disediakan dalam media online)
>
> 3. Publikasi sepuluh ulasan/resensi di media cetak/online

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Indikator tercapai bila novel terbit tahun 2019-2020, maka saat ini belum dapat dievaluasi, dikarenakan sedang proses dilakukan.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Proyek ini mengubah sudut pandang saya mengenai masyarakat Asmat yang banyak diceritakan di media. Mereka menggiring opini bahwa masyarakat Asmat begitu terbelakang, sulit "diatur",  kaku, membahayakan, dan sebagainya. Kenyataannya, tidak seperti itu. Masyarakat Asmat, bagi saya, seperti gelas kosong yang dapat menerima apa saja, sayangnya pendatang dan pemerintah lebih banyak mengisinya dengan air keruh.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Masalah: Perbedaan bahasa.

Solusi: Melakukan pendekatan pada masyarakat ditemani oleh penerjemah dan belajar istilah2 sesuai bahasa masyarakat.

Masalah: Masyarakat tertutup dan malu untuk membicarakan hal-hal tertentu pada orang Asing.

Solusi: Mendekati masyarakat dg melakukan pelayanan sbg dokter dan membeli karya mereka (ukiran/anyaman)

Masalah: Kurangnya kebiasaan menulis, tradisi diturunkan dari mulut ke mulut, sehingga sering terjadi kerancuan ketika wawancara. Banyak dari mereka yang tidak mengerti tradisi mereka sendiri.

Solusi: Mengumpulkan tokoh-tokoh kampung untuk menggali cerita yang lebih terpercaya dan mencocokkan dengan studi literatur.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Proyek ini dapat memperkaya keragaman ekspresi dari perempuan karena para perempuan pekerja seni dapat berkarya dengan mengangkat tema sesuai keinginan mereka tanpa dibatasi. Proyek ini juga menambah keberagaman karena diikuti oleh para pekerja seni dari berbagai bidang.

---

## Presentasi CME-Fest

**Anda dapat mengunduh presentasi Intan Andaru dalam acara CME-Fest melalui tautan di bawah ini**

<a href="/uploads/laporan-naratif/intan-andaru.pdf" class="btn main-btn">Presentasi</a>
