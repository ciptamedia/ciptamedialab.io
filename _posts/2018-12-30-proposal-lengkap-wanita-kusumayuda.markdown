---
title: Proposal Lengkap - Wanita Kusumayuda - Sri Harti
date: 2018-12-30
permalink: /ciptamediaekspresi/proposal-lengkap/wanita-kusumayuda
layout: proposal
author: Sri Harti
categories:
- laporan
- CME
- Wanita Kusumayuda
---

![0523.jpg](/static/img/hibahcme/0523.jpg){: .img-responsive .center-block }

# Sri Harti - Wanita Kusumayuda

**Tentang penerima hibah**

Sri Harti, seorang dalang wayang kulit kelahiran Sukoharjo 38 tahun yang lalu. Di dunia Pedalangan lebih dikenal dengan nama Kenik Asmorowati. Gelar magister seni diperolehnya di ISI Surakarta tahun 2007. Menjadi pengajar di Prodi Pedalangan, Fakultas Seni Pertunjukan,ISI Surakarta sejak tahun 2014.  Sejak tahun 2013 sampai sekarang aktif di kegiatan Safari Ramadhan Padhasuka ( Paguyuban Dalang Surakarta) yang beranggotakan dalang-dalang kondang dan senior di Solo Raya. Akhir tahun 2010 membentuk Komunitas Karawitan Putri Rara Asmoro yang beranggotakan mahasiswa atau alumni mahasiswi ISI Surakarta Fakultas Seni Pertunjukan dari berbagai prodi, ada dalang, pengrawit, sindhen, teater dan penari.  Memberikan ruang dan waktu kepada komunitas ini untuk berlatih bersama terkhusus untuk mengiringi pertunjukan wayang kulit. Kenik mendorong perempuan untuk maju dalam berkarya dan menunjukkan pada dunia bahwa perempuan juga tidak kalah hebat dengan pria. Bersama komunitas Rara Asmoro Kenik menawarkan sebuah pagelaran Wayang kulit dengan dalang perempuan, pendukung sajian perempuan, dan mengangkat tokoh perempuan.  Diharapkan dengan bentuk pertunjukan ini  masyarakat akan lebih terbuka untuk memahami kesetaraan gender.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Solo, Jawa Tengah

**Deskripsi Proyek**

Wayang adalah wewayangane ngaurip artinya cermin dari kehidupan.  Gambaran baik-buruk, benar-salah atau beraneka ragam perilaku dan peristiwa dalam kehidupan akan menjadi hal yang menarik yang dapat direpresentasikan dalam pertunjukan wayang.  Wayang yang mampu bertahan hidup sampai sekarang, sedikit banyak mempunyai andil dalam pembentukan pribadi masyarakat khususnya jawa.  Dengan melihat wayang orang akan mendapat pemahaman bagaimana hidup seharusnya dan bagaimana hidup itu sesungguhnya.
            
Pertunjukan wayang pada dasarnya adalah pertunjukan lakon. Ketika melihat pertunjukan wayang sering muncul pertanyaan, Lakone apa, lakone sapa dan lakone piye (judulnya apa, tokoh utamanya siapa dan bagaimana jalan ceritanya).  Dari hasil pengamatan, sangat jarang dalang baik dalang laki-laki ataupun dalang perempuan yang mengangkat tokoh perempuan ke dalam pertunjukannya.  Sering dijumpai judulnya menyebut tokoh perempuan tetapi kenyataan di dalam pertunjukannya tokoh perempuan tersebut muncul hanya sekilas, dan tidak banyak bicara. Misal Lakon Alap-alapan Dewi Sukesi, Sayembara Drupadi, dan Kunthi Pilih. Tokoh-tokoh perempuan itu hanya menjadi peran pendukung saja, dan dialognya sangat sedikit.  Mungkin berhubungan dengan budaya jawa bahwa wanita itu harus penurut dan nrima sehingga yang tercermin dalam dialog hanya inggih dan sendika dhawuh.  Perempuan seolah tidak punya kebebasan dalam berbicara.

Seiiring merebaknya wacana gender,  penulis berusaha mengangkat wacana tersebut ke dalam pertunjukan wayang.  Memang wanita tidak ada habisnya untuk dibicarakan.  Ungkapan “Swarga nunut , neraka katut” dan wanita sebagai “kanca wingking” tidak selamanya benar.  Beberapa realita kehidupan menunjukkan para wanita justru memiliki peran yang besar kadang melebihi laki-laki.  Banyak sekali istri yang memiliki peranan penting di sector ekonomi dan pengelolaan ekonomi rumah tangga.  Istri menjadi penopang kehidupan ekonomi rumah tangga.  Mereka bekerja keras untuk mencukupi kebutuhan.  Hal ini menunjukkan bahwa kedudukan wanita tidak hanya sekedar sebagai teman hidup, sebagai pendamping suami yang mengurusi urusan belakang saja.  Keberadaan mereka tidak tergantung dan terbawa oleh kedudukan suami semata.

Berangkat dari hal inilah penulis mempunyai gagasan mengangkat sebuah cerita yang menampilkan tokoh perempuan, di mana kehadirannya tidak hanya sekedar pelengkap atau pendamping tokoh pria dalam meraih cita-cita.  Penulis ingin membuat tokoh perempuan tersebut menjadi fokus garap, sebagai tokoh utama yang mempunyai peran penting dalam sebuah cerita.  Kehadirannya tidak muncul secara tiba-tiba seolah hanya tempelan saja, namun hampir di dalam adegan tampil, angkat bicara ataupun dibicarakan oleh tokoh lain.  Wanita yang dipandang oleh masyarakat jawa sebagai sosok lemah, terkungkung, tak bisa berbuat apa-apa, harus tunduk pada keputusan laki-laki, harus menjalankan perintah tanpa membantah, kini akan penulis garap yang berbeda dimana wanita itu sosok yang kuat, tegas, pemberani, gesit, tangkas, trampil, punya power, keras kepala, pantang menyerah, bukan sosok yang bodoh, tetapi punya kepandaian dan kemampuan, ahli strategi, kadang melebihi laki-laki. Seperti dalam cerita Wanita Kusumayuda, karakter tersebut dimiliki oleh tokoh Dewi Mustakaweni dan Dewi Srikandhi.  Dalam cerita tradisi lakon Srikandhi-Mustakaweni hanya menjadi fragmen, bagian dari Lakon Mbangun Candi Saptarga. Lakon ini menceritakan kisah Pandhawa yang membangun Candi/ makam leluhur Pandhawa di Saptaarga.  Yang dijumpai lakon ini memaparkan peristiwa Pandhawa membangun candi, fokus garapan pada tokoh Pandhawa. Kehadiran Srikandhi dan Mustakaweni seolah hanya menempel, di bagian akhir saja.  Penulis ingin  menghadirkan tokoh Srikandi dan Mustakaweni sebagai fokus garap, sehingga kedua tokoh ini akan dibangun dari awal hingga akhir cerita.  

Permasalahan tersebut akan dikemas dalam sebuah cerita Wanita Kusumayuda. Sebuah karya dari perempuan, oleh perempuan dan untuk perempuan. Dari fenomena di atas dapat ditarik rumusan permasalahan sebagai berikut :

  1. Siapakah perempuan yang dimaksud dalam Wanita Kusumayuda?

  2. Siapakah tokoh wanita yang berperan dalam Wanita Kusumayuda dan apa saja perannya dalam lakon tersebut ?

  3. Permasalahan-permasalahan atau pemikiran perempuan apa sajakah yang bisa diangkat dalam lakon tersebut?

**Tujuan dan Manfaat Penyusunan karya**

Tujuan penulisan karya ini adalah untuk mengangkat permasalahan-permasalahan perempuan, memunculkan tentang pemikiran perempuan dan bagaimana seorang dalang perempuan mengupas wacana gender dalam pertunjukannya.   Wacana gender yang akan diangkat di garapan tokoh dalam Wanita Kusumayuda semoga bermanfaat untuk membentuk pribadi masyarakat yang baik terutama pembentukan moral terhadap wanita, memberikan pemahaman dan penyadaran gender terhadap masyarakat luas.

**Metode dalam penyusunan karya**

Penyusunan karya ini dilakukan melalui tiga tahap yaitu tahap pengumpulan data, analisa data dan penyusunan naskah.  Tahap pengumpulan data dilakukan dengan tiga tehnik yaitu studi pustaka, observasi dan wawancara. Studi pustaka bisa berupa buku-buku ataupun naskah pertunjukan wayang.  Observasi bisa berupa pengamatan pertunjukan langsung ataupun dari pengamatan audio visual. Wawancara akan dilakukan dengan para dalang, baik dalang laki-laki dan perempuan. Target utama wawancara dengan para dalang yang punya pengaruh dalam perkembangan pertunjukan wayang dan juga dari tokoh-tokoh perempuan yang bisa memberi masukan tentang permasalahan dan pemikiran perempuan.

**Alasan Pemilihan Judul dan landasan pemikiran**

Cerita yang akan ditampilkan ini bisa diwadahi dalam Lakon Mbangun Candi Saptaarga-Petruk dadi Ratu, Srikandhi-Mustakaweni, Priambada Krama,  Srikandhi Senopati dan Jamus Kalimasada.  Judul seyogyanya dibuat semenarik mungkin.  Bila penulis memilih salah satu judul di atas, seseorang akan mudah menebak cerita apa yang akan ditampilkan.  Wanita Kusumayuda sengaja dipilih sebagai judul karya karena dipandang lebih menarik dan akan memunculkan berbagai pertanyaan, orang akan menebak-nebak sebenarnya cerita apa yang akan diangkat, sehingga muncul hasrat untuk mengikuti dan mengetahui cerita yang akan disajikan.

Wanita Kusumayuda, wanita artinya perempuan, kusuma artinya pahlawan atau bunga, yuda berarti perang. Wanita Kusumayuda dapat diartikan perempuan yang menjadi bunga dalam peperangan. Perang di sini bisa bermakna ganda, perang dalam adu fisik di medan laga, ataupun perang dalam arti berjuang menghadapi kerasnya jaman. Konsep awal Wanita Kusumayuda akan dikemas menjadi sebuah pertunjukan wayang kulit berkolaborasi dengan tari. Dalam konteks ini siapa perempuan yang berjuang? Dari dalang, pengrawit, penari semua perempuan dan mengangkat cerita perempuan juga.  Wanita Kusumayuda mengangkat tokoh perempuan dalam pewayangan yaitu Dewi Srikandhi dan Dewi Mustakaweni. Kedua tokoh ini ikut berjuang untuk bela Negara dengan cara masing-masing. Lakon ini akan membuktikan bahwa wanita mempunyai hak yang sama dengan pria untuk ikut bela negara dan menjaga keutuhan negaranya. Tentunya peran ini patut kita ketengahkan dan bisa dijadikan suri teladan yang baik terkhusus bagi pembentukan moral para wanita. Bagaimana permasalahan wanita dalam keluarga, bermasyarakat dan bernegara.

**Sinopsis Wanita Kusumayuda**

Keberanian menentukan sikap dalam bertindak bukan hanya monopoli hak kaum pria. Bumiloka raja Ima-imantaka yang bertekad membinasakan Pandhawa atas dendam kesumat mendiang ayahnya Prabu Niwatakawaca. Mustakaweni seorang putri Ima-Imantaka dengan segenap keberanian dan kemampuannya  punya keinginan untuk merongrong kekuatan Pandhawa. Berbagai strategi dilancarkan untuk mewujudkan cita-citanya membalaskan dendam Prabu Bumiloka,kakaknya. Usaha Mustakaweni untuk menghancurkan Negara Amarta justru menjadi pemersatu Negara Ima Imantaka dengan Amarta. Demikian halnya dengan Srikandi, istri Harjuna yang selalu siap berkorban demi keluarga dan bangsanya.  Keikutsertaan Srikandi dalam usaha bela negara mampu menyelamatkan keutuhan keluarga Pandhawa.

Perhelatan kehidupan, baik-buruk, benar-salah, saling tindih berbenturan kepentingan.  Berbagai resiko pun ditempuh sebagai upaya pembenaran atas jalan yang diyakini. Akankah kebenaran tetap menjadi kebenaran? Sampai kapan cinta kasih mampu memadamkan api dendam dan permusuhan? Mustakaweni mampu mendamaikan dan menyatukan dua Negara. Inilah Wanita Kusumayuda, Selamat Menyaksikan.

**Durasi Waktu Aktivitas dilaksanakan**

4 bulan (Oktober 2018 - Februari 2019)

**Total kebutuhan dana**

Rp20.000.000

**Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi**

Rp20.000.000

**Sumber dana lainnya**

Belum ada
