---
title: Proposal Lengkap - Perempuan Tidak Biasa - Martha Hebi
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/perempuan-tidak-biasa
layout: proposal
author: Martha Hebi
categories:
- laporan
- CME
- "Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998"
---

![0575.jpg](/static/img/hibahcme/0575.jpg){: .img-responsive .center-block }

# Martha Hebi - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998

**Tentang penerima hibah**

**Kontak**

  - Facebook: [facebook.com/martha.hebi.5](https://www.facebook.com/martha.hebi.5)

  - Instagram: [@martha_hebi](https://www.instagram.com/martha_hebi/)

  - Website: [marthahebi.blogspot.com](http://marthahebi.blogspot.com/)

**Lokasi**

Pulau Sumba (Sumba Timur, Sumba Tengah, Sumba Barat dan Sumba Barat Daya).

**Deskripsi Proyek**

- **Tujuan**

  Menelusuri kisah perempuan yang telah berkarya dengan caranya sendiri di masa 1965-1998. Menggali apa saja yang telah mereka lakukan, mengapa, bagaimana dan dampak karya-karya mereka pada masa lalu dan saat ini.

- **Sasaran**

     1. Mendokumentasikan kisah 15 perempuan inspiratif ini yang akan dipublikasikan dalam bentuk buku dan kemudian disebarluaskan ke masyarakat dengan berbagai cara.

     2. Memberikan inspirasi bagi gerakan perempuan dan gerakan orang muda masa kini.

     3. Secara terus-menerus dan mengalir akan menelusuri kisah perempuan lainnya dengan informasi yang bersumber dari warga sehingga proyek ini tidak berhenti pada 15 kisah saja namun berkembang lebih luas dan masif.

- **Latar Belakang**

  Upaya penguatan kapasitas komunitas warga, terutama perempuan di Sumba cukup berkembang sejak tahun 2000. Pihak-pihak yang terlibat dalam upaya antara lain adalah LSM, lembaga keagamaan, organisasi pemerintah serta lembaga sosial lainnya. Saat ini sudah banyak perempuan yang memiliki prestasi, karya serta memiliki kapasitas yang handal. 

  Namun bagaimana potret kehidupan, aksi, gerakan para perempuan yang berkarya pada era 1965-1998, saat masih sangat sedikit pihak yang memperkuat perempuan, stigma yang negatif terhadap perempuan yang aktif? Dan di masa represif ini, perempuan di Sumba memiliki akses informasi yang sangat terbatas.

  1. Keterkaitan pada kategori: Riset / Kajian / Kuratorial

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Fakta-fakta tentang perempuan yang melakukan perubahan sosial pada masa 1965-1998 cukup banyak. Namun, masih sangat sedikit yang terdokumentasi dan terpublikasi. Padahal kisah-kisah heroik para perempuan ini bisa menjadi inspirasi.

  3. Strategi

        1. Mengidentifikasi perempuan sesuai kriteria melalui proses interaksi dengan komunitas, jaringan, percakapan personal, serta media sosial yang dapat diakses secara terbuka.

        2. Menggali informasi dari mereka tentang pihak-pihak yang mengetahui kisahnya, mendukungnya atau juga pihak yang menantangnya.

        3. Dalam penelusuran informasi, diupayakan untuk mendapatkan informasi/testimoni dari informan lain/saksi mata/saksi yang mengetahui kehidupan mereka.

        4. Membuat pranala/blog serta media sosial yang secara khusus mempublikasikan kisah perempuan inspiratif, mendapatkan informasi terbaru tentang kisah-kisah perempuan lainnya.

        5. Secara terus menerus akan memperbaharui hasil riset dengan tambahan kisah hasil penelusuran yang dilakukan pasca publikasi 15 kisah perempuan inspiratif ini.

        6. Melibatkan komunitas SOPAN (Solidaritas Perempuan dan Anak) Sumba dalam proyek ini dan tindak lanjutnya pasca proyek .

  4. Aktivitas dan keterkaitan pada sasaran

     Untuk sasaran no 1 (Mendokumentasikan kisah 15 perempuan inspiratif ini yang akan dipublikasikan dalam bentuk buku dan kemudian disebarluaskan ke masyarakat dengan berbagai cara), kegiatan yang akan dilakukan:

        - Identifikasi calon narasumber melalui media, informasi dari warga, dan keluarga.

        - Memilih calon narasumber berdasarkan kriteria.

        - Wawancara dengan narasumber.

        - Wawancara dengan saksi/pihak yang tahu tentang narasumber (testimoni).

        - Menuliskan kisah dan mendokumentasikan dalam bentuk buku.

     Untuk sasaran 2 (Memberikan inspirasi bagi  gerakan perempuan dan gerakan orang muda masa kini), kegiatan yang dilakukan:

        - Melakukan peluncuran buku secara terbuka, menghadirkan perempuan pemilik cerita yang kisahnya dituliskan, pegiat sosial yang memiliki minat terkait isu yang diangkat dan mengundang peserta dari berbagai kalangan di beberapa lokasi di Pulau Sumba.

        - Dialog interaktif melalui Radio Max FM yang memiliki pendengar aktif yang cukup banyak dan tersebar di Sumba.

        - Mempublikasikan melalui media sosial, blog personal maupun blog khusus serta kanal lainnya.

     Untuk sasaran no. 3 (Secara terus-menerus dan mengalir akan menelusuri kisah perempuan lainnya dengan informasi yang bersumber dari 15 perempuan, informan lainnya serta warga sehingga proyek ini tidak berhenti pada 15 kisah saja namun berkembang lebih luas dan masif). Kegiatan yang akan dilakukan :

        - Menggali kisah-kisah perjuangan perempuan di Sumba

        - Mengidentifikasi narasumber lainnya, mendokumentasikan dan mempublikasikan.

  5. Latar belakang dan demografi pelaku proyek

  6. Pemimpin proyek

     perempuan dengan pengalaman di dunia pemberdayaan masyarakat desa, penguatan perempuan  serta seni lebih dari 10 tahun. Bekerja dalam Tim bersama dengan 2 perempuan dan didukung oleh Komunitas Solidaritas Perempuan dan Anak (SOPAN) Sumba.

  7. Demografi kelompok target

     Perempuan yang berkarya pada periode 1965-1998 yang tersebar di 4 kabupaten di Pulau Sumba. Mereka rata-rata berusia 56 tahun hingga 75 tahun. Latar belakang pendidikan tidak tamat SD hingga sarjana.

  8. Hasil yang diharapkan & Indikator keberhasilan

     Proyek ini merupakan inisiasi awal untuk pendokumentasian perempuan yang berkarya dengan kekuatannya melampaui tantangan patriarki dan tantangan sosial lainnya.

     Profil 15 perempuan ini diharapkan akan menjadi inspirasi bagi generasi saat ini terutama bagi orang muda dan perempuan. Selain itu, semakin banyak pihak tertarik untuk mendokumentasikan kisah inspiratif perempuan-perempuan di Sumba dan di wilayah lainnya.

  9. Indikator keberhasilan

        1. Paling tidak ada 15 kisah inspiratif perempuan yang terdokumentasikan.

        2. 250 buku berisikan kisah-kisah inspiratif dan foto-foto 15 perempuan.

        3. Peluncuran buku dan diskusi terbuka dengan menghadirkan perempuan pemilik cerita dan narasumber lainnya yang punya interest terhadap isu perempuan dan perubahan sosial. Kegiatan ini dihadiri 100 orang dari berbagai latar belakang dan keragaman gender, status sosial, profesi dan usia.

  10. Durasi Waktu Aktivitas dilaksanakan

      9 Bulan (Mei 2018 – Januari 2019)

  11. Total kebutuhan dana

      Rp.90.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp 90.000.000

  13. Sumber dana lainnya

  14. Kontribusi perorangan

  15. Kontribusi kelompok target
