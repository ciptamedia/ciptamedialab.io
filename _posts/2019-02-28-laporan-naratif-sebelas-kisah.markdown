---
title: Laporan Naratif - 11 Ibu 11 Panggung 11 Kisah - Kadek Sonia Piscayanti
date: 2019-02-28
permalink: /ciptamediaekspresi/laporan-naratif/sebelas_kisah
layout: proposal
author: Kadek Sonia Piscayanti
categories:
- laporan
- Laporan Naratif
- CME
- 11 Ibu 11 Panggung 11 Kisah
---

# Laporan Naratif - 11 Ibu 11 Panggung 11 Kisah

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Pembelajaran terpenting pertama adalah pembelajaran mendengar dengan tingkat pemahaman yang lebih mendalam dari biasanya, sebab 11 Ibu yang terlibat dalam proyek ini berasal dari latar belakang sosial yang berbeda sehingga strategi yang digunakan untuk mendekati para Ibu menjadi lebih menantang. Strategi komunikasi ini harus benar-benar disesuaikan dengan masing-masing Ibu sehingga menjadi satu keutuhan dalam kinerja tim. Pembelajaran berikutnya adalah pembelajaran mengelola produksi sebuah komunitas Ibu-Ibu yang bukan berlatar belakang aktris dan dunia seni dimana tantangannya jauh lebih besar daripada mengelola komunitas seni. Selanjutnya adalah belajar lebih rendah hati dan lebih bersyukur dengan apapun kondisi yang kita hadapi karena perjuangan 11 Ibu dalam pergulatan hidup mereka masing-masing adalah sebuah inspirasi untuk menerima kelebihan dan kelemahan hidup kita.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> 1. Pementasan akan menyasar komunitas, kelompok, dan aktivis perempuan yang fokus pada bidang perempuan
>
> 2. Publikasi dan dokumentasi akan menyasar audiens yang lebih luas, yang tidak dapat hadir secara fisik ketika pementasan berlangsung

**Ceritakan hasil dari aktivitas tersebut.**

1. Pementasan ini hadir menyasar siapa saja yang hadir baik dari komunitas seni, mahasiswa komunitas Ibu-ibu maupun masyarakat biasa. Setiap pementasan memiliki audiens yang berbeda-beda, dan selalu menghadirkan suasana berbeda. Dari pengamatan saya, penonton tidak hanya sekedar menonton, namun mengambil sebuah nilai-nilai positif dari setiap pementasan. Rata-rata audiens yang merupakan penggerak komunitas perempuan (maupun transgender) memiliki pengalaman personal tersendiri yang cukup membuat mereka terharu pada kisah 11 Ibu dan mengambil nilai-nilai positif akan perjuangan 11 Ibu.

2. Publikasi yang menyasar publik lebih luas adalah dokumentasi video melalui youtube dan buku. Diharapkan dokumentasi proses perjalanan panjang 11 Ibu dalam membangun sebuah produksi teater dokumenter akan tersebar ke audiens yang lebih luas. Hal ini dapat menjadi bahan kajian, penelitian maupun acuan melakukan proses teater dokumenter berikutnya.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Selama ini perempuan malu kalau bercerita di depan publik, apalagi menyangkut persoalan domestik. Perempuan menganggap masalahnya hanyalah untuk dia sendiri menangunggnya, dan pantang berbagi aib rumah tangga kepada orang lain. Dengan metode teater, ketakutan, kecemasan dan keraguan ini bisa dikikis perlahan, sambil menguatkan perempuan bahwa persoalan yang terjadi padanya bukanlah miliknya seorang namun juga milik masyarakat, dan menjadi penting untuk didengar. Mendengar adalah awal yang baik untuk memahami persoalan, dan membantu keluar dari masalah. Setidaknya meringankan beban. Kehadiran teman baru maupun komunitas baru bisa menjadi jejaring yang baik untuk memperkuat jaringan perempuan mendengar perempuan.

**Ceritakan hasil dari mengatasi isu tersebut.**

Melalui proses  berteater ini perempuan khususnya 11 Ibu merasa bahwa proses berteater adalah proses mendengar yang paling jujur, untuk mengatasi kecemasan, ketakutan, dan keraguan berbagi cerita. Komunitas ini hadir menjadi ruang aman dan nyaman untuk berbagi isu-isu apapun, termasuk isu domestik dan isu di luar, dari kecemasan masa lalu menjadi kekuatan di masa depan. Proses ini adalah terapi menyembuhkan luka masa lalu. Teater adalah proses berbagi dengan jujur, yang diharapkan mampu memberi inspirasi akan perjuangan perempuan khususnya Ibu. Kehadiran teman dan komunitas Ibu yang dapat dipercaya menjadi sebuah landasan bagi perempuan untuk percaya bahwa mereka tidak sendiri memiliki beban, bahkan ada yang lebih berat dari beban mereka. Dengan proses ini para Ibu menjadi lebih bisa rendah hati belajar dan atau mengambil pelajaran dari Ibu lain.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Pementasan berlangsung sukses
>
> 2. Respon audiens positif
>
> 3. Respon terhadap publikasi dan dokumentasi positif

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Bagi saya pementasan dikatakan berlangsung sukses ketika audiens mampu mengambil nilai-nilai dari pementasan 11 Ibu. Dari 11 kali pementasan, saya selalu menggelar sesi diskusi dan memohon respon penonton dalam bentuk tulisan, atau gambar, apapun jenisnya. Pementasan juga selalu ditulis dan dicatat serta dipublikasikan di media online dan offline. Dari seluruh proses mengumpulkan data usai pementasan itu, saya dapat menyimpulkan bahwa respon penonton positif dan respon terhadap dokumentasi sangat positif.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Perubahan yang paling besar adalah saya lebih bijak dalam mendengar. Mendengar adalah upaya pertama yang harus dilakukan sebelum proses dimulai dan saya merasa kini kemampuan mendengar saya lebih baik. Hal ini menjadi modal dalam dunia berkesenian saya sebab saya biasanya sebagai sutradara, penulis naskah maupun lebih banyak didengar daripada mendengar. Namun dengan project ini saya belajar banyak sekali dari 11 Ibu dan saya lebih merasa rendah hati dan bersyukur pada kehidupan yang membuat saya lebih dewasa melalui proses ini.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Tantangan terbesar adalah mengelola produksi dengan latar belakang Ibu yang sangat berbeda. Diperlukan kematangan emosi dan intelektual dalam mengelola produksi. Terutama ketika mengatur visi pementasan, menyampaikan pesan yang diinginkan dan menterjadikan semua proses menjadi pentas yang utuh.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Sejak awal saya percaya inilah proyek yang sangat luar biasa menghargai keberagaman perempuan, sebab saya merasa tema yang digarap benar-benar beragam, plural dan dinamis. Saya sebagai perempuan Bali merasa sangat bersyukur proyek ini membuat saya mengenal perempuan-perempuan hebat dari seluruh penjuru Indonesia. Khusus bagi proyek saya, pengalaman ini menyatukan 11 ibu dari tukang batu hingga professor, dari Ibu yang normal hingga yang tuli dan bisu. Semua mendapat perlakuan yang sama. Semua mendapat hak yang sama.
