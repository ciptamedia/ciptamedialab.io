---
title: Laporan Naratif - Puisi dan Gerakan Perempuan - Sartika Sari
date: 2019-06-11
permalink: /ciptamediaekspresi/laporan-naratif/puisi-dan-gerakan-perempuan
layout: proposal
author: Sartika Sari
categories:
- laporan
- Laporan Naratif
- CME
- Puisi dan Gerakan Perempuan
---

# Laporan Naratif - Puisi dan Gerakan Perempuan

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Selama menjalankan proyek ini, saya mendapat banyak pembelajaran melalui hal-hal
positif maupun yang datang melalui hal-hal negatif. Sejak memberanikan diri untuk
mengampu proyek ini, tantangan pertama saya adalah membagi jadwal penelitian
dengan waktu mengajar yang padat. Ditambah lagi, saya harus menghabiskan
beberapa minggu untuk penelitian ke luar kota. Dalam kondisi ini, saya sangat belajar
memanfaatkan waktu seefektif mungkin, termasuk menjaga stamina agar tidak
tumbang.

Di Perpustakaan Nasional Republik Indonesia, saya berkesempatan untuk masuk
ke ruang penyimpanan koran di gedung lama. Saya bisa melihat langsung bagaimana
koran-koran tua itu bersemayam. Tentu saja, itu adalah pengalaman berharga bagi
saya. Setidaknya saya semakin melek bagaimana sikap yang baik sebagai seorang
peneliti naskah kuno. Sepanjang proses pencarian data ini juga, saya belajar banyak
dari orang-orang hebat (mulai dari mahasiswa, peneliti sejarah, pakar gender) yang
saya temui untuk memperdalam data dan wawasan penelitian. Persoalan masa lalu
memang tak pernah selesai, namun dari banyak pandangan yang berbeda itu, saya
merasa menemukan perspektif paling arif untuk memperlakukan sejarah, terutama
sejarah perempuan.

Kemudian pada proses penulisan, saya terus belajar untuk memperbaiki cara
saya menulis tentang sastra dan sejarah perempuan. Saya yang „lahir‟ dari puisi, harus
bisa „menjernihkan‟ tulisan saya agar ketajamannya tidak mendayu-dayu. Saya belajar
untuk mengedit tulisan dari beberapa editor dan proofreader. Tantangan lain yang juga
menjadi medium pembelajaran bagi saya adalah penyelenggaraan peluncuran buku dan
pameran. Pada tahap ini, saya belajar untuk menjadi seorang kordinator yang cerdas,
mulai dari merancang, mempersiapkan, hingga melaksanakan kegiatan. Bagi saya,
perjalanan-perjalanan itu sangat bermanfaat untuk pengembangan diri saya. Meski
tidak semua dapat dipetik dari hal-hal positif, karena kenyataannya saya juga 
menghadapi banyak feedback dan interaksi yang cenderung negatif. Tapi dari sanalah,
saya belajar untuk lebih bijak sejak dalam pikiran hingga tindakan.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Kontribusi untuk sasaran A
>
> Mengumpulkan, membaca, dan menganalisis puisi-puisi perempuan yang terbit di surat kabar tahun 1900-an hingga 1930-an untuk mengungkap gagasan yang terkandung di dalam puisi tersebut. Selain itu, untuk mempertajam penafsiran atas kemunculan gagasan-gagasan dalam puisi, digunakan pendekatan New Historicism dengan menyandingkan teks-teks puisi dengan teks-teks lain yang terbit pada masa yang sama. Dengan demikian, dapat diperoleh gambaran kondisi sosial, budaya, dan politik yang berpotensi memiliki keterkaitan dengan isu yang disampaikan dalam puisi perempuan.
>
> Kontribusi untuk sasaran B
>
> Mencari dan mengumpulkan surat kabar yang terbit di tahun 1900-an hingga 1930-an di Sumatera Utara dari perpustakaan Nasional Republik Indonesia, Perpustakaan Ichwan Azhari, beberapa kolektor surat kabar, dan melalui perjalanan ke beberapa wilayah yang pada tahun-tahun tersebut menerbitkan surat kabar (misalnya Lubuk Pakam, Padang Sidempuan, Sibolga). Selain itu, untuk memperluas penggambaran konteks sosial, saya akan berusaha mengumpulkan sejumlah surat kabar dari wilayah lain.
>
> Kontribusi untuk sasaran C
>
>   - Mengadakan diskusi dan bedah buku hasil penelitian terhadap puisi-puisi perempuan di surat kabar di Sumatera Utara tahun 1900-an hingga 1930-an yang menghadirkan sastrawan, kritikus sastra, budayawan, sejarawan, mahasiswa Bahasa dan Sastra Indonesia, serta masyarakat umum.
>
>   - Menyelenggarakan pameran tulisan-tulisan perempuan Sumatera Utara tahun 1900-an sampai 1930-an yang bertendensi menunjukkan gagasan pergerakan.
>
>   - Mempublikasikan kegiatan diskusi dan pameran tersebut di sejumlah media lokal dan jika memungkinkan juga media nasional.
>
>   - Mengirim buku versi cetak kepada beberapa kelompok literasi di Indonesia dan dalam versi daring dapat diakses masyarakat luas dengan lisensi CC-BY
>
> Kontribusi untuk sasaran A
>
> Menyebarluaskan informasi secara aktif melalui tulisan-tulisan di surat kabar cetak maupun daring kepada masyarakat mengenai keberadaan tulisan-tulisan perempuan di surat kabar tahun 1900-an sampai 1930-an. Salah satunya adalah dengan membagikan teks-teks hasil pindaian surat kabar yang membuat tulisan perempuan.

**Ceritakan hasil dari aktivitas tersebut.**

Dalam proses penelitian, saya telah mengumpulkan, membaca, dan menganalisis puisipuisi perempuan yang terbit di surat kabar tahun 1900-an hingga 1930-an. Di pertengahan jalan, saya menemukan surat kabar perempuan yang progresif di tahun 1940-an. Maka, penelitian yang saya lakukan akhirnya diperpanjang rentang waktunya hingga tahun 1940-an, tepatnya 1919-1941. Seperti rencana awal, saya meneliti puisipuisi tersebut dengan pendekatan new historicism untuk melihat bagaimana gagasan perempuan yang dituangkan dalam puisi „bergerak‟ pada masanya. Untuk kepentingan tersebut, saya harus mengetahui bagaimana situasi sosial pada masa itu. Akhirnya, saya meluaskan kembali jangkauan pembacaan saya pada koran-koran, buku, majalah, yang terbit sebelum tahun 1919 sampai tahun 1950-an. Dengan demikian, gambaran kondisi sosial, budaya, dan politik yang berpotensi memiliki keterkaitan dengan isu yang disampaikan dalam puisi perempuan dapat ditemukan. Selain di Perpustakaan Nasional, saya mencari surat kabar perempuan dari sejumlah kolektor. Misalnya, Ichwan Azhari (Museum Kotta Cinna) dan Wannofri Samry (peneliti surat kabar perempuan dari Padang). Sayangnya, rencana saya untuk berkunjung langsung ke tempat percetakan surat-surat kabar itu (dalam data disebutkan bahwa percetakan ada di wilayah Lubuk Pakam, Padangsidempuan, dan Sibolga) harus diurungkan. Dalam sebuah perjalanan, saya pernah mencari lokasi percetakan dan penerbitan surat kabar perempuan di Lubuk Pakam dan Padangsidempuan. Ternyata, kini sudah tidak ada. Sejumlah warga sekitar yang saya tanyai juga tidak mengetahui perihal keberadaan percetakan tersebut. Atas dasar kondisi itu, maka saya memfokuskan penelitian pada data yang sudah saya temukan dari Perpusnas dan para kolektor. Untuk memperluas wawasan mengenai perkembangan situasi sosial terutama yang melingkari perempuan, saya turut membaca dan mengumpulkan beberapa surat kabar perempuan yang terbit di wilayah lain di Indonesia, pada periode yang sama. Hal ini sangat membantu untuk melihat bagaimana gagasan perempuan di Indonesia berkembang.

Setelah penelitian rampung dan buku selesai dicetak, saya telah melaksanakan
peluncuran buku Seroean Kemadjoean, hasil penelitian terhadap puisi-puisi
perempuan di surat kabar di Sumatera Utara tahun 1919-1941, yang dihadiri
sastrawan, kritikus sastra, budayawan, akademisi, sejarawan, peneliti, mahasiswa, 
komunitas sastra, PWI Sumut, serta masyarakat umum. Acara ini digelar di Taman
Budaya Sumatera Utara. Tidak hanya diluncurkan, buku Seroean Kemadjoean juga
dibedah oleh Dr. Rosliani, M.Hum. (peneliti sastra dari Balai Bahasa Sumatera Utara),
Damiri Mahmud (kritikus sastra), dan Nina Nasution (wartawan, aktivis perempuan).
Selain itu, puisi-puisi perempuan yang saya teliti juga dipamerkan di ruangan kegiatan
dan dipentaskan secara teatrikal oleh sejumlah komunitas sastra di Sumatra Utara.
Dalam kesempatan lain, buku Seroean Kemadjoean juga dibahas oleh Katrin Bandel
dalam kegiatan Apresiasi Sastra (APSAS 2019) di Yogyakarta pada 25 Mei 2019.

Publikasi kegiatan ini banyak memanfaatkan media daring, salah satunya
degilzine. Media lain yang turut meliput adalah Tribun Medan. Untuk media nasional,
salah seorang peserta diskusi pernah menghubungi saya bahwa ia telah menulis ulasan
kegiatan pameran dan peluncuran buku Seroean Kemadjoean di media nasional.
Sayangnya, sampai saat ini belum dipublikasikan.

Setelah peluncuran buku, saya juga mengirim buku versi cetak kepada beberapa
kelompok literasi di Indonesia dan dalam versi daring dapat diakses masyarakat luas
dengan lisensi CC-BY. Sementara itu, informasi mengenai keberadaan puisi-puisi dan
berbagai jenis tulisan perempuan sebenarnya sudah saya lakukan sejak saya belum
menanggungjawabi proyek ini. Ulasan tentang puisi perempuan secara singkat
setidaknya pernah terbit di surat kabar Waspada, Analisa, dan Republika. Sejak
mengampu proyek ini, penyebarluasan mengenai tulisan-tulisan perempuan saya
perluas pada ruang-ruang diskusi akademik dan media daring, termasuk buletin Sirkam,
berikut teks-teks hasil pindaian surat kabar yang memuat tulisan perempuan. Saya pun
dengan sangat terbuka menyilakan siapa saja yang hendak membaca langsung dan
memiliki tulisan perempuan dari surat kabar yang menerbitkannya

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Penelitian terhadap karya-karya penulis perempuan tahun 1900-an hingga 1930-an masih terbatas. Padahal sejak tahun 1900-an, perempuan telah menulis dan menyampaikan gagasan-gagasannya pada masyarakat. Alhasil, kedudukan perempuan pada masa-masa itu seringkali dinilai tidak berarti. Sebagian besar kritikus sastra lebih dominan memperhatikan karya penulis laki-laki yang diterbitkan penerbit besar. Maka, proyek ini berusaha meneliti tulisan-tulisan perempuan yang terbit di surat kabar yang justru terbilang kecil dan cenderung belum terjamah namun mengandung gagasangagasan besar. Dengan demikian, produk dari proyek ini diharapkan dapat menjadi referensi baru yang juga berpotensi menjadi wacana tandingan dalam upaya reinterpretasi sejarah sastra Indonesia dari wilayah Sumatera Utara. Selain itu, proyek ini diharapkan dapat turut memperluas khazanah penelitian pergerakan perempuan di Indonesia.

**Ceritakan hasil dari mengatasi isu tersebut.**

Saya teringat, ketika pertama kali meneliti puisi-puisi perempuan Sumatra Utara, saya
kesulitan mendapatkan sumber ataupun narasumber. Dari sekian banyak kritikus dan
penulis yang saya tanyai, mayoritas beranggapan bahwa literasi perempuan di Sumut
memang lemah, sehingga tidak ada yang dapat dikatakan selain ketidaktahuan. Kondisi
ini yang membuat saya semakin tergerak untuk fokus meneliti tulisan perempuan.
Ketika sejumlah orang mengetahui bahwa saya meneliti puisi perempuan di Sumut,
ditambah dengan kelahiran buku Seroean Kemadjoean, saya merasa ada semacam
gerbang lama yang terbuka kembali. Perbincangan tentang literasi perempuan mulai
„hidup‟ dalam berbagai kesempatan. Selain itu, beberapa kali mahasiswa dari dalam dan
luar jurusan Bahasa dan Sastra Indonesia juga pernah mewawancarai saya tentang
peta kepenyairan perempuan di Sumatra Utara sebagai bagian dari perbincangan
tentang sejarah sastra Indonesia. Hal-hal tersebut membuktikan bahwa temuan proyek
ini sangat berpotensi melengkapi kerumpangan dalam peta sejarah sastra Indonesia,
khususnya di Sumatra Utara yang selama ini cenderung abai pada keberadaan
perempuan dan karya sastranya. Proyek ini juga bermanfaat untuk memperluas
khazanah penelitian tentang pergerakan perempuan di Indonesia karena menghadirkan
potret-potret perkembangan gagasan perempuan pada tahun 1919-1941 melalui teksteks
sastra dan nonsastra yang terbit di surat kabar. Maka, buku Seroean
Kemadjoean saya kirimkan ke sejumlah tokoh sastra Indonesia, komunitas, peneliti
sastra di luar Sumut, dan sejumlah taman baca agar informasi yang saya temukan
dapat tersebar luas.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

>  1. Terkumpulnya puisi-puisi dan teks lain yang ditulis perempuan atau tulisan yang
> tidak ditulis oleh perempuan tetapi berisi isu perempuan di surat kabar tahun 1900-an
> sampai 1930-an yang terbit di Sumatera Utara.
>
>  2. Terbitnya buku hasil penelitian “puisi dan gerakan perempuan” di surat kabar tahun
> 1900-an sampai 1930-an yang terbit di Sumatera Utara. 
>
>  3. Terselenggaranya pameran tulisan-tulisan perempuan tahun 1900-an sampai 1930- an
> di Sumatera Utara.
>
>  4. Adanya liputan dari media cetak atau daring baik lokal maupun nasional mengenai
>temuan penelitian dan kegiatan peluncuran buku serta pameran proyek puisi dan
>gerakan perempuan.


**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Berdasarkan proposal yang saya ajukan, saya telah berhasil untuk mengumpulkan puisi
puisi-puisi dan teks lain yang ditulis perempuan atau tulisan yang tidak ditulis oleh
perempuan tetapi berisi isu perempuan di surat kabar tahun 1900-an sampai 1930-an
bahkan sampai tahun 1950-an yang terbit di Sumatra Utara. Puisi-puisi tersebut sudah
saya teliti dan tuliskan dalam bentuk buku yang kemudian diberi judul Seroean
Kemadjoean. Setelah diterbitkan, buku tersebut diluncurkan, dibedah, dan puisi-puisi
di dalamnya di pamerkan di Taman Budaya Sumatera Utara pada tanggal 10 November
2018. Dalam acara peluncuran dan pameran tersebut, sejumlah wartawan turut hadir
dan meliput, misalnya Tribun Medan, Mimbar Umum, dan degilzine. Wartawan dari PWI
Sumut juga hadir, termasuk salah satu redaktur ruang sastra Harian Analisa Medan.
Secara khusus, temuan penelitian sudah dipublikasikan di sejumlah surat kabar. Kendati
demikian, saya merasa tidak semua indikator kesuksesan tersebut dapat saya capai
sempurna. Pertama, waktu pelaksanaan kegiatan peluncuran buku dan pameran
terlambat dua minggu dari tanggal yang saya jadwalkan dalam proposal.
Keterlambatan ini dikarenakan adanya sedikit kendala dalam proses pemilihan lokasi
peluncuran dan proses pengiriman buku. Selain itu, publikasi kegiatan juga belum
berhasil menjangkau media nasional. Untuk hal ini, saya “mengobatinya” dengan
mempublikasikan dan membicarakan temuan penelitian pada ruang-ruang diskusi
akademik maupun nonakademik di Sumatra Utara, Jawa Tengah, Jawa Timur,
Bandung, dan Karawang.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Terbitnya buku yang membahas tulisan-tulisan perempuan di Sumatra Utara,
khususnya puisi, merupakan suatu keberhasilan proyek ini. Mengingat, dalam pencarian
saya, saya belum menemukan buku yang secara khusus memfokuskan kajian pada
karya perempuan di Sumut. Saya berharap, kehadiran buku ini dapat melengkapi
kerumpangan itu.

Tidak hanya keberhasilan tersebut, proyek ini juga membawa pengaruh positif
pada cara berpikir dan cara kerja saya. Sebelumnya, saya hanya pernah menjadi
“pembantu” dalam pelaksanaan program hibah. CME adalah program hibah pertama
yang saya terima. Maka, segala bentuk pengalaman saya sepanjang melakukan
penelitian hingga menyusun laporan akhir adalah pelajaran penting bagi saya. Tidak
sekadar meneliti, tetapi CME telah memberikan saya pengalaman berdiskusi dan
bersinergi dengan tim. Lebih dari itu, saya banyak belajar dari perempuan-perempuan
kreatif yang juga menerima hibah ini. Semua pengalaman tersebut, mengubah
perspektif saya tentang program penelitian. Saya pernah berpikir, program hibah hanya
sekadar menjalankan penelitian. Tetapi CME mengajarkan pada saya bahwa proyek
penelitian ini tidak sekadar mencari lalu menulis, tetapi juga membangun kedekatan
emosional antarperempuan demi membangun titian sejarah baru. 

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Tantangan yang saya hadapi selama mengerjakan penelitian ini berkaitan dengan
proses pengumpulan data dan percetakan buku. Penelitian dengan pendekatan new
historicism menutut saya untuk memiliki banyak pengetahuan sosial dan data-data
sejarah yang dibuktikan melalui tulisan. Tantangan pertama dalam proses ini adalah
memfilter data-data yang relevan dari sekian banyak surat kabar yang ada pada
periode penelitian saya. Untuk mengatasi ini, saya memutuskan untuk meminta
bantuan pada rekan saya, Yuliana Sari, untuk membantu mengumpulkan dan membaca
tulisan-tulisan di surat kabar yang terbit di Sumatra Utara tahun 1900-1950-an secara
keseluruhan, bukan hanya surat kabar perempuan. Selanjutnya pada proses wawancara
sejumlah tokoh. Sulit sekali menemukan tokoh yang tepat untuk diwawancarai
mengenai isu ini. Beberapa sejarawan yang saya temui bahkan tidak memiliki banyak
informasi tentang bagaimana perkembangan surat kabar dan tulisan-tulisan perempuan
ini. Saya mencoba cara lain, yakni mewawancarai sejumlah tokoh sastra. Ternyata,
tidak berbeda jauh hasilnya. Untuk mengatasi ini, saya memperbanyak wawasan
mengenai situasi sosial dan perkembangan surat kabar perempuan tersebut melalui
sejumlah penelitian yang pernah dilakukan, baik di dalam maupun di luar negeri. Meski
tidak utuh, beberapa yang saya temukan cukup memberi petunjuk.

Lalu, tantangan yang saya hadapi ada pada bagian penerbitan buku. Saya harus
benar-benar mempertimbangkan waktu pencetakan, kualitas, dan pengiriman buku
agar tepat waktu sampai di Medan. Dalam proses pencarian tersebut, saya menghadapi
peristiwa-peristiwa yang di luar dugaan saya. Waktu itu, ternyata masa pencetakan
buku butuh waktu lebih lama dari yang dijadwalkan. Maka, saya harus mencari kargo
yang dapat mengirim buku lebih cepat. Akhirnya, saya memutuskan untuk mengirim
buku dari kargo darat dan kargo udara (menyesuaikan dana yang tersedia). Saya
menghubungi sejumlah kargo udara dan membandingkan ongkos kirimnya. Meski
memang jauh lebih mahal, syukurnya, dengan jalan itu, buku bisa sampai tepat waktu
sehingga ketika peluncuran sudah dapat disebarluaskan. Dari pengalaman inilah, saya
belajar banyak mengenai dunia per-kargo-an.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Keragaman ekspresi perempuan, menurut saya, dapat dibuktikan dari bagaimana
“suara-suara” yang menjadi bagian dari ekspresi tersebut dapat disampaikan. Buku
Seroean Kemadjoean berusaha merekam dan menampilkan suara-suara perempuan
di Sumatra Utara pada tahun 1919-1941 melalui teks puisi dan nonpuisi. Buku ini
memfokuskan kajian pada persoalan gender dengan pendekatan new historicism,
sehingga dapat menunjukkan bagaimana suara-suara tersebut „bergerak‟ pada
masanya. Temuan ini, tentu saja memperkaya dokumentasi keragaman ekspresi
perempuan di Indonesia.
