---
title: Proposal Lengkap - Pentas Kolaborasi Sylvia Saartje - Saartje Sylvia
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/pentas-kolaborasi
layout: proposal
author: Saartje Sylvia
categories:
- laporan
- CME
- Pentas Kolaborasi Sylvia Saartje
---

![1163.jpg](/static/img/hibahcme/1163.jpg){: .img-responsive .center-block }

# Saartje Sylvia - Pentas Kolaborasi Sylvia Saartje

**Tentang penerima hibah**

Sylvia Saartje

**Kontak**

  - Facebook: [facebook.com/sylvia.saartjedua](https://www.facebook.com/sylvia.saartjedua)

  - Instagram: [@sylviasaartje](https://www.instagram.com/sylviasaartje/)

**Lokasi**

Studio UBTV Jl. Veteran Gedung Rektorat Lt.2 (Kota Malang)

**Deskripsi Proyek**

Konser Emas Sylvia Saartje

- **Tujuan**

  Mempersembahkan perjalanan karir bermusik Sylvia Saartje sebagai Lady Rocker pertama di Indonesia.

- **Sasaran**

  Memberi warna baru untuk pendokumentasian konser dan  perjalanan karir.

- **Latar Belakang**

  Penampilan karya Sylvia Saartje yang mengangkat perjalanan karir bermusik Sylvia Saartje sebagai Lady Rocker pertama Indonesia. Karya yang ditampilkan ini sebagian besar merupakan materi yang belum pernah ditampilkan sebelumnya ke khalayak umum, liriknya menceritakan pengalaman hidup dan relevan dengan keadaan serta kondisi yang sekarang sedang berkembang.

  1. Keterkaitan pada kategori: Lintas Generasi

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Melengkapi dokumentasi musik rock di Indonesia.

  3. Strategi

     Menggunakan platform media sosial untuk optimasi dan interaksi sosial dengan masyarakat dan komunitas musik.

  4. Aktivitas dan keterkaitan pada sasaran

     Pembuatan video proses behind the scene, pentas konser dan after movie video konser emas Sylvia Saartje.

  5. Latar belakang dan demografi pelaku proyek

     Pegiat seni dan komunitas music di Malang Raya.

  6. Pemimpin proyek

     Pemimpin proyek adalah Sylvia Saartje yang merupakan perempuan dengan pengalaman 50 tahun berkarya dalam dunia musik di Indonesia.

  7. Demografi kelompok target

        - 25% Masyarakat yang tidak mengenal Sylvia Saartje,

        - 25% fans berat Sylvia Saartje,

        - Dan 50% rekanan (EO, studio recording, media partner, komunitas, sponsor).

  8. Hasil yang diharapkan & Indikator keberhasilan

        - Adanya pendokumentasian berupa video behind the scene, pentas konser, dan aftermovie.

        - Sebagai ajang edukasi untuk generasi muda.

        - Menularkan virus semangat tanpa batas.

  9. Indikator keberhasilan

        1. Karya dapat diterima semua kalangan.

        2. Bahwa konser emas ini akan menjadi pemantik dan penonton puas hingga akhir konser.

        3. Target 400 – 500 penonton.

  10. Durasi Waktu Aktivitas dilaksanakan

      4 bulan

  11. Total kebutuhan dana

      Rp.150.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.100.000.000

  13. Sumber dana lainnya

      Donatur dan Sponsorship

  14. Kontribusi perorangan

      Rp.10.000.000

  15. Kontribusi kelompok target

      Rp.40.000.000
