---
title: Proposal Lengkap - Papuan Voices - Wenda Maria Imakulata Tokomonowir
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/papuan-voices
layout: proposal
author: Wenda Maria Imakulata Tokomonowir
categories:
- laporan
- CME
- Papuan Voices
---

![0199.jpg](/static/img/hibahcme/0199.jpg){: .img-responsive .center-block }

# Wenda Maria Imakulata Tokomonowir - Papuan Voices

**Tentang penerima hibah**

Wenda Maria Imakulata Tokomonowir adalah Film Maker Dokumenter yang berbasis di Papua selain sebagai Film Maker ia bekerja juga di salah satu Komisi Penanggulangan AIDS (KPA) Prov. Papua di Jayapura. Ia memperoleh gelar S1 Jurusan Teknik Informatika di Universitas Musamus Merauke pada tahun 2011. Wenda Maria Imakulata Tokomonowir mulai sebelumnya  bekerja di Universitas Muzamus Merauke ditempatkan di Bagian Administrasi Umum (BAU) pada tanggal 28 Oktober Tahun 2004. Selain bekerja di Universitas Muzamus Merauke mendapatkan tawaran bekerja di salah satu lembaga gereja Katolik “Sekretariat Keadilan dan Perdamaian Keuskupan Merauke”  tahun 2005 seiring waktu berjalan Wenda Maria Imakulata Tokomonowir memutuskan untuk meninggalkan pekerjaan di Universitas Musamus Merauke pada tahun 2006 dan tetap bekerja di lembaga gereja Katolik “Sekretariat Keadilan dan Perdamaian Keuskupan Agung Merauke”. Mulai tertarik untuk membuat film dokumenter  awalnya melihat teman yang sering mendokumentasi kegiatan disitulah saya mulai tekun untuk belajar dan awalnya pengambilan gambar untuk mendapatkan hasil yang baik dan gambar tidak goyang maka saya harus tahan nafas. Setelah itu ada undangan untuk pelatihan film yang dilaksanakan oleh EIA Telapak di Gego Studio di Bogor. Saat itu saya langsung ditunjuk untuk mengikuti kegiatan itu dan kami yang hadir pada saat itu semua dari Papua (Sorong s/d Merauke) semua peserta laki-laki dan saya sendiri dari Merauke Perempuan. Dengan setiap peserta satu instruktur dan saya mendapatkan instruktur bernama JAGO dari EIA dia adalah Film Maker Lingkungan dari Inggris. Setelah itu saya terus belajar dengan ilmu yang sudah saya dapat dan mulai produksi beberapa film.Tahun 2011 Wenda Maria Imakulata Tokomonowir mulai bersama salah satu teman mengumpulkan anak-anak muda dan memberikan pelatihan sekaligus membuat komunitas Film Maker yang bernama “ Papuan Voices “ setelah itu mulai berkembang di beberapa Kabupaten hingga sekarang ini.

Pada tanggal 11 November 2011 Wenda Maria Imakulata Tokomonowir bekerja di Lembaga Kemanusiaan “Gereja Katolik” di Jakarta yang juga satu bagian dari Sekretariat Keadilan dan Perdamaian Keuskupan Agung Merauke selain itu produksi film tetap berjalan sampai 2015 saya ke Jayapura dan bekerja di Komisi Penanggulangan AIDS profesi sebagai Film Maker tetap berjalan sampai sekarang.  Wenda Maria Imakulata Tokomonowir juga memiliki kelompok kecil yang bernama “ Gerakan Pembebasan Perempuan Papua (GP3)”.

Saat ini  Wenda Maria Imakulata Tokomonowir menetap di Jayapura – Papua

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Papua

**Deskripsi Proyek**

Pembuatan Film Dokumenter mengenai kehidupan nyata perempuan Papua yang berhadapan langsung dengan situasi sosial dan politik serta kesehatan. Film mini dokumenter yang berlokasi di Kabupaten Sarmi dan Kabupaten Merauke ini akan mengangkat isu mengenai HIV/AIDS dan akan ditayangkan keliling pada komunitas-komunitas di Papua.

- **Tujuan**

  Membuat film dokumenter  yang diambil dari kehidupan nyata perempuan Papua yang berhadapan langsung dengan situasi sosial dan politik serta pendidikan dan kesehatan. Dengan film dokumenter ini sebagai sarana komunikasi. Sehingga suara perempuan Papua yang ada di kota maupun di pelosok-pelosok pedalaman atau desa dapat tersampaikan dan membawah sebuah perubahan.

- **Sasaran**

  1. Melakukan riset Audio Visual di tiga Kabupaten dengan latar belakang cerita yang berbeda

  2. Menemukan tokoh atau peran yang menguatkan cerita

  3. Mengembangkan kelompok seni perempuan Papua untuk terus berkarya lewat film dokumenter

- **Latar Belakang**

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Film Dokumenter ini terinspirasi langsung dengan kehidupan nyata, dalam pengembangan cerita terjadi kerja sama antara Film Maker, Kepala Kampung, dan pemuda sebagai pengantara

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        1. Film dokumenter ini mengangkat cerita tentang peranan perempuan papua yang menjadi tulang punggung atau kepala keluarga, yang menafkahi keluarga serta biaya pendidikan anak sekolah hingga perguruan tinggi dengan cara mengolah buah kelapa yang sudah tua dan dijual ke Pabrik  PHICO.

        2. Seorang ibu rumah tangga Terinfeksi HIV, Suami dan anak Meninggal Dunia, Ia Membuka Status Supaya Menolong Orang Lain Tidak Tertular,kemudian  ia Menikah Lagi Dan Punya Anak, mereka hidup Sehat dan bisa Berguna Bagi Orang Lain. Salah satu faktor penting bagi orang terinfeksi untuk tetap hidup sehat dan produktif adalah mengenali dirinya sendiri, Dengan keadaan yang sehat seperti ini, sehingga bisa lebih produktif ketika menolong orang lain agar mereka tidak terinfeksi HIV.

        3. Kelanjutan Surat Cinta Kepada Sang Prada, yang menceritakan kisah nyata seorang perempuan di perbatasan yang masih mengharapkan kehadiran sang prada. Serta situasi dan keberadaan pos-pos perbatasan yang dulunya berada di tengah kampung

        4. Dari ketiga cerita diatas ini untuk kesulitan dalam mengatur waktu kerja di kantor dan sebagai film maker tidak ada. Dengan tiga cerita diatas diharapkan dana hibah ini dapat memberikan dukungan berupa fasilitas yang dibutuhkan demi terwujudnya ketiga proyek ini

  3. Strategi

     Dibutuhkan ruang bekerja diluar rumah, agar dapat mengerjakan mengedit dan produksi, serta menjadikan sebagai ruang belajar serta diskusi  bagi perempuan  Papua yang memiliki kemampuan dan bakat di bidang audio visual

  4. Aktivitas dan keterkaitan pada sasaran

        1. Aktivitas untuk sasaran A: membuka kesempatan kepada perempuan di setiap daerah untuk belajar mendokumentasi peristiwa, kejadian di sekitarnya mulai dari hal-hal kecil hingga besar.

        2. Aktivitas untuk sasaran B: Membangun jaringan di berbagai kabupaten di Papua agar mempermudah untuk menjangkau serta riset untuk film-film dokumenter berikutnya

        3. Aktivitas untuk sasaran C: Diharapkan film dokumenter ini  dijadikan proposal untuk dipresentasikan

        4. Aktivitas untuk sasaran D: Film dokumenter dapat didistribusikan kepada berbagai kalangan komunitas, non komunitas, mahasiswa masyarakat, pemerintah

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah perempuan dengan pengalaman berkarya 12 tahun terakhir, khususnya media audio visual. Memiliki komitmen mempunyai studio, memiliki tim yang mengerti audio visual serta mampu menyelesaikan proyek dengan baik

  6. Demografi kelompok target

     Perempuan dan laki-laki usia 18-40 tahun dengan ketertarikan cerita film dokumenter  tentang kesehatan,ekonomi, sosial dan politik.

  7. Hasil yang diharapkan & Indikator keberhasilan

     Keberhasilan proyek ini tercapai ketika terselesaikan dalam waktu yang di rencanakan, dalam biaya yang efisien, konsistensi kualitas gambar, dan story telling yang baik, jelas dalam mengungkapkan pesan-pesan, atau isu-isu yang membuka sudut pandang para pembaca, baik di Indonesia maupun secara global. Dengan fondasi riset yang baik dan sadar etika terhadap pihak & komunitas yang mendukung. Kedepannya dapat bekerja sama dengan badan penerbit yang tepat. Dana hibah untuk proyek ini hanya akan digunakan untuk menyelesaikan 3 film dokumenter hingga Produksi.

  8. Durasi Waktu Aktivitas dilaksanakan

     9 bulan

  9. Total kebutuhan dana

     Rp.126.000.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.126.000.000

  11. Sumber dana lainnya

      belum ada

  12. Kontribusi perorangan

      akan memproduksi film dalam 2 tahun  kedepan dengan bantuan dana hibah  dari sumber-sumber lain.

  13. Kontribusi kelompok target

      Memicu berdirinya kelompok baru dalam dunia Audio Visual di berbagai kabupaten di Papua. Menginspirasikan lebih banyak Film Maker perempuan Papua  untuk mengangkat cerita –cerita yang melekat dalam kehidupan sehari-hari , dan berkarya lewat Film.
