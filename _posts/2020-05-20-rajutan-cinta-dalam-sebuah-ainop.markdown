---
title: Rajutan Cinta dalam Sebuah Ainop
date: 2020-05-20
categories:
  - CME
  - Kajian
  - 'Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai'
tags:
  - CME
  - Korowai
  - Papua
  - Suku terasing
  - Antropologi
author: Rhidian Yasminta Wasaraka
comments: true
---

# Rajutan Cinta dalam Sebuah Ainop

“Yang, ada titipan dari kaka perempuan di Korowai” Ujar suamiku yang baru beberapa jam berhasil menjejakkan kaki di rumah kami kembali setelah seminggu lebih menemani tim Kompas.Id Ke tanah Kholufo. saya memandang rajutan dari kulit genemo (melinjo) yang berwarna orange lebih cerah ceria  dari jamur , berpadu dengan warna biru dongkar dari sejenis buah. sungguh indah….

Dan entah memang nyambung alias feeling dengan mereka tapi memang hasil USG kemarin insya Allah kami akan di anugrahi anak perempuan 

Ini adalah kali kedua saya diberikan noken pada oleh mereka. sebagai perayaan, (celebarate) bahwa saya sedang mengandung dan sebentar lagi akan mendapatkan anak kedua. Ainop alias noken kedua ini sedikit berbeda dari yang ainop pertama yang saya dapat dari Neni Anton –mama angkat saya di Korowai, Ainop dengan hiasan bulu burung yang halus diseluruh bagian ainop, berwarna lebih simple, tapi tetap elegan. ukurannya lebih besar .

Kala itu dirumah pesta neni berkata “ Dian ini buat ko pake gendong cucu laki-laki” ujarnya. Walau beliau tau persis saya tak punya kemampuan dan pengetahuan untuk mengendong anak didalam noken dan lagi Rayhan sudah berusia 2 tahun tiga bulan kala kami kesana. Dan pastinya sudah tak mau duduk dan tidur tenang dalam ainop lagi.

Saya termenung sesaat, mengingat bagaimana susahnya seorang perempuan ketika hendak membuat ainop (noken), dari mulai mencari dan mengambil serat dari batang pohon genemo didalam hutan, lalu proses memilin dan menjalinnya hingga menjadi benang, dan lalu memberikannya warna -Warna hitam/biru dongkar dari sejenis buah dan warna orange dari sejenis jamur kayu. setelah proses pewarnaan selesai proses selanjutnya adalah menjalin bermeter-meter benang dari serat kayu tersebut menjadi sebuah rajutan yang indah.  

Namun kini fenomena lain lebih sering nampak dipandangan mata, ketika terakhir mengunjungi Korowai di akhir 2018, saya melihat fenomena perubahan yang bukan hanya menyentuh pola makan dan cara berpakaian saja. tapi juga  miris melihat bagaimana ainop mulai digantikan dengan sarung dan karung beras 

Ya….kain sarung yang dibeli di kota kini nampak banyak digunakan para wanita untuk menggendong buah hati, walaupun dengan cara gendong yang tetap sama seperti cara menggendong menggunakan ainop-dibelakang punggung, sama halnya dengan karung beras yang dibentuk menyerupai tas, cara menggunakannya masih tetap sama, dibelakang-punggung.

Alasan praktis dan tidak perlu repot untuk mengambil, memilin , menjalin dan mewarnailah yang membuat para wanita Korowai sekarang banyak menggunakan kain sarung dan karung beras produksi kota ketimbang ainop.

Namun bagi saya soalnya, bukan hanya soal praktis dan tidak, ini soal “mengikisnya sebuah kearifan”. 

ah…mungkin  ada yang kemudian berkata "mudah saja bagi saya seorang perempuan kota, berpendidikan tinggi dan dijuluk antropolog dan aktivis lingkungan berkomentar"

Namun bisakah saya menyampaikan cemasan saya? pada situasi ini? ketika kemudian semua diganti dengan barang-barang “laleo” akankah  kemudian nilainya digantikan dengan sarung produksi pabrik atau bahkan karung beras?

Sungguh, saya merasakan getaran yang kuat setiap menyentuh ainop. terbayang jalinan kasih antara perempuan dengan semesta ciptaan berupa hutan yang kaya, tempat mengambil semua secara cuma-cuma. walau harus dengan sedikit usaha. 

Sebab sejatinya sarung dan karung bekas itupun tidak gratis, sebab harus membeli di kota dan ada harga yang harus dibayar. dan harga itu bukan sekedar lembar rupiah yang cukup susah di dapat. 

Dan perlu diingat bahwa sekali lagi hampir semua produk modern bukanlah produk tunggal. mereka perlu produk pendamping agar bisa berfungsi dengan baik. jika tidak maka justru akan berdampak negativ karena kandungan bahan kimia yang ada didalamnya. Kain sarung misalnya butuh sabun untuk mencuci, bukan sekedar air saja. jika tidak maka kain akan menjadi sarang kuman dan bisa menyebabkan penyakit kulit.

Sedangkan sabun cuci di Korowai selain langka harganya juga mahal.

berbeda dengan Ainop yang dibuat dari bahan-bahan alami. selain tahan lama dia juga tidak perlu dicuci. dan bilapun basah tidak perlu dijemur lama supaya cepat kering  

Lebih dari itu semua, Ainop jugalah yang akan dibawa kedalam kubur sang perempuan ketika dia meninggal, bersama perlengkapan perempuan yang lain, yang dia butuhkan dialam baka. 

maka ainoppun sejarah, adalah cinta kelak jika sang perempuan sudah meninggal maka ainop miliknya akan digantung disalah satu sudut rumah, sebagai pengingat semua kenangan tentang sosoknya yang begitu dicintai
