---
title: Laporan Naratif - Pendokumentasian Perempuan Intelektual Sunda Lasminingrat - Rena Amalika Asyari
date: 2019-03-13
permalink: /ciptamediaekspresi/laporan-naratif/perempuan-intelektual-lasminingrat
layout: proposal
author: Rena Amalika Asyari
categories:
- laporan
- Laporan Naratif
- CME
- Pendokumentasian Perempuan Intelektual Sunda Lasminingrat
---

# Laporan Naratif - Pendokumentasian Perempuan Intelektual Sunda Lasminingrat

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Menjadi penerima hibah adalah pengalaman pertama bagi saya. Melakukan riset humaniora secara serius juga ini yang pertama kalinya. Mendapat kesempatan melakukan proyek penelitian di bawah hibah CME menjadi asupan semangat bagi saya, bahwa ternyata masih ada ruang untuk perempuan berkarya. Selain itu, para mentor dan panitia CME sangat bertanggung jawab atas kelangsungan proyek penerima hibah. Saya, sebagai periset pemula dibekali banyak sekali materi tentang pemikiran perempuan, dikayakan sudut pandangnya, difokuskan materi penelitiannya, melalui program Sekolah Pemikiran Perempuan. Proses penelitian berjalan lancar, meskipun beberapa kali sempat kecewa karena data yang sudah dijanjikan ada nyatanya tidak dapat ditemui. Terjun ke lapangan selain menambah pengalaman menjelajahi tempat-tempat baru juga mendapat banyak data yang tidak terbayangkan sebelumnya. Dalam melakukan penelitian tentu saja saya banyak dibantu teman-teman, mereka giat mendokumentasikan dan melaporkan temuan baru. Informan dan narasumber menjadi bagian yang tidak dapat terpisahkan dari keberhasilan penelitian ini. Proyek penelitian ini membuat mata saya terbuka ternyata banyak sekali perempuan-perempuan Indonesia yang kiprahnya tidak terdokumentasikan. Proyek penelitian ini juga mengajarkan saya untuk lebih sabar, tidak terburu-buru, agar data yang didapat bisa maksimal dan valid.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Penulisan sejarah memerlukan proses yang panjang agar karya yang dihasilkan berkualitas tinggi serta bisa menjadi bahan referensi di kemudian hari, maka proyek ini tidak akan berhenti sekalipun tenggat waktu yang ditetapkan dari Cipta Media sudah habis.

**Ceritakan hasil dari aktivitas tersebut.**

Proyek penelitian ini berlangsung dari bulan Oktober 2018 - Februari 2019. Awalnya saya sempat kesulitan mengakses data masa lampau, kisaran tahun 1850-1900. Saya bolak-balik ke perpusnas RI, mengunjungi Badan Arsip, perpustakaan di Jawa Barat, dan Museum di Garut. Beragam referensi di dunia maya tak cukup mewakili data sekunder yang hendak dicari. Memasuki bulan Desember semua menjadi lebih terang, data primer dan sekunder sebagian sudah saya dapatkan. Saya cukup kesulitan ketika menggali data primer karena keterbatasan saya pada penguasaan aksara Jawa (semua data primer beraksara Jawa). Tetapi penelitian tak boleh stuck, harus berlanjut. Dengan memaksimalkan data yang ada akhirnya di bulan Januari saya sudah dapat mensarikan hasil penelitian saya melalui media online. Selain mensarikan hasil penelitian melalui tulisan, saya dan tim berinisiatif membuat Instagram @lentera.perempuan agar penelitian dapat menjangkau kaum muda lebih banyak. Tak puas dengan tulisan dan visual, saya dan tim bersepakat berbagi informasi melalui media audio visual, video berdurasi pendek hanya 1 menit, ringkas dan jelas, agar informasi bisa diterima oleh netizen dan tidak membosankan. Meskipun proyek hibah CME usai di februari 2019, tetapi pencarian Lasminingrat akan terus dilakukan, karena tidak mungkin waktu 4 bulan dapat merangkum semua informasi tentang Lasminingrat.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> 1. Artikel akan fokus pada penggalian pemikiran Lasminingrat sebagai perempuan. Perempuan yang berperan sebagai ibu, perempuan yang berperan sebagai istri dan perempuan yang berperan sebagai anak.
>
> 2. Objek penelitian yang hidup hampir satu abad lalu menjadi kendala dalam proses pencarian data. Dibutuhkan waktu yang cukup lama untuk mendapatkan data yang valid. Proses perenungan, memotret, menghubungkan setiap peristiwa ketika mendapatkan realitas-realitas baru ketika di lapangan tidak bisa dilakukan dengan terburu-buru.

**Ceritakan hasil dari mengatasi isu tersebut.**

1. Fokus penelitian saya yaitu menggali pemikiran Lasminingrat yang bisa dilihat dari karyanya. Terbatasnya data-data primer dan sekunder mengenai Lasminingrat cukup menyulitkan penggalian pemikirannya. Hanya melalui karya tulisnya yang berjudul Tjarita Erman (1875), Warnasari 1 (1876) dan Warnasari 2 (1887) saya berusaha memetakan pemikirannya. Dari semua karya tulisnya saya mengambil benang merah tentang Lasminingrat yang berperan sebagai ibu, istri dan anak perempuan. Pada Tjarita Erman, Lasmi seperti menempatkan dirinya menjadi ibu, ibu yang sangat merindukan anaknya yang hilang. Bukan hanya itu, Lasmi seperti memposisikan dirinya seperti ibu dari anak-anak pribumi lainnya, Lasmi ingin anak pribumi mendapatkan pengajaran dan kasih sayang yang layak. Pada buku Warnasari, Lasmi lebih berani lagi mengambil cerita, ia tak segan bicara tentang perempuan. Kisah feminisme Lasmi tampil dengan heroik membabat habis pemikiran tentang perjodohan yang saya tuliskan dalam artikel yang terbit di Magdalene, juga tentang budaya patriarki yang sangat mengakar dalam masyarakat yang merugikan perempuan sedari kecil yang saya tulis di Jurnal Perempuan.                                                                                                                                                                                                                                                                                                                       2. Dari buku-bukunya saya hanya menyinggung 2 naskah Lasmi, masih sangat banyak yang belum saya bahas karena keterbatasan waktu penelitian dan publikasi hasil penelitiannya. Yang masih cukup mengganjal hingga hari ini adalah saya belum menemukan naskah cerita aslinya, sehingga saya benar-benar belum bisa membandingkan keduanya. Naskah asli vs saduran. Tuanya umur naskah asli menyebabkan penelitian sedikit terhambat, karena ternyata tidak mudah menemukan naskah aslinya. Butuh waktu lagi untuk mencari sumber naskah asli agar penggalian pemikiran Lasminingrat bisa berjalan dengan maksimal.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> Proyek penelitian ini dikatakan berhasil jika dapat menerbitkan 2 esai populer, dengan waktu dan biaya yang sudah disepakati.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Di bulan Desember saya sudah selesai membuat 3 buah esai populer, dengan sasaran akan diupload di The Conversation, The Magdalene dan Historia. Ketiga esai tersebut saya buat isi dan narasinya berbeda sesuai dengan karakter media masing-masing. Tetapi saya ternyata tidak dapat mengirim tulisan di The Conversation karena saya bukan akademisi dengan bidang penelitian yang linier. Saya mengajar di kampus dengan bidang sains, sedangkan proyek penelitian ini bidangnya humaniora. Akhirnya sebagai pengganti The Conversation saya tawarkan esai ke Jurnal Perempuan, dan disetujui, esai tersebut dimuat tanggal 22 Januari 2019. Tak lama setelah itu, The Magdalene juga memuat tulisan saya tanggal 7 Februari 2019. Sampai saat ini  esai yang saya ajukan ke Historia belum juga mendapat respon, saya pun sudah berulang kali meng-email mereka untuk memberikan respon. Terbitnya 2 esai saya sudah menjadi catatan kecil keberhasilan buat saya, target yang saya sanggupi untuk CME terpenuhi. Selain itu, publikasi hasil penelitian melalui video sejarah pendek sebanyak 5 buah dan di bagikan di laman media sosial Instagram, Twitter dan Facebook rupanya cukup mendapat respon.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Proyek ini berawal dari kegeraman dan kegelisahan saya akan pemikiran beberapa perempuan Sunda hari ini yang masih menganggap bahwa perempuan tak layak untuk tampil di publik, padahal zaman sudah bergerak sedemikian jauhnya. Tujuan saya sederhana, yaitu ingin menunjukkan tentang perempuan Sunda yang berpikiran maju. Hasil pencarian saya buntu di Rd. Dewi Sartika. Setelah beberapa lama saya mencari akhirnya saya menemukan nama Lasminingrat, kiprahnya ternyata melampaui zaman. Tentu saja saya sangat bersemangat, karena ternyata hampir 2 abad lalu sudah ada perempuan Sunda yang menolak perjodohan. Penggalian saya dengan dukungan dari CME ternyata membuahkan hasil, saya semakin terbelalak ketika membaca satu persatu karyanya. Saya ingin mewartakan tentang Lasminingrat pada khayalak, perempuan Indonesia, khususnya perempuan Sunda agar tahu dan semakin terinpirasi untuk tidak takut melangkah.

Dengan sasaran generasi milenial tentu saya harus menggunakan media yang menarik untuk mereka. Salah satunya menggunakan video berdurasi pendek (1 menit) yang disebar di media sosial Instagram, Youtube, Twitter dan Facebook. Video-video tersebut saya promote berbayar dan ternyata hasil viewer cukup mengejutkan. Hampir 60% viewer yang tertarik dan mengakses video tersebut adalah laki-laki dan kebanyakan berdomisi di Jawa Tengah. Jawa Barat malahan menempati urutan kedua. Terkejut, bangga sekaligus gelisah, mengapa pengakses pengetahuan tentang perempuan ternyata didominasi oleh laki-laki? Kemana perempuan-perempuan itu? Situs, web atau video seperti apakah yang menarik untuk mereka? Penelitian ini saya anggap berhasil karena ternyata menjangkau banyak orang. Pembaca di Magdalene terakhir kali saya lihat sebelum situs mereka berubah template (tidak menampilkan viewer) mencapai 1200 pembaca. Video-video pendek yang dibagikan di Instagram dan twitter menampilkan hampir 2000an viewer. Beberapa orang akademisi bahkan mengatakan keterkejutannya terhadap saya bahwa ada nama Lasminingrat, mereka rata-rata mengaku baru mengetahui setelah mengikuti hasil penelitian saya. Saya rasa, penelitian Lasminingrat yang saya lakukan cukup berhasil menambah informasi tentang kiprah perempuan Sunda pada banyak orang.

Proyek ini membawa perubahan pandangan bahwa ternyata laki-laki juga haus akan informasi tentang perempuan. Rendahnya minat perempuan untuk saling mengapresiasi hasil kerja sesama perempuan mengingatkan saya bahwa masih banyak pekerjaan rumah untuk memapankan pengetahuan, tingkat ketertarikan dan kemandirian mereka. Media sosial menjadi salah satu arena perang, perang melawan konten-konten negatif dan kebohongan dan mengisinya dengan yang positif. Dibutuhkan peran aktif perempuan untuk mengambil bagian dalam mengisi konten positif, bukan hanya pasif menggunakan apa yang sudah ada dan mengakses hal-hal yang tidak perlu. Proyek Lasminingrat ini saya jadikan pijakan awal untuk saya membuka tabir-tabir perempuan berikutnya. Proses penggalian data mempertemukan saya pada banyak hal tentang perempuan di masa lalu yang tidak saya temukan sebelumnya. Mengapa saya harus mengekplorasi kisah perempuan di masa lalu? Karena saya yakin, melalui kisah mereka kita dapat memetakan diri dan meredefinisi ulang tentang siapa kita, perempuan Indonesia. Cara kerja saya juga berubah, sebelum ada proyek ini, saya adalah tipe orang yang susah bekerja dalam tim, dengan adanya proyek ini mewajibkan saya dapat bekerja saja dengan tim.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Penggalian data Lasminingrat melalui observasi, wawancara, lapangan dan literatur. Ketika observasi dan turun ke lapangan peneliti harus sangat hati-hati, tidak gegabah. Tidak semua orang mampu mengapresiasi setiap tindakan kita. Peneliti harus menunjukkan sikap yang baik, tidak arogan, dan rendah hati. Sikap-sikap tersebut dibutuhkan agar narasumber bisa bercerita lebih banyak. Selain sikap yang baik tentu saja peneliti harus sudah kaya akan data terlebih dahulu, agar ketika turun di lapangan dan wawancara tidak terlalu bingung dengan apa yang akan dicari. Sikap bingung peneliti akan mudah ditebak oleh narasumber yang menyebabkan narasumber tidak yakin dengan pergerakan peneliti. Turun ke lapangan untuk mencari data harus siap dengan segala risiko. Cuaca yang tidak dapat diprediksi, wilayah yang tidak familiar, pergi ke gunung, hutan, menyusuri sawah, terjebak di perjalanan karena daerah tersebut baru pertama kali dikunjungi. Mengunjungi pemakaman, bersikap baik kepada orang-orang yang ada di sekitar lokasi itu harus benar-benar dipegang teguh oleh peneliti. Tidak mengeluh dan putus asa, ketika tidak mendapat data. Harus bersedia untuk bolak-balik ke tempat yang sama. Pencarian data literatur sama sulitnya dengan lapangan. Beragam situs pencari, portal akademik dicari dan digeledah siang malam. Belajar hal-hal baru seperti proses penerjemahan dan membaca aksara lain, selain latin, karena ternyata naskah banyak didapatkan dalam aksara Jawa dan Sunda, juga dalam berbagai bahasa. Data literatur yang proses pencariannya cukup susah, membuat saya harus mempercayakan kepada tim (asisten peneliti) untuk membantu dalam proses pencarian.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Proyek Lasminingrat akan sedikit menggeser, menambah dan memperbaharui mengenai sejarah histografi perempuan di Indonesia. Belum banyaknya orang yang meriset Lasminingrat lebih lanjut, menyebabkan proyek Lasmi yang didukung oleh CME ini akan menjadi rujukan para peneliti berikutnya. Proyek Lasminingrat dibuat oleh perempuan, melibatkan para peneliti perempuan, menyibak kisah hidup dan pemikiran perempuan abad 19, hasil penelitian dipublikasikan dengan mengambil pendekatan feminisme. Rasanya dengan proses tersebut, perempuan lainnya akan melirik pada penelitian ini, meski belum banyak dan seberapa. Dengan mengenal proyek Lasminingrat ini, perempuan dapat termotivasi untuk melakukan pergerakan ke arah yang positif. Lasminingrat dapat menjadi satu referensi tambahan, role model perempuan Indonesia. Proyek Lasminingrat dapat diteruskan oleh siapa saja, Sutradara perempuan bisa mementaskannya, penulis perempuan bisa menuliskan dan menggali kisahnya, composer perempuan bisa membuat lagu dan menggubah musik untuk bercerita tentang Lasminingrat. Lebih dari itu, mungkin saja melalui proyek Lasminingrat ini dapat menyatukan beragam perempuan yang berasal dari berbagai daerah. Pada proses penelitiannya saja, Lasminingrat menjadi semacam pemersatu, bahan diskusi yang tiada habisnya bagi saya dan beberapa teman perempuan lain di sini, di Bandung.

---

## Presentasi CME-Fest

**Anda dapat mengunduh presentasi Rena Amalika Asyari dalam acara CME-Fest melalui tautan di bawah ini**

<a href="/uploads/laporan-naratif/rena-amalika.pdf" class="btn main-btn">Presentasi</a>
