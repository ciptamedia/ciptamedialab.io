---
title: Laporan Naratif - Wanita Kusumayuda - Sri Harti
date: 2019-05-06
permalink: /ciptamediaekspresi/laporan-naratif/wanita-kusumayuda
layout: proposal
author: Sri Harti
categories:
- laporan
- Laporan Naratif
- CME
- Wanita Kusumayuda
---

# Laporan Naratif - Wanita Kusumayuda

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Wanita harus ulet, kerja keras, pantang menyerah, disiplin, dan mandiri tidak bergantung pada laki-laki. Hal ini saya lakoni ketika menerima hibah ini, dengan penuh tanggungjawab ketika dalam kondisi hamil meskipun waktu itu sedang teler-telernya tetapi setiap tugas yang diberikan dalam sekolah pemikiran perempuan bisa diselesaikan, lepas dari hasilnya. Setelah dinyatakan menerima hibah, proyek ini dapat diselesaikan meskipun harus pontang-panting kesana kemari mencari sumber di sela-sela mengurus bayi. Selesai penulisan naskah anak pertamaku ketabrak motor dan harus operasi tulang kaki. Sungguh pengalaman yang luar biasa, proyek ini menjadikanku wanita yang kuat dan pantang menyerah dalam menghadapi berbagai situasi. Alhamdulillah atas berkah Tuhan proyek ini bisa diselesaikan.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Penyusunan karya ini dilakukan melalui tiga tahap yaitu tahap pengumpulan data, analisa data dan penyusunan naskah. Tahap pengumpulan data dilakukan dengan tiga teknik yaitu studi pustaka, observasi dan wawancara. Studi pustaka bisa berupa buku-buku ataupun naskah pertunjukan wayang. Observasi bisa berupa pengamatan pertunjukan langsung ataupun dari pengamatan audio visual. Wawancara akan dilakukan dengan para dalang, baik dalang laki-laki dan perempuan. Target utama wawancara dengan para dalang yang punya pengaruh dalam perkembangan pertunjukan wayang dan juga dari tokoh-tokoh perempuan yang bisa memberi masukan tentang permasalahan dan pemikiran perempuan.

**Ceritakan hasil dari aktivitas tersebut.**

Pengumpulan data berupa buku-buku dan audiovisual pertunjukan wayang kulit dalang lain. Buku-buku didapat dari koleksi pribadi penulis, perpustakaan Jurusan Pedalangan Fakultas Seni Pertunjukan ISI Surakarta, Cafe baca Bukuku Lawas dan menkopi atau membeli buku-buku yang berhubungan dengan penyusunan karya tersebut juga dari wawancara dengan beberapa dalang serta pengamatan pertunjukan langsung. Hasil yang didapat di antaranya mendapat masukan dari Ki Manteb Sudarsono berupa teknik menggarap sebuah lakon, contoh-contoh garapan lakon, garap tokoh, membangun karakter tokoh dan sanggit lakon Saptaarga Binangun. Nyi Rumiyati Anjang Mas, sesepuh dalang perempuan menegaskan tentang kemandirian wanita, ketegasan, keuletan, kerja keras, dan kedisiplinan seorang wanita. Ia menantang keras ungkapan swarga nunut neraka katut, wanita sebagai kanca wingking. Rumiyati menegaskan wanita harus memiliki kemampuan dan ketrampilan, mampu berdiri sendiri tanpa bergantung pada laki-laki. Dalang wanita ini mengajak kepada dalang wanita lain untuk mengangkat dan menggarap tokoh wanita dalam pertunjukannya, memberi warna yang baik dan mengungkapkan pemikiran-pemikiran perempuan. Ia memberikan contoh beberapa tokoh yang digarapnya. Ki Hali Jarwo Sularso, sesepuh dalang guru dalang di ISI dan Mangkunegaran lebih memberikan masukan tentang penggunaan ungkapan, arti kata dan penyusunan kalimat. Dari narasumber lain memberikan masukan tentang pandangannya terhadap wanita. Pengamatan pertunjukan langsung atau audio visual pertunjukan wayang ditemukan sanggit yang berbeda dari berbagai masukan dari narasumber dan pengamatan dianalisis, memberi warna dalam naskah Wanita Kusumayuda.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Dari hasil pengamatan, sangat jarang dalang baik dalang laki-laki ataupun dalang perempuan yang mengangkat tokoh perempuan ke dalam pertunjukannya. Sering dijumpai judulnya menyebut tokoh perempuan tetapi kenyataan di dalam pertunjukannya tokoh perempuan tersebut muncul hanya sekilas, dan tidak banyak bicara. Misal Lakon Alap-alapan Dewi Sukesi, Sayembara Drupadi, dan Kunthi Pilih. Tokoh-tokoh perempuan itu hanya menjadi peran pendukung saja, dan dialognya sangat sedikit. Mungkin berhubungan dengan budaya jawa bahwa wanita itu harus penurut dan nerima sehingga yang tercermin dalam dialog hanya inggih dan sendika dhawuh. Perempuan seolah tidak punya kebebasan dalam berbicara.

**Ceritakan hasil dari mengatasi isu tersebut.**

saya mengangkat beberapa tokoh perempuan di dalam cerita Wanita Kusumayuda, yaitu Dewi Srikandhi, Dewi Mustakaweni dan Dewi Drupadi. Ketiga tokoh dalam lakon ini memiliki peran masing-masing. Dialog-dialognya mampu menunjukkan karakter masing-masing tokoh. Mereka bukan sosok wanita yang lemah dan terpedaya. Ketiganya sosok wanita yang tangguh, bertanggungjawab, disiplin, berani, suka bekerja keras, pantang menyerah, memiliki intelektualitas dan ketajaman rasa yang luar biasa. Suaranya dominan dalam adegan, mempunyai ketegasan dalam memutuskan sesuatu. Tokoh-tokoh ini bebas mengemukakan pendapatnya, bebas menyampaikan pemikiran-pemikiran perempuan yang didukung dan dikuatkan oleh tokoh laki-laki. Dari awal hingga akhir naskah ini menyuguhkan suara-suara perempuan dengan pemikirannya.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> Tercapainya penulisan satu naskah pewayangan yang mengangkat cerita yang menampilkan perempuan sebagai tokoh utama.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Sangat berhasil. Sebelumnya saya pernah menggarap dan mempergelarkan pertunjukan wayang kulit dengan mengangkat tokoh perempuan, namun biasanya hanya satu tokoh yang diangkat kali ini mengangkat dua tokoh utama Dewi Srikandhi dan Dewi Mustakaweni serta tokoh peran pembantu Dewi Drupadi. Sebelumnya mengemukakan pemikiran perempuan terbatas pada adegan tertentu saja, kali ini dari awal hingga akhir pemikiran-pemikiran perempuan itu selalu muncul menyesuaikan isi dan kebutuhan adegan.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Tujuan proyek ini adalah menyusun naskah wayang dengan mengangkat perempuan sebagai tokoh utamanya. Hal ini sudah saya buktikan dalam naskah Wanita Kusumayuda yang selesai saya susun. Saya mengangkat tokoh perempuan dan menyampaikan pemikiran perempuan melalui dialog-dialog tokoh. Tentu membawa perubahan. Dulu saya memasukkan pemikiran-pemikiran perempuan paling-paling pada adegan Limbuk-Cangik ataupun pada adegan gara-gara, dimana tokoh Limbuk Cangik dan punakawan lebih enak untuk menyampaikan pesan secara vulgar. Tetapi kali ini saya ditantang untuk mampu mengemasnya di beberapa adegan. Ternyata kalau kita mau kita bisa melakukannya. Dulu mungkin karena kemalasan saya untuk berpikir kreatif, tetapi dengan selesainya proyek ini saya merasa tergugah untuk menulis karya lagi, saya lebih kritis dalam penggarapan karakter tokoh.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Tantangan yang dihadapi adalah :

  1. Minimnya mencari sumber data tertulis berupa naskah-naskah lakon wayang.

  2. Penggunaan atau pengalihbahasaan ke dalam bahasa pedalangan. Kedua tantangan ini ditangani dengan memperbanyak wawancara dengan beberapa narasumber. Sedang untuk pemikiran-pemikiran perempuan yang harus diubah dengan menggunakan bahasa pedalangan saya merasa kesulitan, karena bahasa pedalangan itu bukan translate atau terjemahan apa adanya dari bahasa lain ke bahasa Jawa saja. Bahasa Pedalangan penuh dengan ungkapan-ungkapan, majas, idiom dsb, jadi bukan ungkapan makna harfiah saja. Kesulitan ini saya atasi dengan diskusi dengan beberapa teman dan dalang lain dan beberapa pemikiran saya sampaikan melalui tokoh Bilung dan Punakawan dengan menggunakan bahasa di luar pedalangan yakni dengan bahasa Indonesia dan bahasa Inggris dengan harapan juga penghayat yang tidak bisa berbahasa Jawa bisa menangkap apa yang saya sampaikan.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Proyek ini menunjukkan hasil kreatif perempuan mengungkapkan pemikiran-pemikiran perempuan dan kesetaraan gender, mengangkat tokoh perempuan, melibatkan dan memperdayakan perempuan dan kupersembahkan terkhusus untuk dalang-dalang perempuan, untuk wanita indonesia, dan untuk siapa saja baik perempuan maupun laki-laki. Garapan lakon yang mengangkat tokoh wanita dengan mengungkapkan pemikiran-pemikiran perempuan, membahas kesetaran gender dari awal hingga akhir cerita baru saya temukan di naskah Wanita Kusumayuda ini. Masih sedikit naskah yang mengangkat tokoh perempuan, kalaupun ada biasanya hanya muncul di adegan tertentu saja, porsinya tidak sebanyak yang ada di naskah Wanita Kusumayuda. Masalah kesetaraan gender, stereotip, subordinasi, marginalisasi, double burden diangkat di naskah ini. Wanita harus diberi kesempatan untuk menunjukkan kebolehannya, wanita harus kuat, ulet, mandiri, pandai, punya ketrampilan, dan mampu berdiri sendiri tanpa harus bergantung pada laki-laki.
