---
title: Proposal Lengkap - Aku Perempuan Unik - Nanik Indarti
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/aku-perempuan-unik
layout: proposal
author: Nanik Indarti
categories:
- laporan
- CME
- Aku Perempuan Unik
---

![0378.jpg](/static/img/hibahcme/0378.jpg){: .img-responsive .center-block }

# Nanik Indarti - Aku Perempuan Unik

**Tentang penerima hibah**

Nanik Indarti adalah seorang seniman teater asal Bantul Yogyakarta merupakan lulusan S1 Seni Teater di institut  Seni Indonesia Yogyakarta pada tahun 2011. Terlibat aktif dalam komunitas teater Saturday Acting Club (SAC) sejak tahun 2007 sampai sekarang. Pernah belajar dan bekerja di Padepokan Seni Bagong Kussudiardja sejak tahun 2010-2016. Saat ini banyak terlibat aktif dalam produksi-produksi teater di Yogyakarta. Menolak eksploitasi terhadap pekerja seni bertubuh mini  dalam industri hiburan. Upaya ini diharapkan mampu menyodorkan perspektif gender dan difabel yang kuat di lingkungan seni dan dunia yang lebih luas.

**Kontak**

  - Facebook: [facebook.com/nanik.okedonkdonk](https://www.facebook.com/nanik.okedonkdonk)

  - Instagram: [@nanikokdonkdonk](https://www.instagram.com/nanikokdonkdonk/)

**Lokasi**

Pendhapa Art Space

Jl. Ringroad Selatan, Tegal Krapyak RT 01 Panggungharjo Sewon Bantul DI. Yogyakarta

**Deskripsi Proyek**

Pertunjukan teater dokumenter dan launching buku : Aku Perempuan Unik berdasarkan kisah perempuan-perempuan unik bertubuh mini (peyandang achondroplasia). Pertunjukan ini dikemas dalam pertunjukan teater dan multimedia yang melibatkan komunitas seni Flying Ballons Puppet (teater Boneka), Batik Shadow, Rumah Pantomime, dan Samadhi (musik).  Pertunjukan teater dokumenter ini akan melibatkan  7 perempuan penyandang achondoplasia yang berasal dari Aceh, Kalimantan, Blitar, Surabaya, Banyuwangi, Malang dan Yogyakarta. Upaya ini diharapkan mengurangi perlakuan buruk masyarakat seperti bully, dikucilkan,  diejek, dan dieksploitasi terutama di dunia hiburan.

- **Tujuan**

  Mempertemukan perempuan-perempuan unik bertubuh mini (penyandang achondroplasia)  yang sesama seniman untuk membicarakan masalah-masalah yang dialami melalui kritik sosial yang disampaikan dalam bentuk media cetak tertulis (buku) dan pertunjukan. Dengan harapan dapat  mempresentasikan  lapisan persoalan-persoalan perempuan-perempuan bertubuh mini, pilihan-pilihan jalan hidup yang memiliki perspektif gender dan difabel di lingkungan seni dan masyarakat

- **Sasaran**

  Masyarakat (umum)

- **Latar Belakang**

  Sebagai perempuan yang terlahir dengan tubuh mini kerap kali menjadi bahan ejeken (bully) dan tertawaan di lingkungan masyarakat. Mengalami deskriminasi dan menjadi kaum minoritas di antara manusia lainnya. Pengalaman hidupnya yang buruk pernah dikucilkan karena bertubuh pendek dan diejek karena tidak bisa tinggi. Bahkan pernah mengalami eksploitasi di industri hiburan. Semua pengalaman buruk hidupnya berpengaruh terhadap ketidakberdayaan dan kehilangan kepercayaan diri.

  Berangkat dari gagasan tersebut saya ingin membuat karya tulis cetak dan pertunjukan yang secara khusus akan dimainkan oleh perempuan-perempuan bertubuh mini berdasarkan pada kisah nyata yang mereka alami dalam kehidupan mereka.

  1. Keterkaitan pada kategori: Kolaborasi

     Wujud nyata proyek ini adalah Pameran, Launching Buku Aku Perempuan Unik dan Pementasan Teater yang divisualkan berdasarkan buku tersebut. Pementasan teater tersebut akan menampilkan pertunjukan kolaborasi seni teater, puppet, musik, multimedia dan rupa dalam bentuk teater kontemporer.

     Buku Aku Perempuan Unik ditulis oleh tujuh (7) perempuan bertubuh mini yang ada sebagian wilayah Indonesia (Aceh, Kalimantan, Banyuwangi, Surabaya, Blitar, dan DI. Yogyakarta).

     Metode penciptaan pertunjukan adalah kolaborasi antara saya  sendiri sebagai pemain, sutradara dan penulis naskah. Kolaborasi yang saya ciptakan bersama dua perempuan yang berdomisili di Yogyakarta adalah kolaborasi dalam penciptaan karya berupa ide dan gagasan konten yang akan ditampilkan.

     Selain itu saya juga berkolaborasi dengan melibatkan ± 100 orang yang terdiri dari:

        - Lima puluh (50) orang tim produksi dan wilayah artistik

        - Kolaborasi Pertunjukan:

             1. Flying Ballons Puppets (5) orang

             2. Batik Shadow (5) orang

             3. Musik (2) orang

             4. Dalang (1) orang

             5. Aktor & aktris (11) orang

        - Kolaborasi dengan sepuluh (10) orang perempuan yang akan membuka stand pameran hasil karya ciptanya sebagai perempuan dalam wujud kerajinan, fashion, dan accessories.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Karya Aku Perempuan Unik mengangkat persoalan-persoalan perempuan penyandang achondroplasia yang mengalami eksploitasi dan deskriminasi dalam kehidupan sosial masyarakat. Melalui media seni akan mengungkapkan peristiwa-peristiwa pengalaman hidupnya dan rahasia-rahasia perempuan tersebut yang akan dituangkan dalam sebuah buku untuk dibaca bersama dan akan divisualkan dalam bentuk pertunjukan.   Dengan adanya karya ini harapannya mampu menginspirasi para penyandang achondroplasia lainnya untuk bangkit. Dengan adanya hibah dana ini diharapkan dapat memberi dukungan berupa fasilitas yang dibutuhkan demi mewujudkan proyek ini.

  3. Strategi

     Strategi yang dilakukan dalam penciptaan karya ini adalah setiap pelaku menuliskan segala peristiwa hidupnya. Tulisan tersebut akan dikumpulkan menjadi satu buku. Komunikasi bagi pelaku di luar kota dilakukan melalui media soasial yang dapat menyingkat wilayah kerja. Semua tim produksi adalah seorang sahabat dekat yang bisa bekerjasama untuk proyek ini.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Menjalin kerja sama  dengan para narasumber lintas wilayah di Indonesia untuk menciptakan ruang kreatif bersama sebagai wadah ekspresi.

        2. Menjalin network dengan penerbit

        3. Berkomunikasi dengan narasumber yang dilakukan dengan wawancara, diskusi untuk mendalami konten-konten sebagai bahan karya

  5. Latar belakang dan demografi pelaku proyek

     Pada proyek pemimpin proyek bertindak sebagai sutradara dan penulis naskah.  Proyek ini melibatkan banyak pelaku lintas disiplin seni pertunjukan (sastra, teater, tari, wayang dan musik),  multimedia dan rupa. Yang terdiri dari perempuan dan laki-laki usia 25-40 tahun yang sudah profesional di bidangnya. Melibatkan 100 orang untuk memproduksi dan mendukung karya ini.

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan yang memiliki pengalaman berkesenian teater lebih dari 7 tahun baik sebagai pelaku seni (aktris) maupun pelaku produksi (manajemen) hingga sekarang.

  7. Demografi kelompok target

     7 perempuan bertubuh mini penyandang achondroplasia dan 4 laki-laki bertubuh mini sebagai pendukung karya ini

  8. Hasil yang diharapkan & Indikator keberhasilan

     Proses penulisan buku sampai launching berjalan lancar tanpa halangan apapun. Semua narasumber bersedia dan ikhlas untuk terlibat dalam proyek ini. Hasil karya ini diharapkan dapat memberikan inspirasi bagi orang-orang bertubuh mini lainnya.

  9. Indikator keberhasilan

     Keberhasilan proyek ini akan tercapai ketika dapat menyelesaikan sesuai dengan waktu yang telah direncanakan. Perencanaan-perencanaan terlaksana dengan baik dan lancar dalam biaya yang efisien. Mampu mengungkapkan cerita ke dalam kemasan tertulis (cetak) maupun karya pertunjukan yang dapat mewadahi ide-ide, gagasan berdasarkan pengalaman pelaku dalam lingkungan seni dan sosial masyarakat. Cerita yang disampaikan dapat  mengungkapkan pesan-pesan, atau isu-isu yang membuka sudut pandang audiens. Hibah dana untuk proyek ini hanya akan digunakan untuk menyelesaikan proyek ini hingga selesai.

  10. Durasi Waktu Aktivitas dilaksanakan

      7 bulan (Mei – November)

  11. Total kebutuhan dana

      72 Juta

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      57 Juta

  13. Sumber dana lainnya

      Belum ada

  14. Kontribusi perorangan

      Penggagas akan  meresmikan dan melanjutkan komunitas penyandang achondroplasi menjadi sebuah komunitas yang mewadahi ruang-ruang ekspresi dalam bentuk karya seni.

  15. Kontribusi kelompok target

      Menginspirasikan lebih banyak  penyandang achondroplasia  untuk berkarya
