---
title: Proposal Lengkap - Pengadaan alat dan kostum grup Suluk Samudera - Dida Imada Maulina
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/suluk-samudera
layout: proposal
author: Dida Imada Maulina
categories:
- laporan
- CME
- Pengadaan alat dan kostum grup Suluk Samudera
---

![0588.jpg](/static/img/hibahcme/0588.jpg){: .img-responsive .center-block }

# Dida Imada Maulina - Pengadaan alat dan kostum grup Suluk Samudera

**Tentang penerima hibah**

Dida Imada Maulina adalah seniman musik tradisional Sunda yang berbasis di Bandung, Jawa Barat. Ia memperoleh gelar S1 jurusan Karawitan di STSI Bandung. Pada tahun 2003, Ia menekuni karir di bidang Seni Budaya dan menjadi pengajar di salah satu sekolah dasar di Bandung.

Ia tergabung dalam komunitas teater pada tahun 2005 dan pernah mengisi acara Tong Tong Fair Festival di Belanda, juga berkolaborasi dengan Jubing Kristianto (musisi gitar akustik) dan Jacob ( musisi sasando) dalam festival Batik di Inggris, dan komunitas musik lainnya. Setelah beberapa tahun vakum, pada tahun 2016 Dida kembali mengajak kawan-kawan yang tergabung dalam grup musik suluk samudera untuk aktif melaksanakan kembali latihan secara rutin.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Bandung, Jawa Barat

**Deskripsi Proyek**

Proyek ini merupakan pengajuan alat musik dan kostum, sebagai upaya untuk menciptakan ruang berproses (latihan seni, berkarya atau bertukar pikiran) yang ramah terhadap wanita. 

Proyek ini saya ajukan sebagai ketua grup musik tradisi bernama “Suluk Samudera” yang beranggotakan 10 orang. 

Dalam proyek ini kami mengajukan pembelian alat-alat musik untuk mengganti alat musik kami yang sudah tidak layak digunakan, dan menambah alat musik yang belum kami miliki, diantaranya 6 buah kacapi Sunda, 1 set kendang, 1 buah biola, 1 buah rebab, 1 buah cajon, 1 set suling dan bangsing, dan 1 buah bass. Selain itu, kami juga berkeinginan membuat beberapa pasang kostum untuk pentas.

- **Tujuan**

     1. Mengembangkan seni tradisi

     2. Memotivasi wanita untuk tetap berkarya

- **Sasaran**

     1. Pengadaan alat musik

     2. Pengadaan kostum

     3. Pembuatan karya

- **Latar Belakang**

  1. Keterkaitan pada kategori: Akses

     Untuk pemenuhan kebutuhan alat musik penunjang grup Suluk Samudera

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Grup Suluk Samudera adalah grup musik tradisi Sunda yang beranggotakan 10 orang wanita. Grup ini terbentuk pada tahun 2010, saat kami menjadi mahasiswa di Sekolah Tinggi Seni Indonesia (STSI) atau yang sekarang menjadi Institut Seni Budaya Indonesia (ISBI) Bandung. 

     Pada saat masih menjadi mahasiswa, grup yang seluruhnya beranggotakan wanita masih sangat jarang, karena wanita masih dianggap tabu atau belum lazim menjadi pemain alat musik, apalagi sebagai pembuat karya. Walaupun demikian, pada saat itu kami tetap aktif melakukan latihan dan membuat karya.

     Pada prosesnya, status mahasiswa memudahkan kami untuk meminjam ruangan ataupun berbagai alat musik serta perlengkapan lainnya seperti sound ataupun kostum, dari pihak kampus. Namun kami mengalami masa vakum setelah kami lulus kuliah, karena selain kami sulit mendapatkan sarana prasarana, pernikahan dan kewajiban mengurus anak pun menjadi kendala. Selama mengalami masa vakum tersebut, kami merasa dikebiri karena potensi dan keinginan kami seolah dipaksa untuk dihentikan begitu saja. 

     Beberapa tahun kemudian (tahun 2017), akhirnya kami memutuskan untuk kembali mengadakan latihan walaupun harus sambil membawa anak-anak kami. Kami berpikir bahwa mengurus keluarga memang adalah keharusan, namun bukan berarti kami harus berhenti mengembangkan diri kami. Namun gagasan tersebut terhambat karena kami tidak memiliki alat musik dan kostum yang layak untuk proses latihan, berkarya, dan pentas.

  3. Strategi

     Dibutuhkan alat musik untuk latihan agar dapat fokus berkarya serta kostum sebagai penunjang penampilan grup suluk samudera.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Pengadaan alat musik : alat musik disesuaikan dengan kebutuhan grup suluk samudera. Beberapa diantaranya dibuat secara custom untuk memaksimalkan kebutuhan dalam pembuatan karya.

        2. Pengadaan kostum : menjalin kerjasama dengan penata kostum di STSI Bandung

        3. Pembuatan karya : diadakannya latihan secara rutin kembali dengan proses setiap 2 kali dalam seminggu. Diharapkan dapat membuat suatu pertunjukan dengan sudah terpenuhinya alat musik dan kostum yang dibutuhkan grup suluk samudera.

  5. Latar belakang dan demografi pelaku proyek

     Perempuan berusia sekitar 20-35 tahun, dengan berbagai latar belakang profesi dan spesialisasi alat musik masing-masing.

  6. Pemimpin proyek

     perempuan dengan pengalaman di berbagai komunitas musik serta sebagai pengajar di salah satu sekolah dasar di bandung.

  7. Demografi kelompok target

     Grup musik perempuan Suluk samudera beranggotakan 10 orang berusia sekitar 23-34 tahun, dengan berbagai latar belakang profesi dan spesialisasi alat musik masing-masing. Seluruh anggota merupakan alumni STSI Bandung.

  8. Hasil yang diharapkan & Indikator keberhasilan

     Suluk samudera dapat terus berkarya, mengembangkan potensi perempuan yang ingin terus bergelut di bidang seni musik khususnya musik tradisional.

  9. Indikator keberhasilan

     Proyek ini dikatakan sukses jika kami sudah bisa berproses secara rutin dan pentas dengan alat-alat musik dan kostum yang memadai.

  10. Durasi Waktu Aktivitas dilaksanakan

      6 Bulan

  11. Total kebutuhan dana

      Rp.32.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp.32.000.000

  13. Sumber dana lainnya

      Tidak Ada

  14. Kontribusi perorangan

      Tiap-tiap anggota grup suluk samudera mendapatkan tanggung jawab masing-masing dalam proses pembuatan karya. Dengan bertanggung jawab pada tugas musikal masing-masing dapat tercipta karya yang harmonis.

  15. Kontribusi kelompok target

      Memotivasi perempuan yang bergelut di bidang seni untuk dapat terus berkarya. Keluarga dan kesibukan masing-masing, tidak menjadi kendala dalam berkarya.
