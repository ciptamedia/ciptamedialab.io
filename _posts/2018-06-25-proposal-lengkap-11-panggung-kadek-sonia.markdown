---
title: Proposal Lengkap - 11 Ibu 11 Panggung 11 Kisah - Kadek Sonia Piscayanti
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/11-panggung
layout: proposal
author: Kadek Sonia Piscayanti
categories:
- laporan
- CME
- 11 Ibu 11 Panggung 11 Kisah
---

![0599.jpg](/static/img/hibahcme/0599.jpg){: .img-responsive .center-block }

# Kadek Sonia Piscayanti - 11 Ibu 11 Panggung 11 Kisah

**Tentang penerima hibah**

Kadek Sonia Piscayanti adalah penggiat seni budaya yang telah berkarya selama sepuluh tahun terakhir. Karya-karyanya berupa cerpen, puisi, naskah drama dan esai, juga karya pertunjukan teater. Kadek Sonia lahir di Singaraja, 4 Maret 1984. Karya sastra berupa buku-buku yang sudah diterbitkan antara lain Karena Saya Ingin Berlari (2006), Perempuan Tanpa Nama (2014), The Art of Drama (2013), The Art of Literature (2014), Burning Hair (2017) dan esai-esai sastra dan seni pertunjukan di beberapa media nasional dan internasional. Karya-karya yang pernah dipentaskan di antaranya adalah Negeri Perempuan (2006), The Story of A Tree (2013), Schizophrenia (2013), My Dearest Sister (2013), My Mother and Monster (2013), Because I am Who I am (2013), Layonsari (2014), Reinkarnasi Layonsari (2016), Perempuan Tanpa Nama (2017), Kisah Cinta Nyoman Rai Srimben (2016 dan 2018), dan kini sedang mempersiapkan pentas 11 Ibu, 11 Panggung dan 11 Kisah. Pernah diundang sebagai penampil, pembicara dan kurator di Ubud Writers and Readers Festival, menjadi pembicara di Asia Pasific Writers and Translators, mendapat muhibah seni budaya ke Belanda dan Prancis (2014). Pada tahun 2009 mendirikan Komunitas Mahima dan Mahima Institute Indonesia pada 2010.

**Kontak**

via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Kabupaten Buleleng, Bali, Indonesia

**Deskripsi Proyek**

Proyek kolaborasi “11 Ibu, 11 Panggung, 11 Kisah” adalah proyek teater yang melibatkan 11 perempuan sebagai aktor utama dan menceritakan kisah mereka sendiri dalam bentuk monolog. Monolog 11 Perempuan ini akan melibatkan pula komunitas, jejaring sosial media yang akhirnya akan membentuk inisiasi komunitas perempuan mendengar perempuan.

Konsep kolaborasi akan dijalankan dengan cara melibatkan 11 perempuan ini dalam dialog yang saling melengkapi, sebagai aktor di dalam panggungnya sendiri, namun juga kru di panggung mereka dan panggung perempuan lainnya.

- **Tujuan**

  1. Membuat teater sebagai sebuah gerakan mendengar perempuan

  2. Menciptakan komunitas yang mendengar dan menghargai suara perempuan

  3. Mendukung perempuan dari segala lintas disiplin untuk berkarya di bidang masing-masing secara berkelanjutan

  4. Memberi penguatan-penguatan positif terhadap perempuan di sekitarnya

  5. Membentuk komunitas “perempuan mendengar perempuan” berupa lingkaran solid yang saling menguatkan

  6. Menjadi basis komunitas perempuan berani bersuara dan berkarya

- **Sasaran**

  1. Komunitas perempuan

  2. Komunitas seni budaya

  3. Komunitas kreatif

  4. Aktivis perempuan

  5. Masyarakat umum

- **Latar Belakang**

  Beberapa waktu lalu, di Bali, seorang ibu membunuh 3 orang anaknya lalu bunuh diri!

  [https://www.jawapos.com/baliexpress/read/2018/02/21/51224/ngakunya-mau-ngajar-malah-ke-rumah-bajang-lalu-bunuh-tiga-anaknya](https://www.jawapos.com/baliexpress/read/2018/02/21/51224/ngakunya-mau-ngajar-malah-ke-rumah-bajang-lalu-bunuh-tiga-anaknya)

  Saya mencerna ini dengan hati-hati. Saya perempuan, saya ibu. Tentu saya bersimpati pada sang ibu. Pertanyaannya: mengapa ini semua terjadi, dan mengapa hal ini bisa terjadi, mengapa tak ada yang cukup sensitif mencegah ini terjadi, kemana saja keluarganya, mengapa ibu ini sendiri, dan ribuan pertanyaan lainnya.

  Sebagai perempuan dan ibu, saya merasa sakit, menyayangkan mengapa peristiwa itu bisa terjadi. Terlebih sebagai perempuan Bali, saya makin sedih, mengapa kasus ini hingga separah ini, di lingkungan sekitarnya tak ada yang menolong perempuan ini ketika dia terhimpit masalah. Saya juga sangat kaget dan terpuruk mengetahui bahwa seorang ibu yang juga seorang guru ini berani membunuh anak sendiri, bahkan tak lagi ingat dengan hukum karma, dan Tuhannya. Saya tidak menghakiminya, justru ingin berada bersama perempuan itu, ingin mendukungnya di bawah tekanan social media bullying yang menerpanya. Tak ada sesuatu yang terjadi dengan tiba-tiba, semua pasti ada alasannya. Apapun itu, korban nyawa sia sia telah jatuh, tak dapat dibayar dengan apapun juga. Saya ingin bergerak langsung.

  Saya ingin menciptakan jejaring “perempuan mendengar perempuan”. Jejaring ini ingin saya bentuk dan saya kembangkan di masyarakat, agar perempuan memiliki “teman yang dapat memahaminya”. Di tengah tekanan berbagai persoalan, perempuan terhimpit banyak isu. Ekonomi, kesehatan, sosial, budaya, politik, hingga keamanan. Saya sebagai perempuan Bali dan seorang ibu menyadari bahwa peran perempuan sangat kompleks sehingga ia harus cerdas membagi peran dan tanggung jawab terhadap keluarga, masyarakat, dan terutama terhadap dirinya sendiri. Yang terpenting pula, ia harus menemukan siapa dirinya, apa tujuan hidupnya dan apa bentuk eksistensinya sebagai perempuan. Jika hal ini diabaikan, perempuan akan terpuruk, terjatuh dan merasa putus asa.

  Untuk itulah saya ingin membuat project kolaborasi “11 Ibu, 11 Panggung, 11 Kisah” ini. Project ini adalah kolaborasi yang mewadahi dialog antar perempuan. Dialog didahului dengan mendengar dan merasa. Lalu bersimpati, lalu mendukung. Project ini adalah gerakan kesadaran perempuan bahwa suara mereka layak didengar dan diapresiasi. Bahwa perjuangan mereka layak dihargai. Selama ini perempuan sering berjuang dalam sunyi, menghadapi masalah dalam sunyi, dan frustrasi. Saya ingin mendengar mereka, memberikan mereka panggung bercerita. Bukan untuk mencari sensasi, tapi untuk berbagi. Jadi konsep kolaborasi ini konsep bekerja bersama dalam membuat pementasan dengan teater monolog dengan pendekatan realis, dimana panggung adalah realitas yang mereka jalani, dan mereka bermain sebagai aktor monolog sekaligus sebagai karakter diri sendiri, menjadi juru cerita (story teller) dalam bingkai cerita yang mereka pilih sendiri dari perjalanan hidup mereka sendiri. Sebagai penulis naskah dan sutradara, saya akan menuliskan kisah mereka kembali dalam konteks estetika naskah teater dan mengarahkan mereka dalam konteks estetika panggung, yang sesungguhnya adalah panggung mereka sehari-hari. Mengapa dalam bentuk teater? Sebab mereka adalah sesungguhnya aktor teater yang orisinal di kehidupan sehari-hari. Teater memberi jalan agar suara mereka lebih terdengar dan lebih bisa dirasakan langsung oleh audiens. Konsep kolaborasi akan dijalankan dengan cara melibatkan 11 perempuan ini dalam dialog yang saling melengkapi, mereka bukan hanya aktor di dalam panggungnya sendiri, namun juga kru di panggung mereka dan panggung perempuan lainnya.

  Mereka adalah 11 Ibu yang tangguh, yang menghadapi hidup dengan berani, seberapa kuatnya cobaan menerpa. Mereka bukanlah ibu-ibu gemerlap, ibu-ibu sosialita, bukan pula seniman yang profesional yang selalu berada di bawah sorot lampu gemerlap. Mereka ibu-ibu sederhana yang lahir dan tumbuh bersama keluarga, berjuang untuk keluarga dan bertahan. Mereka pejuang hidup sesungguhnya dan tak peran mereka layak disebut pahlawan keluarga.

  Peran pahlawan dalam hidup mereka tidak sederhana. Kadang mereka berperan sebagai ayah dan ibu sekaligus, menjadi guru, motivator, konselor, pembuat keputusan, sekaligus pelaksana keputusan. Peran-peran ini seperti sebuah peran dalam teater. Oleh sebab itu, kisah mereka akan dipanggungkan, kisah mereka akan digarap untuk menggugah perempuan-perempuan lainnya untuk berani berjuang hidup.

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Metode project ini adalah pementasan teater 11 Ibu, 11 Panggung, 11 Kisah yang akan menampilkan  11 Monolog Realis.

     Metodenya adalah kolaborasi antara saya sendiri sebagai penulis naskah dan sutradara dengan 11 karakter Ibu yang terpilih, lalu di antara kami semua saling membantu sebagai koordinator kru mewujudkan 11 pentas tersebut. Misalnya kebutuhan setting panggung bisa dikoordinatori oleh Ibu Yanti (buruh bangunan) didampingi tim artistik panggung. Kebutuhan kostum panggung bisa dikoordinatori oleh Diah Mode (desainer fashion) dan tim artistik kostum. Kebutuhan make-up bisa dikoordinatori oleh Ibu Hermawati (pemilik salon) dan tim make-up artist. Kebutuhan latihan dan persiapan aktor pementasan selain pemimpin project yang mengarahkan, bisa dibantu oleh Ibu Astuti (mantan aktor). Sedangkan musik dan artistik visual bisa dikoordinatori oleh Ibu Tini Wahyuni (pelukis dan pemusik). Tim publikasi bisa dikoordinatori oleh Ibu Erna Dewi dan pemimpin project yang punya basis massa yang besar di sosial media. Sementara yang lain bisa membantu tim tersebut. Secara umum, semua pekerjaan bisa dilakukan bersama-sama sehingga project ini ringan dan mungkin dilakukan dalam waktu yang ditentukan.


  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Selama ini perempuan malu kalau bercerita di depan publik, apalagi menyangkut persoalan domestik. Perempuan menganggap masalahnya hanyalah untuk dia sendiri menangunggnya, dan pantang berbagi aib rumah tangga kepada orang lain. Dengan metode teater, ketakutan, kecemasan dan keraguan ini bisa dikikis perlahan, sambil menguatkan perempuan bahwa persoalan yang terjadi padanya bukanlah miliknya seorang namun juga milik masyarakat, dan menjadi penting untuk didengar. Mendengar adalah awal yang baik untuk memahami persoalan, dan membantu keluar dari masalah. Setidaknya meringankan beban. Kehadiran teman baru maupun komunitas baru bisa menjadi jejaring yang baik untuk memperkuat jaringan perempuan mendengar perempuan.

  3. Strategi

        1. Membuat pementasan teater monolog 11 Ibu, 11 Panggung, 11 Kisah

        2. Membuat FGD atau diskusi terpumpun

        3. Membuat jejaring komunitas

        4. Membuat publikasi dan dokumentasi untuk menyebarkan informasi

  4. Aktivitas dan keterkaitan pada sasaran

        1. Pementasan akan menyasar komunitas, kelompok, dan aktivis perempuan yang fokus pada bidang perempuan

        2. Publikasi dan dokumentasi akan menyasar audiens yang lebih luas, yang tidak dapat hadir secara fisik ketika pementasan berlangsung

  5. Latar belakang dan demografi pelaku proyek

     Ibu-ibu yang diangkat dalam project ini adalah ibu ibu yang sudah berusia di antara 30-60 tahun yang berasal dari berbagai status sosial. Ada buruh bangunan, guru, dosen muda, guru besar/profesor, pemilik lembaga kecantikan, PNS, dan seniman. Mereka berdomisili di Buleleng dan bekerja di Buleleng.

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman berkarya di bidang seni budaya lebih kurang sepuluh tahun

  7. Demografi kelompok target

     Ibu ibu, remaja, komunitas seni budaya, aktivis perempuan, dan juga komunitas kreatif

  8. Hasil yang diharapkan & Indikator keberhasilan

        1. Pementasan berlangsung sukses

        2. Respon audiens positif

        3. Respon terhadap publikasi dan dokumentasi positif

  9. Durasi Waktu Aktivitas dilaksanakan

     Mei 2018 - Januari 2019

  10. Total kebutuhan dana

      Rp. 89.100.000,-

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp. 89.100.000,-

  12. Sumber dana lainnya

      -

  13. Kontribusi perorangan

      ide cerita, eksplorasi naskah dan eksplorasi estetik

  14. Kontribusi kelompok target

      kritik, saran, masukan, respon positif, motivasi, dorongan untuk lebih baik
