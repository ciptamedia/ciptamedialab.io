---
title: Laporan Naratif - Konser Emas Sylvia Saartje - Sylvia Saartje
date: 2019-05-06
permalink: /ciptamediaekspresi/laporan-naratif/konser-emas
layout: proposal
author: Sylvia Saartje
categories:
- laporan
- Laporan Naratif
- CME
- Pentas Kolaborasi Sylvia Saartje
---

# Laporan Naratif - Konser Emas Sylvia Saartje

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Pembelajaran yang saya dapatkan selama menjadi penanggung jawab proyek di bawah Cipta Media Ekspresi adalah selama persiapan konser saya harus koordinasi ke berbagai pihak yang terlibat secara langsung, serta memberikan gambaran besar goal yang akan dituju. Kemudian setelah konser ada kendala mengenai laporan yang cukup lama dikarenakan saya dan tim keuangan jarang bertemu.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Pembuatan video proses behind the scene, pentas konser dan after movie video konser emas Sylvia Saartje.

**Ceritakan hasil dari aktivitas tersebut.**

Aktivitas yang saya usung adalah saya memberikan suguhan konser emas dalam perjalanan karir bermusik saya yang tepat dilaksanakan pada hari ulang tahun saya yang menyajikan sebagian besar lagu ciptaan saya yang belum pernah beredar dan mengemasnya bersama musisi lintas generasi baik dari band, choir, maupun ensamble string pengiring.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Melengkapi dokumentasi musik rock di Indonesia.

**Ceritakan hasil dari mengatasi isu tersebut.**

Isu yang coba saya atasi antara lain: melengkapi dokumentasi musik rock indonesia, konser emas ini merupakan yang pertama di Indonesia untuk itu perlunya menjadikan konser ini sebagai arsip dokumentasi musik Indonesia.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Karya dapat diterima semua kalangan.
>
> 2. Bahwa konser emas ini akan menjadi pemantik dan penonton puas hingga akhir konser.
>
> 3. Target 400 – 500 penonton.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Adanya pendokumentasian berupa video behind the scene, pentas konser, dan aftermovie sehingga mampu dijadikan sebagai ajang edukasi untuk generasi muda serta menularkan virus semangat tanpa batas.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Lebih memperkenalkan lagi sosok saya sebagai penyanyi rock kepada generasi muda, lagu ciptaan pribadi dibungkus dalam balutan orkestra, menjadikan rekam jejak dokumentasi musik indonesia.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Tantangan yaitu band pengiring yang full perempuan di Malang tidak ada, serta pada pelaksanaan teknis ada kendala kurangnya SDM perempuan seperti sound engineer, lightning engineer serta video mapping crew.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Konser emas ini merupakan lagu rock dibalut dalam nuansa orkestra dengan lirik yang sesuai pengalaman pribadi yang mampu menularkan semangat kepada masyarakat. Dan terbukti setelah diadakanya Konser Emas ini banyak kegiatan yang saya ikuti antara lain Komunitas Perempuan Tour Surabaya Pasuruan serta talkshow di salah satu TV swasta Nasional.
