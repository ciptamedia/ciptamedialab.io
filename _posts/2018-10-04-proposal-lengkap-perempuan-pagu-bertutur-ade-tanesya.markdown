---
title: Proposal Lengkap - Perempuan Pagu Bertutur - Ade Tanesya
date: 2018-10-04
permalink: /ciptamediaekspresi/proposal-lengkap/perempuan-pagu-bertutur
layout: proposal
author: Ade Tanesya
categories:
- laporan
- CME
- Perempuan Pagu Bertutur
---

![0407.jpg](/static/img/hibahcme/0407.jpg){: .img-responsive .center-block }

# Ade Tanesya - Perempuan Pagu Bertutur

**Tentang penerima hibah**

Ade Tanesia adalah seorang aktivis di bidang kebudayaan yang sering menggunakan berbagai medium kreatif untuk memperkuat identitas budaya warga dan relasi antarwarga dalam ekosistem masyarakatnya. Eksplorasi medium kreatif  ini telah dilakukannya dalam beberapa proyek yaitu Papuan Voice, sebuah program pembuatan film advokasi oleh komunitas di Papua untuk menggulirkan isu-isu yang terjadi di daerahnya. Kemudian bersama Yayasan Kelola dengan program “Komunitas Kreatif,” menggunakan film sebagai cara warga untuk memetakan persoalannya dan didistribusikan ke berbagai instansi terkait. Selanjutnya bersama Center for Civic Engagement Studies, menerapkan program “CreativeNet”, yaitu memfasilitasi komunitas untuk membuat karya seni berbasis persoalan yang dihadapi warganya. CreativeNet dilakukan di sejumlah komunitas seni di Klaten dan Jogjakarta.  Ia juga aktif menulis di berbagai media dan menciptakan lagu-lagu yang berangkat dari pengalaman perjalanannya ke berbagai tempat di nusantara.  Salah satu album yang telah dibuatnya adalah “Tanah To Indai Kitai” berisi sejumlah lagu berbahasa Dayak Iban dan Indonesia dengan berkolaborasi dengan sejumlah musisi. Buku yang sudah digagasnya antara lain “Media Warga,” “Tanah To Indai Kitai (Tanah adalah Ibu Kita) mengenai perjuangan masyarakat adat Dayak Iban di Sungai Utik, Kalimantan Barat.

**Kontak**

  - Facebook: [facebook.com/ade.tanesia](https://www.facebook.com/ade.tanesia)

  - Twitter: [@adetanesia](https://twitter.com/adetanesia)

  - Instagram: [@adetanesia](https://www.instagram.com/adetanesia/)

  - Blog: [adetanesia.wordpress.com](https://adetanesia.wordpress.com/)

**Lokasi**

Desa Sosol, Kecamatan Malifut, Kab. Halmahera Utara, Propinsi Maluku Utara

**Deskripsi Proyek**

Proyek ini hendak mengangkat persoalan yang dialami oleh perempuan masyarakat adat Pagu, di Halmahera Utara. Awalnya akan dilakukan semacam pemetaan dan pendalaman kegiatan sehari-hari perempuan Pagu yang berhubungan dengan pengolahan ruang hidup yang semakin terbatas karena wilayah ini telah sejak lama menjadi daerah eksploitasi tambang emas, perusahaan Nusa Mineral Halmahera. Ruang hidup menjadi penting karena di sana pula terdapat tanaman obat, karena banyak tabib perempuan yang menjadi penyembuh bagi warga.

Cerita kehidupan perempuan Pagu ini akan diubah menjadi lirik lagu, digarap oleh seorang musisi Tobelo, dan dinyanyikan kembali oleh para perempuan. Lalu akan dibuat sebuah pementasan di kampung dimana para perempuan akan bernyanyi, menari, membaca puisi. Jadi fokus dari perjalanan ini hendak menggali kehidupan perempuan Pagu dalam hubungannya dengan ruang hidup, pangan, dan kesehatan.

- **Tujuan**

  Memperkuat kepercayaan diri perempuan pagu akan potensi yang mereka miliki serta memberikan ruang bagi perempuan pagu untuk mengekspresikan persoalannya melalui medium kreatif seperti lagu, puisi, dan lain-lain.

- **Sasaran**

  Menghasilkan lagu/nyanyian dan puisi yang merefleksikan persoalan perempuan Pagu.

- **Latar Belakang**

  Gagasan proyek ini merupakan lanjutan dari penggalian sejarah asal usul Suku Pagu yang sudah dilakukan pada tahun 2018. Pada kunjungan pertama itu, saya melihat bahwa perempuan Pagu cukup kuat, dipimpin oleh seorang Kepala Suku Perempuan. Ia membentuk kelompok petani perempuan untuk membangun ketahanan pangan pada keluarga. Adanya tambang emas sejak tahun 1990-an juga mengurangi luasan hutan yang menyebabkan mereka kehilangan sumber alam hutan, dan kini bercocok tanam semakin terbatas yaitu di kebunnya sendiri. Suku Pagu juga mengalami keterdesakan dari program pemerintah, yaitu transmigrasi lokal dari Pulau Makian yang kemudian menggeser pemukiman dan kebun mereka. Setelah ruang hidupnya digeser, mereka pun diberi stigma "primitif". Artinya ada dua hal yang terjadi, yaitu hilangnya ruang hidup dan masalah identitas. Bagaimana keterdesakan ruang hidup ini mempengaruhi kehidupan warga, dan bagaimana perempuan bertahan dengan seluruh perubahan-perubahan tersebut.

  1. Keterkaitan pada kategori: Perjalanan

     Proyek ini masuk dalam kategori perjalanan karena akan dilakukan perjalanan ke Maluku Utara untuk melakukan observasi dan wawancara pada beberapa sumber yang kemudian digubah menjadi sebuah karya musik.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Perempuan adalah kelompok yang paling terkena dampak dari  konflik, baik yang berkenaan dengan penyempitan ruang hidup maupun kerentanan antar kelompok etnis dan agama. Semua hal ini pernah terjadi di Desa Sosol, Kecamatan Malifut, Kabupaten Halmahera Utara.  Penyempitan ruang hidup berdampak pada semakin sulitnya memperoleh bahan pangan langsung dari alam sehingga segala sesuatu harus dibeli. Lalu kerentanan soal etnis dan agama menyebabkan trauma dan rasa takut yang sangat berpengaruh pada anak-anaknya.  Mereka memang telah melalui titik krusial dari kerentanan ini, dan tetap ada potensi yang sebenarnya bisa diolah dari  kelompok perempuan, yaitu kemampuan mengobati dan persalinan, berkebun. Hal ini perlu disadari dan diyakini mereka sebagai potensi.

  3. Strategi

     Untuk memperkuatnya maka dicari medium kreatif yang bisa menginternalisasi kemampuan mereka. Oleh karena para perempuan adalah penyanyi handal di gereja, maka menyanyi adalah salah satu medium yang memungkinkan mereka selalu mengingat apa yang telah hilang dan yang masih mereka miliki sehingga harus dipertahankan.

  4. Aktivitas dan keterkaitan pada sasaran

     Melakukan wawancara dengan beberapa perempuan yang memiliki kemampuan mengobati dan persalinan, dan yang masih tetap berkebun. Kemudian membuat diskusi terbatas untuk memetakan ruang hidup dan persoalan yang mereka hadapi sehari-hari.  Berdasarkan hasil riset ini maka dituliskan dalam bentuk syair atau puisi yang bisa mereka nyanyikan atau bacakan.  Melalui medium ini maka mereka sebenarnya sedang menceritakan dirinya sendiri.  Lalu catatan ini akan dipresentasikan kembali dalam bentuk pertunjukan sederhana di kampung.

  5. Latar belakang dan demografi pelaku proyek

     Adapun yang terlibat dalam proyek ini adalah seorang musisi dari Tobelo. Ia berprofesi sebagai guru musik dan memiliki sanggar di Tobelo.  Perempuan Pagu yang akan terlibat adalah Kepala Suku yang seorang guru, ibu rumah tangga usia 50 tahun yang berprofesi sebagai bidan, penyembuh tradisional, perempuan pemain instrumen musik, dan ibu rumah tangga biasa lainnya.

     Pemimpin proyek adalah perempuan aktivis kebudayaan dengan pengalaman menggunakan berbagai medium kreatif untuk memperkuat identitas budaya warga dan relasi antar warga dalam ekosistem masyarakatnya.

  6. Demografi kelompok target

     Kelompok target yang hendak disasar adalah :

        1. Perempuan Pagu dari berbagai desa , usia antara 25-40 tahun

        2. Generasi muda perempuan Pagu, usia antara 15-25 tahun

        3. Warga Desa Sosol secara umum , baik pria maupun wanita

        4. Masyarakat Halmahera Utara secara umum

  7. Hasil yang diharapkan dan Indikator keberhasilan

        - Perempuan Pagu dapat mengekspresikan persoalannya melalui talenta yang dimilikinya, dalam hal ini melalui nyanyian.

        - Perempuan Pagu semakin memiliki kepercayaan diri untuk mengemukakan pendapatnya dalam forum publik

        - Perempuan Pagu semakin sadar akan persoalan yang dihadapinya seputar ruang hidup dan mau menjaga apa yang dimilikinya.


  8. Durasi Waktu Aktivitas dilaksanakan

     3  Bulan

  9. Total kebutuhan dana

     Rp. 90.000.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp. 90.000.000

  11. Sumber dana lainnya

      Tidak ada

  12. Kontribusi perorangan

      Tidak ada

  13. Kontribusi kelompok target

      Partisipasi dalam penyelenggaraan pementasan
