---
title: Proposal Lengkap - Bakureh Project - Delva Rahman
date: 2018-11-13
permalink: /ciptamediaekspresi/proposal-lengkap/bakureh-project
layout: proposal
author: Delva Rahman
categories:
- laporan
- CME
- Bakureh Project
---

![0214.jpg](/static/img/hibahcme/0214.jpg){: .img-responsive .center-block }

# Delva Rahman - Bakureh Project

**Tentang penerima hibah**

Delva Rahman adalah salah satu pegiat media di Komunitas Gubuak Kopi (Solok) ia juga aktif menari di Ayeq Mapletone Company, sebuah kelompok tari yang berdomisili di Padang, Sumatera Barat. Pernah terlibat dalam lokakarya literasi media "Di Rantau Awak Se" oleh Gubuak Kopi dan Forum Lenteng (2017). Partisipan lokakarya di video performance bersama Oliver Husain di Gubuak Kopi (2017). Pernah terlibat dalam pertunjukan "Perempuan Membaca Kartini, karya sutradara" Irawita Paseban di Gudang Sarina Ekosistem (2017). Fasilitator program Daur Subur (2017-2018). Selain itu juga diundang sebagai narasumber di beberapa festival film, seperti Malang Festival Film (2017), dan Andalas Film Exibition (2017), ia juga merupakan alumna akademi ARKIPEL (2018), dan kurator film di AFFEST (2018). Juga salah satu fasilitator seniman kolaborasi Lapuak-Lapuak Dikajangi (2018).

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Solok, Sumatera Barat

**Deskripsi Proyek**

Bakureh Project adalah kolaborasi riset lintas disiplin dan pameran multimedia. Proyek ini melibatkan 6 orang kolaborator/partisipan perempuan generasi kini, serta ibu-ibu yang aktif dalam tradisi Bakureh, selaku narasumber maupun subjek kolaborasi. Partisipan merupakan individu dari beragam disiplin dan latar belakang pendidikan, dan memiliki ketertarikan pada isu-isu terkait media, perempuan, serta kebudayaan lokal.

- **Tujuan**

  Proyek ini secara khusus membaca dan mengembangkan posisi Bakureh sebagai kekuatan sosial dan media kreatif lokal dengan tetap sadar akan sejarah, tradisi, dan perkembangan kontemporernya.

- **Sasaran**

     - Mengadakan lokakarya, terdiri dari rangkaian kuliah umum, dan pelatihan media, selama 7 hari.

     - Mengadakan riset lapangan yang perkembangannya dipresentasikan setiap minggunya dalam bentuk Focus Group Discussion (FGD), sebanyak 5 kali dalam 2 bulan.

     - Memproduksi karya teks, visual, audio-visual, dan performance, yang nanti akan dipresentasikan dalam bentuk pameran multimedia. 

     - Memproduksi buku yang merangkum proses dan bingkaian isu.

- **Latar Belakang**

  Bakureh adalah salah satu tradisi gotong royong di Solok, negeri yang termasuk dalam wilayah budaya Minangkabau dengan sistem kekerabatan matrilineal. Bakureh dalam Bahasa Indonesia memiliki definisi yang tidak jauh berbeda dengan ‘arisan masak’ oleh ibu-ibu dalam satu kampung. Ia hadir dalam konteks pesta nagari (kampung), seperti pernikahan, pengangkatan pimpinan adat di tingkat nagari, upacara kematian, perayaan panen, dan lainnya.

  Tradisi ini memungkinkan terjadinya pertemuan sejumlah perempuan mewakili keluarga untuk memasak bersama. Proses ini melanggengkan sejumlah adab yang sudah tertata menjadi tradisi, mulai dari cara ia dikabarkan, pilihan menu berdasarkan kegiatan, dan pendidikan kuliner. Dalam pesta adat jamuan makan menjadi salah satu wibawa, dalam upacara kematian orang-orang di Koto Baru akan memasak lamang, ketika hendak meminang di Padang Sibusuk orang membuat nasi lamak, dan beragam adab spesifik lainnya, yang juga diperkaya dengan pemahaman filosofis. 

  Dalam konteks tertentu Bakureh juga bisa kita lihat sebagai kekuatan sosial.  Pada tradisi ini pertemuan juga tidak hanya memungkinkan perkenalan, tetapi juga penyebaran informasi, yang tidak jarang bersifat gosip/hoax. Di beberapa momen ia didiskusikan dan tidak menimbulkan dampak buruk dalam keseharian. Menarik mengangkat persoalan ini menyadari situasi bermedia di Solok yang ramai dan sensitif. 

  Bakureh Sebagai Media Alternatif Lokal

  Beberapa bulan lalu, sempat terjadi persekusi pada seorang dokter perempuan oleh sejumlah kelompok yang mengatasnamakan agama, akibat komentar si dokter di media sosial. Tidak sedikit yang juga merendahkannya sebagai perempuan. Ketakutan ia rasakan hingga ke keseharian, dan ia juga merasa orang di sekitar tidak ada yang menolong.

  Sejumlah orang di generasi saya, di Solok, tidak mendapat pendidikan banyak soal tradisi ini, baik itu dari sekolah maupun dari rumah. Hal ini tentu berkaitan dengan perkembangan situasi sosial di Kota Solok. Perkembangan ini di antaranya mengantarkan kita pada situasi yang ingin serba cepat/instan dan cenderung individual. Proyek ini secara khusus membaca dan mengembangkan posisi Bakureh sebagai kekuatan sosial dan media kreatif lokal dengan tetap sadar akan sejarah, tradisi, dan perkembangan kontemporernya.

  1. Keterkaitan pada kategori: Riset/Kajian/Kuratorial

     Kategori yang dipilih adalah riset/kajian/kuratorial karena proyek ini merupakan riset dan penelitian terkait isu media dan perempuan pada tradisi Bakureh di Solok, melalui kolaborasi seni lintas disiplin dan pameran multimedia.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Persoalan yang menjadi fokus proyek ini, antara lain bagaimana kita mewakili generasi kini melihat tradisi Bakureh, melalui kerangka kerja kesenian dengan tetap sadar akan kearifan lokal, tradisi, dan perkembangan kontemporernya.

  3. Strategi

     Mengadakan pertemuan antara partisipan perempuan generasi kini dan ibu-ibu yang aktif dalam tradisi Bakureh yang tidak hanya memungkinkan perkenalan, tetapi juga penyebaran informasi.

  4. Aktivitas dan keterkaitan pada sasaran

     Proyek ini terdiri dari tiga tahapan kegiatan, pertama: lokakarya, terdiri dari rangkaian kuliah umum, dan pelatihan media, selama 7 hari. Diikuti oleh 6 orang partisipan, dan menghadirkan 5 narasumber. Lokakarya memfokuskan pembahasan terkait: literasi media (sejarah dan perkembangan media), tradisi Bakureh dalam konteks kekinian (keterkaitan tradisi Bakureh dan perkembangan terkininya), pelatihan media (menulis, video, dan performance), serta pembahasan rencana pengembangan proyek. 
     
     Tahapan kedua, partisipan melakukan riset lapangan, serta bertemu dengan pelaku tradisi Bakureh. Perkembangan riset dipresentasikan setiap minggunya dalam bentuk Focus Group Discussion (FGD), sebanyak 5 kali dalam 2 bulan. Presentasi ini juga bertujuan menemukan benang merah kuratorial, sebagai isu sentral dalam produksi. Ketiga, meliputi produksi karya teks, visual, audio-visual, dan performance, yang nanti akan dipresentasikan dalam bentuk pameran multimedia. Selain itu, proyek ini juga merangkum proses dan bingkaian isu dalam bentuk buku.

  5. Latar belakang dan demografi pelaku proyek

     Pelaku proyek adalah perempuan pegiat media yang juga aktif menjadi narasumber di beberapa festival film.

  6. Demografi kelompok target

     Individu dari beragam disiplin dan latar belakang pendidikan, dan memiliki ketertarikan pada isu-isu terkait media, perempuan, serta kebudayaan lokal.

  7. Hasil yang diharapkan & Indikator keberhasilan

        1. terdapatnya minimal 10 video pendek dan 200 foto yang mencitrakan aktivitas terkait tradisi Bakureh. Arsip-arsip ini nantinya akan didistribusikan secara gratis untuk kepentingan pendidikan;

        2. terdapatnya minimal 6 karya tulis yang membingkai isu terkait media, tradisi, dan perempuan dengan Bakureh sebagai objek pemancingnya;

        3. terdapatnya 5 rangkaian FGD yang membicarakan persoalan perempuan, media, dan tradisi;

        4. terdapatnya pameran multimedia sebagai ruang apresiasi dan dialog publik merespon isu bermedia dan perempuan melalui tradisi Bakureh, dengan tetap sadar akan sejarah dan perkembangan terkininya.

        5. Terdokumentasikannya pengetahuan terkait proses penelitian dan bingkaian isu media, perempuan, dan tradisi lokal dalam bentuk buku.

  8. Durasi Waktu Aktivitas dilaksanakan

     3 Bulan (Juni-September 2018)

  9. Total kebutuhan dana

     Rp100.000.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp100.000.000

  11. Sumber dana lainnya

      Tidak ada

  12. Kontribusi perorangan

      -

  13. Kontribusi kelompok target

      -
