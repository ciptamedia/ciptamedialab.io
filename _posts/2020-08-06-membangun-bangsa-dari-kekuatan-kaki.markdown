---
title: Membangun Bangsa Dari Kekuatan Kaki
date: 2020-08-06
categories:
  - CME
  - Kajian
  - 'Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai'
tags:
  - CME
  - Korowai
  - Papua
  - Suku terasing
  - Antropologi
author: Rhidian Yasminta Wasaraka
comments: true
img: /uploads/blog/oche_asisten_chef.jpeg
---

# Membangun Bangsa Dari Kekuatan Kaki

## Kisah Oche dan Boas Dambol
![oche_asisten_chef](/uploads/blog/oche_asisten_chef.jpeg)
Pagi itu udara hutan masih lagi dingin berkabut, sebagian penghuni hutan sudah mulai mengoceh menyambut fajar, beberapa dari kami masih menggelung didalam selimut yang hangat. Tapi aroma kopi dan roti yang tengah dipanggang menyeruak mengoda hati, memanggil saya untuk segera mendekati bevak dapur. Disana dua ada dua sosok yang tengah asik menyiapkan sarapan pagi ala hotel bintang lima. Kopi, teh, jus buah, roti yang baru dipanggang dan lain sebagainya. Siap tersaji di meja. Sosok Oche dan Boas Dambol adalah assistant chef yang handal, di tengah pedalaman rimba Korowai-Papua, kepiayaian keduanya sangatlah luar biasa dan terkenal dikalangan para turis manca negara, tak kurang beberapa pesohor dunia yang pernah berkunjung ke Korowai, pernah merasakan pelayanan mereka. Jika memasak dilakukan chef internasional di kota dengan fasilitas dan bahan serba ada, maka Oche dan Boas “dididik” oleh Papua Jungle Chef untuk memanfaatkan bahan dan alat yang ada tanpa menurunkan standart dan kualitas layanan.


## Kisah Yunus; Bisa karena biasa
![yunus_rayhan_ayah.jpg](/uploads/blog/yunus_rayhan_ayah.jpg)
Lain kisah Oche dan Boas lain pula kisah Yunus, Yunus adalah pemuda dari pegunungan yang kemudian menempuh pendidikan di salah satu universitas negeri di kota Manokwari, Papua barat. Saya berjumpa dengannya ketika saya berlibur kerumah orang tua saya, ayah saya lalu bercerita awalnya bagaimana Yunus akhirnya bisa tinggal dirumah kami. Pertama sekali, dia datang karena protes nilai, menurutnya sudah dua kali dia mengulang matakuliah yang diketuai oleh ayah saya, tetapi selalu tidak lulus,  dengan nada putus asa Yunus  berkata
“ bapa kasih saya tugas babat rumput ka, bersihkan halaman ka apa ka…yang penting kasih saya nilai standart saja buat lulus” . 
Ayah saya menggeleng. “ Yunus, saya ini pendidik bukan pengajar, bagi saya bukan hanya soal nilai, tapi kau harus benar paham yang kau pelajari “. Yunus coba menawar tetapi Ayah saya berkeras tidak akan memberikan nilai dari hasil babat rumput. Semester berikutnya Yunus masuk kembali ke kelas, kali ini ayah saya menyuruhnya duduk didepan. Dan selalu harus didepan, Yunuspun diserahkan tanggung jawab sebagai ketua kelas, semua tugas dari dan untuk teman-temannya jadi tanggung jawab Yunus.  Yunus jadi terbiasa masuk cepat, pulang lambat dan selalu mengumpulkan tugas. 
“saya malu bapak, kalau saya kumpulkan teman-teman punya sedangkan saya  sendiri tidak” ujarnya .

Ketika yang lain dia datang menghadap. “bapak saya ini susah sekali membaca dan ingat nama-nama tumbuhan hutan” . Ayah saya lalu menyodorkan buku katalog nama-nama tumbuhan hutan. Dia menggeleng “saya pusing bapa kalau bapa suruh baca”. “ jadi?” tanya ayah saya kemudian . “Suruh saya lihat boleh baru saya bisa ingat”. Ayah sayapun tersenyum, “ok sudah, sekarang ko pergi ke halaman belakang, itu ada macam-macam tumbuhan, sudah dikasih nama, nanti sudah hafal yang di bapak punya rumah, ko lanjut pergi juga ke hutan tanamanan percobaan di Anggori atau di Gunung meja sana. Tidak usah buru-buru yang penting paham”. 
Diakhir semester Yunus lulus dengan nilai A, saat ini Yunus adalah tenaga ahli penggenal tumbuhan dan tanaman hutan yang sangat diandalkan pada salah satu instansi pemerintah. 

Ayah saya selalu bangga menceritakan bagaimana sosok Yunus berproses dan bilang bahwa “ tidak ada orang bodoh yang ada mungkin dia belum menemukan potensi dan cara untuk mengasah potensinya itu. Maka itulah tugas pendidik, dan untuk mendidik seseorang muliakanlah dia sebagai manusia karena pendidikan memang bertujuan mengangkat derajat seseorang”.


## Membangun dari kekuatan kaki
Jika kita bicara tentang system pendidikan maka sejatinya hal itu akan selalu mencakup tiga hal, peserta didik, tenaga pengajar dan system administrasi. Sayangnya kadangkala peserta didik dan tenaga pengajar lebih banyak disibukan dengan system administrasi yang njelimet.   
Oche, Boas dan Yunus adalah potret buram pendidikan di  Indonesia. Daerah yang terisolir, ketiadaan guru dan fasilitas belajar menjadikan mereka sulit mengenyam pendidikan dasar, apalagi mau bicara akses internet dan gawai, semua jauh dari angan-angan . Sebagaian yang lain hanya “asal lulus” saja. Karena sekolah sistemnya kejar target, takut ditegur oleh dinas jika jumlah lulusan sedikit atau bahkan ada juga ketakutan dikalangan guru menghadapi tekanan dari orangtua. 
Ada juga alasan guru dan dosen sibuk dengan segala administrasi hingga waktu habis untuk mengurusi hal-hal tersebut. Dan itu semua sudah menjadi lagu lama yang diputar berulang-ulang setiap kita bicara tentang situasi pendidikan kita. 

Tetapi pelajaran berhargapun dapat kita lihat dari kisah mereka, bagi Oche dan Boas tidak pernah menempuh pendidikan formal untuk mengasah kemampuan mereka. Mereka tidak berijasah, mereka hanya tau baca dan tulis sekedarnya.  Mereka juga tidak punya timbangan kue , untuk mereka itu tidaklah penting. Yang penting ada sendok dan sendok bisa dibuat dari pelepah sagu. Hingga mereka bisa menakar berapa perbandingan bahan yang harus dipakai saat membuat roti, mereka tak perlu bahan makanan mewah, mereka justru memanfaatkan yang ada dari hutan untuk disajikan dimeja makan para turis. 
Juga tak perlu celemek, topi dan baju chef, akan tetapi, adalah hal wajib untuk mencuci tangan, kaki dan wajah mereka dengan sabun dan air hangat sebelum mulai memasak. Semua makanan harus ditutup rapat selesai dimasak dan semua alat makan dan  masak haruslah dicuci dengan air panas sebelum dan sesudah dipakai. Mereka paham kesehatan para turis adalah yang utama dan itu tanggung jawab mereka. Oche dan Boas tidak dibatasi dengan protocol yang kaku dan administrasi yang tak penting mereka berinovasi, dengan situasi  di lapangan dan terus mengembangkan diri untuk menemukan hal-hal baru disekitar mereka, sesuai dengan bidang yang mereka sukai

Bagi Yunus, melihat langsung di lapangan adalah proses belajar yang paling penting. Sedangkan diskusi santai bersama dosen setelah kunjungan kelapangan adalah sebuah proses pengayaan, koreksi dan evaluasi. 
Sedangkan untuk Ayah saya yang seorang dosen, beliau memiliki banyak waktu luang untuk membimbing, karena Yunus datang padanya ketika ayah saya sudah ada dipuncak karier, hingga beliau tidak dipusingkan dengan segala urusan administarsi dan pemberkasan untuk kenaikan pangkat dan lain sebagainya,  tetapi yang utama adalah kesadaran diri bahwa kehadirannya sebagai pendidik adalah penting, dan bukan sekedar pengajar. Hingga yang terjadi adalah transfer pengetahuan dan bukan sekedar transfer ilmu. Ilmu menjadikan orang hanya berpegangan pada teori-teori dari text book semata, tapi pengetahuan mengajarkan seseorang untuk menjadi kreatif dan inovatif. Berani mencoba hal baru, menjelajahi setiap kemungkinan dan jalan keluar itulah fungsi motivasi yang diembankan untuk seorang pendidik. Hingga peserta didik tetap bersemangat untuk meraih apapun impian mereka. 

Hal inilah yang dibutuhkan oleh bangsa kita, bukan sekedar menghasilkan mereka yang membebek ingin jadi pegawai tapi mereka yang berorientasi untuk mengembangkan diri dan kemampuan untuk kemudian menjadi pemimpin. 
Dibagian akhir ijin kan saya untuk menutup dengan mengutip kata-kata  Ki Hajar Dewantoro, “tiap-tiap rumah adalah sekolah dan tiap-tiap orang adalah guru” karenanya seharusnya pendidikan bukan hanya menjadi tanggung jawab semua orang tapi juga memerdekakan seseorang untuk mengakses pengetahuan itu pada siapa saja dimana saja dan kapan saja.  Inilah inti dari pendidikan yang merdeka yang akan melahirkan generasi emas bangsa. semoga 


