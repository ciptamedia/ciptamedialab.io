---
title: Proposal Lengkap - Pendokumentasian Perempuan Intelektual Sunda Lasminingrat - Rena Amalika Asyari
date: 2018-12-30
permalink: /ciptamediaekspresi/proposal-lengkap/dokumentasi-lasminingrat
layout: proposal
author: Rena Amalika Asyari
categories:
- laporan
- CME
- Pendokumentasian Perempuan Intelektual Sunda Lasminingrat
---

![0529.jpg](/static/img/hibahcme/0529.jpg){: .img-responsive .center-block }

# Rena Amalika Asyari - Pendokumentasian Perempuan Intelektual Sunda Lasminingrat

**Tentang penerima hibah**

Rena Asyari, perempuan yang lahir dan besar di tanah Priangan ini menamatkan studinya di Fisika Universitas Padjadjaran dan menjalani aktivitas sehari hari sebagai pengajar. Tahun 2015 bersama salah seorang temannya, mendirikan komunitas Seratpena, yang mewadahi kegiatan baca dan tulis. Tahun 2017 Ia menerbitkan buku anak untuk pertama kalinya yaitu "Petualangan Rococo". Buku tersebut  dibuat sebagai jawaban atas minimnya bacaan anak yang berkualitas. Ia juga cukup rajin menulis di media cetak atau elektronik untuk menyuarakan kegelisahannya tentang anak, perempuan dan kota kelahirannya Jatiwangi.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Bandung, Jawa Barat

**Deskripsi Proyek**

Buku masih menjadi salah satu media penyampai pengetahuan yang efektif. Beruntung, cerita-cerita masa lalu, kisah para leluhur sempat didokumentasikan dalam penulisan buku-buku sejarah hingga kita hari ini tidak menjadi gagap dengan asal usul. Harus diakui, penulisan sejarah belum mendapat perhatian yang serius dari pemerintah ataupun masyarakat.

Masih sangat jarang sejarahwan ataupun penulis yang mengangkat hidup perempuan di masa lalu. Minimnya ruang untuk mendapat perhatian tersebut, maka melalui CME saya mengajukan diri membuat proyek penulisan buku yang mengangkat sosok perempuan Intelektual Indonesia abad-19, Lasminingrat.

Proyek ini akan fokus untuk mencari data-data mengenai Lasminingrat. Menapaki tempat-tempat yang pernah menjadi persinggahan Lasminingrat, studi literatur di berbagai perpustakaan, juga penelusuran data melalui wawancara. 
Mengapa ini penting untuk dituliskan?

Karena masih sangat sedikit perempuan yang menuliskan tentang sejarah perempuan yang memiliki perspektif feminis.

**Tujuan Penelitian**

  1. Menggali sejarah perempuan Indonesia

  2. Menyampaikan informasi sekaligus harapan dan stimulan bagi perempuan di Indonesia

  3. Memberi pandangan baru pada khalayak tentang perempuan Sunda dan kiprahnya

**Sasaran**

  1. Menulis 2 esai populer mengenai Lasminingrat dan mempublishnya di media online.

  2. Membuat buku yang mengupas kehidupan dan pemikiran Lasminingrat dengan bahasa tutur yang tidak kaku dan bisa diterima oleh kalangan milenial.

  3.   Menerbitkan dan mencetaknya di kemudian hari.

**Latar Belakang**

  1. Keterkaitan pada kategori proyek : Riset Kajian Kuratorial

     Tulisan ini akan mengkaji/membedah pemikiran Lasminingrat melalui tulisan-tulisannya. Metode riset menggunakan persfektif feminis.

  2. Masalah yang ingin diangkat dan keterkaitan dengan aktivitas

        1. Buku ini akan fokus pada penggalian pemikiran Lasminingrat sebagai perempuan. Perempuan yang berperan sebagai ibu, perempuan yang berperan sebagai istri dan perempuan yang berperan sebagai anak.

        2. Objek penelitian yang hidup hampir satu abad lalu menjadi kendala dalam proses pencarian data. Dibutuhkan waktu yang cukup lama untuk mendapatkan data yang valid. Proses perenungan, memotret, menghubungkan setiap peristiwa ketika mendapatkan realitas-realitas baru ketika di lapangan tidak bisa dilakukan dengan terburu-buru. 

  3. Strategi

     Penulisan sejarah memerlukan proses yang panjang agar karya yang dihasilkan berkualitas    tinggi serta bisa menjadi bahan referensi di kemudian hari, maka proyek ini tidak akan berhenti sekalipun tenggat waktu yang ditetapkan dari Cipta Media sudah habis.

  4. Latar Belakang dan demografi pelaku proyek

     Saya menyebutnya proyek ini adalah proyek penulisan lintas generasi, karena objek penelilti  adalah perempuan Sunda abad-19 yang akan dituliskan oleh perempuan Sunda abad-21. Kedekatan emosional sebagai sesama perempuan dan berasal dari ras yang sama akan membantu dan menggiring penelitian ini pada perspektif perempuan Sunda masa lalu dan masa kini.

  5. Demografi kelompok target

     Target Khusus: generasi milenial perempuan Sunda usia 15 – 30 tahun
 
     Target umum: semua golongan, laki-laki dan perempuan Indonesia (tidak terbatas usia)

  6. Hasil yang diharapkan dan indikator keberhasilan

     Proyek penelitian ini dikatakan berhasil jika dapat mempublish 2 esai populer, dengan waktu dan biaya yang sudah disepakati.

  8. Durasi Waktu Aktivitas dilaksanakan

     4 bulan (Oktober 2018 - Februari 2019)

  9. Total kebutuhan dana

     Rp20.000.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp20.000.000

  11. Sumber dana lainnya

      Belum ada
