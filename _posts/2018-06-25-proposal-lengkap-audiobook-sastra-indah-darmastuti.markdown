---
title: Proposal Lengkap - Audiobook Sastra Indonesia - Indah Darmastuti
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/audiobook-sastra
layout: proposal
author: Indah Darmastuti
categories:
- laporan
- CME
- Audiobook Sastra Indonesia
---

![0496.jpg](/static/img/hibahcme/0496.jpg){: .img-responsive .center-block }

# Indah Darmastuti - Audiobook Sastra Indonesia

**Tentang penerima hibah**

Audiobook Sastra Indonesia adalah sebuah kerja yang merekam pembacaan karya sastra berupa Cerpen dan Puisi dari para penulis Indonesia terkini. Karya yang sudah dialihwahanakan adalah lima cerpen dari buku kumpulan cerita “Makan Malam Bersama Dewi Gandari” Karya Indah Darmastuti, yang sudah dilakukan oleh rekan rekan Universitas Sebelas Maret. Melalui kerja ini, diharapankan bisa memperluas dan mempermudah akses terhadap sastra dengan perspektif gender yang lebih kritis dan inklusif."

  - Nama: Indah Darmastuti

  - Tempat lahir: Solo

  - Tanggal lahir: 12 Maret 1973

Riwayat Pendidikan

  - SD Pajang II Solo (1980-1986)

  - SMP Purnama Surakarta (1987-1990)

  - Kejar Paket C tahun 2002

Prestasi

  - Pemenang lomba menulis cerita anak tingkat Jawa Tengah 2005 (Juara 3)

  - Pemenang sayembara novelette Majalah Femina 2012 (Juara 2 tanpa juara 1)

  - Pemenang sayembara menulis cerita panjang komunitas Penjunan 2013 (Juara 2)

  - Diundang sebagai Emerging Writer di Ubud Writers and Readers Festival 2012

  - Diundang sebagai peserta loka karya penulisan kritis seni pertunjukan di IKJ (2016)

Karya

  - Novel “Kepompong” (Jalasutra, 2006)

  - Kumpulan Novelette “Cundamanik” (Sheila, 2012)

  - Sehimpun Cerita “Makan Malam Bersama Dewi Gandari” (BukuKatta, 2016)

**Kontak**

  - Facebook: [facebook.com/indah.darmastuti](https://www.facebook.com/indah.darmastuti)

  - Instagram: [@indah.darmastuti](https://www.instagram.com/indah.darmastuti/)

**Lokasi**

Surakarta

Lokasi peluncuran perdana

  - Hari / Tanggal: Sabtu, 27 Oktober 2018 (bertepatan dengan bulan bahasa)

  - Tempat: Sekolah Luar Biasa YKAB (Yayasan Kesejahteraan Anak-anak Buta) Jagalan, Solo

  - Pukul: 10.00 - Selesai

  - Kapasitas: 100 orang

**Deskripsi Proyek**

- **Tujuan**

  1. Tersedianya audiobook sastra Indonesia terkini.

  2. Audiobook yang dapat diakses secara gratis baik untuk difabel netra dan masyarakat umum.

  3. Adanya apresiasi terhadap karya sastra secara luas.

- **Sasaran**

  Remaja dan umum penyandang difabel netra serta para pendidik di sekolah-sekolah difabel netra, tak menutup kemungkinan untuk masyarakat luas.

- **Latar Belakang**

  Setiap individu mempunyai hak untuk memperoleh pendidikan, pengetahuan, hiburan dan informasi. Sastra adalah salah satu wilayah pemenuhan itu. Itu juga menjadi dasar pemikiran saya. Gagasan mewujudkan Audio Book ini terinspirasi dari pertemuan saya dengan seorang perempuan difabel netra pada sebuah acara bincang sastra yang diadakan oleh komunitas sastra tempat saya bergabung, di Solo.

  Saat kami berbincang, dia menyampaikan tentang minatnya pada sastra tetapi kesulitan karena terbatasnya akses. Mereka, teman-teman difabel netra sangat membutuhkan referensi audio book terutama sastra (cerpen, puisi) terkini.

  Kurangnya referensi terutama sastra yang bisa diakses gratis oleh teman-teman difabel netra itulah yang membuat saya berpendapat bahwa audio book ini memiliki urgensi. Kemudian saya memulai uji coba dengan mengalih-wahanakan beberapa cerpen saya yang terhimpun dalam buku “Makan Malam Bersama Dewi Gandari” bersama teman-teman dari Universitas Sebelas Maret.

  Lebih jauh lagi, saya berpendapat bahwa audio book juga bukan hanya sebagai upaya pemenuhan hak teman-teman difabel netra, tetapi juga untuk masyarakat secara umum. Saya membayangkan ketika dunia semakin riuh, waktu semakin mahal, sastra tetap bisa hadir dengan tenang, bersahabat dan tetap memberi sesuatu yang subtil dengan cara berbeda.

  Barangkali nanti, ketika waktu membaca buku semakin sempit, dan kertas semakin langka, masyarakat bisa memeroleh cerita atau karya fiksi melalui audio book. Mereka bisa mendengar sambil menyetir mobil, memasak, atau bahkan untuk relaksasi. Sastra tetap bisa hadir dalam segala ruang dan waktu, dan semua individu yang membutuhkannya.

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Audiobooks ini bertujuan melisankan karya tertulis dari penulis-penulis Indonesia. Saya mengkoordinir secara keseluruhan, yaitu mengumpulkan naskah cerpen dan puisi, melibatkan teman-teman difabel dan mahasiswa UNS sebagai tim pembaca yang kemudian akan kami rekam. Sebagai editor, ilustrator musik dan pelatih reading saya mengajak teman-teman dari Etnomusikologi dan teater ISI Surakarta. Untuk materi musik, saya meminta beberapa teman untuk bermain rebab, gitar, biola, kecapi, suling.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        1. Pemenuhan hak teman-teman difabel netradan masyarakat secara umum(sastra perspektif gender yang kritis dan inklusif)

        2. Kurangnya referensi terutama sastra yang bisa diakses gratis oleh teman-teman difabel

  3. Strategi

     Edukasi secara kritis akan dilakukan melalui workshop / pelatihan/diskusi bagi masyarakat sebagai langkah pertama pelaksanaan program ini. Pembelajaran ini terdiri dari beberapa issu, seperti hak asasi manusia, hak difabel, dan gerakan sosial. Upaya ini sangat berguna untuk meningkatkan kesadaran masyarakat tentang hak-hak difabel serta membuka akses untuk pengarusutamaan sastra. Hal ini juga diharapkan dapat mengidentifikasi bibit penggerak yang bersedia dan berkomitmen untuk terlibat dalam gerakan ini selain untuk memperkuat kapasitas penggerak local, khususnya peran-peran perempuan masa kini.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Pengumpulan 30 cerpen dan 50 puisi selama produksi secara bertahap

        2. Pendistribusian naskah kepada para narator dan latihan secara bertahap

        3. Perekaman pembacaan cerpen dan puisi secara bertahap

        4. Perekaman musik untuk ilustrasi naskah secara bertahap

        5. Editing naskah

        6. Pendokumentasian naskah dalam bentuk keping CD yang akan didistribusikan ke Sekolah Luar Biasa, dan digital diupload di website.

  5. Latar belakang dan demografi pelaku proyek

     Sumber daya manusia yang terlibat dalam proyek ini adalah jaringan yang peduli dengan sastra, difabel dan gerakan. Secara khusus dibutuhkan tim music, narator dan editor untuk mewujudkan audio book ini. Disisi lain masih ada tim program (organizer, finance dan admin) yang akan membantu untuk membuat karya-karya ini menjadi referensi dimasyarakat yang ramah difabel dan membuat sastra mudah diakses oleh banyak pihak.

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman dalam aktivitas bersastra bersama komunitas

  7. Demografi kelompok target

     Wilayah Solo Raya didorong untuk menjadi kota dan kabupaten ramah difabel, beberapa kabupaten berusaha untuk membuat peraturan daerah sebagai wujud nyata keberpihakan. Komunitas netra disolo menjadi sasaran utama

  8. Hasil yang diharapkan & Indikator keberhasilan

     Yang menjadi target kerja ini adalah ketika audio book sampai kepada para penyandang difabel netra dan para pendidiknya (Solo Raya Khususnya dan Indonesia pada umumnya). Audio Book ini juga diunggah secara gratis sehingga kita bisa melihat secara kuantitatif berapa banyak penggunanya.

  9. Indikator keberhasilan

        - Adanya softfile Audio book untuk netra

        - Akses audio book gratis

        - Audio book diunggah dimedia-media sosial dan web

  10. Durasi Waktu Aktivitas dilaksanakan

      6 bulan

  11. Total kebutuhan dana

      Rp 80.000.000

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      Rp 65.070.000

  13. Sumber dana lainnya

      Jaringan gerakan peduli sastra (perspektif gender yang kritis dan inklusif)

  14. Kontribusi perorangan

      Untuk peran Organizer dan Admin

  15. Kontribusi kelompok target

      Swadaya diskusi komunitas (tempat, kebersihan, konsumsi dll)
