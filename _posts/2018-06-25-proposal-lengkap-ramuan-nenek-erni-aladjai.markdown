---
title: Proposal Lengkap - Ramuan Nenek (merawat perempuan pasca bersalin secara alami) - Erni H Aladjai
date: 2018-06-25
permalink: /ciptamediaekspresi/proposal-lengkap/ramuan-nenek
layout: proposal
author: Erni H Aladjai
categories:
- laporan
- CME
- Ramuan Nenek
---

![0353.jpg](/static/img/hibahcme/0353.jpg){: .img-responsive .center-block }

# Erni H Aladjai - Ramuan Nenek (merawat perempuan pasca bersalin secara alami)

**Tentang penerima hibah**

Erni Aladjai lahir di Banggai Kepulauan 7 Juni 1985. Ia adalah putri tertua sepasang petani cengkeh di Kepulauan Labobo, Kab. Banggai Kepulauan, Sulawesi Tengah. Selain sebagai ibu rumah tangga, ia juga menulis fiksi dan meneliti lepas. Bersama suami dan ibunya, Erni mendirikan taman baca bergerak ‘Bois Pustaka’ di Desa Lipulalongo, Kecamatan Labobo, Kabupaten Banggai Laut. Sebelumnya, Erni bekerja sebagai wartawan dan editor berita di Sulawesi Tengah. Tahun 2014, dia mengikuti pelatihan kuratorial di Ruang Rupa-Jakarta. Tahun 2015, dia meneliti Destructive Fishing (pengeboman dan pembiusan ikan) dan Kehidupan Masyarakat Pesisir di Sulawesi Tengah, Pontianak, Aceh, Papua, Lombok dan Sulawesi Selatan. Pada tahun 2016, dia memperoleh hibah cipta perdamaian Yayasan Kelola dan menggelar residensi—remaja belajar kepada seniman maestro (67 tahun) untuk Seni Paupe—syair perdamaian Suku Banggai. Buku-bukunya yang sudah terbit Dari Kirara untuk Seekor Gagak (Gramedia, 2015), Kei (GagasMedia, 2013), Ning di Bawah Gerhana (BPE,2013) dan Pesan Cinta dari Hujan (Insist Press, 2010).

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Desa Lipulalongo, Kepulauan Labobo, Kabupaten Banggai Laut, Sulawesi Tengah. Desa Lipulaongo, Kecamatan Labobo terdiri dari 400 KK. Desa ini adalah pulau pegunungan dengan penduduknya bekerja sebagai nelayan, petani cengkih dan pegawai negeri sipil. Jumlah penduduknya sekitar 931 Jiwa. Dengan jumlah perempuan sekitar 486 dan laki-laki berjumlah 445. Kaum perempuan di desa ini, kebanyakan bekerja sebagai ibu rumah tangga, sebagian kecil adalah petani dan pegawai negeri sipil.

**Deskripsi Proyek**

‘Ramuan Nenek’ adalah proyek Kajian Etnobotani Medis yang mengangkat perawatan alami perempuan pasca persalinan di Kepulauan Labobo, Kabupaten Banggai Laut, Sulawesi Tengah. Perawatan ini memiliki tata cara khas dan ramuan alami yang diwariskan oleh nenek saya kepada ibu saya untuk merawat kesehatan pasca persalinan. Dalam literasi kesehatan pada umumnya yang terbuka untuk diakses misalnya melalui internet, kita banyak menjumpai tentang menjaga kesehatan selama hamil, pola makan wanita hamil, namun tak banyak literasi yang secara spesifik membahas tentang perawatan tubuh perempuan setelah melahirkan dalam konteks perawatan alami/kebudayaan. Padahal menurut saya, masa setelah persalinan adalah bagian yang sama gentingnya dengan melalui masa mengandung. Tanpa adanya perawatan yang baik pada tubuh perempuan baik secara psikologi dan fisik setelah persalinan, perempuan bisa saja mengalami depresi, gangguan kesehatan dan lain-lain. Proyek ini akan meneliti secara ilmiah warisan pengetahuan tersebut dan bagaimana ia dipraktekkan dalam kehidupan berkeluarga.

Adapun gambaran ramuan perawatan pasca persalinan dalam keluarga saya adalah sebagai berikut:

  1. Ramuan pembersih darah nifas

     Terdiri dari daun kokoa, daun dingin, daun alos mete, daun luwean (kelincahan). Racikannya: setiap jenis daun diambil tiga helai lalu dimasak dengan air sumur. Tidak disarankan menggunakan air ledeng karena mengandung kaporit. Cara konsumsi; Dijadikan air minum untuk ibu yang baru saja melahirkan.

  2. Dadake (Penguat badan perempuan bersalin)

     Dadake adalah ritual menguatkan tubuh perempuan. Orang-orang di kampung saya sering berkata, ketika perempuan habis melahirkan ada banyak urat yang putus di tubuhnya. Tujuan dari Dadake adalah mengembalikan kekuatan tubuh perempuan.

     Caranya dengan menggunakan Daun Patidung (Saya tidak tahu bahasa umumnya, tapi pohon daun ini banyak di desa saya dan tumbuh di pesisir, daunnya lebar dan panjang serta memiliki buah kecil- kecil sebesar kelereng). Daun ini dimasukkan pasir kemudian diikat, lalu dibungkus dengan sehelai kain lalu didiangkan di atas api hingga agak panas lalu ditekan-tekan di tangan, punggung, perut, kaki perempuan yang baru saja habis melahirkan.

  3. Ramuan Ba’uap (Bakukus Badan)

     Ramuan ini berguna mengembalikan stamina, menyegarkan dan meringankan tubuh perempuan. Ramuannya: Daun Pisang Kuning, Daun cengkeh, Daun Pandan, Daun Sereh, Daun Pakundalang Betina dan Daun Luwean (Daun Kelincahan) dimasak.

     Catatan: Daun Pakundalang Betina berbatang putih, sementara Daun Pakundalang Jantan berbatang merah. Daun Pakundalang Jantan jika direbus dan diminum ini menjadi KB Alami. Lalu perempuan bisa telanjang atau hanya menggunakan sarung hingga menutup dada berada di dalam lingkaran tikar pandan, di dalamnya ditaruh panci yang berisi ramuan kukusan tersebut. 5 Lingkaran tikar pandan ini ditutup dengan selembar kain, sehingga uap ramuan hanya menguapi tubuh perempuan.

  4. Kuah Daun Losom pelancar asi

     Daun Losom menjadi bumbu daun yang dimasukkan ke dalam Sup Ikan atau di Sup sayur pepaya muda, menu ini menjadi wajib dalam setiap waktu makan perempuan yang baru saja melahirkan. Daun Losom ini rasanya asam segar.

- **Tujuan**

  1. Berbagi pengetahuan tradisional perawatan perempuan pasca bersalin.

  2. Ramuan dan tahap perawatan tradisional seperti ini tak banyak lagi diketahui generasi muda, dan ketakutan saya, pengetahuan semacam ini akan terputus di generasi tertentu, sementara perawatan alami seperti ini, bagi saya lebih ramah ke tubuh perempuan pasca melahirkan (ibu menyusui).

  3. Pengetahuan kesehatan hak semua orang, dan pengetahuan seperti ini mudah diakses masyarakat, khususnya di pedesaan, dengan sumber daya yang murah dan ramah.

- **Sasaran**

  Mendokumentasikan dan menulis kembali memoar resep ramuan mendiang nenek dalam merawat perempuan pasca melahirkan secara alami perempuan Suku Banggai di Kepulauan Labobo,  Kab.  Banggai Laut,  Sulawesi Tengah.  Menyelesaikan proyek berupa buku digital yang berisi fase perawatan perempuan pasca melahirkan.  Menyebar buku digital tersebut melalui kanal-kanal di internet agar bisa diunduh gratis.

- **Latar Belakang**

  1. Keterkaitan pada kategori: Kolaborasi

     Keinginan untuk riset, mendokumentasikan dan mengarsipkan perentahuan warisan perawatan perempuan pasca persalinan dari nenek bermula dari cerita kawan saya, yang meminum obat-obatan kimiawi untuk memulihkan tubuhnya pasca persalinan. Tujuannya meminum obat-obatan kimiawi tersebut agar tubuhnya cepat kuat sehingga ia mampu mengerjakan pekerjaan rumah, dampak dari itu, luka operasi persalinannya memang lekas mengering, namun air susunya juga ikut mengering, sehingga kawan saya ini tak bisa menyusui anaknya. Pada Desember 2017, ketika saya melahirkan putra pertama saya secara alami, saya melakukan perawatan meminum ramuan dari dedaunan yang diambilkan ibu saya di hutan—di kampung halaman saya di Kepulauan Labobo, Kab. Banggai Laut, Sulawesi Tengah, saya kembali terpikirkan perkara pengarsipan resep ramuan tersebut, sebab di Kepulauan Labobo, merawat tubuh perempuan dengan alami tak banyak lagi dipraktekkan, sementara itu, menurut saya perawatan dengan tetumbuhan jauh lebih ramah tubuh perempuan khususnya ibu menyusui.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        1. Mengatasi ketergantungan metode berobat dengan produk-produk kimiawi

        2. Kaum perempuan memiliki alternatif merawat tubuhnya sendiri dengan metode alami 3. Warisan pengetahuan perawatan perempuan secara alami kepada generasi muda

  3. Strategi

     Pendekatan ngobrol lepas,  diskusi dan dialog dengan ibu saya mengenai perawatan yang diajarkan nenek saya kepada ibu saya.  Dan mewawancarai sejumlah perempuan yang mengalami fase perawatan alami berupa : minum ramuan,  dadake dan pilalang juga bakukus.

  4. Aktivitas dan keterkaitan pada sasaran

     Aktivitas untuk sasaran A : Menggali kembali pengetahuan perawatan tradisional perempuan suku Banggai di masa lalu yang dikuasai mendiang nenek saya.

     Aktivitas untuk sasaran B : Pengetahuan yang diturunkan nenek saya kepada ibu saya didemokan melalui workshop rumahan kepada 14 remaja dan perempuan di Desa Lipulalongo Kepulauan Labobo Kab Banggai Laut,  agar pengetahuan ini didengar dan dilihat kembali.

     Aktivitas untuk Sasaran C : Melakukan riset sederhana dengan mendatangi sejumlah perempuan usia 50-an hingga 100 tahun di desa saya untuk menggali pengetahuan perawatan tradisional suku Banggai pada mereka pasca melahirkan.

     Aktivitas Sasaran D : Menulis hasil riset yang saya peroleh kemudian membukukannya secara digital. Buku digital perawatan tradisional  pasca melahirkan perempuan suku Banggai akan berisi foto dan ilustrasi.

  5. Latar belakang dan demografi pelaku proyek

     Herbalist

     Mardia Abd Karim (57 tahun), beliau adalah seorang petani, ibu rumah tangga dan pengelola taman bacaan di Desa Lipulalongo, Kab. Banggai Laut, Sulawesi Tengah. Ibu Mardia mendapatkan pengetahuan ramuan merawat perempuan pasca persalinan dari mendiang ibunya. Di desanya, beliau kerap dipanggil oleh perempuan yang baru habis melahirkan untuk membersihkan lanugo bayi mereka—biasanya lanugo (rambut halus) belum rontok semuanya di dalam kandungan, sehingga ketika bayi yang terlahir dengan lanugo yang masih tersisa acapkali rewel dan susah tidur.

     Peneliti Botani

     Ihsan Arham lahir di Sulawesi Barat 8 April 1989. Irham saat ini adalah mahasiswa pasca sarjana Institut Pertanian Bogor, jurusan Pengelolaan Sumber Daya Alam dan Lingkungan. Pendidikan S1- nya ditempuh di Jurusan Agronomi Universitas Hasanuddin, Makassar. Selain itu Ihsan juga aktif di Sekolah Riset Sosial.

     Penulis dan Fotografer

     Windy Ariestanty adalah seorang editor, traveler, blogger, fotografer dan penulis. Windy telah banyak melakukan perjalanan ke desa-desa di Indonesia Timur. Saat ini Windy bergiat di Writing Table. Selain menulis catatan perjalanan, Windy kerap terlibat dalam penelitian budaya di sejumlah daerah seperti NTT dan Papua.

     Ilustrator

     Lala Bohang adalah ilustrator sekaligus stylist. Lulusan Universitas Parahyangan ini memiliki hobi membaca, jogging, dan hunting kuas. Ketika balita, ilustrator kelahiran Makassar ini senang menggambar hal-hal sederhana dari lingkungan sekitarnya seperti ikan dan mesjid. Lala belajar menggunakan pensil dan cat air dari Almarhum ayahnya yang merupakan seorang aristek. Lala mengagumi seniwati seperti Frida Kahlo dan Sofia Coppola.

     Pemimpin Proyek

     Erni Aladjai lahir di Banggai Kepulauan 7 Juni 1985. Ia adalah putri tertua sepasang petani cengkeh di Kepulauan Labobo, Kab. Banggai Kepulauan, Sulawesi Tengah. Selain sebagai ibu rumah tangga, ia juga menulis fiksi dan meneliti lepas. Bersama suami dan ibunya, Erni mendirikan taman baca bergerak ‘Bois Pustaka’ di Desa Lipulalongo, Kecamatan Labobo, Kabupaten Banggai Laut. Sebelumnya, Erni bekerja sebagai wartawan dan editor berita di Sulawesi Tengah. Tahun 2014, dia mengikuti pelatihan kuratorial di Ruang Rupa-Jakarta. Tahun 2015, dia meneliti Destructive Fishing (pengeboman dan pembiusan ikan) dan Kehidupan Masyarakat Pesisir di Sulawesi Tengah, Pontianak, Aceh, Papua, Lombok dan Sulawesi Selatan. Pada tahun 2016, dia memperoleh hibah cipta perdamaian Yayasan Kelola dan menggelar residensi—remaja belajar kepada seniman maestro (67 tahun) untuk Seni Paupe—syair perdamaian Suku Banggai. Buku-bukunya yang sudah terbit Dari Kirara untuk Seekor Gagak (Gramedia, 2015), Kei (GagasMedia, 2013), Ning di Bawah Gerhana (BPE,2013) dan Pesan Cinta dari Hujan (Insist Press, 2010).


  6. Demografi kelompok target

     Masyarakat umum, kaum perempuan dan remaja perempuan di Kepulauan Labobo. Praktik perawatan alami dengan meracik sendiri tumbuh-tumbuhan yang bersumber dari hutan di Kepulauan Labobo, tak banyak lagi dilakukan kaum perempuan di Kepulauan Labobo, padahal warisan ini dahulu dipraktikkan oleh kaum perempuan di sana. Menurut Ibu Mardia (Herbalis), sejak tahun 2000-an, terjadi pergeseran, kaum perempuan lebih memilih menggunakan obat-obatan generik, atau salep yang mereka lihat iklannya di TV atau jamu-jamu kemasan yang mereka dengar dari mulut ke mulut.

  7. Hasil yang diharapkan & Indikator keberhasilan

     Pendokumentasian dalam bentuk buku perawatan perempuan pasca melahirkan secara alami, akan disebar di taman-taman baca, perpustakaan desa dan perpustakaan sekolah di Kepulauan Labobo yang terdiri dari delapan desa, diharapkan dengan begitu, pengetahuan ini bisa diakses kemudian dipraktekkan kembali.

  8. Durasi Waktu Aktivitas dilaksanakan

     8 bulan (Juli 2018- Desember 2018)

  9. Total kebutuhan dana

     78 Juta

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      78 Juta

  11. Sumber dana lainnya

      Tidak ada

  12. Kontribusi perorangan

         1. Mardia Abd Karim adalah narasumber dalam proyek ini yang bertugas mengajarkan, menginformasikan tumbuh-tumbuhan botani di Desa Lipulalongo, Kec Labobo yang digunakan sebagai ramuan. Dia juga akan memperlihatkan cara meracik ramuan perawatan perempuan pasca persalinan tersebut dan bagaimana ia dipraktikkan.

         2. Erni Aladjai adalah pengaju hibah yang memimpin proyek ini, melakukan koordinasi pada semua tim. Erni juga akan terlibat dalam proses riset dari konteks kebudayaan, kemudian menuliskannya secara naratif.

         3. Windy Ariestanty bertugas memotret dan mengedit naskah buku.

         4. Lala Bohang, sebagai seniman ilustrasi, Lala menangkap ide dari proses riset selama di Kepulauan Labobo, kemudian menuangkannya dalam ruang ilustrasi yang bercerita dengan ciri khas ilustrasinya sendiri.

         5. Ahsan Irham bertugas meneliti tumbuhan yang digunakan sebagai ramuan perawatan pasca persalinan di Kepulauan Labobo. Dia juga akan menguji kandungan senyawa kimia dari tumbuh-tumbuhan tersebut di laboratorium, kemudian menuliskan laporannya mengenai keterkaitan pengetahuan ilmiah dan pengetahuan tradisional merawat tubuh pasca bersalin.


  13. Kontribusi kelompok target

      Target adalah kelompok tujuh ibu dan delapan remaja putri di Desa Lipulalongo, Kec Labobo yang akan melihat dan terlibat dalam proses meramu dan melihat proses perawatan dipraktikkan pada ibu pasca persalinan, diharapkan melalui pengalaman langsung mereka melihat, ke depannya mereka akan melakukan dan membagi pengetahuan ini pada orang lain.
