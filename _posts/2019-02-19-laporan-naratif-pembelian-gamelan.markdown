---
title: Laporan Naratif - Pembelian Gamelan - Endah Fitriana
date: 2019-02-19
permalink: /ciptamediaekspresi/laporan-naratif/pembelian-gamelan
layout: proposal
author: Endah Fitriana
categories:
- laporan
- Laporan Naratif
- CME
- Pembelian Gamelan
---

# Laporan Naratif - Pembelian Gamelan

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Saya merasakan tanggungjawab yang lumayan berat yang harus saya selesaikan dalam pembelian gamelan ini. Karena dalam proyek ini saya menanggung keberlanjutan "Pasinaon" anak-anak Sedulur Sikep dalam sinau nembang dengan sarana gamelan untuk menjembatani anak-anak menyukai sinau nembang, demi keberlangsungan transformasi ajaran hidup  Sedulur Sikep pada anak dan cucu mereka.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> Aktivitas Macapatan dapat dilakukan dengan baik dan perpindahan pengetahuan lintas generasi dapat terjadi.

**Ceritakan hasil dari aktivitas tersebut.**

Lantaran gamelan, anak-anak menjadi suka belajar nembang. Semula hanya nembang saja yang diajarkan oleh orang tua mereka tanpa ada gamelan. Mungkin, timbul rasa jenuh. Dengan sarana gamelan, anak-anak menjadi senang sinau. Nabuh gamelan kemudian diselingi nembang, ternyata bisa membangkitkan semangat mereka dan mengusir rasa jenuh sinau nembang karena ada warna lain dalam pasinaon. Dengan cara ini (nabuh gamelan yang didalamnya ada nembang), akhirnya perpindahan pengetahuan lintas generasi bisa terjadi. Ajaran Sikep yang disampaikan para orang tua dalam bentuk tembang macapat bisa diterima dengan senang oleh para anak cucu, karena anak cucu yang tergabung dalam pasinaon ini bisa mengikuti nembang ajaran Sikep yang disampaikan oleh para orang tua mereka.

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> Aktivitas Macapatan tidak menarik untuk dilakukan karena gamelan sebagai infrastruktur pendukung rusak parah.

**Ceritakan hasil dari mengatasi isu tersebut.**

Laras gamelan sudah rusak. Bukan hanya larasnya tetapi fisik gamelan dan rancakannya (penyangga gamelan yang terbuat dari kayu) juga rusak parah. Hal ini menjadikan suara gamelan yang terdengar, tidak pas nadanya. Suatu saat ketika pasinaon berlangsung dan belum mendapatkan hibah dari CME seorang peserta pasinaon ketika kuminta untuk mengawali nabuh gamelan berkata," Budhe, gamelannya tidak enak didengar, suaranya tidak pas". Dalam perjalanan pasinaon mereka sudah bisa membedakan bahwa suara gamelan tidak pas, artinya nada yang dihasilkan oleh tabuhan gamelan tidak flat. Dengan hati pilu kujawab, "Semoga suatu saat nanti ada yang berbaik hati pada kita, memberi gamelan yang larasnya bagus sehingga kalian bisa sinau dengan baik. Jadi kalian akan tetap semangat sinau dan mengajak saudara-saudara kalian yang saat ini belum bisa ikut sinau". Dan ketika kukabarkan tentang hibah gamelan, luar biasa gembiranya mereka. Bahkkan saat selamatan pada waktu gamelan datang, mereka menangis, saling berangkulan, terharu karena apa yang mereka butuhkan untuk sarana sinau tercukupkan dengan adanya hibah pembelian gamelan.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> Endah berharap anak anak tumbuh dg nilai.nilai ajaran Sedulur Sikep (Samin):
>
>   1. Tidak iri hati dg sesama.
>
>   2. Tidak bertengkar.
>
>   3. Tidak mengutamakan masalah duniawi.
>
>   4. Tidak mengganggu orang lain.
>
>   5. Tidak mengambil hak/ milik orang lain.
>
>   6. Tidak sombong.
>
>   7. Sabar.
>
>   8. Jujur.
>
>   9. Saling menghormati.

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Berhasil, karena sebelum mendapatkan hibah gamelan ajaran itu sudah diajarkan oleh orang tua masing-masing kepada anak-anaknya. Kehadiran pasinaon nembang dan nabuh gamelan ini berfungsi memperkuat ajaran tersebut untuk tetap dilaksanakan oleh anak-anak peserta sinau.

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Iya. Saya jadi bisa menyimpulkan butuh cara bijak untuk mencukupi sarana dalam pasinaon. Apalagi pasinaon ini saya lakukan bersama anak-anak masyarakat adat yang di dalam kehidupannya ada tata cara yang berbeda yang dianutnya. Saya lebih berhati-hati dalam membantu pasinaon ini agar tidak melanggar adat dan tata cara mereka yang selama ini dilakukan.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Nyaris tidak ada tantangan dalam pengelolaan proyek ini. Hanya satu kuncinya, "menyampaikan dengan jujur". Saya menceritakan kepada para orang tua peserta sinau bagaimana kronologi mendapatkan hibah ini dan kenapa saya lakukan itu. Para orang tua menerima dengan senang, bahkan saat selamatan kedatangan gamelan hibah, seluruh keperluan selamatan disediakan oleh para orang tua peserta sinau. Mereka datang mengikuti acara sampai selesai.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Perempuan berekspresi pada kearifan lokal dimana perempuan tersebut tinggal. Demikian pula dengan saya. Dengan ekspresi yang saya lakukan untuk menjawab kegelisahan saya, saya percaya tindakan ini menjadi salah satu pengisi keragaman yang ada di negeri kita. Walaupun tujuan semula bukanlah itu. Tujuan semula adalah menjaga agar tradisi macapat tentang ajaran Sikep ini tidak punah di lingkup tempat tinggal Sedulur Sikep.
