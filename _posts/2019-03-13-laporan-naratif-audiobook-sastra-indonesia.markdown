---
title: Laporan Naratif - Audiobook Sastra Indonesia - Indah Darmastuti
date: 2019-03-13
permalink: /ciptamediaekspresi/laporan-naratif/audiobook-sastra-indonesia
layout: proposal
author: Indah Darmastuti
categories:
- laporan
- Laporan Naratif
- CME
- Audiobook Sastra Indonesia
---

# Laporan Naratif - Audiobook Sastra Indonesia

## Pembelajaran

**Ceritakan pembelajaran apa saja yang Anda dapatkan selama menjadi penanggungjawab proyek di bawah hibah Cipta Media Ekspresi.**

Selama mengerjakan proyek ini, saya semakin tahu bahwa keberhasilan sebuah rencana kerja juga ditentukan oleh kecakapan seseorang dalam melakukan koordinasi dan menejemen waktu. Dari situ, saya memahami bahwa saya harus melakukan pendekatan yang tepat mengingat saya juga mengajak kerjasama teman-teman difabel daksa, yang dalam soal waktu ada perbedaan kuat. Yang saya maksud adalah: waktu yang diperlukan teman-teman difabel daksa untuk mengerjakan tugas, lebih panjang daripada yang saya perlukan. Dari situ saya belajar untuk menyesuaikan diri dengan rekan kerja, tanpa mengorbankan kepentingan saya.

---

## Aktivitas yang Anda usung

**Berdasarkan proposal yang Anda ajukan, Anda ingin melakukan akivitas-aktivitas berikut ini:**

> 1. Pengumpulan 30 cerpen dan 50 puisi selama produksi secara bertahap
>
> 2. Pendistribusian naskah kepada para narator dan latihan secara bertahap
>
> 3. Perekaman pembacaan cerpen dan puisi secara bertahap
>
> 4. Perekaman musik untuk ilustrasi naskah secara bertahap
>
> 5. Editing naskah
>
> 6. Pendokumentasian naskah dalam bentuk keping CD yang akan didistribusikan ke Sekolah Luar Biasa, dan digital diupload di website

**Ceritakan hasil dari aktivitas tersebut.**

Mulai Mei 2018, saya menghubungi teman-teman penulis untuk mengirimkan karya berupa puisi atau cerpen. Mereka antusias karena saya menjelaskan bahwa kerja ini diperuntukkan teman-teman difabel netra agar mereka lebih akrab lagi dengan sastra. Saya lebih memilih mengontak secara personal, bukan mengumumkan di media sosial untuk memudahkan seleksi dan koordinasi. Kemudian, seberapa pun karya yang terkumpul, saya distribusikan kepada narator (teman-teman sastra yang biasa reading) tetapi karena saya juga mengajak teman-teman difabel daksa untuk terlibat dan sahabat netra yang belum akrab dengan sastra, maka saya perlu mengadakan pelatihan reading. Pada tahap ini saya mengundang dua teman difabel netra untuk berkonsultasi apakah cara baca para narator sudah pas dengan mereka. Kemudian, jika naskah itu sudah layak untuk direkam, maka saya bersama operator dan editor akan merekam pembacaan tersebut. Kemudian file mentah akan kami serahkan kepada editor, baru kemudian diserahkan kepada ilustrator untuk diberi latar musik. Saya melakukan kerja ini terhitung mulai Juli - Oktober 2018. Kemudian 30 Cerpen dan 50 puisi yang sudah jadi, saya kemas dalam bentuk CD (Terbatas) untuk kami serahkan kepada sekolah luar biasa dan beberapa di antaranya saya serahkan kepada pemerintah kota Solo. Tetapi yang utama, saya mengunggah audiobooks itu ke dalam website www.difalitera.org

---

## Isu yang Anda coba atasi

**Berdasarkan proposal yang Anda ajukan, Anda ingin mengatasi isu-isu berikut ini:**

> 1. Pemenuhan hak teman-teman difabel netradan masyarakat secara umum(sastra perspektif gender yang kritis dan inklusif)
>
> 2. Kurangnya referensi terutama sastra yang bisa diakses gratis oleh teman-teman difabel

**Ceritakan hasil dari mengatasi isu tersebut.**

Dalam kerja ini, saya mengundang teman-teman difabel (Netra dan Daksa) untuk terlibat. Saya ingin teman-teman difabel daksa atau difabel lainnya, juga mempunyai kemampuan membaca sastra. Harapan saya, kerja ini juga mempunyai spirit: dari difabel untuk difabel. Dalam Audiobooks ini, meskipun kerjasama dengan Cipta Media Ekspresi sudah berakhir seperti perjanjian, saya masih tetap melanjutkan. Saat ini sedang menggarap Cerita Anak (Proses rekam), Cerita Bahasa Jawa (Proses pengumpulan dan belajar reading), Cerita Rakyat (Proses rekam), termasuk saya sedang menggodog metode pelajaran Bahasa Inggris yang dihadirkan seperti cerita mini untuk memenuhi permintaan teman-teman difabel netra yang memerlukan kemampuan berbahasa Inggris.

---

## Indikator sukses

**Berdasarkan proposal yang Anda ajukan, indikator sukses proyek Anda adalah sebagai berikut:**

> 1. Adanya softfile Audiobook untuk tunanetra
>
> 2. Akses audiobook gratis
>
> 3. Audiobook diunggah dimedia-media sosial dan web

**Ceritakan apakah Anda telah berhasil ataupun tidak dalam mencapai indikator sukses tersebut dan mengapa.**

Saat ini sudah tersedia audibooks Sastra Indonesia yang bisa diunduh secara gratis dan mudah di www.difalitera.org atau kami juga mengunggahnya di media sosial. Instagram, Facebook, Twitter dan Sportify dengan akun: difalitera

---

## Tujuan dan sasaran

**Ceritakan tujuan proyek yang berhasil Anda capai.**

**Apakah proyek Anda ini membawa perubahan pada cara berpikir atau cara kerja Anda dibanding sebelumnya?**

**Apabila ya, seperti apa perubahannya? Apabila tidak, mengapa?**

Tentu saja kerja ini membawa perubahan pada cara berpikir terutama dalam mengelola waktu dan menghadapi berbagai karakter. Apalagi ketika saya semakin mengenal teman-teman difabel netra, bahwa mereka sungguh-sungguh memerlukan teman-teman yang able untuk mitra belajar. Di sini, saya semakin memantabkan diri untuk mengerjakan audiobooks. Dan risikonya kesibukan saya jelas meningkat. karena semakin banyak teman-teman yang ingin terlibat dan saya dengan senang hati menyambutnya.

---

## Tantangan atau masalah yang dihadapi

**Ceritakan tantangan ataupun masalah apa saja yang Anda harus hadapi dalam proses pengelolaan proyek Anda dan bagaimana Anda menanganinya.**

Tantangan utama adalah waktu. Karena saya adalah karyawan sebuah perusahaan, sehingga saya harus cakap mengatur waktu. Tantangan yang lain adalah menjalin kerjasama, melakukan koordinasi dan menyesuaikan jadwal dengan mitra kerja.

---

## Keberagaman

**Menurut Anda, bagaimana proyek ini telah/dapat memperkaya keragaman ekspresi perempuan?**

Tentu saja. Karena di sini dalam kerja ini yang terlibat dalam prosesnya, terlebih narator, adalah perempuan. mereka mengekspresikan diri dan memiliki semangat berbagi.
