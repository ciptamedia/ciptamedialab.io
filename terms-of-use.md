---
title: Syarat Penggunaan (Terms of Use)
layout: legal
---

1. Informasi Umum Mengenai Ketentuan Penggunaan

   Istilah "kami" merujuk pada organisasi yang mengoperasikan Cipta Media, dalam hal ini Perkumpulan
   Wikimedia Indonesia. Sementara istilah "situs web" mengacu pada ciptamedia.org. Kecuali dinyatakan lain di situs
   atau layanan tertentu, ketentuan penggunaan utama ini ("Syarat-Syarat Utama") berlaku untuk penggunaan Anda atas
   semua konten yang terdapat dalam ciptamedia.org, dan proyek proyek didalamnya, termasuk: Cipta Media Bersama, Cipta
   Media Seluler, dan Cipta Media Ekspresi. Persyaratan Utama juga berlaku untuk semua produk, informasi, dan layanan
   yang disediakan melalui Situs Web ini.

   Istilah tambahan: Selain Persyaratan Utama, penggunaan Anda atas layanan pun juga tunduk pada ketentuan khusus yang
   berlaku untuk Layanan tertentu ("Ketentuan Tambahan"). Jika ada pertentangan antara Ketentuan Tambahan dan
   Persyaratan Utama, maka Ketentuan Tambahan yang dinyatakan berlaku dalam kaitannya dengan layanan yang dirujuk.

   Secara kolektif, Syarat Utama, bersama dengan Ketentuan Tambahan lainnya, membentuk perjanjian hukum yang mengikat
   antara Anda dan Creative Commons dalam kaitannya dengan penggunaan Anda atas Layanan. Secara kolektif, perjanjian
   hukum ini disebut di bawah ini sebagai "Persyaratan."

2. Perjanjian Anda dengan Ketentuan

   DENGAN MENGKLIK “SAYA SETUJU” ATAU MENGAKSES ATAU MENGGUNAKAN LAYANAN APAPUN (TERMASUK LISENSI), ANDA MENGAKUI
   BAHWA ANDA TELAH MEMBACA, MEMAHAMI, DAN MENYETUJUI UNTUK TERIKAT OLEH KETENTUAN. Dengan mengklik "SAYA SETUJU" atau
   mengakses atau menggunakan Layanan apa pun, Anda juga menyatakan bahwa Anda memiliki otoritas hukum untuk menerima
   Persyaratan atas nama Anda sendiri dan setiap pihak yang Anda wakili sehubungan dengan penggunaan Anda atas Layanan
   apa pun. Jika Anda tidak menyetujui Persyaratan, Anda tidak diizinkan untuk menggunakan Layanan apa pun. Jika Anda
   adalah individu yang masuk ke dalam Ketentuan ini atas nama entitas, Anda menyatakan dan menjamin bahwa Anda
   memiliki kekuatan untuk mengikat entitas tersebut, dan Anda dengan ini setuju atas nama entitas tersebut untuk
   terikat dengan Ketentuan ini, dengan persyaratan “ Anda, "dan" Anda "melamar Anda, entitas itu, dan pengguna lain
   yang mengakses Layanan atas nama entitas itu.

3. Perubahan Ketentuan

   Dari waktu ke waktu, Cipta Media dapat mengubah, menghapus, atau menambah Ketentuan, dan berhak untuk melakukannya
   dibawah kebijakan yang ditentukan sendiri. Perubahan ketentuan akan diumumkan apabila ada yang diperbaharui dan
   menunjukkan tanggal revisi. Jika perubahan dirasakan penting, maka upaya yang wajar akan dilakukan untuk merujuk
   perubahan penting tersebut sebagai sesuatu yang menonjol pada laman web yang relevan, dan perubahan ini akan
   diinformasikan  ini via surel. Semua Persyaratan baru dan / atau yang direvisi berlaku segera dan berlaku untuk
   penggunaan Anda atas Layanan sejak tanggal itu, kecuali perubahan-perubahan yang sangat penting (material), yang
   dianggap mengubah kebijakan umum, maka berlaku 30 hari setelah perubahan dibuat dan diidentifikasi sebagai
   material. Penggunaan Layanan Anda yang berkelanjutan setelah Persyaratan baru dan / atau revisi efektif menunjukkan
   bahwa Anda telah membaca, memahami, dan menyetujui Ketentuan tersebut.

4. Konten Yang Tersedia

   Sebagaimana adanya: Anda mengakui bahwa Cipta Media tidak membuat pernyataan atau jaminan apa pun tentang materi,
   data, dan informasi, seperti file data, teks, perangkat lunak komputer, kode, musik, file audio atau suara lain,
   foto, video, atau gambar lainnya (secara kolektif, "Konten") yang dapat Anda akses sebagai bagian dari, atau
   melalui penggunaan Anda, Layanan. Dalam keadaan apa pun Cipta Media tidak bertanggung jawab atas Konten apa pun,
   termasuk, namun tidak terbatas pada: Konten apa pun yang melanggar, kesalahan atau kelalaian apa pun dalam Konten,
   atau atas kerugian atau kerusakan apa pun yang terjadi sebagai akibat dari penggunaan Konten yang diposkan,
   dikirim, ditautkan dari, atau dapat diakses melalui atau disediakan melalui Layanan. Anda memahami bahwa dengan
   menggunakan Layanan, Anda mungkin akan menemukan Konten yang menyinggung, tidak senonoh, atau tidak menyenangkan.

   Anda setuju bahwa Anda sepenuhnya bertanggung jawab untuk penggunaan kembali Konten yang tersedia melalui Layanan,
   termasuk memberikan atribusi yang tepat. Anda harus meninjau ketentuan lisensi yang berlaku sebelum Anda
   menggunakan Konten sehingga Anda tahu apa yang dapat dan tidak dapat Anda lakukan.

   Lisensi: Konten yang Dimiliki Cipta Media menggunakan lisensi CC-BY 4.0 kecuali dikatakan tersedia dalam
   persyaratan yang berbeda. Jika Anda menemukan konten melalui tautan di situs web kami, pastikan untuk memeriksa
   persyaratan lisensi sebelum menggunakannya. Lihat halaman
   [Kebijakan CC](https://creativecommons.org/licenses/by/4.0/deed.id){:target="_blank"} untuk informasi lebih lanjut.

   Alat Pencarian: Cipta Media menyediakan alat pencarian untuk konten dibawah situs web. Alat pencarian dalam situs
   akan selalu Cipta Media merujuk ke konten dengan lisensi CC.

5. Konten yang Disediakan oleh Anda

   Tanggung jawab Anda: Anda menyatakan, menjamin, dan setuju bahwa tidak ada konten yang diposting atau dibagikan
   oleh Anda pada atau melalui salah satu Layanan ("Formulir"), melanggar atau melanggar hak-hak pihak ketiga,
   termasuk hak cipta, merek dagang, privasi , publisitas, atau hak-hak pribadi atau kepemilikan lainnya, pelanggaran
   atau konflik dengan kewajiban apa pun, seperti kewajiban kerahasiaan, atau berisi materi yang memfitnah, atau
   melanggar hukum.

   Melisensikan Konten Anda: Anda memiliki hak cipta apa pun yang mungkin Anda miliki di Konten Anda. Anda dengan ini
   setuju bahwa Konten Anda: (a) dengan ini dilisensikan di bawah Lisensi Creative Commons Attribution 4.0 dan dapat
   digunakan berdasarkan ketentuan lisensi itu atau versi Lisensi Pengaitan Creative Commons yang lebih baru, atau (b)
   ada dalam domain publik (seperti Konten yang tidak memiliki hak cipta atau Konten yang Anda sediakan di bawah CC0),
   atau © jika tidak dimiliki oleh Anda, (i) tersedia di bawah Lisensi Creative Commons Attribution 4.0 atau (ii)
   adalah berkas media yang tersedia di bawah lisensi Creative Commons atau bahwa Anda diberi wewenang oleh hukum
   untuk memposting atau berbagi melalui salah satu Layanan, seperti di bawah doktrin penggunaan yang adil, dan yang
   secara jelas ditandai sebagai subjek hak cipta pihak ketiga. Semua Konten Anda harus ditandai secara tepat dengan
   pemberian lisensi (atau status izin lainnya seperti penggunaan wajar) dan informasi atribusi.

   Penghapusan: Cipta Media dapat, tetapi tidak berkewajiban untuk, meninjau Konten Anda dan dapat menghapus atau
   menghilangkan Konten Anda (tanpa pemberitahuan) dari salah situs web atas kebijakannya sendiri. Penghapusan Konten
   Anda (oleh Anda atau Cipta Media) tidak memengaruhi hak apa pun yang Anda berikan dalam Konten Anda menurut
   ketentuan lisensi Creative Commons.

6. Berpartisipasi dalam Cipta Media dan menjadi bagiannya: Pengguna Terdaftar

   Dengan mendaftarkan akun melalui salah satu formulir data diri, termasuk mengamankan akun Layanan Login Cipta
   Media, atau mengajukan permohonan log Cipta Media, Anda menyatakan dan menjamin bahwa Anda sudah dalam batas usia
   legal (diatas {{ site.minimum_legal_age }} tahun). Layanan yang ditawarkan kepada para pengguna terdaftar disediakan dengan
   tunduk pada Ketentuan-Ketentuan Utama ini, [Kebijakan Privasi](/privacy-policy){:target="_blank"}, dan setiap Ketentuan Tambahan yang ditentukan pada
   Situs Web yang relevan, yang semuanya digabungkan dengan referensi ke dalam Ketentuan ini.

   Pendaftaran Data Diri: Anda setuju untuk (a) hanya memberikan informasi yang akurat dan terkini tentang diri Anda
   (penggunaan alias atau nama panggilan sebagai pengganti nama resmi Anda, diperbolehkan, namun tetap harus dapat
   didentifikasikan dengan mudah oleh Admin Cipta Media bahwa nama tersebut merujuk pada nama resmi Anda sehubungan
   dengan Layanan Login Cipta Media), (b) pertahankan keamanan kata sandi Anda dan identifikasi, © segera perbarui alamat surel yang tercantum
   terkait dengan akun Anda untuk membuatnya tetap akurat sehingga kami dapat menghubungi Anda, dan (d) sepenuhnya
   bertanggung jawab atas semua penggunaan akun Anda. Anda tidak boleh membuat akun atas nama individu atau entitas
   lain kecuali Anda berwenang untuk melakukannya.

   Tidak Ada Keanggotaan dalam Cipta Media: Membuat akun Layanan Login Cipta Media atau menggunakan salah satu Situs
   Web atau Layanan terkait, tidak akan  menjadikan Anda anggota, pemegang saham atau afiliasi Cipta Media maupun
   organisasi yang mengoperasikannya untuk tujuan apa pun, Anda juga tidak akan memiliki hak keanggotaan dalam hukum
   yang berlaku.

   Penghentian: Cipta Media berhak untuk mengubah atau menghentikan akun Anda kapan pun dengan alasan apa pun atau
   tanpa alasan sama sekali.

7. Dilarang Untuk

   Anda setuju untuk tidak melakukan satu atau lebih dari aktivitas berikut ini:

    7.1. Melanggar hukum dan hak:

    Anda tidak boleh (a) menggunakan konten dan layanan apa pun untuk tujuan ilegal apa pun atau melanggar hukum adat,
    nasional, atau internasional, (b) melanggar atau mendorong orang lain untuk melanggar hak atau kewajiban apa pun
    kepada pihak ketiga, termasuk dengan melanggar , menyalahgunakan, atau melanggar kekayaan intelektual,
    kerahasiaan, atau hak privasi.

    7.2. Permohonan:

    Anda tidak boleh menggunakan konten, layanan, atau informasi apa pun yang disediakan melalui situs web untuk
    transmisi materi iklan atau promosi, termasuk surel sampah, spam, surat berantai, skema piramida, atau bentuk lain
    dari ajakan yang tidak diminta atau tidak diinginkan.

    7.3. Gangguan:

    Anda tidak boleh menggunakan Layanan dengan cara apa pun yang dapat menonaktifkan, membebani, merusak, atau
    merusak Layanan, atau mengganggu penggunaan dan kesenangan pihak lain dari Layanan; termasuk dengan (a) mengunggah
    atau menyebarkan virus, adware, spyware, worm atau kode berbahaya lainnya, atau (b) mengganggu jaringan,
    peralatan, atau server yang terhubung atau digunakan untuk menyediakan Layanan apa pun, atau melanggar segala
    peraturan, kebijakan, atau prosedur jaringan, peralatan, atau server apa pun.

    7.4. Merugikan orang lain:

    Anda tidak boleh memposting atau mengirimkan Konten pada atau melalui Layanan yang berbahaya, ofensif, cabul,
    kasar, invasif privasi, memfitnah, kebencian atau diskriminatif, salah atau menyesatkan, atau menghasut tindakan
    ilegal;

    Anda tidak boleh mengintimidasi atau melecehkan orang lain melalui Layanan; dan, Anda tidak boleh memposting atau
    mengirimkan informasi yang dapat diidentifikasi secara pribadi tentang orang-orang yang berusia di bawah 13 tahun
    pada atau melalui Layanan.

    7.5. Peniruan identitas atau akses tidak sah:

    Anda tidak boleh menyamar sebagai orang atau entitas lain, atau salah mengartikan afiliasi Anda dengan seseorang
    atau badan saat menggunakan Layanan;

    Anda tidak boleh menggunakan atau mencoba menggunakan akun atau informasi pribadi orang lain tanpa izin; dan

    Anda tidak boleh berusaha mendapatkan akses tidak sah ke Layanan, atau sistem komputer atau jaringan yang
    terhubung ke Layanan, melalui peretasan penambangan kata sandi atau cara lain apa pun.

8. Pengguna Terdaftar

   Dengan mengirimkan formulir registrasi untuk menjadi pengguna terdaftar dalam situs Cipta Media, Anda mengakui
   bahwa Anda telah membaca, memahami, dan, jika Anda diterima untuk menjadi pengguna terdaftar, setuju untuk terikat
   dengan syarat dan kebijakan yang dirujuk dan dimasukkan di dalamnya. Jika Anda diterima sebagai pengguna terdaftar,
   organisasi yang mengoperasionalkan Cipta Media akan mengatur penggunaan dan partisipasi Anda dalam acara-acara
   Cipta Media dan kode etik perilaku dalam situs, termasuk kelompok kerja. Perhatikan bahwa partisipasi Anda dalam
   jaringan maupun luar jaringan juga dapat diatur oleh aturan dan panduan tambahan perilaku. Silakan lihat [Kebijakan
   Privasi](/privacy-policy){:target="_blank"} untuk informasi lebih lanjut tentang bagaimana informasi Anda (sebagai
   pelamar, anggota, dan voucher) akan digunakan.

9. PENOLAKAN JAMINAN

   SEJAUH DIIZINKAN OLEH UNDANG-UNDANG YANG BERLAKU, LAYANAN (TERMASUK SEMUA KONTEN YANG TERSEDIA ATAU MELALUI
   LAYANAN) SEBAGAIMANA ADANYA DAN TIDAK MEMBUAT PERNYATAAN ATAU JAMINAN DALAM BENTUK APAPUN TENTANG LAYANAN, BAIK
   SECARA TERSURAT, TERSIRAT, DINYATAKAN SECARA FORMAL, ATAU LAINNYA, TERMASUK TANPA BATASAN, JAMINAN SANKSI, DAPAT
   DIPERDAGANGKAN, KESESUAIAN UNTUK TUJUAN TERTENTU, ATAU NON-PELANGGARAN. CIPTA MEDIA TIDAK MENJAMIN BAHWA FUNGSI
   LAYANAN AKAN TIDAK TERGANGGU ATAU BEBAS KESALAHAN, KONTEN YANG DIBUAT TERSEDIA ATAU MELALUI LAYANAN AKAN BEBAS DARI
   KESALAHAN, CACAT YANG AKAN DIPERBAIKI, ATAU BAHWA PELAYANAN APA PUN YANG DIGUNAKAN OLEH CIPTA MEDIA BEBAS DARI
   VIRUS ATAU KOMPONEN BERBAHAYA LAINNYA. CREATIVE COMMONS TIDAK MENJAMIN ATAU MEMBERIKAN REPRESENTASI TENTANG
   PENGGUNAAN KONTEN YANG TERSEDIA MELALUI LAYANAN DALAM PERSYARATAN AKURASI, KEANDALAN, ATAU LAINNYA.

10. PEMBATASAN TANGGUNG JAWAB

    SEJAUH DIIZINKAN OLEH UNDANG-UNDANG YANG BERLAKU, DALAM KEADAAN APAPUN, CIPTA MEDIA TIDAK AKAN BERTANGGUNG JAWAB
    KEPADA ANDA PADA SETIAP TEORI HUKUM UNTUK KERUGIAN TIDAK SENGAJA, LANGSUNG, TIDAK LANGSUNG, TINDAKAN YANG
    MENGHUKUM, KEJADIAN SEBENARNYA, SEBAGAI KONSEKUENSI, KHUSUS, MEMBERI CONTOH, ATAU KERUSAKAN LAINNYA, TERMASUK
    TANPA BATASAN, KERUGIAN PENDAPATAN ATAU PENGHASILAN, HILANGNYA KEUNTUNGAN, NYERI DAN PENDERITAAN, KETERBUKAAN
    EMOSIONAL, BIAYA BARANG ATAU LAYANAN PENGGANTI, ATAU GANTI RUGI YANG DITANGGUNG ATAU DITIMBULKAN OLEH ANDA ATAU
    PIHAK KETIGA YANG MUNCUL DALAM KONEKSI DENGAN LAYANAN (ATAU DIAKHIRINYA LAYANAN TERSEBUT UNTUK ALASAN APAPUN) ,
    BAHKAN JIKA CIPTA MEDIA TELAH DIBERITAHU TENTANG KEMUNGKINAN KERUSAKAN TERSEBUT.

    SEPANJANG DIPERBOLEHKAN OLEH HUKUM YANG BERLAKU, CIPTA MEDIA TIDAK BERTANGGUNG JAWAB ATAU BERTANGGUNG JAWAB APA
    PUN DALAM KEADAAN APA PUN UNTUK KONTEN YANG DIMULAI ATAU TERSEDIA MELALUI LAYANAN (TERMASUK KLAIM PELANGGARAN YANG
    TERKAIT DENGAN KONTEN INI), UNTUK PENGGUNAAN LAYANAN, ATAU UNTUK PERILAKU PIHAK KETIGA ATAU MELALUI LAYANAN.

11. Ganti Rugi

    Sejauh diizinkan oleh hukum, Anda setuju untuk mengganti kerugian dan membebaskan Cipta Media, karyawan, pejabat,
    direktur, afiliasi, dan agen dari dan terhadap setiap dan semua klaim, kerugian, biaya, kerusakan, dan biaya,
    termasuk biaya pengacara yang wajar, yang dihasilkan secara langsung atau tidak langsung dari atau timbul dari (a)
    pelanggaran Anda terhadap Ketentuan, (b) penggunaan Anda atas Layanan apa pun, dan / atau © Konten yang Anda
    sediakan di salah satu Layanan.

12. [Kebijakan Privasi](/privacy-policy){:target="_blank"}

    Cipta Media berkomitmen untuk secara bertanggung jawab menangani informasi dan data yang kami kumpulkan melalui
    Layanan kami sesuai dengan [Kebijakan Privasi](/privacy-policy){:target="_blank"}, yang digabungkan dengan
    referensi ke dalam Syarat-Syarat Utama ini. Harap tinjau Kebijakan Privasi sehingga Anda tahu bagaimana kami
    mengumpulkan dan menggunakan informasi pribadi Anda.

13. Kebijakan Merek Dagang

    Nama, logo, ikon, dan merek dagang Cipta Media lainnya hanya dapat digunakan sesuai dengan Kebijakan Merek Dagang
    kami, yang digabungkan dengan referensi ke dalam Syarat-Syarat Utama ini. Harap tinjau Kebijakan Merek Dagang
    sehingga Anda memahami bagaimana merek dagang Cipta Media dapat digunakan.

14. Keluhan Hak Cipta

    Cipta Media menghormati hak cipta, dan kami melarang pengguna Layanan mengirimkan, mengunggah, memposting, atau
    mentransmisikan Konten apa pun pada Layanan yang melanggar hak kepemilikan orang lain.

    Untuk melaporkan Konten yang diduga melanggar yang dihosting di situs web yang dimiliki atau dikontrol oleh Cipta
    Media, kirimkan keluhan Anda melalui surel ke: info@ciptamedia.org.

15. Penghentian

    Cipta Media dapat memodifikasi atau menghentikan pengoperasian, atau akses ke, semua atau sebagian dari Layanan
    kapan saja karena alasan apa pun. Selain itu, akses individu Anda, dan penggunaan, Layanan dapat diakhiri oleh
    Cipta Media kapan saja dan karena alasan apa pun.

    Oleh Anda: Jika Anda ingin mengakhiri perjanjian ini, Anda dapat segera berhenti mengakses atau menggunakan
    Layanan kapan saja.

    Otomatis setelah pelanggaran: Hak Anda untuk mengakses dan menggunakan Layanan (termasuk penggunaan akun Layanan
    Login Cipta Media Anda) secara otomatis setelah Anda melanggar salah satu Ketentuan.

    Keberlanjutan: Penolakan jaminan, pembatasan tanggung jawab, dan yurisdiksi serta ketentuan hukum yang berlaku
    akan tetap berlaku setelah pemberhentian. Lisensi tetap berlaku untuk Konten Anda tidak terpengaruh oleh
    penghentian Ketentuan dan akan terus berlaku tunduk pada ketentuan lisensi yang berlaku.
